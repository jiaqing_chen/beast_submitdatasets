package edu.ucr.cs.bdlab.test

import java.io.File
import java.lang.reflect.Field

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileUtil
import org.apache.spark.{SparkConf, SparkContext}
import org.scalatest.{BeforeAndAfterEach, Suite}

/**
  * A mixin for Scala tests that creates a Spark context and adds methods to create an empty scratch directory
  */
trait ScalaSparkTest extends Suite with BeforeAndAfterEach {
  /**A SparkContext that can be reused for all tests*/
  val sc = new SparkContext("local", "test")

  /**A scratch directory that gets deleted before and after each test*/
  val scratchPath = new File("scratch")

  def clearScratchDir: Unit = {
    FileUtil.fullyDelete(scratchPath)
    scratchPath.mkdirs()
  }

  def resetSparkConfiguration: Unit = {
    val sparkConf: SparkConf = getActualConfiguration(sc)
    getActualHadoopConfiguration(sc).clear()
    for (entry <- sparkConf.getAll)
      sparkConf.remove(entry._1);
    for (entry <- new SparkConf().getAll)
      sparkConf.set(entry._1, entry._2)
  }


  /**
    * Returns the internal configuration of a JavaSparkContext rather than a copy of it. This is useful to allow the
    * tests to change the internal configuration before a job runs. This is only needed in the test environment where
    * each test might want to change the configuration. We do not want to create a new context and configuration for each
    * test because it will make the tests very slow.
    * @param sparkContext the spark context to retrieve its internal configuration
    * @return the configuration object stored in the given context (not a copy of it).
    */
  protected def getActualConfiguration(sparkContext: SparkContext): SparkConf = {
    val sparkContextClass = classOf[SparkContext]
    val confField: Field = sparkContextClass.getDeclaredField("org$apache$spark$SparkContext$$_conf")
    confField.setAccessible(true);
    confField.get(sparkContext).asInstanceOf[SparkConf]
  }

  /**
    * Retrieves the Hadoop configuration stored inside the given context
    * @param sparkContext the context to retrieve its configuration
    * @return the configuration object stored inside the given context (not a copy of it)
    */
  protected def getActualHadoopConfiguration(sparkContext: SparkContext): Configuration = {
    val sparkContextClass = classOf[SparkContext]
    val confField: Field = sparkContextClass.getDeclaredField("_hadoopConfiguration")
    confField.setAccessible(true)
    confField.get(sparkContext).asInstanceOf[Configuration]
  }

  override protected def beforeEach(): Unit = {
    clearScratchDir
    resetSparkConfiguration
  }

  override protected def afterEach(): Unit = beforeEach
}
