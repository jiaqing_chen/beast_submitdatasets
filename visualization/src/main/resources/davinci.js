/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var serverURL = "https://davinci.cs.ucr.edu/";
var visualizeURL = serverURL+"dynamic/visualize.cgi/";

var DaVinci = {
  Visualize: function() {
    var aElement = jQuery(document.currentScript).parent();
    var datasetURL = aElement.attr("data-url");
    var elementToMap = aElement.parent();
    jQuery.ajax(serverURL+"dynamic/metadata.cgi?vizurl="+datasetURL, {
      success: function(data) {
        var tileWidth = data.width;
        var tileHeight = data.height;
        var minLevel = data.minlevel;
        var maxLevel = data.maxlevel;
        var mercator = data.mercator;

        layers = [];
        if (mercator)
          layers.push(new ol.layer.Tile({
            source: new ol.source.OSM()
          }));

        layers.push(new ol.layer.Tile({
          source: new ol.source.XYZ({
            url: visualizeURL+datasetURL+'/tile-{z}-{x}-{y}.png',
            tileSize: [tileWidth, tileHeight],
            attributions: '<a href="https://davinci.cs.ucr.edu">&copy;DaVinci</a>'
          })
        }));
        
        var map = new ol.Map({
          target: elementToMap[0],
          layers: layers,
          view: new ol.View({
            center: [0, 0],
            zoom: 1,
            minZoom: minLevel,
            maxZoom: maxLevel
          })
        });
      }
    });
  },
  createDataLayer: function(datasetURL) {
    return new ol.layer.Tile({
       source: new ol.source.XYZ({
         url: visualizeURL+datasetURL+'/tile-{z}-{x}-{y}.png',
         attributions: '<a href="https://davinci.cs.ucr.edu">&copy;DaVinci</a>'
       })
    });
  }
}

jQuery(function() {
  // Automatic initialization of maps and layers
  jQuery("div.davinci.map").each(function(i, mapItem) {
    var jQueryMapItem = jQuery(mapItem);
    // Initialize the zoom based on the user selected center and zoom
    var center = jQueryMapItem.attr("data-center");
    var latitude = 0.0, longitude = 0.0;
    if (center) {
      var parts = center.split(',');
      latitude = parseFloat(parts[0]);
      longitude = parseFloat(parts[1]);
    }
    var zoom = 1;
    if (jQueryMapItem.attr("data-zoom"))
      zoom = parseInt(jQueryMapItem.attr("data-zoom"));

    var view = new ol.View({
       center: ol.proj.fromLonLat([longitude, latitude]),
       zoom: zoom,
       minZoom: 0,
       maxZoom: 19
     });

    // Now initialize the base OSM layer and any user-selected data layers
    // Add the base OSM layer
    var layers = [new ol.layer.Tile({ source: new ol.source.OSM() })];

    // Add data layers based on the HTML content
    jQueryMapItem.children("a.davinci.layer").each(function(i, layerItem) {
      var datasetURL = jQuery(layerItem).attr("data-url");
      layers.push(DaVinci.createDataLayer(datasetURL));
    });

    // Now create the map
    var map = new ol.Map({
      target: mapItem,
      layers: layers,
      view: view
    });
  });
});