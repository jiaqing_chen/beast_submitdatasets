/***********************************************************************
* Copyright (c) 2015 by Regents of the University of Minnesota.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Apache License, Version 2.0 which 
* accompanies this distribution and is available at
* http://www.opensource.org/licenses/apache2.0.php.
*
*************************************************************************/
package edu.ucr.cs.bdlab.davinci;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.util.WritableExternalizable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**An abstract interface for any canvas*/
public abstract class Canvas implements WritableExternalizable {
  /**The MBR of the this layer in input coordinates*/
  protected final Envelope inputMBR = new Envelope();
  
  /**Width of this layer in pixels*/
  protected int width;
  
  /**Height of this layer in pixels*/
  protected int height;

  /**The scale of the image in terms of pixels per input unit*/
  protected transient double scaleX, scaleY;

  public Canvas() {}
  
  public Canvas(Envelope inputMBR, int width, int height) {
    super();
    this.inputMBR.setCoordinateDimension(inputMBR.getCoordinateDimension());
    this.inputMBR.set(inputMBR.minCoord, inputMBR.maxCoord);
    this.width = width;
    this.height = height;
    calculateScale();
  }

  protected void calculateScale() {
    this.scaleX = width / inputMBR.getSideLength(0);
    this.scaleY = height / inputMBR.getSideLength(1);
  }

  @Override
  public void write(DataOutput out) throws IOException {
    inputMBR.write(out);
    out.writeInt(width);
    out.writeInt(height);
  }
  
  @Override
  public void readFields(DataInput in) throws IOException {
    inputMBR.readFields(in);
    width = in.readInt();
    height = in.readInt();
    calculateScale();
  }

  /**
   * Returns the width of the canvas in pixels
   * @return
   */
  public int getWidth() {
    return width;
  }

  /**
   * Returns the height of the canvas in pixels
   * @return
   */
  public int getHeight() {
    return height;
  }

  public Envelope getInputMBR() {
    return inputMBR;
  }

  /**
   * Transforms the given x-coordinate from input domain to image domain.
   * @param x
   * @return
   */
  public double transformX(double x) {
    return (x - inputMBR.minCoord[0]) * scaleX;
  }

  /**
   * Transforms the given y-coordinate from input domain to image domain.
   * @param y
   * @return
   */
  public double transformY(double y) {
    return (y - inputMBR.minCoord[1]) * scaleY;
  }
}