/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.davinci;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.Feature;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.io.CSVEnvelopeDecoder;
import edu.ucr.cs.bdlab.io.CSVEnvelopeEncoder;
import edu.ucr.cs.bdlab.io.FeatureReader;
import edu.ucr.cs.bdlab.io.MercatorProjector;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.io.SpatialOutputFormat;
import edu.ucr.cs.bdlab.stsynopses.AbstractHistogram;
import edu.ucr.cs.bdlab.util.LongArray;
import edu.ucr.cs.bdlab.util.MathUtil;
import edu.ucr.cs.bdlab.util.OperationParam;
import edu.ucr.cs.bdlab.util.UserOptions;
import edu.ucr.cs.bdlab.util.WritableExternalizable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.LineReader;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

/**
 * A helper class that helps with pyramid multilevel plotting.
 */
public class MultilevelPyramidPlotHelper {
  private static final Log LOG = LogFactory.getLog(MultilevelPyramidPlotHelper.class);

  @OperationParam(
      description = "Write data tiles in the output directory next to images." +
          "If set to false, data tiles are skipped and a link to the input path is added to the output _visualization.properties",
      defaultValue = "true"
  )
  public static final String IncludeDataTiles = "data-tiles";

  /**The width of the tile in pixels*/
  @OperationParam(
      description = "The width of the tile in pixels",
      defaultValue = "256"
  )
  public static final String TileWidth = "width";
  /**The height of the tile in pixels*/
  @OperationParam(
      description = "The height of the tile in pixels",
      defaultValue = "256"
  )
  public static final String TileHeight = "height";

  /**The number of levels for multilevel plot*/
  @OperationParam(
      description = "The total number of levels for multilevel plot. " +
          "Can be specified as a range min..max (inclusive of both) or " +
          "as a single number which indicates the number of levels starting at zero.",
      defaultValue = "7"
  )
  public static final String NumLevels = "levels";

  /**Data tile threshold. Any tile that is larger than this size is considered an image tile*/
  @OperationParam(
      description = "Data tile threshold for adaptive multilevel plot. " +
          "Largest possible size for a data tile in terms of number of bytes",
      defaultValue = "10m"
  )
  public static final String DataTileThreshold = "threshold";

  @OperationParam(
      description = "The maximum size for the histogram used in adaptive multilevel plot",
      defaultValue = "128m"
  )
  public static final String MaximumHistogramSize = "MultilevelPlot.MaxHistogramSize";

  @OperationParam(
      description = "Type of histogram used to classify tiles {simple, euler}",
      defaultValue = "simple"
  )
  public static final String HistogramType = "histogramtype";

  @OperationParam(
      description = "Granularity of pyramid partitioning. How many levels to combine together while partitioning.",
      defaultValue = "1"
  )
  public static final String PartitionGranularity = "Pyramid.Granularity";

  @OperationParam(
      description = "The minimum size for an image tile to use flat partitioning",
      defaultValue = "25m"
  )
  public static final String MinSizeFlatPartitioning = "Pyramid.MinSizeFlatPartitioning";

  /**
   * Returns the range of zoom levels which is encoded as either numLevels or minLevel..maxLevel.
   * If the string value is a single number (n), the returned range is 0..n-1, if the range is value is two numbers
   * separated by two dots, e.g., a..b, the two values are returned
   * @param conf
   * @param key the name of the key to retrieve from the configuration
   * @param defaultValue
   * @return
   */
  public static Range getRange(Configuration conf, String key, String defaultValue) {
    String value = conf.get(key, defaultValue);
    int i = value.indexOf("..");
    if (i == -1)
      return new Range(0, Integer.parseInt(value) - 1);
    int min = Integer.parseInt(value.substring(0, i));
    int max = Integer.parseInt(value.substring(i + 2));
    return new Range(min, max);
  }

  /**
   * Compute the side length of the histogram used for adaptive multilevel plot. The total size of the histogram in
   * bytes cannot be larger than the given {@code maxHistogramSize}. At the same time, it does not have to be any larger
   * than 2<sup>{@code maxLevel}</sup>
   * @param maxHistogramSize
   * @param maxLevel
   * @param cellSize the size of each cell in the histogram in bytes
   * @return
   */
  public static int computeHistogramDimension(long maxHistogramSize, int maxLevel, int cellSize) {
    long idealSize = (1L << (2 * maxLevel)) * cellSize;
    if (idealSize <= maxHistogramSize) {
      // Can compute a histogram of the ideal size
      return 1 << maxLevel;
    }
    // Has to abide by the maximum histogram size in bytes.
    // We still need to return a value that is a power of two
    int maxNumberOfBins = (int) (maxHistogramSize / cellSize);
    int maxDimension = (int) Math.sqrt(maxNumberOfBins);
    return Integer.highestOneBit(maxDimension);
  }

  /**
   * Finds the level of the deepest tile that is at least of the given size.
   * @param h the histogram that represents the data
   * @param sizeThreshold the size threshold to find
   * @return the deepest (maximum) level of the tile with the at least the given size according to the histogram.
   * If the threshold is larger than the entire histogram, a zero is returned.
   */
  public static int findDeepestTileWithSize(AbstractHistogram h, long sizeThreshold) {
    int level = 0;
    boolean tileFound;
    int[] searchPos = new int[2];
    int[] searchSize = new int[2];
    assert h.getNumPartitions(0) == h.getNumPartitions(1) :
        String.format("Histogram dimension %d x %d is not square", h.getNumPartitions(0), h.getNumPartitions(1));
    assert Integer.highestOneBit(h.getNumPartitions(0)) == h.getNumPartitions(0) :
        String.format("Histogram size %d is not power of two", h.getNumPartitions(0));
    int histogramSize = h.getNumPartitions(0);
    int maxLevel = MathUtil.log2(histogramSize);
    // Keep track of the maximum tile size found
    do {
      // Try all the tiles at this level
      tileFound = false;
      int dimension = 1 << level;
      searchSize[0] = searchSize[1] = histogramSize / dimension;
      for (searchPos[0] = 0; searchPos[0] < histogramSize && !tileFound; searchPos[0] += searchSize[0])
        for (searchPos[1] = 0; searchPos[1] < histogramSize && !tileFound; searchPos[1] += searchSize[1])
          tileFound = h.getValue(searchPos, searchSize) >= sizeThreshold;

      // If the tile was found, try the deeper level
      if (tileFound)
        level++;
    } while (level < maxLevel && tileFound);
    // If the loop terminates when the tile is not found, then the maximum level is the one just before it
    if (!tileFound)
      return level - 1;

    // If the tile is not found, we search for the tile at the last level and also locate the tile with max size
    long maxTileSize = 0;
    searchSize[0] = searchSize[1] = 1;
    tileFound = false;
    for (searchPos[0] = 0; searchPos[0] < histogramSize && !tileFound; searchPos[0]++)
      for (searchPos[1] = 0; searchPos[1] < histogramSize && !tileFound; searchPos[1]++)
        tileFound = (maxTileSize = Math.max(maxTileSize, h.getValue(searchPos, searchSize))) >= sizeThreshold;

    // We couldn't find the tile at the deepest level, it  must have been at the previous level
    if (!tileFound)
      return level - 1;

    // If the tile is found, then we need to dig deeper to see if it also exists at deeper levels
    // We need to go deeper delta_d levels such that maxTileSize / 4^(delta_d) <= sizeThreshold
    // delta_d <= log(maxTileSize / sizeThreshold) / log(4)
    // But delta_d is integer.
    // delta_d = Math.floor(log(maxTileSize / sizeThreshold) / log(4))
    int delta_d = MathUtil.log2((int) (maxTileSize / sizeThreshold)) / 2;
    level += delta_d;
    return level;
  }

  /**
   * A class that represents a range (min..max) inclusive of both. Used to store a range of zoom levels.
   */
  public static class Range implements WritableExternalizable {
    public int min, max;

    public Range() {}

    public Range(int min, int max) {
      this.min = min;
      this.max = max;
    }

    @Override
    public String toString() {
      return min == 0? Integer.toString(max + 1) : String.format("%d..%d", min, max);
    }

    @Override
    public void write(DataOutput out) throws IOException {
      out.writeInt(min);
      out.writeInt(max);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
      this.min = in.readInt();
      this.max = in.readInt();
    }
  }


  /** The classes of tiles according to our design */
  public enum TileClass {ImageTile, DataTile, ShallowTile, EmptyTile};

  public interface PyramidConsumer<T> {
    void consume(long tileID, T t);
  }

  /**
   * Computes the size of the given tile in the histogram.
   * @param h
   * @param z
   * @param x
   * @param y
   * @return
   */
  public static long getTileSize(AbstractHistogram h, int z, int x, int y) {
    assert h.getCoordinateDimension() == 2;
    assert h.getNumPartitions(0) == h.getNumPartitions(1);
    int gridDimensionAtLevelZ = 1 << z;
    int histogramDimension = h.getNumPartitions(0);
    int numHistogramCellsPerTile;
    double fractionOfGridCellCoveredByTile;
    int col1, row1;
    if (gridDimensionAtLevelZ <= histogramDimension) {
      // The given tile (at level Z) covers multiple cells in the histogram
      numHistogramCellsPerTile = histogramDimension / gridDimensionAtLevelZ;
      fractionOfGridCellCoveredByTile = 1.0;
      col1 = x * numHistogramCellsPerTile;
      row1 = y * numHistogramCellsPerTile;
    } else { // gridDimensionAtLevelZ > histogramDimension
      // The given tile (at level Z) covers only a partial cell in the histogram
      numHistogramCellsPerTile = 1;
      fractionOfGridCellCoveredByTile = ((double) histogramDimension) / gridDimensionAtLevelZ;
      col1 = (x / (gridDimensionAtLevelZ / histogramDimension)) * numHistogramCellsPerTile;
      row1 = (y / (gridDimensionAtLevelZ / histogramDimension)) * numHistogramCellsPerTile;
    }
    long size = (long) (fractionOfGridCellCoveredByTile * fractionOfGridCellCoveredByTile *
            h.getValue(new int[] {col1, row1}, new int[] {numHistogramCellsPerTile, numHistogramCellsPerTile}));
    return size;
  }

  /**
   * Classifies a tile according to the file histogram into one of the four
   * classes: ImageTile, DataTile, EmptyTile, and ShallowTile
   * @param h A precomputed histogram
   * @param threshold The size threshold of image and data tiles
   * @param z The level of the tile
   * @param x The column index of the tile in the level z
   * @param y The row index of the tile in the level z
   * @return
   */
  public static TileClass classifyTile(AbstractHistogram h, long threshold, int z, int x, int y) {
    long tileSize = getTileSize(h, z, x, y);
    // If the size is larger than the threshold then it has to be an image tile
    if (tileSize > threshold)
      return TileClass.ImageTile;
    // The code below is incorrect because the histogram is not 100% accurate
    if (tileSize == 0)
      return TileClass.EmptyTile;

    // It could be either a data tile or shallow/empty tile based on its parent size
    if (z == 0)
      return TileClass.DataTile; // No parent

    // Compute the size of the parent
    z--;
    x /= 2;
    y /= 2;
    long parentSize = getTileSize(h, z, x, y);
    if (parentSize > threshold)
      return TileClass.DataTile;

    return TileClass.ShallowTile;
  }

  static class CanvasModified {
    Canvas canvas;
    boolean modified;

    CanvasModified(Canvas c) {
      this.canvas = c;
      this.modified = false;
    }
  }

  /**
   * Creates and returns all the tiles in the given sub pyramid that contains
   * the given set of shapes.
   * @param features The features to be plotted
   * @param featuresMBR the minimum bounding rectangle of the features
   * @param partitioner the pyramid partitioner that defines the tiles to generate
   * @param tileWidth Width of each tile in pixels
   * @param tileHeight Height of each tile in pixels
   * @param plotter The plotter used to create canvases
   * @param tiles (output) The set of tiles that have been created already. It could be
   *              empty which indicates no tiles created yet.
   */
  public static void createTiles(
      Iterable<? extends IFeature> features, Envelope featuresMBR, PyramidPartitioner partitioner,
      int tileWidth, int tileHeight, Plotter plotter, Map<Long, Canvas> tiles) {

    Envelope shapeMBR = null;
    Envelope tileMBR = null;

    LongArray overlappingTiles = new LongArray();
    TileIndex tileIndex = new TileIndex();

    Map<Long, CanvasModified> tempTiles = new HashMap<>();

    Feature tempFeature = new Feature();
    for (IFeature feature : features) {
      if (shapeMBR == null) {
        shapeMBR = new Envelope(feature.getGeometry().getCoordinateDimension());
        tileMBR = new Envelope(feature.getGeometry().getCoordinateDimension());
      }
      feature.getGeometry().envelope(shapeMBR);

      partitioner.overlapPartitions(shapeMBR, overlappingTiles);
      for (long tileID : overlappingTiles) {
        CanvasModified c = tempTiles.get(tileID);
        if (c == null) {
          // First time to encounter this tile, create the corresponding canvas
          TileIndex.decode(tileID, tileIndex);
          TileIndex.getMBR(featuresMBR, tileIndex.z, tileIndex.x, tileIndex.y, tileMBR);
          c = new CanvasModified(plotter.createCanvas(tileWidth, tileHeight, tileMBR));
          tempTiles.put(tileID, c);
        }
        c.modified = plotter.plot(c.canvas, feature) || c.modified ;
      }
    }
    for (Map.Entry<Long, CanvasModified> tempEntry : tempTiles.entrySet())
      if (tempEntry.getValue().modified)
        tiles.put(tempEntry.getKey(), tempEntry.getValue().canvas);
  }

  public static String getTileFileName(int z, int x, int y) {
    return String.format("tile-%d-%d-%d", z, x, y);
  }

  /**
   * Plot the tile without checking its modification time.
   * @param fs
   * @param vizPath
   * @param tileID
   * @param out
   * @return
   * @throws IOException
   * @throws InterruptedException
   */
  public static boolean plotTile(FileSystem fs, Path vizPath, long tileID, OutputStream out)
      throws IOException, InterruptedException {
    return plotTile(fs, vizPath, tileID, out, new LongWritable(0));
  }

  /**
   * Runs the visualization query and returns the image of a single tile.
   * For efficiency, this function can skip creating the image based on the given timestamp of a previously cached
   * image as follow.
   * <ol>
   *   <li>If the modification time of the metadata file is older than the given timestamp, skip.</li>
   *   <li>If the modification time of the data tile is older than the given timestamp, skip.</li>
   *   <li>If no data tiles are present and the modification time of the master file of the index is older than
   *   the given timestamp, skip</li>
   * </ol>
   * @param fs
   * @param vizPath
   * @param tileID
   * @param clientTimestamp (inout) as input, it holds the timestamp of a cached version on the client or zero if the client
   *                  did not mention a cached version. As output, it will hold the latest timestamp of a file used
   *                  to generate the image.
   * @return
   */
  public static boolean plotTile(FileSystem fs, Path vizPath, long tileID, OutputStream out, LongWritable clientTimestamp)
      throws IOException, InterruptedException {
    TileIndex t = new TileIndex();
    TileIndex.decode(tileID, t);
    boolean movedUp = false;
    Path vizMetadataPath = new Path(vizPath, "_visualization.properties");
    UserOptions opts = fs.exists(vizMetadataPath)?
        UserOptions.loadFromTextFile(fs, vizMetadataPath) : new UserOptions(fs.getConf());

    String imageExtension = Plotter.getImageExtension(opts.get(CommonVisualizationHelper.PlotterName));
    String dataExtension = FeatureReader.getFileExtension(opts.get(SpatialInputFormat.InputFormat));
    do {
      String filename = getTileFileName(t.z, t.x, t.y);
      Path imgFilePath = new Path(vizPath, filename + imageExtension);
      if (fs.exists(imgFilePath)) {
        if (movedUp)
          break;
        long localTimestamp = fs.getFileStatus(imgFilePath).getModificationTime();
        if (localTimestamp <= clientTimestamp.get()) {
          // The client-cached version is at least as new as the local file, skip file generation
          return false;
        }
        // Return the timestamp of that image
        clientTimestamp.set(localTimestamp);
        InputStream in = fs.open(imgFilePath);
        try {
          IOUtils.copyBytes(in, out, 8192);
          return true;
        } finally {
          in.close();
        }
      } else {
        // Test if there is a data tile
        Path dataFilePath = new Path(vizPath, filename + dataExtension);
        if (fs.exists(dataFilePath)) {
          long dataFileTimestamp = Math.max(fs.getFileStatus(dataFilePath).getModificationTime(),
              fs.getFileStatus(vizMetadataPath).getModificationTime());
          if (dataFileTimestamp <= clientTimestamp.get()) {
            // The client-cached version is newer than the disk version
            return false;
          }
          // Update the timestamp of the generated tile to the timestamp of this data file
          clientTimestamp.set(dataFileTimestamp);
          // Visualize the data tile on the fly
          Plotter plotter = Plotter.createAndConfigurePlotter(opts);
          DataOutputStream dos = new DataOutputStream(out);
          int tileWidth = opts.getInt(SingleLevelPlotHelper.ImageWidth, 256);
          int tileHeight = opts.getInt(SingleLevelPlotHelper.ImageHeight, 256);
          // Disable Mercator projection since data tiles are already projected
          opts.setBoolean(CommonVisualizationHelper.UseMercatorProjection, false);
          // Set the MBR based on the tile
          Envelope inputMBR = CSVEnvelopeDecoder.instance.apply(opts.get("mbr"), new Envelope());
          Envelope tileMBR = new Envelope(inputMBR.getCoordinateDimension());
          TileIndex.decode(tileID, t);
          if (opts.getBoolean(CommonVisualizationHelper.VerticalFlip, true))
            t.vflip();
          tileID = TileIndex.encode(t.z, t.x, t.y);
          TileIndex.getMBR(inputMBR, tileID, tileMBR);
          opts.set("mbr", CSVEnvelopeEncoder.defaultEncoder.apply(tileMBR));
          SingleLevelPlotHelper.plotLocal(dataFilePath, dos, plotter.getClass(), tileWidth, tileHeight, false, opts);
          dos.flush();
          return true;
        } else {
          t.moveToParent();
          movedUp = true;
        }
      }
    } while (t.z >= 0);
    // No image or data file found. Check if there is an associated data index to generate the image on the fly
    if (opts.get("data") == null)
      return false;
    Path dataPath = new Path(vizPath, opts.get("data")); // Path is relative to the vizPath
    FileStatus[] masterFiles = fs.listStatus(dataPath, SpatialInputFormat.MasterFileFilter);
    if (masterFiles.length > 0) {
      if (masterFiles[0].getModificationTime() <= clientTimestamp.get())
        return false; // An up-to-date version is already cached by client
      clientTimestamp.set(masterFiles[0].getModificationTime());
    }
    // There is an associated data index. Use it to generate the image
    Envelope inputMBR;
    TileIndex.decode(tileID, t); // Restore the requested tile ID
    if (opts.getBoolean(CommonVisualizationHelper.UseMercatorProjection, false)) {
      // For Mercator projection, the data MBR is fixed
      inputMBR = new Envelope(2, 0.0, 0.0, 256.0, 256.0);
    } else {
      inputMBR = CSVEnvelopeDecoder.instance.apply(opts.get("mbr"), new Envelope());
      // For non-mercator projection, adjust the tile ID to get the correct MBR if vflip was used
      if (opts.getBoolean(CommonVisualizationHelper.VerticalFlip, true))
        t.y = ((1 << t.z) - 1) - t.y;
    }
    // Set the MBR based on the tile
    Envelope tileMBR = new Envelope(inputMBR.getCoordinateDimension());
    TileIndex.getMBR(inputMBR, t.z, t.x, t.y, tileMBR);
    if (opts.getBoolean(CommonVisualizationHelper.UseMercatorProjection, false)) {
      // Web Mercator projection is in use while original data in world coordinates.
      // We need to convert the MBR and FitlerMBR back to the world coordinates to match
      MercatorProjector.mercatorToWorld(tileMBR);
    }
    opts.set("mbr", CSVEnvelopeEncoder.defaultEncoder.apply(tileMBR));
    opts.set(SpatialInputFormat.FilterMBR, CSVEnvelopeEncoder.defaultEncoder.apply(tileMBR));

    Plotter plotter = Plotter.createAndConfigurePlotter(opts);
    DataOutputStream dos = new DataOutputStream(out);
    int tileWidth = opts.getInt(MultilevelPyramidPlotHelper.TileWidth, 256);
    int tileHeight = opts.getInt(MultilevelPyramidPlotHelper.TileHeight, 256);
    SingleLevelPlotHelper.plotLocal(dataPath, dos, plotter.getClass(), tileWidth, tileHeight, false, opts);
    dos.flush();
    // TODO: if the image is empty, we might want to return false
    return true;
  }

  /**
   * Write some additional files that help visualizing a multilevel visualization image.
   * @param fs
   * @param vizPath
   * @param opts
   * @throws IOException
   */
  public static void writeVisualizationAddOnFiles(FileSystem fs, Path vizPath, UserOptions opts) throws IOException {
    int tileWidth = opts.getInt(SingleLevelPlotHelper.ImageWidth, 256);
    int tileHeight = opts.getInt(SingleLevelPlotHelper.ImageHeight, 256);
    boolean mercator = opts.getBoolean(CommonVisualizationHelper.UseMercatorProjection, false);
    // Get the correct levels.
    String[] strLevels = opts.get(MultilevelPyramidPlotHelper.NumLevels, "7").split("\\.\\.");
    int minLevel, maxLevel;
    if (strLevels.length == 1) {
      minLevel = 0;
      maxLevel = Integer.parseInt(strLevels[0]) - 1;
    } else {
      minLevel = Integer.parseInt(strLevels[0]);
      maxLevel = Integer.parseInt(strLevels[1]);
    }

    // Add an HTML file that visualizes the result using OpenLayers API
    String templateFile =  "/zoom_view.html";
    LineReader templateFileReader = new LineReader(MultilevelPyramidPlotHelper.class.getResourceAsStream(templateFile));
    PrintStream htmlOut = new PrintStream(fs.create(new Path(vizPath, "index.html")));
    try {
      Text line = new Text();
      while (templateFileReader.readLine(line) > 0) {
        String lineStr = line.toString();
        lineStr = lineStr.replace("#{MERCATOR}", Boolean.toString(mercator));
        lineStr = lineStr.replace("#{TILE_WIDTH}", Integer.toString(tileWidth));
        lineStr = lineStr.replace("#{TILE_HEIGHT}",  Integer.toString(tileHeight));
        lineStr = lineStr.replace("#{MAX_ZOOM}", Integer.toString(maxLevel));
        lineStr = lineStr.replace("#{MIN_ZOOM}", Integer.toString(minLevel));
        lineStr = lineStr.replace("#{TILE_URL}", "tile-{z}-{x}-{y}.png");

        htmlOut.println(lineStr);
      }
    } finally {
      templateFileReader.close();
      htmlOut.close();
    }

    // Store the user options
    // Whatever we used as an output format should be used as an input format

    String oformat = opts.get(SpatialOutputFormat.OutputFormat);
    if (oformat != null) {
      opts.set(SpatialInputFormat.InputFormat, oformat);
    }
    opts.storeToTextFile(fs, new Path(vizPath, "_visualization.properties"));
  }

}
