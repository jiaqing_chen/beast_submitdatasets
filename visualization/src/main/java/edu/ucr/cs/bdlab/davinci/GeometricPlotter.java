/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.davinci;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.GeometryCollection;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.ILineString;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.twod.MultiLineString2D;
import edu.ucr.cs.bdlab.geolite.twod.Polygon2D;
import edu.ucr.cs.bdlab.util.OperationParam;
import org.apache.hadoop.conf.Configuration;

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * A plotter that draws the geometry of the features (e.g., the point location or polygon boundary)
 */
@Plotter.Metadata(
    shortname = "gplot",
    imageExtension = ".png",
    description = "A plotter that draws the geometric shape of objects on a raster canvas"
)
public class GeometricPlotter extends Plotter {

  @OperationParam(
      description = "The color of the stroke of the objects. Used for points, envelopes, lines, and polygons.",
      defaultValue = "blacksuper.readFields(in);"
  )
  public static final String StrokeColor = "stroke";

  @OperationParam(
     description = "The color of the fill of the objects. Used for envelopes and polygons",
      defaultValue = "none"
  )
  public static final String FillColor = "fill";

  @OperationParam(
      description = "Reduce memory footprint of the plotter at the cost of increased processing time",
      defaultValue = "false"
  )
  public static final String Compact = "compact";

  /**Color for the outer stroke or for points*/
  private Color strokeColor;

  /**Color for the fill of polygons or envelopes*/
  private Color fillColor;

  /**When this flag is set, use the BitImageCanvas*/
  private boolean compact;

  /**Temporary arrays for conversion of shapes from input space to image space*/
  private final int[] xs = new int[1000];
  private final int[] ys = new int[1000];
  private final Point tempPoint = new Point(2);
  private final Envelope tempEnvelope = new Envelope(2);

  @Override
  public void setup(Configuration clo) {
    super.setup(clo);
    this.strokeColor = CommonVisualizationHelper.getColor(clo.get(StrokeColor, "Black"));
    this.fillColor = CommonVisualizationHelper.getColor(clo.get(FillColor, "None"));
    this.compact = clo.getBoolean(Compact, true);
  }

  @Override
  public Canvas createCanvas(int width, int height, Envelope mbr) {
    if (compact)
      return new BitImageCanvas(mbr, width, height, 2);
    else {
      return new FastImageCanvas(mbr, width, height);
    }
  }

  @Override
  public boolean plot(Canvas canvasLayer, IFeature shape) {
    return plotGeometry(canvasLayer, shape.getGeometry());
  }

  /**
   * Plots the given geometry to the canvas. The temporary arrays xs and ys are used for converting geometry
   * objects from model space to image space.
   * @param canvas
   * @param geom
   */
  protected boolean plotGeometry(Canvas canvas, IGeometry geom) {
    BitImageCanvas bitCanvas = null;
    Graphics2D g = null;
    if (canvas instanceof BitImageCanvas)
      bitCanvas = (BitImageCanvas) canvas;
    else if (canvas instanceof FastImageCanvas)
      g = ((FastImageCanvas) canvas).getOrCreateGrahics();
    else
      throw new RuntimeException("Unknown canvas type "+canvas.getClass());

    if (geom.isEmpty())
      return false;
    assert geom.getCoordinateDimension() == 2;

    boolean changed = false;
    int startPoint, endPoint;
    switch (geom.getType()) {
      case POINT:
        int x = (int) canvas.transformX(((Point)geom).coords[0]);
        int y = (int) canvas.transformY(((Point)geom).coords[1]);
        if (bitCanvas != null) {
          bitCanvas.fillRect(x, y, 1, 1, 1);
        } else if (g != null) {
          g.setColor(strokeColor);
          g.fillRect(x, y, 1, 1);
        }
        changed = true;
        break;
      case ENVELOPE:
        Envelope env = (Envelope) geom;
        int x1, y1, x2, y2;
        x1 = (int) canvas.transformX(env.minCoord[0]);
        y1 = (int) canvas.transformY(env.minCoord[1]);
        x2 = (int) canvas.transformX(env.maxCoord[0]);
        y2 = (int) canvas.transformY(env.maxCoord[1]);
        if (bitCanvas != null) {
          if (fillColor.getAlpha() != 0) {
            bitCanvas.fillRect(x1, y1, x2 - x1, y2 - y1, 0);
            changed = true;
          }
          bitCanvas.drawRect(x1, y1, x2 - x1, y2 - y1, 1);
        } else if (g != null) {
          if (fillColor.getAlpha() != 0) {
            g.setColor(fillColor);
            bitCanvas.fillRect(x1, y1, x2 - x1, y2 - y1, 0);
            changed = true;
          }
          g.setColor(strokeColor);
          g.drawRect(x1, y1, x2 - x1, y2 - y1);
        }
        changed = changed || x1 >= 0 || y1 >= 0 || x2 < canvas.getWidth() || y2 < canvas.getHeight();
        break;
      case LINESTRING:
        ILineString lineString = (ILineString) geom;
        startPoint = 0;
        endPoint = (int) lineString.getNumPoints();
        changed = plotLineSubstring(canvas, bitCanvas, g, lineString, startPoint, endPoint, false, false) || changed;
        break;
      case POLYGON:
      case MULTIPOLYGON:
        Polygon2D polygon = (Polygon2D) geom;
        // TODO handle filled polygons with holes
        for (int iRing = 0; iRing < polygon.getNumRings(); iRing++) {
          startPoint = polygon.getRingStart(iRing);
          endPoint = polygon.getRingEnd(iRing);
          changed = plotLineSubstring(canvas, bitCanvas, g, polygon, startPoint, endPoint, true, iRing == 0) || changed;
        }
        break;
      case MULTILINESTRING:
        MultiLineString2D mline = (MultiLineString2D) geom;
        for (int i = 0; i < mline.getNumLineStrings(); i++) {
          startPoint = mline.getLineStringStart(i);
          endPoint = mline.getLineStringEnd(i);
          changed = plotLineSubstring(canvas, bitCanvas, g, mline, startPoint, endPoint, false, false) || changed;
        }
        break;
      case GEOMETRYCOLLECTION:
        GeometryCollection geomCollection = (GeometryCollection) geom;
        for (int iGgeom = 0; iGgeom < geomCollection.numGeometries(); iGgeom++)
          plotGeometry(canvas, geomCollection.getGeometry(iGgeom));
        break;
      default:
        throw new RuntimeException(String.format("Cannot plot geometries of type '%s'", geom.getType()));
    }
    return changed;
  }

  private boolean plotLineSubstring(Canvas canvas, BitImageCanvas bitCanvas, Graphics2D g, ILineString lineString,
                                    int startPoint, int endPoint, boolean closeRing, boolean fill) {
    boolean canvasChanged = false;
    int x;
    int y;
    int arraySize = 0;
    // The mask of a point has four bits set as described below.
    // Bit #0: If set, it means that the point is outside the canvas from the right
    // Bit #1: If set, it means that the point is outside the canvas from the left
    // Bit #2: If set, it means that the point is outside the canvas from the top
    // Bit #3: If set, it means that the point is outside the canvas from the bottom
    // If two consecutive points have the same bit set (whatever the bit is), it indicates a line segment that
    // is not visible on the image.
    int prevMask = 0;
    for (int iPoint = startPoint; iPoint < endPoint; iPoint++) {
      lineString.getPointN(iPoint, tempPoint);
      x = (int) canvas.transformX(tempPoint.coords[0]);
      y = (int) canvas.transformY(tempPoint.coords[1]);
      int mask = 0;
      if (x < 0) mask |= 1;
      if (x > canvas.getWidth()) mask |= 2;
      if (y < 0) mask |= 4;
      if (y > canvas.getHeight()) mask |= 8;
      if ((mask & prevMask) != 0) {
        // Line segment not visible. Plot whatever we have right now and only add the last point
        if (arraySize > 1) {
          drawPolyline(xs, ys, bitCanvas, g, arraySize, fill);
          canvasChanged = true;
        }
        xs[0] = x;
        ys[0] = y;
        arraySize = 1;
      } else {
        // Segment might be visible
        xs[arraySize] = x;
        ys[arraySize] = y;
        if (arraySize == 0 || xs[arraySize] != xs[arraySize-1] || ys[arraySize] != ys[arraySize-1])
          arraySize++;
      }
      prevMask = mask;
      if (arraySize == xs.length || iPoint == endPoint - 1) {
        // Plot the points that we converted so far and advance to the next batch
        if (arraySize > 1) {
          drawPolyline(xs, ys, bitCanvas, g, arraySize, fill);
          canvasChanged = true;
        }
        xs[0] = xs[arraySize - 1];
        ys[0] = ys[arraySize - 1];
        arraySize = 1;
        if (iPoint == endPoint - 1 && closeRing) {
          // If the ring needs to be closed, draw one more line
          lineString.getPointN(startPoint, tempPoint);
          x = (int) canvas.transformX(tempPoint.coords[0]);
          y = (int) canvas.transformY(tempPoint.coords[1]);
          xs[arraySize] = x;
          ys[arraySize] = y;
          arraySize++;
          drawPolyline(xs, ys, bitCanvas, g, arraySize, fill);
        }
      }
    }
    return canvasChanged;
  }

  private void drawPolyline(int[] xs, int[] ys, BitImageCanvas bitCanvas, Graphics2D g, int numPoints, boolean fill) {
    if (bitCanvas != null) {
      bitCanvas.drawPolyline(xs, ys, numPoints, 1);
      // TODO handle filled polygons in bit canvas
    } else if (g != null) {
      if (fill) {
        g.setColor(fillColor);
        g.fillPolygon(xs, ys, numPoints);
      }
      g.setColor(strokeColor);
      g.drawPolyline(xs, ys, numPoints);
    }
  }

  @Override
  public Class<? extends Canvas> getCanvasClass() {
    return compact? BitImageCanvas.class : FastImageCanvas.class;
  }

  @Override
  public Canvas merge(Canvas finalLayer, Canvas intermediateLayer) {
    if (finalLayer instanceof BitImageCanvas)
      ((BitImageCanvas)finalLayer).mergeCanvas((BitImageCanvas) intermediateLayer);
    else
      ((FastImageCanvas)finalLayer).mergeWith((FastImageCanvas) intermediateLayer);
    return finalLayer;
  }

  @Override
  public void writeImage(Canvas layer, DataOutputStream out, boolean vflip) throws IOException {
    BufferedImage finalImage;
    if (layer instanceof BitImageCanvas) {
      BitImageCanvas imageCanvas = (BitImageCanvas) layer;
      finalImage = new BufferedImage(imageCanvas.width, imageCanvas.height, BufferedImage.TYPE_INT_ARGB);
      imageCanvas.plotToImage(finalImage, new Color[]{fillColor, strokeColor});
    } else if (layer instanceof FastImageCanvas) {
      finalImage = ((FastImageCanvas)layer).getImage();
    } else {
      throw new RuntimeException("Unknown canvas type "+layer.getClass());
    }
    // Flip image vertically if needed
    if (vflip) {
      AffineTransform tx = AffineTransform.getScaleInstance(1, -1);
      tx.translate(0, -finalImage.getHeight());
      AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
      finalImage = op.filter(finalImage, null);
    }
    ImageIO.write(finalImage, "png", out);
  }

  @Override
  public void write(DataOutput out) throws IOException {
    out.writeInt(strokeColor.getRGB());
    out.writeInt(fillColor.getRGB());
    out.writeBoolean(compact);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    this.strokeColor = new Color(in.readInt());
    this.fillColor = new Color(in.readInt());
    this.compact = in.readBoolean();
  }
}
