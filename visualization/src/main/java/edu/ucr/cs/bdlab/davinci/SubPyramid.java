/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.davinci;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.util.WritableExternalizable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Represents a part of the pyramid structure defined by a minimum level,
 * a maximum level, and a rectangular range of tiles at the maximum level.
 * Created by Ahmed Eldawy on 1/2/18.
 */
public class SubPyramid implements WritableExternalizable {
  /**
   * The MBR covered by the full pyramid (not this subpyramid).
   * The range is inclusive for (x1, y1) and exclusive for (x2, y2)
   */
  public double x1, y1, x2, y2;

  /** The range of levels (inclusive) covered by the subpyramid */
  public int minimumLevel, maximumLevel;

  /**
   * The range of tiles covered by this subpyramid at the maximumLevel
   * The range includes the first row (r1), the first column (c1), and
   * excludes the last row (r2) and last column (c2).
   */
  public int c1, r1, c2, r2;

  /** The width and height of each tile at maximumLevel */
  private transient double tileWidth, tileHeight;

  /**Empty constructor for the Writable interface*/
  public SubPyramid() {
  }

  /**
   * Construct a new SubPyramid
   * @param mbr the minimum bounding rectangle of the full pyramid, i.e., the MBR of the tile at level 0
   * @param minLevel the minimum level covered by this subpyramid (0-based inclusive)
   * @param maxLevel the maximum level covereted by this subpyramid (0-based and inclusive)
   * @param c1 the first column at maxLevel covered by this subpyramid (0-based and inclusive)
   * @param r1 the first row at maxLevel covered by this subpyramid (0-based and inclusive)
   * @param c2 the last column at maxLevel covered by this subpyramid (0-based and exclusive)
   * @param r2 the last row at maxLevel covered by this subpyramid (0-based and exclusive)
   */
  public SubPyramid(Envelope mbr, int minLevel, int maxLevel, int c1, int r1, int c2, int r2) {
    this.set(mbr, minLevel, maxLevel, c1, r1, c2, r2);
  }

  /**
   * Creates a pyramid that covers the entire region of interest in the given zoom levels (inclusive of both)
   * @param mbr the region of interest
   * @param minLevel the minimum level (inclusive)
   * @param maxLevel the maximum level (inclusive)
   */
  public SubPyramid(Envelope mbr, int minLevel, int maxLevel) {
    this.set(mbr, minLevel, maxLevel, 0, 0, 1 << maxLevel, 1 << maxLevel);
  }

  public void set(SubPyramid that) {
    this.x1 = that.x1;
    this.y1 = that.y1;
    this.x2 = that.x2;
    this.y2 = that.y2;
    this.minimumLevel = that.minimumLevel;
    this.maximumLevel = that.maximumLevel;
    this.c1 = that.c1;
    this.r1 = that.r1;
    this.c2 = that.c2;
    this.r2 = that.r2;
    this.tileWidth = that.tileWidth;
    this.tileHeight = that.tileHeight;
  }

  /**
   *
   * @param mbr the MBR of the input space covered by the entire pyramid (not the subpyramid)
   * @param minLevel the minimum level covered by this sub-pyramid (inclusive)
   * @param maxLevel the maximum level covered by this sub-pyramid (inclusive)
   * @param c1 At {@code maxLevel} the index of the first column covered by this sub-pyramid (inclusive)
   * @param r1 At {@code maxLevel} the index of the first row covered by this sub-pyramid (inclusive)
   * @param c2 At {@code maxLevel} the index of the last column covered by this sub-pyramid (exclusive)
   * @param r2 At {@code maxLevel} the index of the last row covered by this sub-pyramid (exclusive)
   */
  public void set(Envelope mbr, int minLevel, int maxLevel,
                  int c1, int r1, int c2, int r2) {
    this.x1 = mbr.minCoord[0];
    this.x2 = mbr.maxCoord[0];
    this.y1 = mbr.minCoord[1];
    this.y2 = mbr.maxCoord[1];
    this.minimumLevel = minLevel;
    this.maximumLevel = maxLevel;
    this.c1 = c1;
    this.r1 = r1;
    this.c2 = c2;
    this.r2 = r2;
    this.tileWidth = (x2 - x1) / (1 << maximumLevel);
    this.tileHeight = (y2 - y1) / (1 << maximumLevel);
  }

  /**
   * Set the sub-pyramid to cover only one tile
   * @param mbr The MBR of the entire data set
   * @param z The level of the given tile starting at zero
   * @param c The column of the given tile
   * @param r The row of the given tile
   */
  public void set(Envelope mbr, int z, int c, int r) {
    this.x1 = mbr.minCoord[0];
    this.x2 = mbr.maxCoord[0];
    this.y1 = mbr.minCoord[1];
    this.y2 = mbr.maxCoord[1];
    this.minimumLevel = this.maximumLevel = z;
    this.c1 = c;
    this.c2 = c + 1;
    this.r1 = r;
    this.r2 = r + 1;
    this.tileWidth = (x2 - x1) / (1 << maximumLevel);
    this.tileHeight = (y2 - y1) / (1 << maximumLevel);
  }

  /**
   * Returns the range of tiles that overlaps the given rectangle at the maximum (deepest) level
   * @param rect
   * @param overlaps
   */
  public void getOverlappingTiles(Envelope rect, java.awt.Rectangle overlaps) {
    overlaps.x = (int) Math.floor((rect.minCoord[0] - this.x1) / tileWidth);
    if (rect.minCoord[0] == rect.maxCoord[0] && rect.minCoord[0] == x2) {
      // Special case when the rect represent a point aligned along the upper edge of the input along the x-axis
      // Setting
      overlaps.x--;
    }
    if (overlaps.x >= c2) {
      overlaps.width = 0;
      return;
    }
    overlaps.y = (int) Math.floor((rect.minCoord[1] - this.y1) / tileHeight);
    if (rect.minCoord[1] == rect.maxCoord[1] && rect.minCoord[1] == y2) {
      // Special case when the rect represent a point aligned along the upper edge of the input along the y-axis
      overlaps.y--;
    }
    if (overlaps.y >= r2) {
      overlaps.height = 0;
      return;
    }
    // Compute the width and height
    int x2 = (int) Math.ceil((rect.maxCoord[0] - this.x1) / tileWidth);
    int y2 = (int) Math.ceil((rect.maxCoord[1] - this.y1) / tileHeight);
    // To accommodate points, the minimum width and height should be 1
    overlaps.width = Math.max(1, x2 - overlaps.x);
    overlaps.height = Math.max(1, y2 - overlaps.y);
    if (overlaps.x + overlaps.width > c2)
      overlaps.width = c2 - overlaps.x;
    if (overlaps.y + overlaps.height > r2)
      overlaps.height = r2 - overlaps.y;
    if (overlaps.x < c1) {
      overlaps.width -= c1 - overlaps.x;
      overlaps.x = c1;
    }
    if (overlaps.y < r1) {
      overlaps.height -= r1 - overlaps.y;
      overlaps.y = r1;
    }
  }

  public Envelope getInputMBR() {
    return new Envelope(2, x1, y1, x2, y2);
  }

  /**
   * Returns the total number of tiles covered by this subpyramid
   * @return
   */
  public long getTotalNumberOfTiles() {
    // The following formulae only work if the entire levels are covered.
    // All tiles until z_max = (4^{z_max+1} - 1) / (4 - 1)
    //long totalNumTilesUntilMaximumLevel = (1L << (2 * (maximumLevel + 1))) / (4 - 1);
    // All tiles up-to (not including) minimum level = (4^{z_min} - 1) / (4 - 1)
    //long totalNumTilesBeforeMinimumLevel = (1L << (2 * minimumLevel)) / (4 - 1);
    //return totalNumTilesUntilMaximumLevel - totalNumTilesBeforeMinimumLevel;

    long totalNumOfTiles = 0;
    int $c1 = c1, $c2 = c2, $r1 = r1, $r2 = r2;
    for (int $l = maximumLevel; $l >= minimumLevel; $l--) {
      totalNumOfTiles += ($c2 - $c1) * ($r2 - $r1);
      $c1 /= 2;
      $c2 /= 2;
      $r1 /= 2;
      $r2 /= 2;
    }
    return totalNumOfTiles;
  }

  public void getTileMBR(int tileID, Envelope tileMBR) {
    // TODO: Avoid creating the envelope
    TileIndex.getMBR(new Envelope(2, x1, y1, x2, y2), tileID, tileMBR);
  }

  @Override
  public void write(DataOutput out) throws IOException {
    out.writeDouble(x1);
    out.writeDouble(y1);
    out.writeDouble(x2);
    out.writeDouble(y2);
    out.writeInt(minimumLevel);
    out.writeInt(maximumLevel);
    out.writeInt(c1);
    out.writeInt(r1);
    out.writeInt(c2);
    out.writeInt(r2);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    x1 = in.readDouble();
    y1 = in.readDouble();
    x2 = in.readDouble();
    y2 = in.readDouble();
    minimumLevel = in.readInt();
    maximumLevel = in.readInt();
    c1  = in.readInt();
    r1  = in.readInt();
    c2  = in.readInt();
    r2  = in.readInt();
    this.tileWidth = (x2 - x1) / (1 << maximumLevel);
    this.tileHeight = (y2 - y1) / (1 << maximumLevel);
  }
}
