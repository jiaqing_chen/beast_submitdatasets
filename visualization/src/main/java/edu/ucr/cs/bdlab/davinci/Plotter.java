/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.davinci;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.util.OperationUtil;
import edu.ucr.cs.bdlab.util.WritableExternalizable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;

import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**An abstract interface for a component that draws shapes*/
public abstract class Plotter implements WritableExternalizable {
  private static final Log LOG = LogFactory.getLog(Plotter.class);

  public static final Map<String, Class<? extends Plotter>> plotters = loadPlotters();

  @Target(ElementType.TYPE)
  @Retention(RetentionPolicy.RUNTIME)
  public @interface Metadata {
    /**A short name for the plotter*/
    String shortname();

    /**A short description for this partitioner*/
    String description();

    /**Extension of the images created using this plotter including the dot, e.g., &quot;.png&quot;*/
    String imageExtension();
  }

  /**
   * Creates and configures a plotter from the given user options. It first searches for a plotter class defined in
   * the parameter {@link CommonVisualizationHelper#PlotterClassName}. Then, it tries to find a plotter defined by
   * a short user-friendly name in the configuration entry {@link CommonVisualizationHelper#PlotterName}. If neither
   * is found, a null is returned.
   * @param conf
   * @return
   */
  public static Plotter createAndConfigurePlotter(Configuration conf) {
    // 1- Try a class name
    Class<? extends Plotter> plotterClass = conf.getClass(CommonVisualizationHelper.PlotterClassName, null, Plotter.class);
    if (plotterClass != null)
      return getConfiguredPlotter(plotterClass, conf);
    // 2- Try a short name
    String plotterName = conf.get(CommonVisualizationHelper.PlotterName);
    if (plotterName != null)
      return getConfiguredPlotter(plotterName, conf);
    // Neither is found
    return null;
  }



  /**
   * Creates and configures a plotter for visualization.
   * @param plotterShortName the short name of the plotter
   * @param conf additional options that might be set by the user for each plotter type
   * @return
   */
  public static Plotter getConfiguredPlotter(String plotterShortName, Configuration conf) {
    return getConfiguredPlotter(getPlotterClass(plotterShortName), conf);
  }

  public static Plotter getConfiguredPlotter(Class<? extends Plotter> plotterClass, Configuration conf) {
    try {
      Plotter plotter = plotterClass.newInstance();
      plotter.setup(conf);
      return plotter;
    } catch (InstantiationException e) {
      throw new RuntimeException("Error creating plotter", e);
    } catch (IllegalAccessException e) {
      throw new RuntimeException("Error constructing plotter", e);
    }
  }

  public static String getImageExtension(String plotterName) {
    Class<? extends Plotter> plotterClass = getPlotterClass(plotterName);
    if (plotterClass == null)
      return null;
    Plotter.Metadata metadata = plotterClass.getAnnotation(Plotter.Metadata.class);
    if (metadata == null)
      return null;
    return metadata.imageExtension();
  }

  public static Class<? extends Plotter> getPlotterClass(String plotterName) {
    return plotters.get(plotterName);
  }

  public static Map<String, Class<? extends Plotter>> loadPlotters() {
    Map<String, Class<? extends Plotter>> partitioners = new TreeMap<>();
    List<String> partitionerClasses = OperationUtil.readConfigurationXML("beast.xml").get("Plotters");
    for (String partitionerClassName : partitionerClasses) {
      try {
        Class<? extends Plotter> plotterClass =
            Class.forName(partitionerClassName).asSubclass(Plotter.class);
        Plotter.Metadata metadata = plotterClass.getAnnotation(Plotter.Metadata.class);
        if (metadata == null) {
          LOG.warn(String.format("Skipping plotter '%s' without a valid Metadata annotation", plotterClass.getName()));
        } else {
          partitioners.put(metadata.shortname(), plotterClass);
        }
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
    }
    return partitioners;
  }

  /**
   * Reads and configures a plotter to use in visualization.
   * @param clo any additional parameters that could be set by the user
   */
  public void setup(Configuration clo) {
    // An empty body to allow plotters to skip implementing this function if not needed
  }
  
  /**
   * Smooth a set of records that are spatially close to each other and returns
   * a new set of smoothed records. This method is called on the original
   * raw data before it is visualized. The results of this function are the
   * records that are visualized.
   * @param features
   * @return
   */
  public Iterable<? extends IFeature> smooth(Iterable<? extends IFeature> features) {
    return features;
  }

  /**
   * Creates an empty canvas of the given width and height.
   * @param width - Width of the created layer in pixels
   * @param height - Height of the created layer in pixels
   * @param mbr - The minimal bounding rectangle of the layer in the input
   * @return
   */
  public abstract Canvas createCanvas(int width, int height, Envelope mbr);

  /**
   * Plots one shape to the given layer
   * @param layer - the canvas to plot to. This canvas has to be created
   * using the method {@link #createCanvas(int, int, Envelope)}.
   * @param feature - the spatial feature to plot
   * @return {@code true} if the canvas was changed as a result of this plot.
   */
  public abstract boolean plot(Canvas layer, IFeature feature);

  /**
   * Merges the second canvas into the first canvas and returns the first canvas
   * @param finalCanvas the final canvas that will contain the result
   * @param intermediateCanvas the intermediate canvas that will be merged into the final canvas
   * @return the final canvas after merging
   */
  public abstract Canvas merge(Canvas finalCanvas, Canvas intermediateCanvas);

  /**
   * Writes a canvas as an image to the output.
   * @param layer - the layer to be written to the output as an image
   * @param out - the output stream to which the image will be written
   * @param vflip - if <code>true</code>, the image is vertically flipped before written
   */
  public abstract void writeImage(Canvas layer, DataOutputStream out, boolean vflip) throws IOException;
  
  /**
   * Returns the raster class associated with this rasterizer
   * @return
   */
  public Class<? extends Canvas> getCanvasClass() {
    return this.createCanvas(0, 0, new Envelope()).getClass();
  }

  /**
   * Plots a set of shapes to the given layer
   * @param layer - the canvas to plot to. This canvas has to be created
   * using the method {@link #createCanvas(int, int, Envelope)}.
   * @param shapes - a set of shapes to plot
   * @return {@code true} if the canvas was changed as a result of the plot
   */
  public boolean plot(Canvas layer, Iterable<? extends IFeature> shapes) {
    boolean changed = false;
    for (IFeature shape : shapes)
      changed = plot(layer, shape) || changed;
    return changed;
  }
}