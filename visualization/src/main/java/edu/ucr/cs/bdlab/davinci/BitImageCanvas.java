package edu.ucr.cs.bdlab.davinci;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.util.BitArray;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * A raster canvas that maintains one bit per pixel for space efficiency
 */
public class BitImageCanvas extends Canvas {

  /**A bit array that stores all the pixels that are set in the canvas in each of the requested layers*/
  protected BitArray[] bits;

  /** Default constructor for serialization/deserialization */
  public BitImageCanvas() {}

  public BitImageCanvas(Envelope inputMBR, int width, int height, int nLayers) {
    super(inputMBR, width, height);
    bits = new BitArray[nLayers];
    for (int $i = 0; $i < nLayers; $i++)
      bits[$i] = new BitArray(width * height);
    calculateScale();
  }

  @Override
  public void write(DataOutput out) throws IOException {
    super.write(out);
    out.writeInt(bits.length); // Write number of layers
    for (int $i = 0; $i < bits.length; $i++)
      bits[$i].write(out);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    super.readFields(in);
    int nLayers = in.readInt();
    if (bits == null || nLayers != bits.length) {
      bits = new BitArray[nLayers];
      for (int $iLayer = 0; $iLayer < nLayers; $iLayer++)
        bits[$iLayer] = new BitArray();
    }
    for (int $i = 0; $i < bits.length; $i++)
      bits[$i].readFields(in);
    calculateScale();
  }

  /**
   * Sets the given pixel while applying the translation defined in this canvas.
   */
  public void setPixel(int x, int y, int iLayer) {
    // Skip out of bound pixels
    if (x < 0 || y < 0 || x >= width || y >= height)
      return;
    bits[iLayer].set((long) y * width + x, true);
  }

  /**
   * Sets all the bits in the given rectangle. Translation is not applied to the given coordinates.
   * @param rx origin of the rectangle
   * @param ry
   * @param rw width of the rectangle in pixels
   * @param rh height of the rectangle in pixels
   * @param iLayer the index of the layer to modify
   */
  protected void dumpRect(int rx, int ry, int rw, int rh, int iLayer) {
    // Do not apply the transformation here as it is assumed to be applied already
    if (rx < 0) {
      rw += rx;
      rx = 0;
    }
    if (ry < 0) {
      rh += ry;
      ry = 0;
    }
    if (ry + rh > this.height)
      rh -= ry + rh - this.height;
    if (rx + rw > this.width)
      rw = this.width - rx;
    if (rx >= this.width || ry >= this.height || rw < 0 || rh < 0)
      return;
    int offset = ry * this.width + rx;
    while (rh-- > 0) {
      bits[iLayer].setRange(offset, offset + rw, true);
      offset += this.width;
    }
  }

  /**
   * Fills the given rectangle with bits (apply the translation)
   * @param x
   * @param y
   * @param width
   * @param height
   * @param iLayer the index of the layer to modify
   */
  public void fillRect(int x, int y, int width, int height, int iLayer) {
    dumpRect(x, y, width, height, iLayer);
  }

  public void drawRect(int x, int y, int width, int height, int iLayer) {
    // Do not apply the translation in this function as the drawLine function will apply it
    if ((width <= 0) || (height <= 0)) {
      return;
    }

    dumpRect(x, y, width, 1, iLayer);
    dumpRect(x, y, 1, height, iLayer);
    dumpRect(x + width - 1, y, 1, height, iLayer);
    dumpRect(x, y + height - 1, width, 1, iLayer);
  }

  /**
   * Draws a straight line between the given two points.
   * @param x1
   * @param y1
   * @param x2
   * @param y2
   * @param iLayer the index of the layer to draw the line in
   */
  public void drawLine(int x1, int y1, int x2, int y2, int iLayer) {
    if (y1 == y2 || x1 == x2) {
      if (x1 > x2) {
        x1 ^= x2;
        x2 ^= x1;
        x1 ^= x2;
      }
      if (y1 > y2) {
        y1 ^= y2;
        y2 ^= y1;
        y1 ^= y2;
      }
      if (x2 < 0 || y2 < 0)
        return;
      if (x1 >= width && y1 >= height)
        return;
      if (x1 < 0)
        x1 = 0;
      if (y1 < 0)
        y1 = 0;
      if (x2 >= width)
        x2 = width - 1;
      if (y2 >= height)
        y2 = height - 1;
      // Draw an orthogonal line
      dumpRect(x1, y1, x2 - x1 + 1, y2 - y1 + 1, iLayer);
    } else {
      int dx = Math.abs(x2 - x1);
      int dy = Math.abs(y2 - y1);
      if (dx > dy) {
        if (x1 > x2) {
          // Ensure that x1 <= x2
          x1 ^= x2; x2 ^= x1; x1 ^= x2;
          y1 ^= y2; y2 ^= y1; y1 ^= y2;
        }
        int incy = y1 < y2? 1 : -1;
        int p = dy - dx / 2;
        int y = y1;
        for (int x = x1; x <= x2; x++) {
          // Use dumpRect rather than setPixel because we do not want to apply the transformation
          dumpRect(x, y, 1, 1, iLayer);
          if (p > 0) {
            y += incy;
            p += dy - dx;
          } else
            p += dy;
        }
      } else {
        if (y1 > y2) {
          // Ensure that y1 < y2
          x1 ^= x2; x2 ^= x1; x1 ^= x2;
          y1 ^= y2; y2 ^= y1; y1 ^= y2;
        }
        int incx = x1 < x2? 1 : -1;
        int p = dx - dy / 2;
        int x = x1;
        for (int y = y1; y <= y2; y++) {
          // Use dumpRect rather than setPixel because we do not want to apply the transformation
          dumpRect(x, y, 1, 1, iLayer);
          if (p > 0) {
            x += incx;
            p += dx - dy;
          } else
            p += dx;
        }
      }
    }
  }

  public void drawPolyline(int[] xPoints, int[] yPoints, int nPoints, int iLayer) {
    for (int i = 1; i < nPoints; i++)
      drawLine(xPoints[i-1], yPoints[i-1], xPoints[i], yPoints[i], iLayer);
  }

  public void drawPolygon(int[] xPoints, int[] yPoints, int nPoints, int iLayer) {
    // Do not apply the transformation here as drawLine will apply it
    for (int i = 1; i < nPoints; i++)
      drawLine(xPoints[i-1], yPoints[i-1], xPoints[i], yPoints[i], iLayer);
    drawLine(xPoints[nPoints-1], yPoints[nPoints-1], xPoints[0], yPoints[0], iLayer);
  }

  /**
   * Merges the given canvas into this one at the given position
   * @param another the other canvas to merge
   */
  public void mergeCanvas(BitImageCanvas another) {
    // Both canvases should have the same number of layers
    assert this.bits.length == another.bits.length;
    // Transform the origin of the given canvas to the image space of this canvas
    int xOrigin = (int) this.transformX(another.getInputMBR().minCoord[0]);
    int yOrigin = (int) this.transformY(another.getInputMBR().minCoord[1]);
    // Special case that is efficient when merging two identical canvases
    if (xOrigin == 0 && yOrigin == 0 && another.width == this.width && another.height == this.height) {
      for (int $iLayer = 0; $iLayer < bits.length; $iLayer++)
        this.bits[$iLayer].inplaceOr(another.bits[$iLayer]);
      return;
    }
    // Merge row by row
    for (int $iLayer = 0; $iLayer < bits.length; $iLayer++) {
      long destinationOffset = yOrigin * width + xOrigin;
      long sourceOffset = 0;
      for (int $y = 0; $y < another.height; $y++) {
        this.bits[$iLayer].inplaceOr(destinationOffset, another.bits[$iLayer], sourceOffset, another.width);
        destinationOffset += width;
        sourceOffset += another.width;
      }
    }
  }

  /**
   * Draws this bit canvas to a given image with the same dimensions by replacing each set bit to the given color
   * @param img
   * @param colors the colors to use for all the layers
   */
  public void plotToImage(BufferedImage img, Color[] colors) {
    assert this.bits.length == colors.length;
    assert this.width == img.getWidth();
    assert this.height == img.getHeight();
    for (int $iLayer = 0; $iLayer < bits.length; $iLayer++) {
      for (int $i = 0; $i < bits[$iLayer].size(); $i++) {
        if (bits[$iLayer].get($i)) {
          int y = $i / width;
          int x = $i % width;
          img.setRGB(x, y, colors[$iLayer].getRGB());
        }
      }
    }
  }
}
