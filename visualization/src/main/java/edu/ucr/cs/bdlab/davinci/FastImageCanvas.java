/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.davinci;

import edu.ucr.cs.bdlab.geolite.Envelope;

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * A canvas that contains an in-memory image backed by a regular Java-provided BufferedImage
 * @author Ahmed Eldawy
 *
 */
public class FastImageCanvas extends Canvas {

  /**
   * The underlying image
   */
  protected BufferedImage image;

  /**
   * The graphics associated with the image if the image is in draw mode.
   * If the image is not in draw mode, graphics will be null.
   */
  protected Graphics2D graphics;

  /**Default constructor is necessary to be able to deserialize it*/
  public FastImageCanvas() {
    System.setProperty("java.awt.headless", "true");
  }

  /**
   * Creates a canvas of the given size for a given (portion of) input
   * data.
   * @param inputMBR - the MBR of the input area to plot
   * @param width - width the of the image to generate in pixels
   * @param height - height of the image to generate in pixels
   */
  public FastImageCanvas(Envelope inputMBR, int width, int height) {
    super(inputMBR, width, height);
    System.setProperty("java.awt.headless", "true");
    image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
  }

  @Override
  public void write(DataOutput out) throws IOException {
    super.write(out);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ImageIO.write(getImage(), "png", baos);
    baos.close();
    byte[] bytes = baos.toByteArray();
    out.writeInt(bytes.length);
    out.write(bytes);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    super.readFields(in);
    int length = in.readInt();
    byte[] bytes = new byte[length];
    in.readFully(bytes);
    this.image = ImageIO.read(new ByteArrayInputStream(bytes));
  }

  public void mergeWith(FastImageCanvas another) {
    int x = (int) this.transformX(another.getInputMBR().minCoord[0]);
    int y = (int) this.transformY(another.getInputMBR().minCoord[1]);
    getOrCreateGrahics().drawImage(another.getImage(), x, y, null);
  }

  public BufferedImage getImage() {
    if (graphics != null) {
      graphics.dispose();
      graphics = null;
    }
    return image;
  }
  
  protected Graphics2D getOrCreateGrahics() {
    if (graphics == null) {
      // Create graphics for the first time
      try {
        graphics = image.createGraphics();
      } catch (Throwable e) {
        graphics = new SimpleGraphics(image);
      }
    }
    return graphics;
  }
}
