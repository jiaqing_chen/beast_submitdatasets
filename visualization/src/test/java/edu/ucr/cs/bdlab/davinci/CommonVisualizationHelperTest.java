package edu.ucr.cs.bdlab.davinci;

import edu.ucr.cs.bdlab.geolite.EmptyGeometry;
import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.io.MercatorProjector;
import junit.framework.TestCase;

import java.awt.Color;

public class CommonVisualizationHelperTest extends TestCase {

  public void testParseNamedColors() {
    assertEquals(Color.BLACK, CommonVisualizationHelper.getColor("Black"));
    assertEquals(Color.BLACK, CommonVisualizationHelper.getColor("black"));
  }

  public void testMercatorProjection() {
    Point p = new Point(0.0, 0.0);
    MercatorProjector.worldToMercator(p);
    assertEquals(128.0, p.coords[0], 1E-5);
    assertEquals(128.0, p.coords[1], 1E-5);
    p.set(-180.0, 85.051128);
    MercatorProjector.worldToMercator(p);
    assertEquals(0.0, p.coords[0], 1E-5);
    assertEquals(0.0, p.coords[1], 1E-5);

    Envelope e = new Envelope(2, -180.0, -85.051129, 180.0, 85.051129);
    MercatorProjector.worldToMercator(e);
    assertEquals(0.0, e.minCoord[0], 1E-5);
    assertEquals(0.0, e.minCoord[1], 1E-5);
    assertEquals(256.0, e.maxCoord[0], 1E-5);
    assertEquals(256.0, e.maxCoord[1], 1E-5);

    // Test should not throw error with empty geometries
    MercatorProjector.worldToMercator(EmptyGeometry.instance);
  }

  public void testReverseMercatorProjection() {
    Point p = new Point(128.0, 128.0);
    MercatorProjector.mercatorToWorld(p);
    assertEquals(0.0, p.coords[0], 1E-5);
    assertEquals(0.0, p.coords[1], 1E-5);

    p.set(0.0, 0.0);
    MercatorProjector.mercatorToWorld(p);
    assertEquals(-180.0, p.coords[0], 1E-5);
    assertEquals(85.051129, p.coords[1], 1E-5);

    Envelope e = new Envelope(2, 0.0, 0.0, 256.0, 256.0);
    MercatorProjector.mercatorToWorld(e);
    assertEquals(-180.0, e.minCoord[0], 1E-5);
    assertEquals(-85.051129, e.minCoord[1], 1E-5);
    assertEquals(180.0, e.maxCoord[0], 1E-5);
    assertEquals(85.051129, e.maxCoord[1], 1E-5);

    // Test should not throw error with empty geometries
    MercatorProjector.mercatorToWorld(EmptyGeometry.instance);
  }
}