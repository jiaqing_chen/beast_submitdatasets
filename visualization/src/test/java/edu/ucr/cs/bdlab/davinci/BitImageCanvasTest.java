/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.davinci;

import edu.ucr.cs.bdlab.geolite.Envelope;
import junit.framework.TestCase;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class BitImageCanvasTest extends TestCase {
  public void testFillRectangle() {
    Color[] colors = {Color.RED, Color.GREEN};
    Envelope e = new Envelope(2, 0, 0, 10, 10);
    BitImageCanvas canvas = new BitImageCanvas(e, 100, 100, colors.length);
    canvas.fillRect(0, 0, 20, 30, 0);
    canvas.fillRect(50, 50, 10, 5, 1);

    BufferedImage image = new BufferedImage(canvas.width, canvas.height, BufferedImage.TYPE_INT_ARGB);
    canvas.plotToImage(image, colors);
    assertEquals(Color.RED.getRGB(), image.getRGB(0, 0));
    assertEquals(Color.RED.getRGB(), image.getRGB(10, 25));
    assertEquals(Color.GREEN.getRGB(), image.getRGB(50, 50));
    assertEquals(Color.GREEN.getRGB(), image.getRGB(55, 52));
    assertEquals(0, new Color(image.getRGB(50, 20), true).getAlpha());
  }

  public void testSimplePlotWithTransparentColor() {
    Color[] colors = {Color.RED, Color.GREEN, new Color(0, true)};
    Envelope e = new Envelope(2, 0, 0, 10, 10);
    BitImageCanvas canvas = new BitImageCanvas(e, 100, 100, colors.length);
    canvas.fillRect(0, 0, 20, 30, 0);
    canvas.fillRect(50, 50, 10, 5, 1);

    BufferedImage image = new BufferedImage(canvas.width, canvas.height, BufferedImage.TYPE_INT_ARGB);
    canvas.plotToImage(image, colors);
    assertEquals(Color.RED.getRGB(), image.getRGB(0, 0));
    assertEquals(Color.RED.getRGB(), image.getRGB(10, 25));
    assertEquals(Color.GREEN.getRGB(), image.getRGB(50, 50));
    assertEquals(Color.GREEN.getRGB(), image.getRGB(55, 52));
    assertEquals(0, new Color(image.getRGB(50, 20), true).getAlpha());
  }

  public void testSerialization() throws IOException {
    Color[] colors = {Color.RED, Color.GREEN};
    Envelope e = new Envelope(2, 0, 0, 10, 10);
    BitImageCanvas canvas = new BitImageCanvas(e, 100, 100, colors.length);
    canvas.fillRect(0, 0, 20, 30, 0);
    canvas.fillRect(50, 50, 10, 5, 1);

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    canvas.write(dos);
    dos.close();

    DataInputStream din = new DataInputStream(new ByteArrayInputStream(baos.toByteArray()));
    canvas = new BitImageCanvas();
    canvas.readFields(din);

    BufferedImage image = new BufferedImage(canvas.width, canvas.height, BufferedImage.TYPE_INT_ARGB);
    canvas.plotToImage(image, colors);
    assertEquals(Color.RED.getRGB(), image.getRGB(0, 0));
    assertEquals(Color.RED.getRGB(), image.getRGB(10, 25));
    assertEquals(Color.GREEN.getRGB(), image.getRGB(50, 50));
    assertEquals(Color.GREEN.getRGB(), image.getRGB(55, 52));
    assertEquals(0, new Color(image.getRGB(50, 20), true).getAlpha());
  }

  public void testSerializationWithBlackColor() throws IOException {
    Color[] colors = {Color.BLACK};
    Envelope e = new Envelope(2, 0, 0, 10, 10);
    BitImageCanvas canvas = new BitImageCanvas(e, 100, 100, colors.length);
    canvas.fillRect(0, 0, 20, 30, 0);
    canvas.fillRect(50, 50, 10, 5, 0);

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    canvas.write(dos);
    dos.close();

    DataInputStream din = new DataInputStream(new ByteArrayInputStream(baos.toByteArray()));
    canvas = new BitImageCanvas();
    canvas.readFields(din);

    BufferedImage image = new BufferedImage(canvas.width, canvas.height, BufferedImage.TYPE_INT_ARGB);
    canvas.plotToImage(image, colors);
    assertEquals(255, new Color(image.getRGB( 0,  0), true).getAlpha());
    assertEquals(255, new Color(image.getRGB(10, 25), true).getAlpha());
    assertEquals(255, new Color(image.getRGB(50, 50), true).getAlpha());
    assertEquals(255, new Color(image.getRGB(55, 52), true).getAlpha());
    assertEquals(0, new Color(image.getRGB(50, 20), true).getAlpha());
  }

  public void testMerging() {
    Color[] colors = {Color.RED, Color.GREEN};
    Envelope e = new Envelope(2, 0, 0, 10, 10);
    BitImageCanvas canvas1 = new BitImageCanvas(e, 100, 100, colors.length);
    canvas1.fillRect(0, 0, 20, 30, 0);

    BitImageCanvas canvas2 = new BitImageCanvas(e, 100, 100, colors.length);
    canvas2.fillRect(50, 50, 10, 5, 1);

    BitImageCanvas canvas3 = new BitImageCanvas(e, 100, 100, colors.length);
    canvas3.mergeCanvas(canvas1);
    canvas3.mergeCanvas(canvas2);
    BufferedImage image = new BufferedImage(canvas3.width, canvas3.height, BufferedImage.TYPE_INT_ARGB);
    canvas3.plotToImage(image, colors);
    assertEquals(colors[0].getRGB(), image.getRGB(0, 0));
    assertEquals(colors[0].getRGB(), image.getRGB(10, 25));
    assertEquals(colors[1].getRGB(), image.getRGB(50, 50));
    assertEquals(colors[1].getRGB(), image.getRGB(55, 52));
    assertEquals(0, new Color(image.getRGB(50, 20), true).getAlpha());
  }

  public void testMergingWithBlackColor() throws IOException {
    Color[] colors = {Color.BLACK};
    Envelope e = new Envelope(2, 0.0, 0.0, 1.0, 1.0);
    BitImageCanvas canvas1 = new BitImageCanvas(e, 100, 100, colors.length);
    canvas1.fillRect(0, 0, 20, 30, 0);

    BitImageCanvas canvas2 = new BitImageCanvas(e, 100, 100, colors.length);
    canvas2.fillRect(50, 50, 10, 5, 0);

    canvas1.mergeCanvas(canvas2);
    BufferedImage image = new BufferedImage(canvas1.width, canvas1.height, BufferedImage.TYPE_INT_ARGB);
    canvas1.plotToImage(image, colors);

    assertEquals(255, new Color(image.getRGB(0, 0), true).getAlpha());
    assertEquals(255, new Color(image.getRGB(10, 25), true).getAlpha());
    assertEquals(255, new Color(image.getRGB(50, 50), true).getAlpha());
    assertEquals(255, new Color(image.getRGB(55, 52), true).getAlpha());
    assertEquals(0, new Color(image.getRGB(50, 20), true).getAlpha());
  }

}