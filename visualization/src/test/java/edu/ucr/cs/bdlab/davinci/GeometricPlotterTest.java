/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.davinci;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.Feature;
import edu.ucr.cs.bdlab.geolite.twod.LineString2D;
import edu.ucr.cs.bdlab.geolite.twod.MultiPolygon2D;
import edu.ucr.cs.bdlab.util.UserOptions;
import edu.ucr.cs.bdlab.wktparser.ParseException;
import edu.ucr.cs.bdlab.wktparser.WKTParser;
import junit.framework.TestCase;

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class GeometricPlotterTest extends TestCase {

  public void testPlotMultiPolygon() throws IOException {
    GeometricPlotter plotter = new GeometricPlotter();
    Envelope mbr = new Envelope(2, 0.0, 0.0, 1.0, 1.0);
    UserOptions opts = new UserOptions();
    plotter.setup(opts);
    Canvas canvas = plotter.createCanvas(100, 100, mbr);
    MultiPolygon2D poly = new MultiPolygon2D();
    poly.addPoint(0.0, 0.0);
    poly.addPoint(0.0, 0.5);
    poly.closeLastRing(false);
    poly.addPoint(0.5, 0.5);
    poly.addPoint(0.5, 0.0);
    poly.closeLastRing(false);
    plotter.plot(canvas, new Feature(poly));

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    plotter.writeImage(canvas, dos, false);
    dos.close();
    BufferedImage img = ImageIO.read(new ByteArrayInputStream(baos.toByteArray()));
    // There should not be any set pixel in the column half way between 0 and 50
    int x = 25;
    for (int $y = 0; $y < 100; $y++) {
      int rgb = img.getRGB(x, $y);
      assertEquals(String.format("A pixel is incorrectly set at (%d, %d)", x, $y), 0, rgb);
    }
    // There should be exactly two pixels set in the first row
    int y = 0;
    int count = 0;
    for (int $x = 0; $x < 100; $x++) {
      int rgb = img.getRGB($x, y);
      if (rgb != 0)
        count++;
    }
    assertEquals(2, count);
  }

  public void testLinestring() throws IOException {
    GeometricPlotter plotter = new GeometricPlotter();
    Envelope mbr = new Envelope(2, 0.0, 0.0, 1.0, 1.0);
    UserOptions opts = new UserOptions();
    opts.setBoolean(GeometricPlotter.Compact, true);
    plotter.setup(opts);
    Canvas canvas = plotter.createCanvas(100, 100, mbr);
    LineString2D linestring = new LineString2D();
    linestring.addPoint(0.1, 0.1);
    linestring.addPoint(0.2, 0.98);
    linestring.addPoint(0.99, 0.99);
    plotter.plot(canvas, new Feature(linestring));

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    plotter.writeImage(canvas, dos, false);
    dos.close();
    BufferedImage img = ImageIO.read(new ByteArrayInputStream(baos.toByteArray()));
    // Count non-transparent pixels along the middle lines
    int count = 0;
    for (int $i = 0; $i < 100; $i++) {
      if (new Color(img.getRGB(50, $i), true).getAlpha() != 0)
        count++;
      if (new Color(img.getRGB($i, 50), true).getAlpha() != 0)
        count++;
    }
    assertEquals(2, count);
  }

  public void testPlotMultiPolygon2() throws IOException {
    GeometricPlotter plotter = new GeometricPlotter();
    Envelope mbr = new Envelope(2, 0.0, 0.0, 1.0, 1.0);
    UserOptions opts = new UserOptions();
    plotter.setup(opts);
    Canvas canvas = plotter.createCanvas(100, 100, mbr);
    MultiPolygon2D poly = new MultiPolygon2D();
    poly.addPoint(0.01, 0.01);
    poly.addPoint(0.98, 0.02);
    poly.addPoint(0.99, 0.99);
    poly.addPoint(0.02, 0.98);
    poly.closeLastRing(false);
    plotter.plot(canvas, new Feature(poly));

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    plotter.writeImage(canvas, dos, false);
    dos.close();
    BufferedImage img = ImageIO.read(new ByteArrayInputStream(baos.toByteArray()));

    // Count non-transparent pixels along the middle lines
    int count = 0;
    for (int $i = 0; $i < 100; $i++) {
      if (new Color(img.getRGB(50, $i), true).getAlpha() != 0)
        count++;
      if (new Color(img.getRGB($i, 50), true).getAlpha() != 0)
        count++;
    }
    assertEquals(4, count);
  }

  public void testLinestring2() throws IOException, ParseException {
    // Try both the fast plotter and the memory saving one.
    for (int i = 1; i < 2; i++) {
      GeometricPlotter plotter = new GeometricPlotter();
      LineString2D linestring = new LineString2D();
      new WKTParser().parse("LINESTRING(61 897,65 891)", linestring);
      Envelope mbr = new Envelope(2);
      linestring.envelope(mbr);
      UserOptions opts = new UserOptions();
      opts.setBoolean(GeometricPlotter.Compact, i == 0);
      int imageSize = 100;
      plotter.setup(opts);
      Canvas canvas = plotter.createCanvas(imageSize, imageSize, mbr);
      plotter.plot(canvas, new Feature(linestring));

      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      DataOutputStream dos = new DataOutputStream(baos);
      plotter.writeImage(canvas, dos, false);
      dos.close();
      BufferedImage img = ImageIO.read(new ByteArrayInputStream(baos.toByteArray()));
      // Count non-transparent pixels along the middle lines
      int count = 0;
      for (int $i = 0; $i < imageSize; $i++) {
        if (new Color(img.getRGB($i, imageSize/2), true).getAlpha() != 0)
          count++;
      }
      assertEquals(1, count);
    }
  }

  public void testLinestringThatGoesOffCanvas() throws IOException, ParseException {
    GeometricPlotter plotter = new GeometricPlotter();
    Envelope mbr = new Envelope(2, 0.0, 0.0, 1.0, 1.0);
    UserOptions opts = new UserOptions();
    opts.setBoolean(GeometricPlotter.Compact, true);
    plotter.setup(opts);
    int imageSize = 100;
    Canvas canvas = plotter.createCanvas(imageSize, imageSize, mbr);
    LineString2D linestring = new LineString2D();
    linestring.addPoint(0.1, 0.1);
    linestring.addPoint(1.2, 0.1);
    linestring.addPoint(1.2, 1.2);
    linestring.addPoint(0.1, 1.2);
    linestring.addPoint(0.1, 0.1);
    plotter.plot(canvas, new Feature(linestring));

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    plotter.writeImage(canvas, dos, false);
    dos.close();
    BufferedImage img = ImageIO.read(new ByteArrayInputStream(baos.toByteArray()));

    // Count number of pixels
    int count = 0;
    for (int $x = 0; $x < imageSize; $x++) {
      for (int $y = 0; $y < imageSize; $y++) {
        int alpha = new Color(img.getRGB($x, $y), true).getAlpha();
        if (alpha != 0)
          count++;
      }
    }
    assertEquals(179, count);
  }
  public void testMerge() throws IOException {
    GeometricPlotter plotter = new GeometricPlotter();
    Envelope mbr = new Envelope(2, 0.0, 0.0, 1.0, 1.0);
    UserOptions opts = new UserOptions();
    plotter.setup(opts);
    Canvas canvas1 = plotter.createCanvas(100, 100, mbr);
    MultiPolygon2D poly = new MultiPolygon2D();
    poly.addPoint(0.0, 0.0);
    poly.addPoint(0.0, 0.5);
    poly.closeLastRing(false);
    plotter.plot(canvas1, new Feature(poly));

    Canvas canvas2 = plotter.createCanvas(100, 100, mbr);
    poly = new MultiPolygon2D();
    poly.addPoint(0.5, 0.5);
    poly.addPoint(0.5, 0.0);
    poly.closeLastRing(false);
    plotter.plot(canvas2, new Feature(poly));

    plotter.merge(canvas1, canvas2);

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    plotter.writeImage(canvas1, dos, false);
    dos.close();
    BufferedImage img = ImageIO.read(new ByteArrayInputStream(baos.toByteArray()));
    // There should be exactly two pixels set in the first row
    int y = 0;
    int count = 0;
    for (int $x = 0; $x < 100; $x++) {
      int rgb = img.getRGB($x, y);
      if (rgb != 0)
        count++;
    }
    assertEquals(2, count);
  }

  public void testPlotMultiPolygonWithManyPoints() throws IOException {
    GeometricPlotter plotter = new GeometricPlotter();
    Envelope mbr = new Envelope(2, 0.0, 0.0, 1000.0, 1000.0);
    UserOptions opts = new UserOptions();
    plotter.setup(opts);
    int imageSize = 1000;
    Canvas canvas = plotter.createCanvas(imageSize, imageSize, mbr);
    MultiPolygon2D poly = new MultiPolygon2D();
    for (int $i = 0; $i < 750; $i++)
      poly.addPoint($i, 0.0);
    poly.addPoint(749.0, 10);
    poly.addPoint(0.0, 10);
    poly.closeLastRing(false);
    // Add second polygon
    for (int $i = 0; $i < 7500; $i++)
      poly.addPoint($i/10.0, 50.0);
    poly.addPoint(749.0, 60);
    poly.addPoint(0.0, 60);
    poly.closeLastRing(false);

    plotter.plot(canvas, new Feature(poly));

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    plotter.writeImage(canvas, dos, false);
    dos.close();
    BufferedImage img = ImageIO.read(new ByteArrayInputStream(baos.toByteArray()));
    // Count number of black pixels
    int count = 0;
    for (int $x = 0; $x < imageSize; $x++) {
      for (int $y = 0; $y < imageSize; $y++) {
        int alpha = new Color(img.getRGB($x, $y), true).getAlpha();
        if (alpha != 0)
          count++;
      }
    }
    assertEquals((750 + 9) * 2 * 2, count);
  }
}