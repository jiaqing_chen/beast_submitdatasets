/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.davinci;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.Feature;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.wktparser.ParseException;
import edu.ucr.cs.bdlab.wktparser.WKTParser;
import junit.framework.TestCase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FlattenFeaturesTest extends TestCase {

  public void testWithPolygon() throws IOException, ParseException {
    List<IFeature> features = new ArrayList<>();
    WKTParser wktParser = new WKTParser();
    features.add(new Feature(wktParser.parse("POLYGON((0 0, 1 0, 1 1, 0 0), (3 3, 4 3, 4 4, 3 3))", null)));

    FlattenFeatures ff = new FlattenFeatures(features.iterator());
    int count = 0;
    for (IFeature f : ff) {
      count++;
    }
    assertEquals(2, count);
  }

  public void testWithMultiPolygon() throws IOException, ParseException {
    List<IFeature> features = new ArrayList<>();
    WKTParser wktParser = new WKTParser();
    features.add(new Feature(wktParser.parse("MULTIPOLYGON(((0 0, 1 0, 1 1, 0 0), (3 3, 4 3, 4 4, 3 3)), " +
        "((0 0, 1 0, 1 1, 0 0), (3 3, 4 3, 4 4, 3 3)))", null)));

    FlattenFeatures ff = new FlattenFeatures(features.iterator());
    int count = 0;
    for (IFeature f : ff) {
      count++;
    }
    assertEquals(4, count);
  }

  public void testWithMultiLineString() throws IOException, ParseException {
    List<IFeature> features = new ArrayList<>();
    WKTParser wktParser = new WKTParser();
    features.add(new Feature(wktParser.parse("MULTILINESTRING((0 0, 1 0, 1 1, 0 0), (3 3, 4 3, 4 4, 3 3), (3 3, 4 3, 4 4, 3 3))", null)));

    FlattenFeatures ff = new FlattenFeatures(features.iterator());
    int count = 0;
    for (IFeature f : ff) {
      count++;
    }
    assertEquals(3, count);
  }

  public void testPrimitives() throws IOException, ParseException {
    List<IFeature> features = new ArrayList<>();
    WKTParser wktParser = new WKTParser();
    features.add(new Feature(wktParser.parse("LINESTRING(0 0, 1 0, 1 1, 0 0)", null)));
    features.add(new Feature(wktParser.parse("POINT(0 0)", null)));
    features.add(new Feature(new Envelope(2, 0, 0, 1, 1)));

    FlattenFeatures ff = new FlattenFeatures(features.iterator());
    int count = 0;
    for (IFeature f : ff) {
      count++;
    }
    assertEquals(3, count);
  }

  public void testGeometryCollectionWithPrimitives() throws IOException, ParseException {
    List<IFeature> features = new ArrayList<>();
    WKTParser wktParser = new WKTParser();
    features.add(new Feature(wktParser.parse("GEOMETRYCOLLECTION(POINT(0 0), LINESTRING(1 1, 3 4))", null)));

    FlattenFeatures ff = new FlattenFeatures(features.iterator());
    int count = 0;
    for (IFeature f : ff) {
      count++;
    }
    assertEquals(2, count);
  }
}