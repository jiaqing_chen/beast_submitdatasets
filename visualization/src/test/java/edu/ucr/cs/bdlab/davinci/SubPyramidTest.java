package edu.ucr.cs.bdlab.davinci;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.Point;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class SubPyramidTest extends TestCase {

  /**
   * @return the suite of tests being tested
   */
  public static Test suite() {
    return new TestSuite(SubPyramidTest.class);
  }

  public void testOverlappingCells() {
    Envelope mbr = new Envelope(2, 0.0,0.0,10.0,10.0);
    SubPyramid subPyramid = new SubPyramid(mbr, 0, 1, 0,0,2,2);

    java.awt.Rectangle overlaps = new java.awt.Rectangle();
    subPyramid.getOverlappingTiles(new Envelope(2, 5.0,5.0,10.0,10.0), overlaps);
    assertEquals(1, overlaps.x);
    assertEquals(1, overlaps.y);
    assertEquals(1, overlaps.width);
    assertEquals(1, overlaps.height);
    subPyramid.getOverlappingTiles(new Envelope(2, 4.9,4.9,5.1,5.1), overlaps);
    assertEquals(0, overlaps.x);
    assertEquals(0, overlaps.y);
    assertEquals(2, overlaps.width);
    assertEquals(2, overlaps.height);
  }

  public void testOverlappingCellsWithPoints() {
    Envelope mbr = new Envelope(2, 0.0,0.0,10.0,10.0);
    SubPyramid subPyramid = new SubPyramid(mbr, 0, 1, 0,0,2,2);

    java.awt.Rectangle overlaps = new java.awt.Rectangle();
    Envelope e = new Envelope(2);
    new Point(0.0, 0.0).envelope(e);
    subPyramid.getOverlappingTiles(e, overlaps);
    assertEquals(0, overlaps.x);
    assertEquals(0, overlaps.y);
    assertEquals(1, overlaps.width);
    assertEquals(1, overlaps.height);
  }

  public void testOverlappingCellsAligned() {
    Envelope mbr = new Envelope(2, 0.0,0.0,8.0,8.0);
    SubPyramid subPyramid = new SubPyramid(mbr, 0, 1, 0,0,2,2);

    java.awt.Rectangle overlaps = new java.awt.Rectangle();
    subPyramid.getOverlappingTiles(new Envelope(2, 0.0, 0.0, 4.0, 4.0), overlaps);
    assertEquals(0, overlaps.x);
    assertEquals(0, overlaps.y);
    assertEquals(1, overlaps.width);
    assertEquals(1, overlaps.height);

    // Test special case of a point at the very end
    subPyramid.getOverlappingTiles(new Envelope(2, 8.0, 8.0, 8.0, 8.0), overlaps);
    assertEquals(1, overlaps.x);
    assertEquals(1, overlaps.y);
    assertEquals(1, overlaps.width);
    assertEquals(1, overlaps.height);
  }

  public void testOverlappingCellsShouldClip() {
    Envelope mbr = new Envelope(2, 0.0,0.0,1024.0,1024.0);
    SubPyramid subPyramid = new SubPyramid(mbr, 4, 6, 16,8,20,12);
    java.awt.Rectangle overlaps = new java.awt.Rectangle();

    // Case 1: No clipping required
    subPyramid.getOverlappingTiles(new Envelope(2, 256.0,128.0,320.0,192.0), overlaps);
    assertEquals(16, overlaps.x);
    assertEquals(8, overlaps.y);
    assertEquals(4, overlaps.width);
    assertEquals(4, overlaps.height);

    // Case 2: Clip part on the top left
    subPyramid.getOverlappingTiles(new Envelope(2, 128.0,64.0,320.0,192.0), overlaps);
    assertEquals(16, overlaps.x);
    assertEquals(8, overlaps.y);
    assertEquals(4, overlaps.width);
    assertEquals(4, overlaps.height);

    // Case 3: Clip part on the bottom right
    subPyramid.getOverlappingTiles(new Envelope(2, 256.0,128.0,512.0,256.0), overlaps);
    assertEquals(16, overlaps.x);
    assertEquals(8, overlaps.y);
    assertEquals(4, overlaps.width);
    assertEquals(4, overlaps.height);

    // Case 4: If completely outside, return an empty rectangle with non-positive width or height
    subPyramid.getOverlappingTiles(new Envelope(2, 128.0,64.0,196.0,96.0), overlaps);
    assertTrue(overlaps.width <= 0 || overlaps.height <= 0);
  }

  public void testSetFromTileID() {
    Envelope mbr = new Envelope(2, 0.0, 0.0, 1024.0, 1024.0);
    SubPyramid subPyramid = new SubPyramid();
    subPyramid.set(mbr, 3, 1, 3);

    assertEquals(3, subPyramid.minimumLevel);
    assertEquals(3, subPyramid.maximumLevel);
    assertEquals(1, subPyramid.c1);
    assertEquals(2, subPyramid.c2);
    assertEquals(3, subPyramid.r1);
    assertEquals(4, subPyramid.r2);
  }

  public void testGetNumberOfTiles() {
    SubPyramid p = new SubPyramid(new Envelope(2, 0.0, 0.0, 1.0, 1.0), 0, 0, 0, 0, 1, 1);
    assertEquals(1, p.getTotalNumberOfTiles());

    p = new SubPyramid(new Envelope(2, 0.0, 0.0, 1.0, 1.0), 0, 2, 0, 0, 4, 4);
    assertEquals(21, p.getTotalNumberOfTiles());

    p = new SubPyramid(new Envelope(2, 0.0, 0.0, 1.0, 1.0), 2, 3, 0, 0, 8, 8);
    assertEquals(80, p.getTotalNumberOfTiles());

    p = new SubPyramid(new Envelope(2, 0.0, 0.0, 1.0, 1.0), 1, 2, 2, 2, 4, 4);
    assertEquals(5, p.getTotalNumberOfTiles());
  }
}
