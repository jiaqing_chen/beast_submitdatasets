package edu.ucr.cs.bdlab.davinci;

import edu.ucr.cs.bdlab.test.SparkTest;
import junit.framework.TestCase;

public class PlotterTest extends SparkTest {

  public void testGetExtension() {
    String expectedExtension = ".png";
    String actualExtension = Plotter.getImageExtension("gplot");
    assertEquals(expectedExtension, actualExtension);
  }

}