/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.util;

import edu.ucr.cs.bdlab.test.SparkTest;

import java.util.List;
import java.util.Map;

public class OperationUtilTest extends SparkTest {
  public void testParseArity() {
    assertArrayEquals(new int[] {0,1}, OperationUtil.parseArity("?"));
    assertArrayEquals(new int[] {0,Integer.MAX_VALUE}, OperationUtil.parseArity("*"));
    assertArrayEquals(new int[] {1,Integer.MAX_VALUE}, OperationUtil.parseArity("+"));
    assertArrayEquals(new int[] {4,4}, OperationUtil.parseArity("4"));
    assertArrayEquals(new int[] {5,7}, OperationUtil.parseArity("5-7"));
  }

  public void testReadXMLConfigFile() {
    Map<String, List<String>> conf = OperationUtil.readConfigurationXML("test-beast.xml");
    assertEquals(4, conf.get("Operations").size());
    assertEquals(1, conf.get("Indexers").size());
    assertEquals("Op1", conf.get("Operations").get(0));
  }
}