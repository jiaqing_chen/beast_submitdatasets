/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.util;

import org.apache.hadoop.io.Writable;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * An interface for objects that are both {@link Writable} (for Hadoop) and {@link Externalizable} for Spark and other
 * object serialization. It automatically uses the more restrictive {@link Writable} interface to implement the methods
 * for the {@link Externalizable} interface so you do not have to implement both.
 */
public interface WritableExternalizable extends Writable, Externalizable {

  @Override
  default void writeExternal(ObjectOutput out) throws IOException {
    this.write(out);
  }

  @Override
  default void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
    this.readFields(in);
  }
}
