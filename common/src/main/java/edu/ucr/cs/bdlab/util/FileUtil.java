/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;

public class FileUtil {
  private static final Log LOG = LogFactory.getLog(FileUtil.class);

  /**
   * Replaces the existing extension with the given extension. If the file does not have an extension, the given
   * extension is appended.
   * @param filename An existing filename with or without an extension.
   * @param newExtension The extension to use in the given file without the leading period (.)
   * @return
   */
  public static String replaceExtension(String filename, String newExtension) {
    int iLastDot = filename.lastIndexOf('.');
    if (iLastDot == -1)
      return filename + '.' + newExtension;
    return filename.substring(0, iLastDot + 1) + newExtension;
  }

  /**
   * Flatten the given directory by removing one lever from the directory
   * hierarchy. It finds all subdirectories under the given directory and merges
   * them together while putting their contents into the given directory.
   * It is assumed that the given path does not have any files, only subdirs.
   * @param path
   */
  public static void flattenDirectory(FileSystem fs, Path path) throws IOException {
    // Decide which directory to use as a destination directory based on the
    // number of files in each one
    FileStatus[] subdirs = fs.listStatus(path);
    if (subdirs.length == 0) {
      LOG.warn(String.format("No contents of the directory '%s' to flatten", path.toString()));
      return;
    }
    int maxSize = 0;
    Path destinationPath = null;
    for (FileStatus subdir : subdirs) {
      if (subdir.isDirectory()) {
        int size = fs.listStatus(subdir.getPath()).length;
        System.out.println("subdir="+subdir+"size="+size);
        if (size > maxSize) {
          maxSize = size;
          destinationPath = subdir.getPath();
        }
      }
    }

    // Scan the paths again and move their contents to the destination path
    for (FileStatus subdir : subdirs) {
      if (subdir.isDirectory() && subdir.getPath() != destinationPath) {
        // Scan all the contents of this path and move it to the destination path
        FileStatus[] files = fs.listStatus(subdir.getPath());
        for (FileStatus file : files) {
          fs.rename(file.getPath(), new Path(destinationPath, file.getPath().getName()));
        }
        // Now, since the path is empty, we can safely delete it
        // We delete it with non-recursive option for safety
        fs.delete(subdir.getPath(), true);
        // XXX We delete it with non-recursive option for safety
        // In HDFS, .crc files are not moved and hence we delete them recursively
      }
    }

    // Finally, rename the destination directory to make it similar to its parent
    Path parentPath = path;
    Path renamedParent = new Path(parentPath.getParent(), Math.random()+".tmp");
    fs.rename(parentPath, renamedParent);
    // Destination path has now changed since we renamed its parent
    destinationPath = new Path(renamedParent, destinationPath.getName());
    fs.rename(destinationPath, parentPath);
    fs.delete(renamedParent, true);
  }

  /**
   * Rewrites the given {@code path} to be relative to {@code refPath}
   * @param path
   * @param refPath
   * @return
   */
  public static Path relativize(Path path, Path refPath) {
    String str = path.toString();
    String refStr = refPath.toString();
    // Remove the common prefix
    int prefix = 0;
    int lastSlashInPrefix = 0;
    while (prefix < str.length() && prefix < refStr.length() &&
      str.charAt(prefix) == refStr.charAt(prefix)) {
      if (str.charAt(prefix) == '/')
        lastSlashInPrefix = prefix;
      prefix++;
    }
    prefix = str.charAt(lastSlashInPrefix) == '/'? lastSlashInPrefix + 1 : lastSlashInPrefix;
    // If refPath is empty, it means that the given path is a subdirectory of it
    if (prefix == refStr.length())
      return new Path(str.substring(prefix));
    // Paths have a common parent; remove it!
    if (prefix > 0) {
      str = str.substring(prefix);
      refStr = refStr.substring(prefix);
    }

    // Prepend ".."s to the path as equal to the depth of the refPath
    int defDepth = new Path(refStr).depth();
    path = new Path(str);
    for (int $i = 0; $i < defDepth; $i++) {
      path = new Path("..", path);
    }

    return path;
  }

  /**
   * Tests if the extension of the given file name matches the given extension.
   * It assumed that the extension already contains a dot.
   * @param filename
   * @param extension
   * @return
   */
  public static boolean extensionMatches(String filename, String extension) {
    int lastDot = filename.lastIndexOf('.');
    if (lastDot == -1)
      return false;
    // If the extension matches, return the short name of the input format
    return filename.substring(lastDot).equalsIgnoreCase(extension);
  }
}
