/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Creates an iterator that returns a single object. Useful with Spark functions that expect an iterator to be returned
 * while a single item needs to be returned.
 * @param <T>
 */
public class SingletonIterator<T> implements Iterator<T> {
  private final T object;
  private boolean finished = false;

  public SingletonIterator(T o) {
    this.object = o;
  }

  @Override
  public boolean hasNext() {
    return !finished;
  }

  @Override
  public T next() {
    if (finished)
      throw new NoSuchElementException("Iterator has finished");
    finished = true;
    return object;
  }
}
