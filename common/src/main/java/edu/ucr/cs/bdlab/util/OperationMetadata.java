/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for classes that can run as command line operations. In addition to this metadata, operations must also
 * implement a static function {@code run(UserOptions)} and its full name must be added to the file
 * &lt;resources&gt;/operations.xml
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface OperationMetadata {
  /**The short name used to access this operation from command line*/
  String shortName();

  /**A description of this operation*/
  String description();

  /**How many input files this operation can accept*/
  String inputArity() default "1";

  /**Hoe many output paths this operation can accept*/
  String outputArity() default "1";

  /**Optional list of classes to inherit their parameters*/
  Class<?>[] inheritParams() default {};
}
