/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.util;

public class MathUtil {
  public static int getNumberOfDecimalDigits(int x) {
    if (x == 0)
      return 1;
    int nDigits = 0;
    if (x > 100000) {
      nDigits += 5;
      x /= 100000;
    }
    if (x > 1000) {
      nDigits += 3;
      x /= 1000;
    }
    if (x > 100) {
      nDigits += 2;
      x /= 100;
    }
    if (x > 10) {
      nDigits++;
      x /= 10;
    }
    if (x > 0) {
      nDigits++;
    }
    return nDigits;
  }

  public static int getNumberOfDecimalDigits(long x) {
    if (x == 0)
      return 1;
    int nDigits = 0;
    if (x > 1000000000) {
      nDigits += 9;
      x /= 1000000000;
    }
    if (x > 100000) {
      nDigits += 5;
      x /= 100000;
    }
    if (x > 1000) {
      nDigits += 3;
      x /= 1000;
    }
    if (x > 100) {
      nDigits += 2;
      x /= 100;
    }
    if (x > 10) {
      nDigits++;
      x /= 10;
    }
    if (x > 0) {
      nDigits++;
    }
    return nDigits;
  }

  /**
   * Computes the number of significant bits in the given integer. This also represents the minimum number of bits
   * needed to represent this number.
   * Special case, if {@code x} is 0, the return value is 1.
   * @param x
   * @return
   */
  public static int numberOfSignificantBits(int x) {
    if (x == 0)
      return 1;
    int numBits = 0;
    if ((x & 0xffff0000) != 0) {
      numBits += 16;
      x >>>= 16;
    }
    if ((x & 0xff00) != 0) {
      numBits += 8;
      x >>>= 8;
    }
    if ((x & 0xf0) != 0) {
      numBits += 4;
      x >>>= 4;
    }
    if ((x & 0xc) != 0) {
      numBits += 2;
      x >>>= 2;
    }
    if ((x & 0x2) != 0) {
      numBits += 1;
      x >>>= 1;
    }
    if (x != 0)
      numBits++;
    return numBits;
  }

  /**
   * Retrieves the specified number of bits from the given array of integers.
   * @param data the sequence of all bytes
   * @param position the bit position to start retrieving
   * @param length the number of bits to retrieve
   * @return the retrieved bits stored in an integer
   */
  public static int getBits(byte[] data, int position, int length) {
    int value = 0;
    int start = position / 8;
    int end = (position + length - 1) / 8;
    while (start <= end)
      value = (value << 8) | (data[start++] & 0xff);
    value = value >>> (7 - (position + length - 1) % 8);
    value = value & (0xffffffff >>> (32 - length));
    return value;
  }

  public static int nextPowerOfTwo(int i) {
    return Integer.highestOneBit(i) << 1;
  }

  /**
   * Integer floor of base to the log 2. For the special case when the input is zero, this function returns -1.
   * This function is not defined for negative values.
   * @param i the value to compute
   * @return &lceil; log<sub>2</sub>(i)&rceil;
   */
  public static int log2(int i) {
    return 32 - Integer.numberOfLeadingZeros(i) - 1;
  }
}
