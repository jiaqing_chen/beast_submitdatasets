/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.stsynopses;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.GeometryType;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.IGeometry;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;

/**
 * Holds and computes the minimum bounding rectangle of some shapes along with their numFeatures and total size.
 */
public class Summary extends Envelope {
  /**Total estimated storage size for the features*/
  public long size;

  /**Total number of features in the data file*/
  public long numFeatures;

  /**Total number of points in the geometries. e.g., linestrings and polygons can contribute more than one point*/
  protected long numPoints;

  /**Count the number of non-empty geometries. This is used to calculate the average side length correctly*/
  protected long numNonEmptyGeometries;

  /**The sum of side length along each dimension. Can be used to find the average side length.*/
  protected double[] sumSideLength;

  /**The type of geometry stored in the dataset.*/
  protected GeometryType geometryType = GeometryType.EMPTY;

  public Summary() {
  }

  public Summary(Summary other) {
    this.setCoordinateDimension(other.getCoordinateDimension());
    if (other.getCoordinateDimension() != 0)
      this.set(other.minCoord, other.maxCoord);
    this.size = other.size;
    this.numFeatures = other.numFeatures;
    this.sumSideLength = Arrays.copyOf(other.sumSideLength, other.sumSideLength.length);
    this.numNonEmptyGeometries = other.numNonEmptyGeometries;
    this.numPoints = other.numPoints;
    this.geometryType = other.geometryType;
  }

  public long getRecordCount() {
    return numFeatures;
  }

  public long getDataSize() {
    return size;
  }

  public long getNumPoints() {
    return numPoints;
  }

  public GeometryType getGeometryType() {
    return geometryType;
  }

  @Override
  public void setCoordinateDimension(int numDimensions) {
    super.setCoordinateDimension(numDimensions);
    if (this.sumSideLength == null || this.sumSideLength.length != numDimensions)
      this.sumSideLength = new double[numDimensions];
  }

  @Override
  public int getGeometryStorageSize() {
    return super.getGeometryStorageSize() + 8 * 2 + 8 * getCoordinateDimension();
  }

  /**
   * Expands the summary to enclose another partial summary. This can be used for incremental computation relying on
   * the associative and commutative properties on the attributes in the summary.
   * @param other
   */
  public Summary expandToSummary(Summary other) {
    super.merge(other);
    this.numFeatures += other.numFeatures;
    this.size += other.size;
    this.numPoints += other.numPoints;
    for (int $d = 0; $d < this.getCoordinateDimension(); $d++)
      this.sumSideLength[$d] += other.sumSideLength[$d];
    this.numNonEmptyGeometries += other.numNonEmptyGeometries;
    mergeWithGeometryType(other.geometryType);
    return this;
  }

  /**
   * Expands the summary to enclose the given geometry and update the size with the given size
   * @param geometry
   */
  public void expandToGeometryWithSize(IGeometry geometry, int size) {
    super.merge(geometry);
    this.numFeatures++;
    this.size += size;
    this.mergeWithGeometryType(geometry.getType());
    if (geometry.isEmpty())
      return;
    this.numNonEmptyGeometries++;
    this.numPoints += geometry.getNumPoints();
    Envelope e = new Envelope(this.getCoordinateDimension());
    geometry.envelope(e);
    for (int $d = 0; $d < getCoordinateDimension(); $d++)
      sumSideLength[$d] += e.getSideLength($d);
  }

  /**
   * Update the geometry type based on the given one.
   * @param otherGeometryType
   */
  protected void mergeWithGeometryType(GeometryType otherGeometryType) {
    this.geometryType = this.geometryType.coerce(otherGeometryType);
  }

  /**
   * Expands the summary to enclose the given geometry
   * @param geom
   */
  public void expandToGeometry(IGeometry geom) {
    expandToGeometryWithSize(geom, geom.getGeometryStorageSize());
  }

  /**
   * Expands the summary to enclose the given feature. In addition to what {@link #expandToGeometry(IGeometry)} does,
   * it increase the total size to account for the non-geometric attributes in this feature.
   * @param f
   */
  public void expandToFeature(IFeature f) {
    this.expandToGeometryWithSize(f.getGeometry(), f.getStorageSize());
  }

  @Override
  public void write(DataOutput out) throws IOException {
    super.write(out);
    out.writeLong(numFeatures);
    out.writeLong(size);
    out.writeLong(numPoints);
    out.writeLong(numNonEmptyGeometries);
    out.writeInt(geometryType.ordinal());
    for (int $d = 0; $d < getCoordinateDimension(); $d++)
      out.writeDouble(sumSideLength[$d]);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    super.readFields(in);
    this.numFeatures = in.readLong();
    this.size = in.readLong();
    this.numPoints = in.readLong();
    this.numNonEmptyGeometries = in.readLong();
    this.geometryType = GeometryType.values()[in.readInt()];
    for (int $d = 0; $d < getCoordinateDimension(); $d++)
      this.sumSideLength[$d] = in.readDouble();
  }

  @Override
  public void setEmpty() {
    super.setEmpty();
    this.numFeatures = this.size = this.numPoints = this.numNonEmptyGeometries = 0;
    this.geometryType = GeometryType.EMPTY;
  }

  @Override
  public String toString() {
    StringBuilder str = new StringBuilder();
    // Write MBR
    str.append("MBR: [(");
    for (int $d = 0; $d < getCoordinateDimension(); $d++) {
      if ($d != 0)
        str.append(", ");
      str.append(minCoord[$d]);
    }
    str.append("), (");
    for (int $d = 0; $d < getCoordinateDimension(); $d++) {
      if ($d != 0)
        str.append(", ");
      str.append(maxCoord[$d]);
    }
    str.append(")]");
    str.append(", size: ");
    str.append(size);
    str.append(", numFeatures: ");
    str.append(numFeatures);
    str.append(", numPoints: ");
    str.append(numPoints);
    str.append(", avgSideLength: [");
    for (int $d = 0; $d < getCoordinateDimension(); $d++) {
      if ($d != 0)
        str.append(", ");
      str.append(getAverageSideLength($d));
    }
    str.append("]");
    return str.toString();
  }

  public double getAverageSideLength(int i) {
    return sumSideLength[i] / numNonEmptyGeometries;
  }
}
