/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.stsynopses;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.GeometryType;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.twod.LineString2D;
import edu.ucr.cs.bdlab.geolite.twod.Polygon2D;
import edu.ucr.cs.bdlab.wktparser.ParseException;
import edu.ucr.cs.bdlab.wktparser.WKTParser;
import junit.framework.TestCase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SummaryTest extends TestCase {
  public void testComputeAverageSideLength() {
    // Test with points
    List<IGeometry> geoms = new ArrayList<>();
    geoms.add(new Point(0.0, 3.5));
    geoms.add(new Point(0.5, 1.3));

    Summary s = new Summary();
    s.setCoordinateDimension(2);
    for (IGeometry geom : geoms)
      s.expandToGeometry(geom);

    assertEquals(0.0, s.getAverageSideLength(0));
    assertEquals(0.0, s.getAverageSideLength(1));

    // Test with envelopes
    geoms.clear();
    geoms.add(new Envelope(2, 0.0, 0.0, 5.0, 5.0));
    geoms.add(new Envelope(2, 5.0, 2.0, 8.0, 3.0));
    s.setEmpty();
    for (IGeometry geom : geoms)
      s.expandToGeometry(geom);
    assertEquals(4.0, s.getAverageSideLength(0));
    assertEquals(3.0, s.getAverageSideLength(1));
  }

  public void testComputeNumPoints() {
    // Test with points
    List<IGeometry> geoms = new ArrayList<>();
    geoms.add(new Point(0.0, 3.5));
    geoms.add(new Point(0.5, 1.3));

    Summary s = new Summary();
    s.setCoordinateDimension(2);
    for (IGeometry geom : geoms)
      s.expandToGeometry(geom);

    assertEquals(2L, s.getNumPoints());

    // Test with envelopes
    geoms.clear();
    geoms.add(new Envelope(2, 0.0, 0.0, 5.0, 5.0));
    geoms.add(new Envelope(2, 5.0, 2.0, 8.0, 3.0));
    s.setEmpty();
    for (IGeometry geom : geoms)
      s.expandToGeometry(geom);
    assertEquals(8L, s.getNumPoints());
  }


  public void testMerge() {
    List<IGeometry> geoms = new ArrayList<>();
    geoms.add(new Envelope(2, 0.0, 0.0, 5.0, 5.0));
    geoms.add(new Envelope(2, 5.0, 2.0, 8.0, 3.0));

    Summary s0 = new Summary();
    s0.setCoordinateDimension(2);
    Summary s1 = new Summary();
    s1.setCoordinateDimension(2);

    s0.expandToGeometry(geoms.get(0));
    s1.expandToGeometry(geoms.get(1));

    Summary s = new Summary();
    s.setCoordinateDimension(2);
    s.expandToSummary(s1);
    s.expandToSummary(s0);
    assertEquals(2, s.numFeatures);
    assertEquals(4.0, s.getAverageSideLength(0));
    assertEquals(3.0, s.getAverageSideLength(1));
    assertEquals(8L, s.getNumPoints());
  }

  public void testCopyConstructor() {
    Summary s1 = new Summary();
    s1.setCoordinateDimension(2);
    s1.set(new double[] {1.0, 2.0}, new double[] {3.0, 4.0});
    s1.numFeatures = 10;
    s1.numPoints = 20;
    s1.size = 1000;

    Summary s2 = new Summary(s1);
    assertEquals(1.0, s2.minCoord[0]);
    assertEquals(2.0, s2.minCoord[1]);
    assertEquals(3.0, s2.maxCoord[0]);
    assertEquals(4.0, s2.maxCoord[1]);
    assertEquals(10, s2.numFeatures);
    assertEquals(20L, s2.getNumPoints());
    assertEquals(1000L, s2.getDataSize());
    assertEquals(s1, s2);
  }

  public void testSerialization() throws IOException {
    List<IGeometry> geoms = new ArrayList<>();
    geoms.add(new Envelope(2, 0.0, 0.0, 5.0, 5.0));
    geoms.add(new Envelope(2, 5.0, 2.0, 8.0, 3.0));
    Summary s = new Summary();
    s.setCoordinateDimension(2);
    for (IGeometry geom : geoms)
      s.expandToGeometry(geom);

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    s.write(dos);
    dos.close();

    DataInputStream din = new DataInputStream(new ByteArrayInputStream(baos.toByteArray()));
    Summary s2 = new Summary();
    s2.readFields(din);
    assertEquals(2, s2.numFeatures);
    assertEquals(4.0, s2.getAverageSideLength(0));
    assertEquals(3.0, s2.getAverageSideLength(1));
    assertEquals(8L, s.getNumPoints());
  }

  public void testSkipEmptyPolygons() {
    IGeometry[] geometries = {
        new Polygon2D(),
        new Envelope(2, 1.0, 2.0, 3.0, 5.0),
        new Envelope(2, 2.0, 10.0, 5.0, 15.0),
    };
    Summary summary = new Summary();
    summary.setCoordinateDimension(2);
    for (IGeometry geometry : geometries)
      summary.expandToGeometry(geometry);
    assertEquals(2.5, summary.getAverageSideLength(0), 1E-5);
    assertEquals(4.0, summary.getAverageSideLength(1), 1E-5);
  }

  public void testSummaryWithSize() throws IOException, ParseException {
    String[] wkts = {
        "POINT(1 2)",
        "LINESTRING(1 2, 3 4, 5 6)"
    };
    WKTParser parser = new WKTParser();
    Summary summary = new Summary();
    summary.setCoordinateDimension(2);
    for (String wkt : wkts)
      summary.expandToGeometryWithSize(parser.parse(wkt, null), wkt.length());
    assertEquals(1.0, summary.minCoord[0], 1E-5);
    assertEquals(6.0, summary.maxCoord[1], 1E-5);
    assertEquals(35, summary.size);
  }

  public void testSummaryWithDataType() throws IOException, ParseException {
    String[] wkts = {
        "POINT(1 2)",
        "POINT(3 4)"
    };
    WKTParser parser = new WKTParser();
    Summary summary = new Summary();
    summary.setCoordinateDimension(2);
    for (String wkt : wkts)
      summary.expandToGeometry(parser.parse(wkt, null));
    assertEquals(GeometryType.POINT, summary.getGeometryType());

    // Test coercing linestring to multinlinestring
    wkts = new String[] {
        "LINESTRING(1 2, 3 4)",
        "MULTILINESTRING((1 2, 3 4))"
    };
    summary.setEmpty();
    summary.setCoordinateDimension(2);
    for (String wkt : wkts)
      summary.expandToGeometry(parser.parse(wkt, null));
    assertEquals(GeometryType.MULTILINESTRING, summary.getGeometryType());

    // Coerce envelope to polygon
    summary.setEmpty();
    summary.setCoordinateDimension(2);
    summary.expandToGeometry(new Envelope(2, 0, 0, 1, 1));
    summary.expandToGeometry(new Polygon2D());
    assertEquals(GeometryType.POLYGON, summary.getGeometryType());

    // Coerce mixed to GeometryCollection
    summary.setEmpty();
    summary.setCoordinateDimension(2);
    summary.expandToGeometry(new Envelope(2, 0, 0, 1, 1));
    summary.expandToGeometry(new LineString2D());
    assertEquals(GeometryType.GEOMETRYCOLLECTION, summary.getGeometryType());
  }
}