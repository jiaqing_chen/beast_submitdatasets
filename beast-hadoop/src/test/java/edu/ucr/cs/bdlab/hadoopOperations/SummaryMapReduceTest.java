/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.io.CSVFeatureReader;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.stsynopses.Summary;
import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.io.IOException;

public class SummaryMapReduceTest extends SparkTest {

  public void testComputeSummary() throws IOException, InterruptedException, ClassNotFoundException {
    Path path = new Path(scratchPath, "test.points");
    copyResource("/test.rect", new File(path.toString()));
    UserOptions opts = new UserOptions();
    opts.set(SpatialInputFormat.InputFormat, "pointk(4)");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    Summary summary = SummaryMapReduce.computeMapReduce(new Path[] {path}, opts);
    assertEquals(14, summary.numFeatures);
  }
}