/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.io.CSVFeatureReader;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.io.IOException;

public class SampleMapReduceTest extends SparkTest {

  public void testReadSample() throws IOException, InterruptedException, ClassNotFoundException {
    Path in = new Path(scratchPath, "test.points");
    copyResource("/test.rect", new File(in.toString()));

    UserOptions opts = new UserOptions();
    opts.set(SpatialInputFormat.InputFormat, "pointk(4)");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    double[][] coords = SampleMapReduce.drawPointSample(new Path[] {in}, 1.0f, opts);
    assertEquals(4, coords.length);
    assertEquals(14, coords[0].length);
  }

  public void testReadSampleZero() throws IOException, InterruptedException, ClassNotFoundException {
    Path in = new Path(scratchPath, "test.points");
    copyResource("/test.rect", new File(in.toString()));

    UserOptions opts = new UserOptions();
    opts.set(SpatialInputFormat.InputFormat, "pointk(4)");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    double[][] coords = SampleMapReduce.drawPointSample(new Path[] {in}, 0.5f, opts);
    assertTrue("Too many records", coords[0].length < 14);
  }
}