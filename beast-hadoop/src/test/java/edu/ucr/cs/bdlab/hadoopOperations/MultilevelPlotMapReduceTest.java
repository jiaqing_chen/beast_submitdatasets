/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.davinci.CommonVisualizationHelper;
import edu.ucr.cs.bdlab.davinci.MultilevelPyramidPlotHelper;
import edu.ucr.cs.bdlab.io.CSVFeatureReader;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

public class MultilevelPlotMapReduceTest extends SparkTest {

  public void testPlotPointsAllImagesFlatOnly() throws IOException, ClassNotFoundException, InterruptedException {
    Path input = new Path(scratchPath, "input");
    Path output = new Path(scratchPath, "output");
    copyResource("/test_small.point", new File(input.toString()));
    UserOptions opts = new UserOptions();
    opts.set(CommonVisualizationHelper.PlotterName, "gplot");
    opts.setInt(MultilevelPyramidPlotHelper.NumLevels, 3);
    opts.setInt(MultilevelPyramidPlotHelper.DataTileThreshold, 0);
    opts.setInt(MultilevelPyramidPlotHelper.MinSizeFlatPartitioning, 0);
    opts.set(SpatialInputFormat.InputFormat, "point");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    opts.setBoolean(CommonVisualizationHelper.VerticalFlip, false);
    opts.setInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1);

    MultilevelPlotMapReduce.plotMapReduce(input, output, opts);

    FileSystem outFileSystem = output.getFileSystem(opts);
    FileStatus[] actualFiles = outFileSystem.listStatus(output, p -> p.getName().matches("tile-\\d+-\\d+-\\d+.\\w+"));
    List<String> expectedFiles = Arrays.asList("tile-0-0-0.png",
        "tile-1-0-0.png", "tile-1-1-0.png", "tile-1-1-1.png",
        "tile-2-0-0.png", "tile-2-2-0.png", "tile-2-1-1.png", "tile-2-3-1.png", "tile-2-2-3.png");
    assertEquals(expectedFiles.size(), actualFiles.length);
    for (FileStatus actualFile : actualFiles) {
      assertTrue(String.format("File '%s' not expected", actualFile.getPath().getName()),
          expectedFiles.contains(actualFile.getPath().getName()));
    }
  }

  public void testPlotPointsAllImagesFlatAndPyramid() throws IOException, ClassNotFoundException, InterruptedException {
    Path input = new Path(scratchPath, "input");
    Path output = new Path(scratchPath, "output");
    copyResource("/test_small.point", new File(input.toString()));
    UserOptions opts = new UserOptions();
    opts.set(CommonVisualizationHelper.PlotterName, "gplot");
    opts.setInt(MultilevelPyramidPlotHelper.NumLevels, 3);
    opts.setInt(MultilevelPyramidPlotHelper.DataTileThreshold, 0);
    opts.setInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1);
    // The threshold of 25 means that three or more records will be plotted using pyramid partitioning
    opts.setInt(MultilevelPyramidPlotHelper.MinSizeFlatPartitioning, 25);
    opts.set(SpatialInputFormat.InputFormat, "point");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    opts.setBoolean(CommonVisualizationHelper.VerticalFlip, false);

    MultilevelPlotMapReduce.plotMapReduce(input, output, opts);

    FileSystem outFileSystem = output.getFileSystem(opts);
    FileStatus[] actualFiles = outFileSystem.listStatus(output, p -> p.getName().matches("tile-\\d+-\\d+-\\d+.\\w+"));
    List<String> expectedFiles = Arrays.asList("tile-0-0-0.png",
        "tile-1-0-0.png", "tile-1-1-0.png", "tile-1-1-1.png",
        "tile-2-0-0.png", "tile-2-2-0.png", "tile-2-1-1.png", "tile-2-3-1.png", "tile-2-2-3.png");
    assertEquals(expectedFiles.size(), actualFiles.length);
    for (FileStatus actualFile : actualFiles) {
      assertTrue(String.format("File '%s' not expected", actualFile.getPath().getName()),
          expectedFiles.contains(actualFile.getPath().getName()));
    }
  }

  public void testFlatten() throws IOException, ClassNotFoundException, InterruptedException {
    Path input = new Path(scratchPath, "input");
    Path output = new Path(scratchPath, "output");
    copyResource("/complex-geoms.wkt", new File(input.toString()));
    UserOptions opts = new UserOptions();
    opts.set(CommonVisualizationHelper.PlotterName, "gplot");
    opts.setInt(MultilevelPyramidPlotHelper.NumLevels, 3);
    opts.setInt(MultilevelPyramidPlotHelper.DataTileThreshold, 0);
    opts.setInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1);
    opts.setInt(MultilevelPyramidPlotHelper.MinSizeFlatPartitioning, 75);
    opts.setInt(MultilevelPlotMapReduce.MaxLevelForFlatPartitioning, 1);
    opts.setBoolean(CommonVisualizationHelper.FlattenFeatures, true);
    opts.set(SpatialInputFormat.InputFormat, "wkt");
    opts.setBoolean(CommonVisualizationHelper.VerticalFlip, false);

    MultilevelPlotMapReduce.plotMapReduce(input, output, opts);

    // No assertions for now. Just make sure it does not throw an exception
  }

  public void testSkipDataTiles() throws IOException, ClassNotFoundException, InterruptedException {
    Path input = new Path(scratchPath, "input");
    Path output = new Path(scratchPath, "output");
    copyResource("/test_small.point", new File(input.toString()));
    UserOptions opts = new UserOptions();
    opts.set(CommonVisualizationHelper.PlotterName, "gplot");
    opts.setInt(MultilevelPyramidPlotHelper.NumLevels, 3);
    opts.setInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1);
    // The threshold of 10 means that all tiles with one record will be data tile. 2 or more are image tiles
    opts.setInt(MultilevelPyramidPlotHelper.DataTileThreshold, 10);
    // The threshold of 25 means that three or more records will be plotted using pyramid partitioning
    opts.setInt(MultilevelPyramidPlotHelper.MinSizeFlatPartitioning, 25);
    opts.set(SpatialInputFormat.InputFormat, "point");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    opts.setBoolean(CommonVisualizationHelper.VerticalFlip, false);

    MultilevelPlotMapReduce.plotMapReduce(input, output, opts);

    FileSystem outFileSystem = output.getFileSystem(opts);
    FileStatus[] actualFiles = outFileSystem.listStatus(output, p -> p.getName().matches("tile-\\d+-\\d+-\\d+.\\w+"));
    List<String> expectedFiles = Arrays.asList("tile-0-0-0.png",
        "tile-1-0-0.png", "tile-1-1-0.png",
        "tile-2-2-0.png", "tile-2-1-1.png");
    assertEquals(expectedFiles.size(), actualFiles.length);
    for (FileStatus actualFile : actualFiles) {
      assertTrue(String.format("File '%s' not expected", actualFile.getPath().getName()),
          expectedFiles.contains(actualFile.getPath().getName()));
    }
    UserOptions vizOpts = UserOptions.loadFromTextFile(outFileSystem, new Path(output, "_visualization.properties"));
    assertEquals("../"+input.getName(), vizOpts.get("data"));
  }

  public void testCoarseGrainedPyramidPartitioning() throws IOException, ClassNotFoundException, InterruptedException {
    Path input = new Path(scratchPath, "input");
    Path output = new Path(scratchPath, "output");
    copyResource("/test_small.point", new File(input.toString()));
    UserOptions opts = new UserOptions();
    opts.set(CommonVisualizationHelper.PlotterName, "gplot");
    opts.setInt(MultilevelPyramidPlotHelper.NumLevels, 3);
    opts.setInt(MultilevelPyramidPlotHelper.PartitionGranularity, 2);
    // The threshold of 10 means that all tiles with one record will be data tile. 2 or more are image tiles
    opts.setInt(MultilevelPyramidPlotHelper.DataTileThreshold, 10);
    // The threshold of 1000 means that flat partitioning will not generate any tiles
    opts.setInt(MultilevelPyramidPlotHelper.MinSizeFlatPartitioning, 1000);
    opts.set(SpatialInputFormat.InputFormat, "point");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    opts.setBoolean(CommonVisualizationHelper.VerticalFlip, false);

    MultilevelPlotMapReduce.plotMapReduce(input, output, opts);

    FileSystem outFileSystem = output.getFileSystem(opts);
    FileStatus[] actualFiles = outFileSystem.listStatus(output, p -> p.getName().matches("tile-\\d+-\\d+-\\d+.\\w+"));
    List<String> expectedFiles = Arrays.asList("tile-0-0-0.png",
        "tile-1-0-0.png", "tile-1-1-0.png",
        "tile-2-2-0.png", "tile-2-1-1.png");
    assertEquals(expectedFiles.size(), actualFiles.length);
    for (FileStatus actualFile : actualFiles) {
      assertTrue(String.format("File '%s' not expected", actualFile.getPath().getName()),
          expectedFiles.contains(actualFile.getPath().getName()));
    }
  }

  public void testMercatorProjection() throws IOException, ClassNotFoundException, InterruptedException {
    Path inputFile = new Path(scratchPath, "in.point");
    Path output = new Path(scratchPath, "out");
    copyResource("/test-mercator.rect", new File(inputFile.toString()));

    UserOptions opts = new UserOptions("plotter:gplot", "iformat:envelope", "separator:,",
        "levels:3", "threshold:0", "-mercator", "fill:black");
    opts.setInt(MultilevelPyramidPlotHelper.PartitionGranularity, 1);
    MultilevelPlotMapReduce.plotMapReduce(inputFile, output, opts);

    // Make sure that there are no extra tiles were created
    FileSystem outFileSystem = output.getFileSystem(opts);
    FileStatus[] actualFiles = outFileSystem.listStatus(output, p -> p.getName().matches("tile-\\d+-\\d+-\\d+.\\w+"));
    assertEquals(21, actualFiles.length);
    for (FileStatus actualFile : actualFiles) {
      try (FSDataInputStream in = outFileSystem.open(actualFile.getPath())) {
        BufferedImage actualImage = ImageIO.read(in);
        try (InputStream in2 = this.getClass().getResourceAsStream("/rect_mercator_plot/" + actualFile.getPath().getName())) {
          BufferedImage expectedImage = ImageIO.read(in2);
          assertImageEquals(String.format("Error in tile '%s'", actualFile.getPath().getName()), expectedImage, actualImage);
        }
      }
    }

    UserOptions vizOpts = UserOptions.loadFromTextFile(outFileSystem, new Path(output, "_visualization.properties"));
    assertTrue("Input mercator should be set", vizOpts.getBoolean(SpatialInputFormat.Mercator, false));
    assertFalse(vizOpts.getBoolean(CommonVisualizationHelper.VerticalFlip, true));
  }
}