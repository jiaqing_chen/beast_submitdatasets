package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.geolite.Feature;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.io.CSVFeature;
import edu.ucr.cs.bdlab.io.CSVFeatureReader;
import edu.ucr.cs.bdlab.io.DBFFeature;
import edu.ucr.cs.bdlab.io.FeatureReader;
import edu.ucr.cs.bdlab.io.ShapefileFeatureReader;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;

public class MapReduceUtil {
  /**
   * For Hadoop MapReduce, this method is used to set the intermediate value class so that it matches the input.
   * @param conf
   * @return
   */
  public static Class<? extends IFeature> guessFeatureClass(Configuration conf, Path input) {
    Class<? extends FeatureReader> readerClass = SpatialInputFormat.getFeatureReaderClass(conf, input);
    if (readerClass.equals(CSVFeatureReader.class))
      return CSVFeature.class;
    else if (readerClass.equals(ShapefileFeatureReader.class))
      return DBFFeature.class;
    else
      return Feature.class;
  }
}
