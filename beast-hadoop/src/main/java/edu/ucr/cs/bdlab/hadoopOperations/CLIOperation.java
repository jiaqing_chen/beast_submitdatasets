/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.util.UserOptions;

import javax.security.auth.login.Configuration;
import java.io.IOException;

/**
 * Interface for command-line operations
 */
public interface CLIOperation {

  /**
   * Setup the operation before it runs. At this step, the job can modify the user options, if needed, and can throw
   * an exception of it cannot run.
   * @param opts
   */
  default void setup(UserOptions opts) {}

  /**
   * Starts the operation.
   * @param opts
   */
  void run(UserOptions opts) throws IOException, InterruptedException, ClassNotFoundException;
}
