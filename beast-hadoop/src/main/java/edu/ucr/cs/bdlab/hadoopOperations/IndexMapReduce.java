/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.indexing.IndexOutputFormat;
import edu.ucr.cs.bdlab.indexing.IndexerParams;
import edu.ucr.cs.bdlab.indexing.SpatialPartitioner;
import edu.ucr.cs.bdlab.io.CSVEnvelopeEncoder;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.io.SpatialOutputFormat;
import edu.ucr.cs.bdlab.stsynopses.AbstractHistogram;
import edu.ucr.cs.bdlab.stsynopses.Summary;
import edu.ucr.cs.bdlab.util.IntArray;
import edu.ucr.cs.bdlab.util.OperationException;
import edu.ucr.cs.bdlab.util.OperationMetadata;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapred.ClusterStatus;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.StringUtils;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

@OperationMetadata(
    shortName =  "indexmr",
    description = "Builds a distributed spatial index using MapReduce",
    inputArity = "+",
    outputArity = "1",
    inheritParams = {SpatialInputFormat.class, SpatialOutputFormat.class, IndexerParams.class})
public class IndexMapReduce implements CLIOperation {
  /**Logger for this class*/
  private static final Log LOG = LogFactory.getLog(IndexMapReduce.class);

  /**
   * A map function that maps each feature to a single or multiple overlapping partitions.
   */
  static class SpatialPartitionerMap extends Mapper<Object, IFeature, IntWritable, IFeature> {
    /**The spatial partitioner created and stored*/
    protected SpatialPartitioner partitioner;

    /**The output partition ID*/
    protected IntWritable partitionId;

    /**A temporary array to hold the list of aoverlapping partitions to an object*/
    protected IntArray tempPartitions;

    /**The MBR of a geometry*/
    protected Envelope geometryMBR;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
      super.setup(context);
      partitioner = IndexerParams.readPartitionerFromHadoopConfiguration(context.getConfiguration());
      partitionId = new IntWritable();
      tempPartitions = new IntArray();
    }

    @Override
    protected void map(Object dummy, IFeature feature, Context context) throws IOException, InterruptedException {
      if (geometryMBR == null)
        geometryMBR = new Envelope(feature.getGeometry().getCoordinateDimension());
      feature.getGeometry().envelope(geometryMBR);
      if (partitioner.isDisjoint()) {
        // To create disjoint partitions, we have to replicate to all overlapping partitions
        partitioner.overlapPartitions(geometryMBR, tempPartitions);
        for (int i : tempPartitions) {
          partitionId.set(i);
          context.write(partitionId, feature);
        }
      } else {
        // Assign to one partition
        partitionId.set(partitioner.overlapPartition(geometryMBR));
        context.write(partitionId, feature);
      }
    }
  }

  /**
   * A reduce function that writes the partitioned features to the output. This function does not do much of a logic
   * but its main job is to adjust the format of the input and output objects.
   */
  static class IndexWriterReduce extends Reducer<IntWritable, IFeature, Integer, IFeature> {
    @Override
    protected void reduce(IntWritable key, Iterable<IFeature> values, Context context) throws IOException, InterruptedException {
      for(IFeature value: values)
        context.write(Integer.valueOf(key.get()), value);
    }
  }

  /**
   * Builds an index using Hadoop MapReduce. It is assumes that the partitioner is already created and assigned to the
   * given Hadoop configuration.
   * @param ins
   * @param out
   * @param conf
   */
  public static Job buildIndexMapReduce(Path[] ins, Path out, Configuration conf) throws IOException, ClassNotFoundException, InterruptedException {
    // Create the job
    Job job = Job.getInstance(conf, "Indexer");
    job.setJarByClass(IndexMapReduce.class);

    // Set input details
    job.setInputFormatClass(SpatialInputFormat.class);
    for (Path in : ins)
      SpatialInputFormat.addInputPath(job, in);

    // Set mapper details
    job.setMapperClass(SpatialPartitionerMap.class);
    job.setMapOutputKeyClass(IntWritable.class);
    // Due to Hadoop limitation, we have to configure the exact class of the map output
    Class<? extends IFeature> featureClass = MapReduceUtil.guessFeatureClass(conf, ins[0]);
    job.setMapOutputValueClass(featureClass);

    // Set reducer details
    job.setReducerClass(IndexWriterReduce.class);
    ClusterStatus clusterStatus = new JobClient(new JobConf()).getClusterStatus();
    job.setNumReduceTasks(Math.max(1, clusterStatus.getMaxReduceTasks() * 9 / 10));

    // Set output details
    job.setOutputFormatClass(IndexOutputFormat.class);
    IndexOutputFormat.setOutputPath(job, out);
    job.setOutputKeyClass(Integer.class);
    job.setOutputValueClass(Iterable.class); // Iterable<IFeature>

    // Start the job and wait for completion
    job.waitForCompletion(true);

    return job;
  }

  public static Job buildIndexMapReduce(Path in, Path out, Configuration conf) throws IOException, ClassNotFoundException, InterruptedException {
    return buildIndexMapReduce(new Path[]{in}, out, conf);
  }

  static class PartitioningInfo {
    SpatialPartitioner.PartitionCriterion pCriterion;
    long pValue;

    public PartitioningInfo(String str) {
      int i1 = str.indexOf('(');
      int i2 = str.indexOf(')');
      switch (str.substring(0, i1).toLowerCase()) {
        case "fixed": pCriterion = SpatialPartitioner.PartitionCriterion.FIXED; break;
        case "count": pCriterion = SpatialPartitioner.PartitionCriterion.COUNT; break;
        case "size": pCriterion = SpatialPartitioner.PartitionCriterion.SIZE; break;
        default: throw new RuntimeException(String.format("Unrecognized partitioning criterion '%s'", str));
      }
      pValue = StringUtils.TraditionalBinaryPrefix.string2long(str.substring(i1 + 1, i2));
    }
  }

  private static SpatialPartitioner createPartitioner(UserOptions opts) throws InterruptedException, IOException, ClassNotFoundException {
    String gindex = opts.get(IndexerParams.GlobalIndex);
    Class<? extends SpatialPartitioner> partitionerClass = IndexerParams.partitioners.get(gindex);
    if (partitionerClass == null) {
      String errMessage = String.format("Unknown global index '%s'. ", gindex);
      errMessage += "Please choose one of the following: "+ IndexerParams.partitioners.keySet();
      throw new OperationException(errMessage);
    }
    boolean disjoint = opts.getBoolean(IndexerParams.DisjointIndex, false);
    long synopsisSize = opts.getLongBytes(IndexerParams.SynopsisSize, 10 * 1024 * 1024);
    String criterionThreshold = opts.get(IndexerParams.PartitionCriterionThreshold, "Size(128m)");
    PartitioningInfo pInfo = new PartitioningInfo(criterionThreshold);
    SpatialPartitioner partitioner = createPartitioner(opts, partitionerClass, disjoint, pInfo, synopsisSize);
    return partitioner;
  }

  private static SpatialPartitioner createPartitioner(UserOptions opts,
                                                      Class<? extends SpatialPartitioner> partitionerClass,
                                                      boolean disjoint,
                                                      PartitioningInfo pInfo,
                                                      long synopsisSize) throws InterruptedException, IOException, ClassNotFoundException {
    try {
      SpatialPartitioner.Metadata partitionerMetadata =
          partitionerClass.getAnnotation(SpatialPartitioner.Metadata.class);
      if (partitionerMetadata == null)
        throw new RuntimeException(String.format("Partitioner '%s' should be annotated with '%s'",
            partitionerClass.getName(), SpatialPartitioner.Metadata.class.getName()));

      SpatialPartitioner partitioner = partitionerClass.newInstance();
      partitioner.setup(opts, disjoint, pInfo.pCriterion, pInfo.pValue);

      if (disjoint && !partitionerMetadata.disjointSupported())
        throw new RuntimeException("Partitioner " + partitionerClass.getName() + " does not support disjoint partitioning");

      long t1 = System.nanoTime();
      String[] inputs = opts.getInputs();
      Path[] ins = new Path[inputs.length];
      for (int $i = 0; $i < inputs.length; $i++)
        ins[$i] = new Path(inputs[$i]);
      Summary summary = SummaryMapReduce.computeMapReduce(ins, opts);
      double[][] sample = null;
      AbstractHistogram histogram = null;
      Method constrctMethod = partitionerClass.getMethod("construct", Summary.class, double[][].class, AbstractHistogram.class);
      Annotation[][] parameterAnnotations = constrctMethod.getParameterAnnotations();
      boolean sampleRequired = false, histogramRequired = false, balanced = opts.getBoolean(IndexerParams.BalancedPartitioning, false);
      for (Annotation a : parameterAnnotations[1])
        if (a.annotationType() == SpatialPartitioner.Required.class || (balanced && a.annotationType() == SpatialPartitioner.Preferred.class))
          sampleRequired = true;
      for (Annotation a : parameterAnnotations[2])
        if (a.annotationType() == SpatialPartitioner.Required.class || (balanced && a.annotationType() == SpatialPartitioner.Preferred.class))
          histogramRequired = true;
      if (sampleRequired && histogramRequired)
        synopsisSize /= 2;
      if (sampleRequired) {
        int sampleSize = (int) (synopsisSize / (8 * summary.getCoordinateDimension()));
        float samplingRatio = Math.min(1.0f, (float)sampleSize / summary.numFeatures);
        LOG.info(String.format("Reading a %f sample with expected %d points", samplingRatio, sampleSize));
        sample = SampleMapReduce.drawPointSample(ins, samplingRatio, opts);
      }
      if (histogramRequired) {
        // ------------ Read the histogram ---------------------------------------------------------------------------
        int binSize;
        String histogramType = opts.get(HistogramMapReduce.HistogramType, "simple");
        switch (histogramType) {
          case "simple": binSize = 8; break;
          case "euler": binSize = 32; break;
          default: throw new RuntimeException(String.format("Unknown histogram type '%s'", histogramType));
        }

        int numBuckets = (int) (synopsisSize / binSize);
        // Start a MapReduce job that computes the histogram
        UserOptions histogramOpts = new UserOptions(opts);
        histogramOpts.setInt(HistogramMapReduce.NumBuckets, numBuckets);
        histogramOpts.set("mbr", CSVEnvelopeEncoder.defaultEncoder.apply(summary));
        histogramOpts.set(HistogramMapReduce.HistogramValue, "size");
        // Compute and read the histogram
        histogram = HistogramMapReduce.computeMapReduce(ins, histogramOpts);
        // ----- End of histogram computation ------------------------------------------------------------------------
      }
      long t2 = System.nanoTime();
      partitioner.construct(summary, sample, histogram);
      long t3 = System.nanoTime();
      LOG.info(String.format("Partitioner '%s' constructed in %f seconds and synopsis created in %f seconds",
          partitionerClass.getSimpleName(), (t3-t2)*1E-9, (t2-t1)*1E-9));

      return partitioner;
    } catch (InstantiationException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    } catch (NoSuchMethodException e) {
      e.printStackTrace();
    }
    LOG.warn("Could not create a partitioner. Returning null!");
    return null;
  }

  @Override
  public void run(UserOptions opts) throws IOException, InterruptedException {
    try {
      // Create partitioner
      SpatialPartitioner partitioner = createPartitioner(opts);

      // Build the index using the partitioner
      Configuration conf = opts.loadIntoHadoopConfiguration(null);
      IndexerParams.savePartitionerToHadoopConfiguration(conf, partitioner);
      buildIndexMapReduce(opts.getInputPaths(), new Path(opts.getOutput()), conf);
    } catch (ClassNotFoundException e) {
      throw new RuntimeException("Error creating the partitioner", e);
    }

  }
}
