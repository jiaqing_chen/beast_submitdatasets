/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.hadoopOperations;

import edu.ucr.cs.bdlab.util.OperationException;
import edu.ucr.cs.bdlab.util.OperationUtil;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * A main class that runs all operations supported by the system.
 */
public class Main {

  /**Logger for this class*/
  private static final Log LOG = LogFactory.getLog(Main.class);

  public static void main(String[] args) throws IllegalAccessException, InvocationTargetException {
    if (args.length == 0)
      OperationUtil.printUsageAndExit();
    OperationUtil.Operation operation = OperationUtil.getOperation(args[0]);

    UserOptions commandLine = OperationUtil.createAndCheckCLO(operation, args);
    // Invoke the run method
    long t1 = System.nanoTime();
    try {
      CLIOperation op = operation.operationClass.asSubclass(CLIOperation.class).newInstance();
      op.setup(commandLine);
      op.run(commandLine);
    } catch (IllegalAccessException e) {
      throw new RuntimeException(String.format("Cannot access the run method", operation.operationClass), e);
    } catch (InstantiationException e) {
      throw new RuntimeException(String.format("Cannot initialize the operation class '%s'", operation.operationClass), e);
    } catch (InterruptedException | IOException | ClassNotFoundException e) {
      throw new RuntimeException(e);
    } finally {
      long t2 = System.nanoTime();
      LOG.info(String.format("The operation %s finished in %f seconds", operation.metadata.shortName(), (t2 - t1) * 1E-9));
    }
  }
}
