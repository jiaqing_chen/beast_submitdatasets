/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.hadoopOperations;

import org.apache.hadoop.mapreduce.TaskInputOutputContext;

import java.io.IOException;
import java.util.Iterator;

/**
 * A utility class that wraps a MapReduce context into an iterator over the values
 */
public class ValuesIterable<T> implements Iterable<T>, Iterator<T> {

  private final TaskInputOutputContext<?, T, ?, ?> context;

  public ValuesIterable(TaskInputOutputContext<?, T, ?, ?> context) {
    this.context = context;
  }

  @Override
  public Iterator<T> iterator() {
    return this;
  }

  @Override
  public boolean hasNext() {
    // NOTE: This is not a very accurate implementation because hasNext is not supposed to advance the iterator
    // However, this might be costly to fix as we will have to make a copy of each value before it is returned.
    // So, we will assume that the reader will not use the previous object between a call of hasNext and next
    try {
      return this.context.nextKeyValue();
    } catch (IOException e) {
      throw new RuntimeException(e);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public T next() {
    try {
      return this.context.getCurrentValue();
    } catch (IOException e) {
      throw new RuntimeException(e);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }
}
