/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.cg;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.util.IndexedSortable;
import org.apache.hadoop.util.QuickSort;

import java.util.function.BiConsumer;

/**
 * A utility class that contains implementations of spatial join algorithms.
 */
public class SpatialJoinAlgorithms {
  /**Logger for this class*/
  private static final Log LOG = LogFactory.getLog(SpatialJoinAlgorithms.class);

  /**
   * Computes the spatial join between two sets of rectangles and reports all overlapping pairs of indexes.
   *
   * @param minCoord1 an array of coordinates for the lower corners of the first set of rectangles
   * @param maxCoord1 an array of coordinates for the upper corners of the first set of rectangles
   * @param minCoord2 an array of coordinates for the lower corners of the second set of rectangles
   * @param maxCoord2 an array of coordinates for the upper corners of the second set of rectangles
   * @param results the output is reported by calling this function with the indexes of the two rectangles that
   *                overlap. If {@code null}, results are not reported; only the total count is.
   * @return the total number of results found.
   */
  public static int planeSweepRectangles(double[][] minCoord1, double[][] maxCoord1,
                                         double[][] minCoord2, double[][] maxCoord2,
                                         BiConsumer<Integer, Integer> results) {
    assert minCoord1.length == maxCoord1.length;
    assert minCoord1.length == minCoord2.length;
    assert minCoord1.length == maxCoord2.length;
    int count = 0;
    int numDimensions = minCoord1.length;
    int n1 = minCoord1[0].length;
    int n2 = minCoord2[0].length;
    LOG.info(String.format("Running the plane sweep join algorithm between lists sizes of %d and %d", n1, n2));
    // Generate indexes to report the answer
    int[] indexes1 = new int[n1];
    int[] indexes2 = new int[n2];
    for (int $i = 0; $i < n1; $i++)
      indexes1[$i] = $i;
    for (int $i = 0; $i < n2; $i++)
      indexes2[$i] = $i;
    // Sort by the first coordinate and keep track of the array index for each entry for reporting
    IndexedSortable sortable1 = new IndexedSortable() {
      @Override
      public int compare(int i, int j) {
        return (int) Math.signum(minCoord1[0][i] - minCoord1[0][j]);
      }

      @Override
      public void swap(int i, int j) {
        // Swap indexes
        indexes1[i] ^= indexes1[j];
        indexes1[j] ^= indexes1[i];
        indexes1[i] ^= indexes1[j];
        // Swap coordinates
        for (int $d = 0; $d < numDimensions; $d++) {
          double t = minCoord1[$d][i];
          minCoord1[$d][i] = minCoord1[$d][j];
          minCoord1[$d][j] = t;
          t = maxCoord1[$d][i];
          maxCoord1[$d][i] = maxCoord1[$d][j];
          maxCoord1[$d][j] = t;
        }
      }
    };
    IndexedSortable sortable2 = new IndexedSortable() {
      @Override
      public int compare(int i, int j) {
        return (int) Math.signum(minCoord2[0][i] - minCoord2[0][j]);
      }

      @Override
      public void swap(int i, int j) {
        // Swap indexes
        indexes2[i] ^= indexes2[j];
        indexes2[j] ^= indexes2[i];
        indexes2[i] ^= indexes2[j];
        // Swap coordinates
        for (int $d = 0; $d < numDimensions; $d++) {
          double t = minCoord2[$d][i];
          minCoord2[$d][i] = minCoord2[$d][j];
          minCoord2[$d][j] = t;
          t = maxCoord2[$d][i];
          maxCoord2[$d][i] = maxCoord2[$d][j];
          maxCoord2[$d][j] = t;
        }
      }
    };
    QuickSort sorter = new QuickSort();
    sorter.sort(sortable1, 0, n1);
    sorter.sort(sortable2, 0, n2);

    // Now, run the planesweep algorithm
    int i = 0, j = 0;
    while (i < n1 && j < n2) {
      if (minCoord1[0][i] < minCoord2[0][j]) {
        // R1[i] is the left-most rectangle. Activate it and compare to all rectangles R2 until passing the right end
        // of R1[i]
        int jj = j;
        while (jj < n2 && minCoord2[0][jj] < maxCoord1[0][i]) {
          // Compare the two rectangles R1[i] and R2[jj] and report if needed
          if (rectanglesOverlap(minCoord1, maxCoord1, i, minCoord2, maxCoord2, jj)) {
            // Found an overlap
            count++;
            if (results != null)
              results.accept(indexes1[i], indexes2[jj]);
          }
          jj++;
        }
        i++;
      } else {
        // R2[j] is the left-most rectangle. Activate it and compare to all rectangles of R1 until passing the right
        // end of $2[j]
        int ii = i;
        while (ii < n1 && minCoord1[0][ii] < maxCoord2[0][j]) {
          // Compare the two rectangles R1[ii] and R2[j] and report if needed
          if (rectanglesOverlap(minCoord1, maxCoord1, ii, minCoord2, maxCoord2, j)) {
            // Found an overlap
            count++;
            if (results != null)
              results.accept(indexes1[ii], indexes2[j]);
          }
          ii++;
        }
        j++;
      }
    }
    return count;
  }

  /**
   * Test if two rectangles overlap R1[i] and R2[j].
   * @param minCoord1
   * @param maxCoord1
   * @param i
   * @param minCoord2
   * @param maxCoord2
   * @param j
   * @return
   */
  static boolean rectanglesOverlap(double[][] minCoord1, double[][] maxCoord1, int i,
                                   double[][] minCoord2, double[][] maxCoord2, int j) {
    for (int $d = 0; $d < minCoord1.length; $d++)
      if (minCoord1[$d][i] >= maxCoord2[$d][j] || minCoord2[$d][j] >= maxCoord1[$d][i])
        return false;
    return true;
  }
}
