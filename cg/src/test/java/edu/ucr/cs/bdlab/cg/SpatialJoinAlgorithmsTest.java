/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.cg;

import edu.ucr.cs.bdlab.test.SparkTest;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class SpatialJoinAlgorithmsTest extends SparkTest {
  public void testPlaneSweepRectangles() throws IOException {
    double[][] coords1 = readCoordsResource("/test1.rect");
    double[][] coords2 = readCoordsResource("/test2.rect");
    double[][] minCoords1 = new double[][]{coords1[1], coords1[2]};
    double[][] maxCoords1 = new double[][]{coords1[3], coords1[4]};
    double[][] minCoords2 = new double[][]{coords2[1], coords2[2]};
    double[][] maxCoords2 = new double[][]{coords2[3], coords2[4]};
    List<String> expectedResults = Arrays.asList("1,9", "2,9", "3,10");
    int count = SpatialJoinAlgorithms.planeSweepRectangles(minCoords1, maxCoords1, minCoords2, maxCoords2, (i,j) -> {
      String result = (int) coords1[0][i]+","+(int) coords2[0][j];
      assertTrue("Unexpected result pair "+result, expectedResults.indexOf(result) != -1);
    });
    assertEquals(expectedResults.size(), count);
  }
}