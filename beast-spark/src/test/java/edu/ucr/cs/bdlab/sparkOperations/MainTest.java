/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations;

import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.OperationMetadata;
import edu.ucr.cs.bdlab.util.OperationUtil;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class MainTest extends SparkTest {

  public void testCommandLineOperationScala() throws IOException {
    Path inPath = new Path(scratchPath, "test111.points");
    copyResource("/test111.points", new File(inPath.toString()));
    Path outPath = new Path(scratchPath, "test111.wkt_points");
    String[] args = {"cat", "iformat:point(0,1)", "oformat:wkt(0)", "separator:,",
      inPath.toString(), outPath.toString()};
    // Spark does not allow (by default) two concurrent contexts.
    // We close the current context and reopen it after the test is done
    closeSC();
    Main.main(args);
    FileSystem fs = outPath.getFileSystem(new Configuration());
    assertTrue("Output file not found", fs.exists(outPath));
    String[] lines = readFile(new Path(outPath, "part-r-00000.csv").toString());
    assertEquals(111, lines.length);
    assertEquals("POINT(101.7075756 3.2407152)", lines[0]);
  }

  @OperationMetadata(shortName = "testjava", description = "...", inputArity = "0", outputArity = "0")
  static class JavaMethod implements JCLIOperation {

    public static boolean methodRun = false;

    @Override
    public Object run(UserOptions opts, JavaSparkContext sc) {
      methodRun = true;
      return null;
    }
  }

  public void testCommandLineOperationJava() {
    // Force the test operation into the configuration
    OperationUtil.readConfigurationXML("beast.xml").get("Operations").add(JavaMethod.class.getName());
    String[] args = {"testjava"};
    // Spark does not allow (by default) two concurrent contexts.
    // We close the current context and reopen it after the test is done
    closeSC();
    Main.main(args);
    assertTrue("Method did not run correctly", JavaMethod.methodRun);
   }
}