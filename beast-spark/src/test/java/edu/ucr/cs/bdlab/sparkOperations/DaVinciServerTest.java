package edu.ucr.cs.bdlab.sparkOperations;

import edu.ucr.cs.bdlab.io.FeatureReader;
import edu.ucr.cs.bdlab.io.SpatialInputFormat;
import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.zip.GZIPInputStream;

public class DaVinciServerTest extends SparkTest {

  public void testHandleDownload() throws Exception {
    BeastServer server = new BeastServer();
    try {
      // Download should not convert data to Mercator
      Path inputData = new Path(scratchPath, "input");
      copyResource("/test-mercator3.points", new File(inputData.toString()));
      // Plot the data
      Path plotData = new Path(scratchPath, "plot");
      UserOptions opts = new UserOptions(inputData.toString(), "iformat:point", "separator:,",
          plotData.toString(), "-mercator", "-no-data-tiles", "plotter:gplot", "levels:1");
      opts.assignAllInputsExceptOneOutput();
      MultilevelPlot.run(opts, getSC().sc());
      // Start the server
      int port = 9999;
      UserOptions opts2 = new UserOptions("port:" + port);
      server.setup(opts2);
      Thread serverThread = new Thread(() -> server.run(opts2, getSC()));
      serverThread.start();
      // Wait until the server is started
      server.waitUntilStarted();

      // Send a request
      URL url = new URL(String.format("http://127.0.0.1:%d/dynamic/download.cgi/%s.csv?point=true",
          port, plotData.toString()));
      InputStream inputStream = url.openStream();
      byte[] buffer = new byte[4096];
      int compressedBufferSize = 0;
      int readSize;
      while ((readSize = inputStream.read(buffer, compressedBufferSize, buffer.length - compressedBufferSize)) > 0) {
        compressedBufferSize += readSize;
      }
      assert compressedBufferSize < buffer.length;
      inputStream.close();

      // Now, parse the result as text
      GZIPInputStream gzin = new GZIPInputStream(new ByteArrayInputStream(buffer, 0, compressedBufferSize));
      byte[] decompressedBuffer = new byte[4096];
      int decompressedBufferSize = 0;
      while ((readSize = gzin.read(decompressedBuffer, decompressedBufferSize, decompressedBuffer.length - decompressedBufferSize)) > 0) {
        decompressedBufferSize += readSize;
      }
      gzin.close();
      String str = new String(decompressedBuffer, 0, decompressedBufferSize);

      String[] lines = str.split("[\\r\\n]+");
      assertEquals("x\ty", lines[0]);
      assertEquals("-170.0\t90.0", lines[1]);
      assertEquals("170.0\t-90.0", lines[2]);
      assertEquals("-40.0\t0.0", lines[3]);

    } finally {
      // Stop the server and wait until it is closed
      server.stop();
      server.waitUntilStopped();
    }
  }

  public void testDownloadFromIndexDirectly() throws Exception {
    BeastServer server = new BeastServer();
    try {
      // Download should not convert data to Mercator
      Path indexPath = new Path(scratchPath, "index");
      copyDirectoryFromResources("/test_index", new File(indexPath.toString()));
      // Start the server
      int port = 9998;
      UserOptions opts2 = new UserOptions("port:" + port);
      server.setup(opts2);
      Thread serverThread = new Thread(() -> server.run(opts2, getSC()));
      serverThread.start();
      // Wait until the server is started
      server.waitUntilStarted();

      // Send a request
      URL url = new URL(String.format("http://127.0.0.1:%d/dynamic/download.cgi/%s.csv",
              port, indexPath.toString()));
      InputStream inputStream = url.openStream();
      byte[] buffer = new byte[4096];
      int compressedBufferSize = 0;
      int readSize;
      while ((readSize = inputStream.read(buffer, compressedBufferSize, buffer.length - compressedBufferSize)) > 0) {
        compressedBufferSize += readSize;
      }
      assert compressedBufferSize < buffer.length;
      inputStream.close();

      // Now, parse the result as text
      GZIPInputStream gzin = new GZIPInputStream(new ByteArrayInputStream(buffer, 0, compressedBufferSize));
      byte[] decompressedBuffer = new byte[4096];
      int decompressedBufferSize = 0;
      while ((readSize = gzin.read(decompressedBuffer, decompressedBufferSize, decompressedBuffer.length - decompressedBufferSize)) > 0) {
        decompressedBufferSize += readSize;
      }
      gzin.close();
      String str = new String(decompressedBuffer, 0, decompressedBufferSize);

      String[] lines = str.split("[\\r\\n]+");
      assertEquals(4, lines.length);
      assertEquals("LINESTRING(0.0 0.0,1.0 1.0)\t1", lines[1]);
    } finally {
      // Stop the server and wait until it is closed
      server.stop();
      server.waitUntilStopped();
    }
  }

  public void testDownloadAsShapefile() throws Exception {
    BeastServer server = new BeastServer();
    try {
      // Download should not convert data to Mercator
      Path indexPath = new Path(scratchPath, "index");
      copyDirectoryFromResources("/test_index", new File(indexPath.toString()));
      // Start the server
      int port = 9997;
      UserOptions opts = new UserOptions("port:" + port);
      server.setup(opts);
      Thread serverThread = new Thread(() -> server.run(opts, getSC()));
      serverThread.start();
      // Wait until the server is started
      server.waitUntilStarted();

      // Send a request
      URL url = new URL(String.format("http://127.0.0.1:%d/dynamic/download.cgi/%s.shp",
          port, indexPath.toString()));
      InputStream inputStream = url.openStream();
      byte[] buffer = new byte[4096];
      int compressedBufferSize = 0;
      int readSize;
      while ((readSize = inputStream.read(buffer, compressedBufferSize, buffer.length - compressedBufferSize)) > 0) {
        compressedBufferSize += readSize;
      }
      assert compressedBufferSize < buffer.length;
      inputStream.close();

      // Now, parse the result back
      Path downloadedFile = new Path(scratchPath, "downloadeFile.zip");
      FileSystem fileSystem = downloadedFile.getFileSystem(opts);
      FSDataOutputStream out = fileSystem.create(downloadedFile);
      out.write(buffer, 0, compressedBufferSize);
      out.close();

      SpatialInputFormat iformat = new SpatialInputFormat();
      opts.set(SpatialInputFormat.InputFormat, "shapefile");
      FeatureReader reader = iformat.createAndInitRecordReader(new FileSplit(downloadedFile, 0, compressedBufferSize, null), opts);
      int count = 0;
      while (reader.nextKeyValue()) {
        count++;
      }
      assertEquals(3, count);
    } finally {
      // Stop the server and wait until it is closed
      server.stop();
      server.waitUntilStopped();
    }
  }
}