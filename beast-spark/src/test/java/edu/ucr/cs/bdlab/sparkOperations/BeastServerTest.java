package edu.ucr.cs.bdlab.sparkOperations;

import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.OperationUtil;
import edu.ucr.cs.bdlab.util.UserOptions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;

public class BeastServerTest extends SparkTest {

  static boolean method1Called = false;
  static boolean method2Called = false;
  static boolean method3Called = false;
  static int method3Param = 0;
  static boolean method4Called = false;
  static String method4Param = null;
  static boolean method5Called = false;
  static int method5ParamX = 0;
  static int method5ParamY = 0;

  public static class FakeHandler extends WebHandler {

    public boolean makeSuccessful(String target, HttpServletRequest request, HttpServletResponse response) throws IOException {
      response.setStatus(HttpServletResponse.SC_OK);
      PrintWriter writer = response.getWriter();
      writer.println("success");
      writer.close();
      return true;
    }

    @WebMethod
    public boolean method1(String target, HttpServletRequest request, HttpServletResponse response) throws IOException {
      method1Called = true;
      return makeSuccessful(target, request, response);
    }

    @WebMethod(url = "/callMethod2")
    public boolean method2(String target, HttpServletRequest request, HttpServletResponse response) throws IOException {
      method2Called = true;
      return makeSuccessful(target, request, response);
    }

    @WebMethod(url = "/callMethod3/{x}")
    public boolean method3(String target, HttpServletRequest request, HttpServletResponse response, int x) throws IOException {
      method3Called = true;
      method3Param = x;
      return makeSuccessful(target, request, response);
    }

    @WebMethod(url = "/callMethod4/{x}-(\\d+)")
    public boolean method4(String target, HttpServletRequest request, HttpServletResponse response, String x) throws IOException {
      method4Called = true;
      method4Param = x;
      return makeSuccessful(target, request, response);
    }

    @WebMethod(url = "/callMethod5/{x}-{y}")
    public boolean method4(String target, HttpServletRequest request, HttpServletResponse response,
                           int x, int y) throws IOException {
      method5Called = true;
      method5ParamX = x;
      method5ParamY = y;
      return makeSuccessful(target, request, response);
    }

  }

  @Override
  protected void setUp() throws Exception {
    super.setUp();
  }

  public void testCallMethodByName() throws Exception {
    // Force the fake handler into the configuration
    OperationUtil.readConfigurationXML("beast.xml").get("WebHandlers").add(FakeHandler.class.getName());
    BeastServer beastServer = new BeastServer();
    int port = 9996;
    UserOptions opts = new UserOptions("port:"+port);
    try {
      beastServer.setup(opts);
      new Thread(() -> beastServer.run(opts, getSC())).start();
      beastServer.waitUntilStarted();
      String prefix = String.format("http://127.0.0.1:%d", port);
      // Try method1 defined by its name
      getRequest(prefix+"/method1");
      assertTrue("Method 1 should be called", method1Called);
      // Try method2 defined by a URL
      getRequest(prefix+"/callMethod2");
      assertTrue("Method 2 should be called", method2Called);
      // Try method3 defined by a URL with parameter
      getRequest(prefix+"/callMethod3/44");
      assertTrue("Method 3 should be called", method3Called);
      assertEquals(44, method3Param);
      // Try method4 defined by a URL with a parameter and regular expression
      getRequest(prefix+"/callMethod4/para-34234");
      assertTrue("Method 4 should be called", method4Called);
      assertEquals("para", method4Param);
      // Try method5 defined by a URL with two parameters
      getRequest(prefix+"/callMethod5/5555-66");
      assertTrue("Method 5 should be called", method5Called);
      assertEquals(5555, method5ParamX);
      assertEquals(66, method5ParamY);
    } finally {beastServer.stop(); beastServer.waitUntilStopped();}
  }

  void getRequest(String path) throws IOException {
    URL url = new URL(path);
    InputStream inputStream = url.openStream();
    byte[] buffer = new byte[1024];
    while (inputStream.read(buffer) > 0) {}
    inputStream.close();
  }

}
