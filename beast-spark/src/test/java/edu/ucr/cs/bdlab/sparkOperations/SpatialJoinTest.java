/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations;

import edu.ucr.cs.bdlab.io.SpatialOutputFormat;
import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.UserOptions;

import java.io.File;
import java.io.IOException;

public class SpatialJoinTest extends SparkTest {
  public void testSimpleSpatialJoin() throws IOException {
    File inputFile1 = new File(scratchPath.toString(), "in.rect");
    File inputFile2 = new File(scratchPath.toString(), "in.point");
    File outputFile = new File(scratchPath.toString(), "out");
    copyResource("/test.rect", inputFile1);
    copyResource("/test2.points", inputFile2);

    UserOptions opts = new UserOptions(inputFile1.getPath(),
        inputFile2.getPath(), "iformat[0]:envelope", "iformat[1]:point(1,2)", "separator:,",
        "-skipheader[1]", "predicate:contains", outputFile.getPath());
    SpatialOutputFormat.setOutputFormat(opts, "wkt");
    opts.assignAllInputsExceptOneOutput();
    SpatialJoin.run(opts, getSC().sc());

    assertFileExists(outputFile.getPath());
    String[] lines = readFilesInDirAsLines(outputFile);
    assertEquals(2, lines.length);
  }

}