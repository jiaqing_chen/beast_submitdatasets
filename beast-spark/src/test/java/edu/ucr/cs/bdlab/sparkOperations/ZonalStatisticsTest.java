/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations;

import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.io.CSVFeatureReader;
import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.io.IOException;

public class ZonalStatisticsTest extends SparkTest {
  static final double[] expectedCounts = {17,25,15,2,14,13,17,27,15,17,25,17,11,20,8,8,12,12,16,14,10,7,39,1,3,1,0,2,9,9,
      10,7,5,9,8,6,7,8,5,8,14,3,0,0,2,0,10,8,6,17,178};
  static final double[] expectedSums = {203,260,210,40,119,113,169,229,133,180,228,164,126,201,90,128,164,168,228,147,
      124,40,461,2,24,6,0,4,30,105,95,57,58,144,132,54,82,118,60,32,96,10,0,0,4,0,66,58,28,246,2326};

  public void testReprojectVector() throws IOException {
    File vectorInput = new File(scratchPath.toString(), "in.zip");
    File rasterInput = new File(scratchPath.toString(), "in.tif");
    File outputFile = new File(scratchPath.toString(), "out");
    copyResource("/raptor/ne_110m_admin_1_states_provinces_EPSG3857.zip", vectorInput);
    copyResource("/raptor/glc2000_small.tif", rasterInput);

    UserOptions opts = new UserOptions(vectorInput.getPath(),
        rasterInput.getPath(), "iformat[0]:shapefile", "rformat:geotiff", outputFile.getPath(), "oformat:wkt");
    opts.assignAllInputsExceptOneOutput();
    ZonalStatistics.run(opts, getSC().sc());

    assertFileExists(outputFile.getPath());
    // Read the output and verify
    opts = new UserOptions("iformat:wkt", "-skipheader");
    CSVFeatureReader reader = new CSVFeatureReader();
    try {
      reader.initialize(new Path(outputFile.getPath(), "part-r-00000.csv"), opts);
      int count = 0;
      while (reader.nextKeyValue()) {
        IFeature feature = reader.getCurrentValue();
        while (count < expectedCounts.length && expectedCounts[count] == 0.0)
          count++;
        assertEquals(expectedCounts[count], Double.parseDouble((String) feature.getAttributeValue("count")));
        assertEquals(expectedSums[count], Double.parseDouble((String) feature.getAttributeValue("sum")));
        count++;
      }
      assertEquals(expectedCounts.length, count);
    } finally {
      reader.close();
    }
  }

  public void testProcessHDFDirectory() throws IOException {
    File vectorInput = new File(scratchPath.toString(), "in.zip");
    File rasterDir = new File(scratchPath.toString(), "raster");
    rasterDir.mkdirs();
    File rasterInput = new File(rasterDir, "in.hdf");
    File outputFile = new File(scratchPath.toString(), "out");
    copyResource("/raptor/ne_110m_admin_1_states_provinces_EPSG3857.zip", vectorInput);
    copyResource("/raptor/MYD11A1.A2002185.h09v06.006.2015146150958.hdf", rasterInput);

    UserOptions opts = new UserOptions(vectorInput.getPath(),
        rasterInput.getPath(), "iformat[0]:shapefile", "rformat:hdf", "layer:LST_Day_1km", outputFile.getPath(), "oformat:wkt");
    opts.assignAllInputsExceptOneOutput();
    ZonalStatistics.run(opts, getSC().sc());

    assertFileExists(outputFile.getPath());
    // Read the output and verify
    opts = new UserOptions("iformat:wkt", "-skipheader");
    CSVFeatureReader reader = new CSVFeatureReader();
    try {
      reader.initialize(new Path(outputFile.getPath(), "part-r-00000.csv"), opts);
      int count = 0;
      while (reader.nextKeyValue())
        count++;
      // For this specific file, only Texas and Louisiana should overlap
      assertEquals(2, count);
    } finally {
      reader.close();
    }
  }
}