/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations;

import edu.ucr.cs.bdlab.io.CSVFeatureReader;
import edu.ucr.cs.bdlab.io.SpatialOutputFormat;
import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.io.IOException;

public class CatTest extends SparkTest {

  public void testConvertToRTree() throws IOException {
    String input = new Path(scratchPath, "test.input").toString();
    copyResource("/test111.points", new File(input));
    String output = new Path(scratchPath, "test.rtree").toString();
    UserOptions opts = new UserOptions(input, output, "iformat:point", "oformat:rtree");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    SpatialOutputFormat.setOutputFormat(opts, "rtree");
    opts.assignAndCheckInputsAndOutputs(new int[]{1, 1}, new int[]{0, 1});

    Cat.run(opts, getSC().sc());
    assertFileExists(output);

    // Now convert it back to CSV to make sure it was written correctly
    String rtreeFile = output;
    output = new Path(scratchPath, "converted_back.csv").toString();
    opts = new UserOptions(rtreeFile, output, "iformat:rtree", "oformat:point");
    opts.set(CSVFeatureReader.FieldSeparator, ",");
    opts.assignAndCheckInputsAndOutputs(new int[]{1, 1}, new int[]{0, 1});
    SpatialOutputFormat.setOutputFormat(opts, "point");
    Cat.run(opts, getSC().sc());
    assertFileExists(output);
    String[] lines = readFile(new File(new File(output), "part-r-00000.csv").getPath());
    assertEquals(111, lines.length);
  }
}