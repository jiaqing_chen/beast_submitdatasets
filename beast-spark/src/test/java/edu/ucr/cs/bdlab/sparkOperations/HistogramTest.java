/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations;

import edu.ucr.cs.bdlab.geolite.EmptyGeometry;
import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.Feature;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.io.CSVFeature;
import edu.ucr.cs.bdlab.io.SpatialOutputFormat;
import edu.ucr.cs.bdlab.stsynopses.AbstractHistogram;
import edu.ucr.cs.bdlab.stsynopses.EulerHistogram2D;
import edu.ucr.cs.bdlab.stsynopses.UniformHistogram;
import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaRDD;
import scala.Tuple2;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

public class HistogramTest extends SparkTest {

  public void testComputeTwoRoundsPoints() {
    JavaRDD<IFeature> points = getSC().parallelize(Arrays.asList( new IFeature[] {
        new Feature(new Point(2, 1.0, 1.0)),
        new Feature(new Point(2, 3.0, 3.0)),
    }));
    UniformHistogram h = Histogram.computeSimpleCountHistogramJ(points, 4);
    assertEquals(2, h.getNumPartitions(0));
    assertEquals(2, h.getNumPartitions(1));
    assertEquals(1, h.getValue(new int[] {0,0},new int[] {1,1}));
    assertEquals(0, h.getValue(new int[] {1,0},new int[] {1,1}));
    assertEquals(2, h.getValue(new int[] {0,0},new int[] {2,2}));
  }

  public void testComputeTwoRoundsPointsWithSize() {
    JavaRDD<Tuple2<Point, Long>> points = getSC().parallelize(Arrays.asList( new Tuple2[] {
        new Tuple2(new Point(2, 1.0, 1.0), 100L),
        new Tuple2(new Point(2, 3.0, 3.0), 50L)
    }));
    Envelope mbr = new Envelope(2, 1.0, 1.0, 3.0, 3.0);
    UniformHistogram h = Histogram.computeSimpleHistogramJ(points.mapToPair(pl -> pl), mbr, 2, 2);
    assertEquals(100, h.getValue(new int[] {0,0},new int[] {1,1}));
    assertEquals(0, h.getValue(new int[] {1,0},new int[] {1,1}));
    assertEquals(150, h.getValue(new int[] {0,0},new int[] {2,2}));
  }

  public void testComputeTwoRoundsPointsWithRandomPoints() {
    int numPoints = 1000;
    Feature[] points = new Feature[numPoints];
    Random random = new Random(1);
    for (int i = 0; i < points.length; i++)
      points[i] = new Feature(new Point(2, random.nextDouble() * 2, random.nextDouble()));
    JavaRDD<IFeature> pointsRDD = getSC().parallelize(Arrays.asList(points), 4);
    int numBuckets = 100;
    UniformHistogram h = Histogram.computeSimpleCountHistogramJ(pointsRDD, numBuckets);
    assertTrue(String.format("Histogram too big. %d buckets > %d", h.getNumPartitions(0) * h.getNumPartitions(1), numBuckets), h.getNumPartitions(0) * h.getNumPartitions(1) <= numBuckets);
    assertEquals(numPoints, h.getValue(new int[] {0, 0}, new int[] {h.getNumPartitions(0), h.getNumPartitions(1)}));
  }

  public void testShouldSkipEmptyGeometries() {
    IFeature[] arfeatures = {
        new CSVFeature(new Point(2, 1.0, 1.0)),
        new CSVFeature(new Point(2, 3.0, 3.0)),
        new CSVFeature(EmptyGeometry.instance),
    };
    JavaRDD<IFeature> features = getSC().parallelize(Arrays.asList(arfeatures));
    UniformHistogram h = Histogram.computeSimpleCountHistogramJ(features, 4);
    assertEquals(2, h.getNumPartitions(0));
    assertEquals(2, h.getNumPartitions(1));
    assertEquals(1, h.getValue(new int[] {0,0},new int[] {1,1}));
    assertEquals(0, h.getValue(new int[] {1,0},new int[] {1,1}));
    assertEquals(2, h.getValue(new int[] {0,0},new int[] {2,2}));
  }

  public void testShouldSkipEmptyGeometriesWithSizeHistogram() {
    IFeature[] arfeatures = {
        new CSVFeature(new Point(2, 1.0, 1.0)),
        new CSVFeature(new Point(2, 3.0, 3.0)),
        new CSVFeature(EmptyGeometry.instance),
    };
    JavaRDD<IFeature> features = getSC().parallelize(Arrays.asList(arfeatures));
    AbstractHistogram h = Histogram.computeSimpleSizeHistogramJ(features, new Envelope(2, 1.0, 1.0, 3.0, 3.0), 2, 2);
    assertEquals(2, h.getNumPartitions(0));
    assertEquals(2, h.getNumPartitions(1));
    assertEquals(2 * arfeatures[0].getStorageSize(), h.getValue(new int[] {0,0},new int[] {2,2}));
  }

  public void testHistogramWithWriteSize() {
    String[] wkts = {
        "POINT(2.0 2.0),xxxxxxx",
        "POINT(100.0 1.0)",
        "POINT(1.0 100.0)",
    };
    UserOptions opts = new UserOptions();
    opts.set(SpatialOutputFormat.OutputFormat, "wkt");
    JavaRDD<String> wktRDD = getSC().parallelize(Arrays.asList(wkts));
    JavaRDD<IFeature> features = SpatialReader.parseWKT(wktRDD, 0, ',');

    AbstractHistogram histogram = Histogram.computeSimpleWriteSizeHistogramJ(opts, features,
        new Envelope(2, 1.0, 1.0, 100.0, 100.0), 2, 2);
    assertEquals(23, histogram.getValue(new int[]{0, 0}, new int[] {1,1}));
  }

  public void testHistogramWithMemorySize() throws IOException {
    Path inPath = new Path(scratchPath, "input.points");
    copyResource("/test111.points", new File(inPath.toString()));

    UserOptions opts = new UserOptions(inPath.toString(), "iformat:point", "separator:,", "histogramvalue:size");
    opts.assignAllInputsAndNoOutputs();
    UniformHistogram h = (UniformHistogram) Histogram.run(opts, getSC().sc());
    assertEquals(1776, h.sumRectangle(-180, -90.0, 180.0, 90.0));
  }

  public void testComputeEulerHistogramWithOutOfBoundEnvelopes() {
    // This is used when computing the Euler histogram with visualization and some rectangles are out of map boundaries
    int numRecords = 1000;
    Feature[] envelopes = new Feature[numRecords + 1];
    Random random = new Random(0);
    for (int i = 0; i < envelopes.length; i++) {
      int x = random.nextInt(1000);
      int y = random.nextInt(1000);
      envelopes[i] = new Feature(new Envelope(2, x, y, x + 100, y + 100));
    }
    // Add a record that is completely out of bounds
    envelopes[numRecords] = new Feature(new Envelope(2, 2000, 2000, 2000 + 100, 2000 + 100));
    JavaRDD<IFeature> envelopesRDD = getSC().parallelize(Arrays.asList(envelopes), 4);
    Envelope mbr = new Envelope(2, 0, 0, 1000, 1000);
    EulerHistogram2D histogram = Histogram.computeEulerSizeHistogram(envelopesRDD, mbr, 10, 10);
    // Only the last record should be completely dropped. All other records should be counted
    assertEquals(numRecords * 8 * 4, histogram.getValue(0, 0, 10, 10));
  }
}