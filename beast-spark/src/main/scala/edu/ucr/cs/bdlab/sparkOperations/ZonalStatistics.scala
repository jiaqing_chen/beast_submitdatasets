/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations

import java.io.{IOException, PrintWriter}

import edu.ucr.cs.bdlab.geolite.{Feature, IFeature}
import edu.ucr.cs.bdlab.io.{CSVFeatureWriter, SpatialInputFormat, SpatialOutputFormat}
import edu.ucr.cs.bdlab.raptor.{Collector, GeoTiffReader, HDF4Reader, Intersections, RasterReader, Statistics}
import edu.ucr.cs.bdlab.util.{OperationMetadata, OperationParam, UserOptions}
import org.apache.commons.logging.{Log, LogFactory}
import org.apache.hadoop.fs.Path
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

@OperationMetadata(shortName = "zs",
  description = "Computes zonal statistics between a vector file and a raster file",
  inputArity = "2",
  outputArity = "1",
  inheritParams = Array(classOf[SpatialInputFormat], classOf[SpatialOutputFormat], classOf[ZonalStatisticsParams]))
object ZonalStatistics extends CLIOperation {
  @transient lazy val logger: Log = LogFactory.getLog(getClass)

  @throws(classOf[IOException])
  override def run(opts: UserOptions, sc : SparkContext): Unit = {
    val aggregateFunction = Symbol(opts.get(ZonalStatisticsParams.AggregateFunction, "aggregates"))
    val raster = new GeoTiffReader
    // Ensure that vector data is converted to the same CRS of the raster data
    // TODO extract the CRS from the raster file
    opts.set(SpatialInputFormat.TargetCRS, "EPSG:4326")
    val zones = SpatialReader.readInput(sc, opts, inputIndex=0)
    val iLayer = opts.get(ZonalStatisticsParams.RasterLayer, "0")
    val collectorClass: Class[_ <: Collector] = aggregateFunction match {
      case 'aggregates => classOf[Statistics]
      case other => throw new RuntimeException(s"Unrecognized aggregate function $other")
    }
    val method = Symbol(opts.get(ZonalStatisticsParams.ClipperMethod, "scanline"))
    val results: RDD[(IFeature, Collector)] = zonalStats(zones, opts.getInput(1), iLayer, method, collectorClass, opts)

    // Write results to the output
    val normalizedResults = results.filter(fc => fc._2 != null).map(fc => {
      val statistics: Statistics = fc._2.asInstanceOf[Statistics]
      val newFeature = new Feature(fc._1)
      newFeature.appendAttribute("sum", statistics.sum.mkString(","))
      newFeature.appendAttribute("count", statistics.count.mkString(","))
      newFeature.appendAttribute("min", statistics.min.mkString(","))
      newFeature.appendAttribute("max", statistics.max.mkString(","))

      newFeature
    })
    val oFormat = opts.get(SpatialOutputFormat.OutputFormat, opts.get(SpatialInputFormat.InputFormat))
    if (opts.get(CSVFeatureWriter.WriteHeader) == null)
      opts.setBoolean(CSVFeatureWriter.WriteHeader, true)
    SpatialWriter.saveFeatures(normalizedResults, oFormat, opts.getOutput, opts)
  }

  /**
    * Computes zonal statistics between a set of zones (polygons) and a raster file given by its path and a layer in
    * that file. The result is an RDD of pairs of a feature and a collector value
    * @param zones
    * @param rasterInput
    * @param iLayer
    * @param method
    * @param collectorClass
    * @param opts
    * @return
    */
  def zonalStats(zones: RDD[IFeature], rasterInput: String, iLayer: String, method: Symbol,
                 collectorClass: Class[_ <: Collector], opts: UserOptions):
    RDD[(IFeature, Collector)] = {
    val rasterPath: Path = new Path(rasterInput)
    val rasterFileSystem = rasterPath.getFileSystem(opts.loadIntoHadoopConfiguration(null))
    // Store raster files as a list of string because Hadoop Path is not serializable
    val rasterFiles: List[String] =
    if (rasterFileSystem.getFileStatus(rasterPath).isFile) {
      // A single file
      List(rasterPath.toString)
    } else {
      // A directory. Read all contained files
      val dirContents = rasterFileSystem.listStatus(rasterPath)
      dirContents.map(fileStatus => fileStatus.getPath.toString).toList
    }

    opts.setArrayPosition(1)
    val rasterFormat = opts.get(ZonalStatisticsParams.RasterFormat, "hdf")

    zones.mapPartitions(iFeatures => {
      // Store the vectors in an array to use them and output them later
      val features = iFeatures.toArray
      val geometries = features.map(f => f.getGeometry)
      if (rasterFormat.equals("hdf")) {
        geometries.foreach(g => HDF4Reader.wgsToSinusoidal(g))
      }

      val finalStats: Array[Collector] = Array.fill(geometries.length){null}

      // Open the raster files one by one
      val rasterPath: Path = new Path(rasterInput)
      val rasterFileSystem = rasterPath.getFileSystem(opts)
      val raster: RasterReader = rasterFormat match {
        case "geotiff" => new GeoTiffReader
        case "hdf" => new HDF4Reader
        case _other => throw new RuntimeException(s"Uncrecognized raster format '${_other}'")
      }

      for (rasterFile <- rasterFiles) {
        rasterFormat match {
          case "geotiff" => raster.asInstanceOf[GeoTiffReader].initialize(rasterFileSystem, new Path(rasterFile), iLayer.toInt)
          case "hdf" => raster.asInstanceOf[HDF4Reader].initialize(rasterFileSystem, new Path(rasterFile), iLayer)
        }

        // Compute zonal statistics
        val stats = method match {
          case 'scanline => edu.ucr.cs.bdlab.raptor.ZonalStatistics.computeZonalStatisticsScanline(raster, geometries, collectorClass)
          case 'qsplit => edu.ucr.cs.bdlab.raptor.ZonalStatistics.computeZonalStatisticsQuadSplit(raster, geometries, collectorClass)
          case 'masking => edu.ucr.cs.bdlab.raptor.ZonalStatistics.computeZonalStatisticsMasking(raster, geometries, collectorClass)
          case 'naive => edu.ucr.cs.bdlab.raptor.ZonalStatistics.computeZonalStatisticsNaive(raster, geometries, collectorClass)
        }

        // Combine the results of this file with previous files
        for (i <- 0 until stats.length) {
          if (finalStats(i) == null)
            finalStats(i) = stats(i)
          else if (stats(i) != null)
            finalStats(i).accumulate(stats(i))
        }
        raster.close()
      }

      // Combine the features with the zonal statistics
      val featureStats: Array[(IFeature, Collector)] = new Array(finalStats.length)
      for (i <- finalStats.indices)
        featureStats(i) = (features(i), finalStats(i))

      featureStats.iterator
    })
  }

  def printUsage(out: PrintWriter): Unit = {
    out.println("zs <raster file> <raster file> method:<method> layer:<layer> collector:<collector>")
  }
}
