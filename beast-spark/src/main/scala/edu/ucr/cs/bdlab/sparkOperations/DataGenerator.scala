/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations

import edu.ucr.cs.bdlab.geolite.{Envelope, Feature, GeometryType, IFeature, IGeometry, Point}
import edu.ucr.cs.bdlab.io.{FeatureWriter, SpatialOutputFormat}
import edu.ucr.cs.bdlab.sparkOperations.GeometricSummary.getClass
import edu.ucr.cs.bdlab.util.{CounterOutputStream, OperationMetadata, OperationParam, UserOptions}
import org.apache.commons.logging.{Log, LogFactory}
import org.apache.hadoop.conf.Configuration
import org.apache.spark.SparkContext
import org.apache.spark.api.java.JavaRDD
import org.apache.spark.rdd.RDD

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.Random

@OperationMetadata(
  shortName =  "generate",
  description = "Generate geospatial objects with given distribution",
  inputArity = "0",
  outputArity = "1",
  inheritParams = Array(classOf[SpatialOutputFormat])
)
object DataGenerator extends CLIOperation {
  @transient lazy val logger:Log = LogFactory.getLog(getClass)

  @OperationParam(
    description = "Total size of generated data in bytes",
    required = true
  )
  val Size = "size"

  @OperationParam(
    description = "Distribution name {uniform, dia, parcel}",
    required = true
  )
  val Distribution = "dist"

  @OperationParam(
    description = "Number of dimensions",
    defaultValue = "2"
  )
  val Dimensions = "dims"

  @OperationParam(
    description = "Type of geometries to generate {point, envelope}",
    required = false,
    defaultValue = "point"
  )
  val Type = "type"

  @OperationParam(
    description = "Upper bound of geometry's size. If upper bound is 0, we create point",
    defaultValue = "0.001"
  )
  val UpperBound = "upper_bound"

  @OperationParam(
    description = "Diagonal width only for diagonal distribution",
    defaultValue = "0.01"
  )
  val DiagonalWidth = "dia_width"

  @OperationParam(
    description = "Seed for random number generation",
    defaultValue = "random"
  )
  val RandomSeed = "seed"

  /**
    * Distributions for generated data
    */
  object DistributionType extends Enumeration {
    /** Uniform distribution */
    val Uniform = Value("uniform")
    /** Diagonal distribution */
    val Diagonal = Value("dia")
    /** Parcel distribution */
    val Parcel = Value("parcel")
    /** Gaussian distribution */
    val Gaussian = Value("gaussian")
  }

  private def getGeometryType(strType: String, defaultValue: GeometryType): GeometryType = {
    strType match {
      case "point" => GeometryType.POINT
      case "envelope" => GeometryType.ENVELOPE
      case _ => defaultValue
    }
  }


  /**
    * Generates a uniform geometry with the given parameters
    * @param random
    * @param geom the geometry to modify
    * @param upperBound
    */
  private def makeUniformGeometry(random:Random, geom: IGeometry, upperBound: Double): Unit = {
    geom.getType match {
      case GeometryType.POINT => {
        val point: Point = geom.asInstanceOf[Point]
        for ($d <- 0 until point.getCoordinateDimension)
          point.coords($d) = random.nextDouble
      }
      case GeometryType.ENVELOPE => {
        val envelope: Envelope = geom.asInstanceOf[Envelope]
        for ($d <- 0 until envelope.getCoordinateDimension) {
          envelope.minCoord($d) = random.nextDouble
          envelope.maxCoord($d) = envelope.minCoord($d) + random.nextDouble * upperBound
        }
      }
      case _ => throw new RuntimeException(s"Unsupported geometry type '${geom.getType()}'")
    }
  }

  private def makeDiagonalGeometry(random:Random, geom: IGeometry, upperBound: Double,
                                   diagonalWidth: Double): Unit = {
    geom.getType match {
      case GeometryType.POINT => {
        val point: Point = geom.asInstanceOf[Point]
        point.coords(0) = random.nextDouble
        for ($d <- 1 until point.getCoordinateDimension)
          point.coords($d) = point.coords(0) + random.nextDouble * diagonalWidth
      }
      case GeometryType.ENVELOPE => {
        val envelope: Envelope = geom.asInstanceOf[Envelope]
        envelope.minCoord(0) = random.nextDouble
        envelope.maxCoord(0) = envelope.minCoord(0) + random.nextDouble * upperBound
        for ($d <- 1 until envelope.getCoordinateDimension) {
          envelope.minCoord($d) = envelope.minCoord(0) + random.nextDouble * diagonalWidth
          envelope.maxCoord($d) = envelope.minCoord($d) + random.nextDouble * upperBound
        }
      }
      case _ => throw new RuntimeException(s"Unsupported geometry type '${geom.getType()}'")
    }
  }


  /**
    * Splits the given envelope into two envelopes at a random point
    * @param envelope
    * @return
    */
  private def randomSplit(random: Random, envelope: Envelope): Array[Envelope] = {
    // Split the envelope into 2 envelopes
    def splitEnvelopes: Array[Envelope] = Array.ofDim[Envelope](2)
    val nDims: Int = envelope.getCoordinateDimension()

    val tilingRange: Double = 0.6
    val minSplitValueRatio: Double = (1 -tilingRange) / 2
    val maxSplitValueRatio: Double = 1 - minSplitValueRatio
    val randomSplitRatio: Double = minSplitValueRatio + (maxSplitValueRatio - minSplitValueRatio) * random.nextDouble()

    // Randomly choose the axis to split
    var axis: Int = random.nextInt(nDims)
    if(nDims == 2) {
      if((envelope.maxCoord(0) - envelope.minCoord(0)) > (envelope.maxCoord(1) - envelope.minCoord(1)))
        axis = 0
      else
        axis = 1
    }
    val splitValue: Double = envelope.minCoord(axis) + (envelope.maxCoord(axis) - envelope.minCoord(axis)) * randomSplitRatio
    val splitMinCoord: Array[Double] = envelope.minCoord.clone
    splitMinCoord(axis) = splitValue
    val splitMaxCoord: Array[Double] = envelope.maxCoord.clone()
    splitMaxCoord(axis) = splitValue

    splitEnvelopes(0) = new Envelope(envelope.minCoord, splitMaxCoord)
    splitEnvelopes(1) = new Envelope(splitMinCoord, envelope.maxCoord)
    splitEnvelopes
  }


  /**
    * Generates splits using the parcel distribution to further process each part
    * @param random the random number generator
    * @param nDims number of dimensions
    * @param numSplits number of splits to generate
    * @return a list of envelopes for the generated splits
    */
  private def generateParcelSplits(random: Random, nDims: Int, numSplits: Int): List[Envelope] = {
    val minCoord: Array[Double] = Array.ofDim[Double](nDims)
    val maxCoord: Array[Double] = Array.ofDim[Double](nDims)
    for (i <- minCoord.indices) {
      minCoord(i) = 0.0
      maxCoord(i) = 1.0
    }
    val rootEnvelope: Envelope = new Envelope(minCoord, maxCoord)
    val queue: mutable.Queue[Envelope] = new mutable.Queue[Envelope]
    queue.enqueue(rootEnvelope)
    while (queue.length < numSplits) {
      val envelope: Envelope = queue.dequeue
      val splitEnvelopes: Array[Envelope] = randomSplit(random, envelope)
      for (i <- splitEnvelopes.indices)
        queue.enqueue(splitEnvelopes(i))
    }
    queue.toList
  }

  /**
    * Generates an RDD of geometries according to the given user parameters.
    * @param opts the generation user parameters
    * @param sc the spark context for creating the RDD
    * @return
    */
  def generateGeometries(opts: UserOptions, sc: SparkContext): RDD[IGeometry] = {
    // Extract index parameters from the command line arguments
    val size: Long = opts.getLongBytes(DataGenerator.Size, 1024L * 1024 * 1024)
    val distribution: DistributionType.Value = DistributionType.withName(opts.get(DataGenerator.Distribution, "uniform"))
    val numDimensions: Int = opts.getInt(DataGenerator.Dimensions, 2)
    val geometryType = getGeometryType(opts.get(DataGenerator.Type), GeometryType.POINT)
    val upperBound: Double = opts.getDouble(DataGenerator.UpperBound, 0.001)
    val diagonalWidth: Double = opts.getDouble(DataGenerator.DiagonalWidth, 0.01)
    val seed: Long = opts.getLong(RandomSeed, Random.nextLong)
    val random: Random = new Random(seed)
    distribution match {
      case DistributionType.Parcel => generateParcel(sc, size, numDimensions, upperBound, seed, random, opts)
      case DistributionType.Uniform | DistributionType.Diagonal =>
        generateUniformDiagonal(sc, size, distribution, numDimensions, geometryType, upperBound, diagonalWidth, seed, opts)
      case other => throw new RuntimeException(s"Unsupported distributed type '$other'")
    }
  }

  /**
    * Generates uniformly distributed or diagonal distribute data
    * @param sc
    * @param size
    * @param distribution
    * @param numDimensions
    * @param geometryType
    * @param upperBound
    * @param diagonalWidth
    * @param seed
    * @param conf
    * @return
    */
  def generateUniformDiagonal(sc: SparkContext, size: Long, distribution: DistributionType.Value,
                              numDimensions: Int, geometryType: GeometryType, upperBound: Double,
                              diagonalWidth: Double, seed: Long, conf: Configuration): RDD[IGeometry] = {
    // Generate random of splits so that each split will generate around 128 MB of data
    val numSplits: Int = Math.max(1, (size / (128L * 1024 * 1024 * 1024)).toInt)
    val featureWriterClass: Class[_ <: FeatureWriter] = SpatialOutputFormat.getConfiguredFeatureWriterClass(conf)
    val opts = new UserOptions(conf) // For serialization
    val splitSize = size / numSplits
    sc.parallelize(1 to numSplits, numSplits).flatMap(i => {
      val featureWriter = featureWriterClass.newInstance
      featureWriter.initialize(new CounterOutputStream, opts)
      val localRandom: Random = new Random(seed + i)
      val geometries: ListBuffer[IGeometry] = new ListBuffer[IGeometry]
      var totalSplitSize: Long = 0
      val feature: Feature = new Feature
      while(totalSplitSize < splitSize) {
        val geometry: IGeometry = geometryType.createInstance
        geometryType match {
          case GeometryType.POINT => geometry.asInstanceOf[Point].setCoordinateDimension(numDimensions)
          case GeometryType.ENVELOPE => geometry.asInstanceOf[Envelope].setCoordinateDimension(numDimensions)
          case other => throw new RuntimeException(s"Unsupported geometry type '$other'")
        }
        distribution match {
          case DistributionType.Uniform => makeUniformGeometry(localRandom, geometry, upperBound)
          case DistributionType.Diagonal => makeDiagonalGeometry(localRandom, geometry, upperBound, diagonalWidth)
          case other => throw new RuntimeException(s"Unsupported distributed type '$other'")
        }
        feature.setGeometry(geometry)
        totalSplitSize += featureWriter.estimateSize(feature)
        geometries += geometry
      }
      geometries
    })
  }

  def generateParcel(sc: SparkContext, size: Long, numDimensions: Int, upperBound: Double, seed: Long, random: Random,
                     conf: Configuration): RDD[IGeometry] = {
    // Use the configured feature writer class to estimate the output size
    val featureWriterClass: Class[_ <: FeatureWriter] = SpatialOutputFormat.getConfiguredFeatureWriterClass(conf)
    val opts = new UserOptions(conf) // For serialization
    // Generate random of splits so that each split will generate around 128 MB of data
    val numSplits: Int = Math.max(1, (size / (128L * 1024 * 1024 * 1024)).toInt)
    val splitEnvelopes: List[Envelope] = generateParcelSplits(random, numDimensions, numSplits)
    val splitSize: Long = size / numSplits
    sc.parallelize(splitEnvelopes).flatMap(envelope => {
      // Initialize the feature writer that we will use to estimate the size
      val featureWriter = featureWriterClass.newInstance
      featureWriter.initialize(new CounterOutputStream, opts)
      // Create a feature that we will use to pass to the feature writer
      val feature: Feature = new Feature
      val localRandom = new Random(seed)
      val geometries: mutable.Queue[Envelope] = new mutable.Queue[Envelope]
      geometries.enqueue(envelope)
      var totalSplitSize = 0

      while (totalSplitSize < splitSize) {
        val e: Envelope = geometries.dequeue()
        val twoEnvelopes: Array[Envelope] = randomSplit(localRandom, e)
        for (oneEnvelope <- twoEnvelopes) {
          geometries.enqueue(oneEnvelope)
          feature.setGeometry(oneEnvelope)
          totalSplitSize += featureWriter.estimateSize(feature)
        }
      }

      // Dither all envelopes
      val density: Double = 1.0
      geometries.foreach(e => {
        val width = e.getSideLength(0) * density
        for ($d <- 0 until numDimensions) {
          e.maxCoord($d) = e.minCoord($d) + (e.maxCoord($d) - e.minCoord($d)) * density
          e.maxCoord($d) += width * upperBound * random.nextDouble()
        }
      })
      geometries
    })
  }

  /**
    * Run the main function using the given user command-line options and spark context
    *
    * @param opts
    * @param sc
    * @return
    */
  override def run(opts: UserOptions, sc: SparkContext): Any = {
    val generatedGeoms:RDD[IGeometry] = generateGeometries(opts, sc)
    // Convert geometries to features
    val generatedFeatures:RDD[IFeature] = generatedGeoms.map(g => new Feature(g))
    SpatialWriter.saveFeatures(generatedFeatures, opts.get(SpatialOutputFormat.OutputFormat), opts.getOutput(), opts)
  }
}
