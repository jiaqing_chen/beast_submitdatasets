/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations

import java.io.{Externalizable, ObjectInput, ObjectOutput}

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter
import com.fasterxml.jackson.core.{JsonFactory, JsonGenerator}
import edu.ucr.cs.bdlab.geolite.IFeature
import edu.ucr.cs.bdlab.io.{FeatureWriter, SpatialInputFormat, SpatialOutputFormat}
import edu.ucr.cs.bdlab.stsynopses.Summary
import edu.ucr.cs.bdlab.util.{CounterOutputStream, OperationMetadata, UserOptions}
import org.apache.commons.logging.{Log, LogFactory}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.SparkContext
import org.apache.spark.api.java.JavaRDD
import org.apache.spark.rdd.RDD
import org.apache.spark.util.AccumulatorV2

/**
  * Computes the summary of the input features including some geometric attributes.
  */
@OperationMetadata(
  shortName =  "summary",
  description = "Computes the minimum bounding rectangle (MBR), count, and size of a dataset",
  inputArity = "1",
  outputArity = "0",
  inheritParams = Array(classOf[SpatialInputFormat], classOf[SpatialOutputFormat])
)
object GeometricSummary extends CLIOperation {
  @transient lazy val logger:Log = LogFactory.getLog(getClass)

  /**
    * Compute the MBR, count, and size of a JavaRDD of geometries
    * @param features the JavaRDD of features to compute the summary for
    * @return a Summary for the given features
    */
  def computeForFeaturesJ(features : JavaRDD[_ <: IFeature]) : Summary = computeForFeatures(features.rdd)

  /**
    * Compute the MBR, count, and size of an RDD of geometries
    *
    * @param features the JavaRDD of features to compute the summary for
    * @return a Summary for the given features
    */
  def computeForFeatures(features : RDD[_ <: IFeature]) : Summary = {
    val partialSummaries = features.mapPartitions(iFeatures => {
      val summary = new Summary
      while (iFeatures.hasNext) {
        val feature = iFeatures.next()
        if (summary.getCoordinateDimension == 0)
          summary.setCoordinateDimension(feature.getGeometry.getCoordinateDimension)
        summary.expandToFeature(feature)
      }
      Option(summary).iterator
    })
    partialSummaries.reduce((mbr1, mbr2) => mbr1.expandToSummary(mbr2))
  }

  /** Java shortcut */
  def computeForFeaturesWithOutputSize(features: JavaRDD[_ <: IFeature], opts: UserOptions) : Summary =
    computeForFeaturesWithOutputSize(features.rdd, opts)

  /**
    * Compute the summary of the given features while computing the size based on the write size of the configured
    * [[FeatureWriter]] (or output format) in the given user options
    * @param features features to compute the summary for
    * @param opts the user options that contains the configured output format
    * @return
    */
  def computeForFeaturesWithOutputSize(features : RDD[_ <: IFeature], opts : UserOptions) : Summary = {
    val writerClass = SpatialOutputFormat.getConfiguredFeatureWriterClass(opts)
    val partialSummaries = features.mapPartitions(iFeatures => {
      val summary = new Summary
      // Create the feature writer that will compute the output size
      val featureWriter = writerClass.newInstance
      val counter = new CounterOutputStream
      featureWriter.initialize(counter, opts)
      // Iterate over features and accumulate them
      while (iFeatures.hasNext) {
        // Initialize the summary if not initialized yet
        val feature = iFeatures.next
        if (summary.getCoordinateDimension == 0)
          summary.setCoordinateDimension(feature.getGeometry.getCoordinateDimension)
        // Accumulate the geometry of the feature
        summary.expandToGeometryWithSize(feature.getGeometry, 0)
        // Accumulate the size based on the feature writer
        summary.size += featureWriter.estimateSize(feature)
      }
      featureWriter.close(null)
      Option(summary).iterator
    })
    partialSummaries.reduce((mbr1, mbr2) => mbr1.expandToSummary(mbr2))
  }

  /**
    * Create a summary accumulator that uses the method [[IFeature#getStorageSize]] to accumulate the sizes of
    * the features.
    *
    * @param sc the spark context to register the accumulator to
    * @return the initialized and registered accumulator
    */
  def createSummaryAccumulator(sc: SparkContext) : SummaryAccumulator = {
    val accumulator = new SummaryAccumulator
    sc.register(accumulator, "SummaryAccumulator")
    accumulator
  }

  /**
    * Create a summary accumulator that uses the configured output format to measure the size of the output.
    *
    * @param sc the spark context to register the accumulator to
    * @param opts user options that contain information about which FeatureWriter (or output format) to use
    * @return the initialized and registered accumulator
    */
  def createSummaryAccumulatorWithWriteSize(sc: SparkContext, opts : UserOptions) : SummaryAccumulator = {
    val writerClass = SpatialOutputFormat.getConfiguredFeatureWriterClass(opts)
    val accumulator = new SummaryAccumulator(writerClass, opts)
    sc.register(accumulator, name="SummaryAccumulator")
    accumulator
  }

  /**
    * An accumulator that computes the MBR of a dataset, the number of features, and its total size.
    * Keep in mind the following points:
    * <ul>
    * <li>It is the responsibility of the caller to use the accumulator correctly. It should be used either in
    * an action, or in a transformation that gets applies.</li>
    * <li>If applied in a transformation, the accumulator might be applied multiple times. This will affect the
    * [[Summary#numFeatures]] and [[Summary#size]] values but the MBR will be accurate if applied many times.
    * From Spark's documentation:
    * <li>For accumulator updates performed inside actions only, Spark guarantees that each task’s update to the
    * accumulator will only be applied once, i.e. restarted tasks will not update the value. In transformations,
    * users should be aware of that each task’s update may be applied more than once if tasks or job stages are
    * re-executed.</li>
    * <li>If an accurate result is vital, use one of the methods [[computeForFeatures()]]
    * or [[computeForFeaturesWithOutputSize()]] instead.</li>
    * </ul>
    */
  class SummaryAccumulator(_writerClass: Class[_ <: FeatureWriter] = null,
                           _conf: UserOptions = null) extends AccumulatorV2[IFeature, Summary] {
    /**A wrapper around feature writer that closes it automatically before this accumulator is serialized*/
    val _writer = new AutoCloseWrapper

    /**The internal summary value*/
    val summary = new Summary

    /**Configuration of the environment*/
    val conf:Configuration = if (_conf != null) new UserOptions(_conf) else new UserOptions()

    /**If the size is calculated based on output size, this stores the feature writer class to use. Otherwise, null*/
    var writerClass:Class[_ <: FeatureWriter] = _writerClass

    /**Used to count the output size if the write output size is needed*/
    val counter = new CounterOutputStream

    def getOrCreateWriter() : FeatureWriter = {
      if (writerClass != null && _writer.wrapped == null) {
        // Initialize the wrapped writer using the writer class
        _writer.wrapped = writerClass.newInstance
        _writer.wrapped.initialize(counter, conf)
      }
      _writer.wrapped
    }

    override def isZero: Boolean = summary.isEmpty

    override def copy(): AccumulatorV2[IFeature, Summary] = {
      val other = new SummaryAccumulator()
      other.summary.setCoordinateDimension(this.summary.getCoordinateDimension)
      other.summary.expandToSummary(this.summary)
      other.writerClass = this.writerClass
      other.conf.clear()
      other.conf.addResource(this.conf)
      other
    }

    override def reset(): Unit = {
      summary.setEmpty()
      counter.reset()
    }

    override def add(f: IFeature): Unit = {
      if (summary.getCoordinateDimension == 0)
        summary.setCoordinateDimension(f.getGeometry.getCoordinateDimension)
      val writer = getOrCreateWriter()
      if (writer == null) {
        // No writer class, use the regular expand based on memory size
        summary.expandToFeature(f)
      } else {
        // Do no accumulate the size and use the writer instead
        summary.expandToGeometryWithSize(f.getGeometry, 0)
        summary.size += writer.estimateSize(f)
      }
    }

    override def merge(other: AccumulatorV2[IFeature, Summary]): Unit = {
      val otherAcc = other.asInstanceOf[SummaryAccumulator]
      otherAcc.copyActualValue()
      if (this.summary.isEmpty)
        this.summary.setCoordinateDimension(otherAcc.summary.getCoordinateDimension)
      this.summary.expandToSummary(otherAcc.summary)
    }

    override def value: Summary = {
      copyActualValue()
      summary
    }

    def copyActualValue(): Unit = {
      if (_writer.wrapped != null) {
        _writer.wrapped.close(null)
        _writer.wrapped = null
      }
      counter.reset()
    }
  }

  /**
    * A wrapper around a feature writer that automatically closes it upon serialization.
    * This is used to ensure that any pending writes are finalized before the accumulator is serialized
    */
  class AutoCloseWrapper extends Externalizable {
    var wrapped: FeatureWriter = _

    override def writeExternal(out: ObjectOutput): Unit = {
      if (this.wrapped != null) {
        this.wrapped.close(null)
        this.wrapped = null
      }
    }

    override def readExternal(in: ObjectInput): Unit = {}
  }

  /**
    * Writes the summary of the dataset in JSON format
    * @param jsonGenerator the generate to write the output to
    * @param summary the summary of the dataset
    * @param feature a sample feature to get the attribute names and types
    */
  def writeDatasetSchema(jsonGenerator: JsonGenerator, summary: Summary, feature: IFeature): Unit = {
    jsonGenerator.writeStartObject
    // Write MBR
    jsonGenerator.writeFieldName("extent")
    jsonGenerator.writeStartArray(4)
    jsonGenerator.writeNumber(summary.minCoord(0))
    jsonGenerator.writeNumber(summary.minCoord(1))
    jsonGenerator.writeNumber(summary.maxCoord(0))
    jsonGenerator.writeNumber(summary.maxCoord(1))
    jsonGenerator.writeEndArray
    // Write sizes and number of records
    jsonGenerator.writeNumberField("size", summary.getDataSize)
    jsonGenerator.writeNumberField("num_features", summary.numFeatures)
    jsonGenerator.writeNumberField("num_points", summary.getNumPoints)
    // Write average side length
    jsonGenerator.writeFieldName("avg_sidelength")
    jsonGenerator.writeStartArray(2)
    jsonGenerator.writeNumber(summary.getAverageSideLength(0))
    jsonGenerator.writeNumber(summary.getAverageSideLength(1))
    jsonGenerator.writeEndArray
    // Write geometry type
    jsonGenerator.writeFieldName("geometry_type")
    jsonGenerator.writeString(summary.getGeometryType.toString)


    if (feature.getNumAttributes > 0) {
      jsonGenerator.writeFieldName("attributes")
      jsonGenerator.writeStartArray(feature.getNumAttributes)
      for (i <- 0 until feature.getNumAttributes) {
        jsonGenerator.writeStartObject
        var fieldName = feature.getAttributeName(i)
        if (fieldName == null || fieldName.length == 0)
          fieldName = "attribute#" + i
        jsonGenerator.writeStringField("name", fieldName);
        val attValue = feature.getAttributeValue(i)
        if (attValue != null) {
          val fieldType = attValue.getClass.getSimpleName match {
            case "String" => "string"
            case "Integer" | "Long" => "integer"
            case "Float" | "Double" => "number"
            case "Boolean" => "boolean"
            case _ => "unknown"
          }
          jsonGenerator.writeStringField("type", fieldType)
        }
        jsonGenerator.writeEndObject // End of attribute type
      }
      jsonGenerator.writeEndArray // End of attributes
    }
    jsonGenerator.writeEndObject // End of root object
  }

  /**
    * Run the main function using the given user command-line options and spark context
    *
    * @param opts user options and configuration
    * @param sc the spark context to use
    * @return the summary computed for the input defined in the user options
    */
  override def run(opts: UserOptions, sc: SparkContext): Any = {
    val inputFeatures = SpatialReader.readInput(sc, opts)
    val summary = computeForFeaturesWithOutputSize(inputFeatures, opts)
    val inputPath: Path = opts.getInputPaths()(0)
    val configuration: Configuration = opts.loadIntoHadoopConfiguration(null)
    val fileSystem: FileSystem = inputPath.getFileSystem(configuration)
    val feature: IFeature = SpatialInputFormat.readOne(fileSystem, inputPath, configuration)
    val jsonGenerator: JsonGenerator = new JsonFactory().createGenerator(System.out)
    jsonGenerator.setPrettyPrinter(new DefaultPrettyPrinter)
    writeDatasetSchema(jsonGenerator, summary, feature)
    jsonGenerator.close
    System.out.println
    summary
  }
}
