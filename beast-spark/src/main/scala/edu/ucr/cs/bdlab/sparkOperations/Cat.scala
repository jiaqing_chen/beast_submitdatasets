/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations
import edu.ucr.cs.bdlab.io.{SpatialInputFormat, SpatialOutputFormat}
import edu.ucr.cs.bdlab.util.{OperationMetadata, UserOptions}
import org.apache.commons.logging.{Log, LogFactory}
import org.apache.spark.SparkContext

@OperationMetadata(
  shortName =  "cat",
  description = "Writes a file to the output. Used to convert file format",
  inputArity = "1",
  outputArity = "1",
  inheritParams = Array(classOf[SpatialInputFormat], classOf[SpatialOutputFormat])
)
object Cat extends CLIOperation {
  @transient lazy val logger:Log = LogFactory.getLog(getClass)

  override def run(opts: UserOptions, sc: SparkContext): Any = {
    // Read the input features and create pairs to be able to use Hadoop output format
    val features = SpatialReader.readInput(sc, opts)
    // Store to the output
    val oFormat = opts.get(SpatialOutputFormat.OutputFormat, opts.get(SpatialInputFormat.InputFormat))
    SpatialWriter.saveFeatures(features, oFormat, opts.getOutput, opts)
  }
}
