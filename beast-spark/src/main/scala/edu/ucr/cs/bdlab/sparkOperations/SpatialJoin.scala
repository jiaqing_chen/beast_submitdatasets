/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.sparkOperations

import java.io.IOException
import java.util.function.BiConsumer

import edu.ucr.cs.bdlab.cg.SpatialJoinAlgorithms
import edu.ucr.cs.bdlab.geolite.{Envelope, IFeature}
import edu.ucr.cs.bdlab.io.{SpatialInputFormat, SpatialOutputFormat}
import edu.ucr.cs.bdlab.util.{OperationMetadata, OperationParam, UserOptions}
import org.apache.commons.logging.{Log, LogFactory}
import org.apache.spark.SparkContext
import org.apache.spark.api.java.{JavaPairRDD, JavaRDD}
import org.apache.spark.rdd.RDD

import scala.collection.mutable.ListBuffer

@OperationMetadata(shortName = "sj",
  description = "Computes spatial join that finds all overlapping features from two files.",
  inputArity = "2",
  outputArity = "?",
  inheritParams = Array(classOf[SpatialInputFormat], classOf[SpatialOutputFormat]))
object SpatialJoin extends CLIOperation {
  @transient lazy val logger: Log = LogFactory.getLog(getClass)

  @OperationParam(description = "The spatial join algorithm to use. Currently, only 'bnlj' is supported.", defaultValue = "bnlj")
  val SpatialJoinMethod = "method"

  @OperationParam(description = "The spatial predicate to use in the join. Supported predicates are {intersects, contains}", defaultValue = "intersects")
  val SpatialJoinPredicate = "predicate"

  @OperationParam(description = "Overwrite the output file if it exists", defaultValue = "true")
  val OverwriteOutput = "overwrite"

  @throws(classOf[IOException])
  override def run(opts: UserOptions, sc : SparkContext): Unit = {
    val joinPredicate = Symbol(opts.get(SpatialJoinPredicate, "intersects"))
    val f1rdd = SpatialReader.readInput(sc, opts, inputIndex=0)
    val f2rdd = SpatialReader.readInput(sc, opts, inputIndex=1)
    val joinResults = spatialJoinBNLJ(f1rdd, f2rdd, joinPredicate)
    joinResults.map(f1f2 => f1f2._1.toString + f1f2._2.toString).saveAsTextFile(opts.getOutput)
  }

  /**Java shortcut*/
  def spatialJoinBNLJ(r1: JavaRDD[IFeature], r2: JavaRDD[IFeature],
                      joinPredicate: String): JavaPairRDD[IFeature, IFeature] =
    JavaPairRDD.fromRDD(spatialJoinBNLJ(r1.rdd, r2.rdd, Symbol(joinPredicate)))

  /**
    * Runs a spatial join between the two given RDDs using the block-nested-loop join algorithm.
    *
    * @param r1 the first set of features
    * @param r2 the second set of features
    * @param joinPredicate the predicate that joins a feature from r1 with a feature in r2
    * @return
    */
  def spatialJoinBNLJ(r1: RDD[IFeature], r2: RDD[IFeature], joinPredicate: Symbol) : RDD[(IFeature, IFeature)] = {
    // Convert the two RDD to arrays
    val f1 = r1.glom()
    val f2 = r2.glom()

    // Combine them using the Cartesian product (as in block nested loop)
    val f1f2 = f1.cartesian(f2)

    // For each pair of blocks, run the spatial join algorithm
    f1f2.flatMap(p1p2 => {
      // Extract the two arrays of features
      val p1 = p1p2._1
      val p2 = p1p2._2

      // Compute the minimum bounding rectangle (MBR) of each list
      val numDimensions = p1(0).getGeometry.getCoordinateDimension

      var mbr = new Envelope(numDimensions)
      val minCoords1 = Array.ofDim[Double](numDimensions, p1.length)
      val maxCoords1 = Array.ofDim[Double](numDimensions, p1.length)
      for (i <- p1.indices) {
        p1(i).getGeometry.envelope(mbr)
        for (d <- 0 until numDimensions)
        {
          minCoords1(d)(i) = mbr.minCoord(d)
          maxCoords1(d)(i) = mbr.maxCoord(d)
        }
      }

      val minCoords2 = Array.ofDim[Double](numDimensions, p2.length)
      val maxCoords2 = Array.ofDim[Double](numDimensions, p2.length)
      for (i <- p2.indices) {
        p2(i).getGeometry.envelope(mbr)
        for (d <- 0 until numDimensions) {
          minCoords2(d)(i) = mbr.minCoord(d)
          maxCoords2(d)(i) = mbr.maxCoord(d)
        }
      }

      // Initialize the empty result list
      var result = new ListBuffer[(IFeature, IFeature)]()

      // Create the refine function
      var refine: (Int, Int) => Unit =
        joinPredicate match {
          case 'contains => (i1: Int, i2: Int) =>
            if (p1(i1).getGeometry.contains(p2(i2).getGeometry))
              result += ((p1(i1), p2(i2)))
          case 'intersects => (i1: Int, i2: Int) =>
            if (p1(i1).getGeometry.intersects(p2(i2).getGeometry))
              result += ((p1(i1), p2(i2)))
          // For MBR intersects, no refine step is needed. Write the results directly to the output
          case 'mbrintersects => (i1: Int, i2: Int) => result += ((p1(i1), p2(i2)))
        }

      // TODO: Is there a way to avoid creating an explicit BiConsumer
      // Call the plane-sweep join algorithm
      SpatialJoinAlgorithms.planeSweepRectangles(minCoords1, maxCoords1, minCoords2, maxCoords2, new BiConsumer[Integer, Integer] {
        def accept(i1 : Integer, i2 : Integer) {
          refine(i1, i2)
        }
      })
      result
    })
  }
}
