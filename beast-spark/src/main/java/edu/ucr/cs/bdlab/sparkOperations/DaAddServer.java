package edu.ucr.cs.bdlab.sparkOperations;

import edu.ucr.cs.bdlab.util.OperationParam;
import edu.ucr.cs.bdlab.util.OperationMetadata;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.spark.SparkContext;
import org.mortbay.jetty.Server;

import org.bson.Document;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class DaAddServer extends WebHandler {

//    @OperationParam(
//            description = "Sender's email address",
//            required = true
//            //required = false,
//            //defaultValue = "chenjiaqingtest@outlook.com"
//    )
//    public static String Email = "email";
//
//    @OperationParam(
//            description = "Sender's email password",
//            required = true
//            //required = false,
//            //defaultValue = "chen950815"
//    )
//    public static String Password = "password";
//
//    /**Sender's email account*/
//    protected String email;
//
//    /**Sender's email password*/
//    protected String password;

    @Override public void setup(SparkContext sc, UserOptions opts) {
        super.setup(sc, opts);
        try{
            this.email = opts.get(BeastServer.Email);
            this.password = opts.get(BeastServer.Password);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //Set the data parameters obtained from the front-end
    public static String datasetName;
    public static String datasetDescription;
    public static String urls;
    public static String urlType;
    public static String publisher;
    public static String contactEmail;
    public static String homepage;
    public static String format;
    public static String license;
    public static String xcolumn;
    public static String ycolumn;
    public static String separatorSymbol;
    public static String tags;
    public static Document dataDocument;
    public static String id_number;
    public static String email;
    public static String password;

    //Create a new Document including all the data parameters obtained from the front-end
    public Document createDocument(String datasetName, String datasetDescription, String urls, String URLstype, String publisher,
                                   String contactEmail, String homepage, String format, String license, String xcolumn,
                                   String ycolumn, String separatorSymbol, String tags){
        Document document = new Document("Dataset name", datasetName).
                append("Description", datasetDescription).
                append("URL", urls).
                append("URL Type", URLstype).
                append("Publisher", publisher).
                append("Email", contactEmail).
                append("Homepage", homepage).
                append("License", license).
                append("Format", format).
                append("X column", xcolumn).
                append("Y column", ycolumn).
                append("Separator Symbol", separatorSymbol).
                append("Tags", tags).
                append("Status", "created");
        return document;
    }

    @WebMethod(url="/dynamic/submit-dataset.cgi/")
    public boolean main(String target, HttpServletRequest request, HttpServletResponse response) throws IOException{
        request.setCharacterEncoding("UTF-8");
        datasetName= request.getParameter("datasetName");
        datasetDescription = request.getParameter("datasetDescription");
        urls = request.getParameter("URLs");
        publisher = request.getParameter("publisher");
        contactEmail = request.getParameter("contactEmail");
        homepage = request.getParameter("homepage");
        format = request.getParameter("format");
        license = request.getParameter("license");
        xcolumn = request.getParameter("xcolumn");
        ycolumn = request.getParameter("ycolumn");
        separatorSymbol = request.getParameter("separatorSymbol");
        tags = request.getParameter("tags");

        //determine the URLs type
        if(urls.indexOf("drive.google.com") != -1){
            urlType = "GoogleDriveLink";
        }else if(urls.indexOf("onedrive.live.com") != -1){
            urlType = "OneDriveLink";
        }else{
            urlType = "DirectLink";
        }

        //Convert the urls to a format stored in the mongoDB database
        String database_urls = urls.replaceAll("\r\n"," ");

        //Create the document
        dataDocument = createDocument(datasetName,datasetDescription,database_urls,urlType,publisher,contactEmail,homepage,format,license,xcolumn,ycolumn,separatorSymbol,tags);

        // Insert the new document into the mongoDB database
        id_number = MongoDBOperation.InsertDocument("myDatabase","DataInfo",dataDocument);

        //Give relevant feedback to the front-end
        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType("plain-text");
        PrintWriter writer = response.getWriter();
        writer.println("You have successfully submitted a request for the data set named "+ datasetName);
        writer.println("The id of your submission is " + id_number);
        writer.close();
        String[] inputProcess = new String[]{email,password};
        DaProcess.main(inputProcess);
        return true;
    }
}