package edu.ucr.cs.bdlab.sparkOperations;

import java.io.File;
import java.util.*;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;

public class SendEmail {

    public static final String password = "password";

    // Email address and password
    public static String myEmailAccount;
    public static String myEmailPassword;

    //Set the path saving the error information files
    public static String errorfilespath = DaAddServer.id_number + "_error/";

    // SMTP server address
    public static String myEmailSMTPHost = "smtp.office365.com";

    // Recipient Email address
    public static String receiveMailAccount = "jchen632@ucr.edu";

    public static boolean main(String[] args) throws Exception {
        //Two input parameters are the sender's email and password
        myEmailAccount = args[0];
        myEmailPassword = args[1];

        // Create parameter configurations for connecting to Email servers
        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.smtp.host", myEmailSMTPHost);
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.port", "587");
        props.setProperty("mail.smtp.starttls.enable", "true");

        // Create session objects based on configuration to interact with Email servers
        Session session = Session.getInstance(props);
        session.setDebug(true);  // Set it to debug mode to view the detailed sending log

        // Create an Email
        MimeMessage message = createMimeMessage(session, myEmailAccount, receiveMailAccount);

        // Getting Email transfer objects according to Session
        Transport transport = session.getTransport();

        // Connecting Mail Server with Email address and password
        transport.connect(myEmailAccount, myEmailPassword);

        // Send mail to all listing Email addresses
        transport.sendMessage(message, message.getAllRecipients());

        // Close connection
        transport.close();

        // Return the successful status
        return true;
    }

    public static MimeMessage createMimeMessage(Session session, String sendMail, String receiveMail) throws Exception {
        // Create an email
        MimeMessage message = new MimeMessage(session);
        // From
        message.setFrom(new InternetAddress(sendMail, "UCR STAR", "UTF-8"));
        // To
        message.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(receiveMail, "USER", "UTF-8"));
        // Subject and Content
        if(DaProcess.flag == true){
            message.setSubject("Dataset " + DaAddServer.datasetName + "(ID# " + DaAddServer.id_number + ") processed Successfully", "UTF-8");
            message.setContent("Dear USER,</br></br>Thank you for adding your dataset to UCR STAR open archive. " +
                            "Your dataset has been processed successfully.</br></br>The index path is</br></br>" + DaProcess.path +
                    "/" + DaAddServer.id_number + "_index"+
                    "</br></br>The visualization path is</br></br>" + DaProcess.path + "/" + DaAddServer.id_number + "_plot" +
                    "</br></br>The ID of the dataset is</br></br>" + DaAddServer.id_number +
                    "</br></br>If you have any questions or suggestions, please feel free to contact our team. " +
                    "We are very glad to help.</br></br>Best wishes,</br>UCR STAR team", "text/html;charset=UTF-8");
        }
        else{
            message.setSubject("Error processing Dataset " + DaAddServer.datasetName + "(ID# " + DaAddServer.id_number + ")", "UTF-8");
            Multipart multipart = new MimeMultipart();
            BodyPart contentPart = new MimeBodyPart();
            contentPart.setContent("Dear USER,</br></br>Thank you for adding your dataset to UCR STAR open archive. " +
                    "However, there is an error processing your dataset.</br></br>In the step</br></br>" + DaProcess.status +"</br></br>The command is</br></br>" +
                    DaProcess.command + "</br></br>The command exit code is</br></br>" + DaProcess.exitCode +
                    "</br></br>For more details, please check the attachment." +
                    "</br></br>If you have any questions or suggestions, please feel free to contact our team. " +
                    "We are very glad to help.</br></br>Best wishes,</br>UCR STAR team", "text/html;charset=UTF-8");
            multipart.addBodyPart(contentPart);
            if(DaProcess.status=="Summary"||DaProcess.status=="Index"||DaProcess.status=="Mplot"){
                File ExcepStream = new File(errorfilespath + "ExceptionError.txt");
                BodyPart attachmentPart_excep = new MimeBodyPart();
                DataSource source_input = new FileDataSource(ExcepStream);
                attachmentPart_excep.setDataHandler(new DataHandler(source_input));
                attachmentPart_excep.setFileName(MimeUtility.encodeWord(ExcepStream.getName()));
                multipart.addBodyPart(attachmentPart_excep);
            }else{
                File InputStream = new File(errorfilespath + "InputStream.txt");
                File ErrorStream = new File(errorfilespath + "ErrorStream.txt");
                BodyPart attachmentPart_input = new MimeBodyPart();
                DataSource source_input = new FileDataSource(InputStream);
                attachmentPart_input.setDataHandler(new DataHandler(source_input));
                attachmentPart_input.setFileName(MimeUtility.encodeWord(InputStream.getName()));
                multipart.addBodyPart(attachmentPart_input);
                BodyPart attachmentPart_error = new MimeBodyPart();
                DataSource source_error = new FileDataSource(ErrorStream);
                attachmentPart_error.setDataHandler(new DataHandler(source_error));
                attachmentPart_error.setFileName(MimeUtility.encodeWord(ErrorStream.getName()));
                multipart.addBodyPart(attachmentPart_error);
            }
            message.setContent(multipart);
        }
        // Date
        message.setSentDate(new Date());
        // Save
        message.saveChanges();
        return message;
    }
}