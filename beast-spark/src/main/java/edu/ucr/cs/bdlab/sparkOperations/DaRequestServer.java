package edu.ucr.cs.bdlab.sparkOperations;

import com.mongodb.util.JSON;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class DaRequestServer extends WebHandler {

    //Set the type of the request and the ID of the dataset
    public String requestType;
    public String datasetID;
    public List<Object> documentList;
    public String documentString;

    //Change the JSON string into a specific format
    public static String format(String original_jsonStr) {
        int level = 0;
        int layer = 0;
        String jsonStr = original_jsonStr.substring(1,original_jsonStr.length()-1);
        //Pre-processing of the input JSON string
        jsonStr = jsonStr.replace(" {","{").replace("} ","}");
        StringBuffer jsonForMatStr = new StringBuffer();
        jsonForMatStr.append("[");
        for(int i=0;i<jsonStr.length();i++){
            char c = jsonStr.charAt(i);
            if(level>0&&'\n'==jsonForMatStr.charAt(jsonForMatStr.length()-1)){
                jsonForMatStr.append(getLevelStr(level));
            }
            //Specific formatting for different characters
            switch (c) {
                case '{':
                    jsonForMatStr.append(c+"\n\t");
                    if(layer > 0){
                        jsonForMatStr.append("\t");
                    }
                    layer++;
                    break;
                case '[':
                    if(level>0) {
                        jsonForMatStr.append("\n\t");
                    }
                    jsonForMatStr.append(c);
                    level++;
                    break;
                case ',':
                    jsonForMatStr.append(c+"\n\t");
                    break;
                case '}':
                    if(layer > 0){
                        jsonForMatStr.append("\n\t");
                    }
                    jsonForMatStr.append(c);
                    layer--;
                    break;
                case ']':
                    jsonForMatStr.append("\n");
                    level--;
                    jsonForMatStr.append(getLevelStr(level));
                    jsonForMatStr.append(c);
                    break;
                default:
                    jsonForMatStr.append(c);
                    break;
            }
        }
        jsonForMatStr.append("]");
        String result = jsonForMatStr.toString();
        //Final processing of processed strings according to format requirements
        result = result.replace("\t},\n\t{","},\n{").replace("\t}]","}]");
        return result;
    }

    private static String getLevelStr(int level) {
        StringBuffer levelStr = new StringBuffer();
        for (int levelI = 0; levelI < level; levelI++) {
            levelStr.append("\t");
        }
        return levelStr.toString();
    }

    @WebMethod(url = "/dynamic/get-dataset.cgi/")
    public boolean main(String target, HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setCharacterEncoding("UTF-8");

        //Get the type and the ID from the front-end
        requestType = request.getParameter("type");
        datasetID = request.getParameter("datasetID");

        System.out.println(requestType);
        System.out.println(datasetID);

        if(requestType.equals("all_datasets")){ //Get all the datasets with key information
            documentList = MongoDBOperation.FindCollection("myDatabase","DataInfo");
        }else{ //Get all the information of one specific dataset
            documentList = MongoDBOperation.FindDocument("myDatabase","DataInfo",datasetID);
        }

        //Give relevant feedback to the front-end
        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType("application/json");
        PrintWriter writer = response.getWriter();
        //Change the JSON Objects to JSON String
        documentString = JSON.serialize(documentList);
        writer.println(documentString);
        writer.close();
        return true;
    }
}