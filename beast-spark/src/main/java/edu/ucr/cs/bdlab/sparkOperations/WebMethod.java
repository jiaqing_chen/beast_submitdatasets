package edu.ucr.cs.bdlab.sparkOperations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
  * Annotates methods that handles a web request. The URL defines the URL path to access this method and
  * the format of any parameters given as part of it.
  */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface WebMethod{
  /**
   * the path on which this web method can be accessed from a web request. If not set, the name of the method will
   * be used.
   */
  String url() default "";

  /**
   * the order on which this method should be matched. The higher the number, the latter this
   * method will be considered while matching. Ties will be resolved using the alphabetical
   * order of the class#method name
   */
  int order() default 1;

  /**
   * Whether this web method should be handled in asynchronous mode
   */
  boolean async() default false;
}
