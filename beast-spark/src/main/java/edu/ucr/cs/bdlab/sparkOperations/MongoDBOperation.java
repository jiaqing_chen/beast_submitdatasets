package edu.ucr.cs.bdlab.sparkOperations;

import com.mongodb.*;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;
import org.bson.Document;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.types.ObjectId;
import java.util.ArrayList;
import java.util.List;

public class MongoDBOperation {

    // Connect to a specific database
    public static void ConnectDatabase(String databaseName) {
        try {
            // Connect to the MongoDB Service
            MongoClient mongoClient = new MongoClient("localhost", 27017);

            // Connect to the database
            MongoDatabase mongoDatabase = mongoClient.getDatabase(databaseName);
            System.out.println("Connect to Database Successfully");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    // Create a specific collection in a specific database
    public static void CreateCollection(String databaseName, String collectionName) {
        try {
            // Connect to the MongoDB Service
            MongoClient mongoClient = new MongoClient("localhost", 27017);

            // Connect to the database
            MongoDatabase mongoDatabase = mongoClient.getDatabase(databaseName);
            System.out.println("Connect to Database Successfully");

            // Create a collection
            mongoDatabase.createCollection(collectionName);
            System.out.println("Create Collection Successfully");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    // Connect to a specific collection in a specific database
    public static void GetCollection(String databaseName, String collectionName) {
        try {
            // Connect to the MongoDB Service
            MongoClient mongoClient = new MongoClient("localhost", 27017);

            // Connect to the database
            MongoDatabase mongoDatabase = mongoClient.getDatabase(databaseName);
            System.out.println("Connect to Database Successfully");

            // Get the collection
            MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
            System.out.println("Get Collection Successfully");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    // Insert one document into a specific collection in a specific database
    public static String InsertDocument(String databaseName, String collectionName, Document document) {
        String id_number = null;
        try {
            // Connect to the MongoDB Service
            MongoClient mongoClient = new MongoClient("localhost", 27017);

            // Connect to the database
            MongoDatabase mongoDatabase = mongoClient.getDatabase(databaseName);
            System.out.println("Connect to Database Successfully");

            // Get the collection
            MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
            System.out.println("Get Collection Successfully");

            //Insert the list which contains the new document
            collection.insertOne(document);
            ObjectId id = document.getObjectId("_id");
            id_number = id.toString();
            System.out.println("Insert One Document Successfully and the _id is" + id);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return id_number;
    }

    // Insert multiple documents into a specific collection in a specific database
    public static void InsertDocuments(String databaseName, String collectionName, List<Document> documents) {
        try {
            // Connect to the MongoDB Service
            MongoClient mongoClient = new MongoClient("localhost", 27017);

            // Connect to the database
            MongoDatabase mongoDatabase = mongoClient.getDatabase(databaseName);
            System.out.println("Connect to Database Successfully");

            // Get the collection
            MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
            System.out.println("Get Collection Successfully");

            //Insert the list which contains all new documents
            collection.insertMany(documents);
            System.out.println("Insert Documents Successfully");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    // Update the records in the database by giving a document value
    public static void UpdateCollectionDocument(String databaseName, String collectionName, String documentID, Document document) {
        try {
            // Connect to the MongoDB Service
            MongoClient mongoClient = new MongoClient("localhost", 27017);

            // Connect to the database
            MongoDatabase mongoDatabase = mongoClient.getDatabase(databaseName);
            System.out.println("Connect to Database Successfully");

            // Get the collection
            MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
            System.out.println("Get Collection Successfully");

            // Update the collection
            Document updateQuery = new Document();
            updateQuery.append("$set", document);
            Document searchQuery = new Document();
            searchQuery.append("_id", new ObjectId(documentID));
            collection.updateMany(searchQuery, updateQuery);
            System.out.println("Update Collection Successfully");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    // Update the records in the database by giving a string value
    public static void UpdateCollectionString(String databaseName, String collectionName, String documentID, String key, String value) {
        try {
            // Connect to the MongoDB Service
            MongoClient mongoClient = new MongoClient("localhost", 27017);

            // Connect to the database
            MongoDatabase mongoDatabase = mongoClient.getDatabase(databaseName);
            System.out.println("Connect to Database Successfully");

            // Get the collection
            MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
            System.out.println("Get Collection Successfully");

            // Update the collection
            Document updateQuery = new Document();
            updateQuery.append("$set", new Document().append(key, value));
            Document searchQuery = new Document();
            searchQuery.append("_id", new ObjectId(documentID));
            collection.updateMany(searchQuery, updateQuery);
            System.out.println("Update Collection Successfully");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    // Update the records in the database by giving a long value
    public static void UpdateCollectionLong(String databaseName, String collectionName, String documentID, String key, Long value) {
        try {
            // Connect to the MongoDB Service
            MongoClient mongoClient = new MongoClient("localhost", 27017);

            // Connect to the database
            MongoDatabase mongoDatabase = mongoClient.getDatabase(databaseName);
            System.out.println("Connect to Database Successfully");

            // Get the collection
            MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
            System.out.println("Get Collection Successfully");

            // Update the collection
            Document updateQuery = new Document();
            updateQuery.append("$set", new Document().append(key, value));
            Document searchQuery = new Document();
            searchQuery.append("_id", new ObjectId(documentID));
            collection.updateMany(searchQuery, updateQuery);
            System.out.println("Update Collection Successfully");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    // Update the records in the database by giving an int value
    public static void UpdateCollectionInt(String databaseName, String collectionName, String documentID, String key, int value) {
        try {
            // Connect to the MongoDB Service
            MongoClient mongoClient = new MongoClient("localhost", 27017);

            // Connect to the database
            MongoDatabase mongoDatabase = mongoClient.getDatabase(databaseName);
            System.out.println("Connect to Database Successfully");

            // Get the collection
            MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
            System.out.println("Get Collection Successfully");

            // Update the collection
            Document updateQuery = new Document();
            updateQuery.append("$set", new Document().append(key, value));
            Document searchQuery = new Document();
            searchQuery.append("_id", new ObjectId(documentID));
            collection.updateMany(searchQuery, updateQuery);
            System.out.println("Update Collection Successfully");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    // Update the records in the database by giving a list value
    public static void UpdateCollectionList(String databaseName, String collectionName, String documentID, String key, List value) {
        try {
            // Connect to the MongoDB Service
            MongoClient mongoClient = new MongoClient("localhost", 27017);

            // Connect to the database
            MongoDatabase mongoDatabase = mongoClient.getDatabase(databaseName);
            System.out.println("Connect to Database Successfully");

            // Get the collection
            MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
            System.out.println("Get Collection Successfully");

            // Update the collection
            Document updateQuery = new Document();
            updateQuery.append("$set", new Document().append(key, value));
            Document searchQuery = new Document();
            searchQuery.append("_id", new ObjectId(documentID));
            collection.updateMany(searchQuery, updateQuery);
            System.out.println("Update Collection Successfully");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    // Print out a specific collection in a specific database
    public static void PrintCollection(String databaseName, String collectionName) {
        try {
            // Connect to the MongoDB Service
            MongoClient mongoClient = new MongoClient("localhost", 27017);

            // Connect to the database
            MongoDatabase mongoDatabase = mongoClient.getDatabase(databaseName);
            System.out.println("Connect to Database Successfully");
            //DB mogoDB = mongoClient.getDB()

            // Get the collection
            MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
            System.out.println("Get Collection Successfully");

            // Print the collection
            FindIterable<Document> findIterable = collection.find();
            MongoCursor<Document> mongoCursor = findIterable.iterator();
            //DBCursor cursor = collection.find();
            while(mongoCursor.hasNext()){
                System.out.println(mongoCursor.next());
            }
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    // Return all the documents in a specific collection of a specific database
    public static List<Object> FindCollection(String databaseName, String collectionName) {
        // Create a list to save the collections
        List<Object> documentList = new ArrayList();

        try {
            // Connect to the MongoDB Service
            MongoClient mongoClient = new MongoClient("localhost", 27017);

            // Connect to the database
            MongoDatabase mongoDatabase = mongoClient.getDatabase(databaseName);
            System.out.println("Connect to Database Successfully");

            // Get the collection
            MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
            System.out.println("Get Collection Successfully");

            // Return the collection with key information
            FindIterable<Document> findIterable = collection.find();
            MongoCursor<Document> mongoCursor = findIterable.iterator();

            while(mongoCursor.hasNext()){
                documentList.add(mongoCursor.next());
            }
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return documentList;
    }

    //  Return one document in a specific collection of a specific database by giving the document ID #
    public static List FindDocument(String databaseName, String collectionName, String documentID) {
        // Create a list to save the collections
        List<Document> documentList = new ArrayList();

        try {
            // Connect to the MongoDB Service
            MongoClient mongoClient = new MongoClient("localhost", 27017);

            // Connect to the database
            MongoDatabase mongoDatabase = mongoClient.getDatabase(databaseName);
            System.out.println("Connect to Database Successfully");

            // Get the collection
            MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
            System.out.println("Get Collection Successfully");

            // Return the document
            FindIterable<Document> findIterable = collection.find(new Document("_id",new ObjectId(documentID)));
            MongoCursor<Document> mongoCursor = findIterable.iterator();
            while(mongoCursor.hasNext()){
                documentList.add(mongoCursor.next());
            }
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return documentList;
    }
}