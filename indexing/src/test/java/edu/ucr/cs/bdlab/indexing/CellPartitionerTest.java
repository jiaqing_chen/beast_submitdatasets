/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.indexing;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.test.SparkTest;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.io.PrintStream;

public class CellPartitionerTest extends SparkTest {

  public void testOverlapPartition() throws IOException {
    String[] masterFile = readTextResource("/test.partitions");
    Path masterPath = new Path(scratchPath, "tempindex/_master.grid");
    Configuration conf = new Configuration();
    FileSystem fs = masterPath.getFileSystem(conf);
    PrintStream printStream = new PrintStream(fs.create(masterPath));
    for (String l : masterFile)
      printStream.println(l);
    printStream.close();
    //List<Partition> partitions = MetadataUtil.getPartitions(masterPath);
    PartitionInfo[] cells = PartitionInfo.readPartitions(masterPath.getFileSystem(conf), masterPath);
    CellPartitioner p = new CellPartitioner(cells);
    int cellId = p.overlapPartition(new Envelope(2, -124.7306754,40.4658126,-124.7306754,40.4658126));
    assertEquals(42, cellId);
    cellId = p.overlapPartition(new Envelope(2, 10.0, 46.0, 10.0, 46.0));
    assertEquals(10, cellId);
  }
}