package edu.ucr.cs.bdlab.indexing;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.stsynopses.Summary;
import edu.ucr.cs.bdlab.test.SparkTest;
import org.apache.hadoop.conf.Configuration;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class AbstractRTreeBBPartitionerTest extends SparkTest {

  public void testConstruct() throws IOException {
    double[][] coords = readCoordsResource("/test.points");
    AbstractRTreeBBPartitioner p = new AbstractRTreeBBPartitioner.RTreeGuttmanBBPartitioner();
    p.setup(new Configuration(), false, SpatialPartitioner.PartitionCriterion.FIXED, 4);
    Summary summary = new Summary();
    summary.set(new double[] {1.0, 0.0}, new double[] {12.0, 12.0});
    summary.numFeatures = 11;
    summary.size = 11 * 2 * 8;
    p.construct(summary, coords, null);
    assertTrue("Too few partitions", p.getPartitionCount() > 2);
    Set<Integer> partitions = new HashSet();
    Envelope mbr = new Envelope(2);
    for (int iPoint = 0; iPoint < coords[0].length; iPoint++) {
      for (int d = 0; d < coords.length; d++) {
        mbr.minCoord[d] = mbr.maxCoord[d] = coords[d][iPoint];
      }
      partitions.add(p.overlapPartition(mbr));
    }
    assertEquals(p.getPartitionCount(), partitions.size());
  }

  public void testOverlapPartitionShouldChooseMinimalArea() {
    Envelope[] partitions = { new Envelope(2, 0.0,0.0,4.0,4.0),
        new Envelope(2, 1.0,1.0,3.0,3.0)};
    AbstractRTreeBBPartitioner p = new AbstractRTreeBBPartitioner.RTreeGuttmanBBPartitioner();
    initializeBBPartitioner(p, partitions);
    assertEquals(0, p.overlapPartition(new Envelope(2, 0.5, 0.5, 0.5, 0.5)));
    assertEquals(1, p.overlapPartition(new Envelope(2, 2.0, 2.0, 2.0, 2.0)));
  }

  private void initializeBBPartitioner(AbstractRTreeBBPartitioner p, Envelope[] partitions) {
    int dimensions = partitions[0].getCoordinateDimension();
    int envelopes = partitions.length;
    p.minCoord = new double[dimensions][envelopes];
    p.maxCoord = new double[dimensions][envelopes];
    for (int i = 0; i < partitions.length; i++) {
      for (int d = 0; d < dimensions; d++) {
        p.minCoord[d][i] = partitions[i].minCoord[d];
        p.maxCoord[d][i] = partitions[i].maxCoord[d];
      }
    }
  }
}