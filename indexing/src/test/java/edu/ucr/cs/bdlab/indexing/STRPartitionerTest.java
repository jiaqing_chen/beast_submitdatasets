/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.indexing;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.stsynopses.Summary;
import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.IntArray;

import java.io.IOException;

public class STRPartitionerTest extends SparkTest {

  public void testPartition2DPoints() throws IOException {
    double[][] coords = readCoordsResource("/test.points");
    STRPartitioner partitioner = new STRPartitioner();
    partitioner.setup(getSC().hadoopConfiguration(), true,
        SpatialPartitioner.PartitionCriterion.FIXED, 4);
    Summary summary = new Summary();
    summary.set(new double[] {1.0, 0.0}, new double[] {12.0, 12.0});
    summary.numFeatures = 11;
    summary.size = 11 * 2 * 8;
    partitioner.construct(summary, coords, null);
    assertEquals(3, partitioner.splitCoords.length);
    assertEquals(5.5, partitioner.splitCoords[0]);
    assertEquals(8.0, partitioner.splitCoords[1]);
    assertEquals(7.0, partitioner.splitCoords[2]);

    // Test overlap a single partition
    Envelope env = new Envelope(2, 0.0, 0.0, 0.0, 0.0);
    assertEquals(0, partitioner.overlapPartition(env));
    env.set(new double[] {0.0, 10.0}, new double[] {0.0, 10.0});
    assertEquals(1, partitioner.overlapPartition(env));
    env.set(new double[] {10.0, 0.0}, new double[] {10.0, 0.0});
    assertEquals(2, partitioner.overlapPartition(env));
    env.set(new double[] {10.0, 10.0}, new double[] {10.0, 10.0});
    assertEquals(3, partitioner.overlapPartition(env));

    // Test overlap multiple partitions
    env.set(new double[] {0.0, 0.0}, new double[] {1.0, 10.0});
    IntArray partitions = new IntArray();
    partitioner.overlapPartitions(env, partitions);
    assertEquals(2, partitions.size());
    assertTrue(partitions.contains(0));
    assertTrue(partitions.contains(1));

    // Test partition MBR
    partitioner.getPartitionMBR(0, env);
    assertEquals(new Envelope(2, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, 5.5, 8.0), env);
    partitioner.getPartitionMBR(1, env);
    assertEquals(new Envelope(2, Double.NEGATIVE_INFINITY, 8.0, 5.5, Double.POSITIVE_INFINITY), env);
    partitioner.getPartitionMBR(2, env);
    assertEquals(new Envelope(2, 5.5, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, 7.0), env);
    partitioner.getPartitionMBR(3, env);
    assertEquals(new Envelope(2, 5.5, 7.0, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY), env);
  }

  public void testGetPartitionCount() throws IOException {
    double[][] coords = readCoordsResource("/test111.points");
    int numDimensions = coords.length;
    int numPoints = coords[0].length;
    STRPartitioner partitioner = new STRPartitioner();
    partitioner.setup(getSC().hadoopConfiguration(), true, SpatialPartitioner.PartitionCriterion.FIXED, 25);
    Summary summary = new Summary();
    summary.setCoordinateDimension(numDimensions);
    Point p = new Point(numDimensions);
    for (int $i = 0; $i < numPoints; $i++) {
      for (int $d = 0; $d < numDimensions; $d++) {
        p.coords[$d] = coords[$d][$i];
      }
      summary.expandToGeometry(p);
    }
    summary.numFeatures = numPoints;
    summary.size = numPoints * numDimensions * 8;
    partitioner.construct(summary, coords, null);
    assertEquals(25, partitioner.getPartitionCount());
  }
}