/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.indexing;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.Feature;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.io.FeatureWriter;
import edu.ucr.cs.bdlab.util.CounterOutputStream;
import edu.ucr.cs.bdlab.util.OperationParam;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Writes features to one file that stores both the record data and an R-tree local index.
 * First, it writes all the features to a temporary file. Then, it constructs the R-tree in memory and flushes it to
 * disk along with the record data.
 */
@FeatureWriter.Metadata(extension = ".rtree", shortName = "rtree")
public class RTreeFeatureWriter extends FeatureWriter {

  @OperationParam(
      description = "The type of rtree to build as a local index for each output file {rtree, rstree, rrstree}",
      defaultValue = "rrstree"
  )
  public static final String RTreeTypeConf = "rtreetype";

  /**Final output path to write the R-tree as the writer is closed*/
  protected Path finalOutputPath;

  /**An output stream where the final Rtree will be written*/
  protected DataOutputStream finalOutputStream;

  /**Type of tree to build*/
  enum RTreeType {RTree, RSTree, RRSTree};

  /**Type of rtree to build*/
  protected RTreeType rtreeType;

  /**The path to the temporary file for writing the features*/
  protected File tempFile;

  /**A temporary file to write the features until the file is closed before building the index*/
  protected DataOutputStream tempOut;

  /**A standard feature to convert any input feature type to a standard type*/
  protected Feature f;

  /**Number of features written so far. Needed to efficiently build the R-tree*/
  protected int numFeatures;

  /**Number of dimensions for input records*/
  protected int numDimensions;

  /**Hadoop environment configuration*/
  protected Configuration conf;

  @Override
  public void initialize(Path p, Configuration conf) throws IOException {
    this.finalOutputPath = p;
    initialize(conf);
  }

  @Override
  public void initialize(OutputStream out, Configuration conf) throws IOException {
    this.finalOutputStream = out instanceof DataOutputStream? (DataOutputStream) out : new DataOutputStream(out);
    initialize(conf);
  }

  protected void initialize(Configuration conf) throws IOException {
    this.conf = conf;
    String rtreeTypeStr = conf.get(RTreeTypeConf, "rrstree");
    if (rtreeTypeStr.equalsIgnoreCase("rtree"))
      this.rtreeType = RTreeType.RTree;
    else if (rtreeTypeStr.equalsIgnoreCase("rstree"))
      this.rtreeType = RTreeType.RSTree;
    else if (rtreeTypeStr.equalsIgnoreCase("rrstree"))
      this.rtreeType = RTreeType.RRSTree;
    else
      throw new RuntimeException("Unidentified R-tree type: "+rtreeTypeStr);

    tempFile = File.createTempFile(String.format("%06d", (int)(Math.random() * 1000000)), "rtree");
    // Mark file to delete on exit just in case the process fails without explicitly deleting it
    tempFile.deleteOnExit();
    tempOut = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(tempFile)));
    f = new Feature();
    numFeatures = 0;
  }

  @Override
  public void write(Object key, IFeature value) throws IOException, InterruptedException {
    if (numFeatures == 0) {
      // First record, write the header once
      f.copyAttributeMetadata(value);
      f.writeHeader(tempOut);
      numDimensions = value.getGeometry().getCoordinateDimension();
    }
    f.copyAttributeValues(value);
    f.writeValue(tempOut);
    numFeatures++;
  }

  @Override
  public void close(TaskAttemptContext context) throws IOException {
    try {
      // Close the temporary file and build the index
      tempOut.close();
      if (numFeatures == 0 || counter != null) {
        // If no records were written, just close the output stream
        // This is used when this writer is used to estimate the size but not to actually write any records
        finalOutputStream.close();
        return;
      }

      // Create a new feature to scan over the features in the temporary file
      Feature f = new Feature();
      int[] recordOffsets = new int[numFeatures + 1];
      double[][] minCoord = new double[numDimensions][numFeatures];
      double[][] maxCoord = new double[numDimensions][numFeatures];
      Envelope mbr = new Envelope(numDimensions);

      FileSystem localFS = FileSystem.getLocal(conf);
      // We use teh FSDataInputStream to allow retrieving the position of records
      FSDataInputStream tempIn = localFS.open(new Path(tempFile.getPath()));
      // The size of the biggest feature Max(Size(Feature))
      f.readHeader(tempIn);
      int biggestFeatureSize = (int) tempIn.getPos(); // Initialize to header size
      for (int i$ = 0; i$ < numFeatures; i$++) {
        recordOffsets[i$] = (int) tempIn.getPos();
        f.readValue(tempIn);
        f.getGeometry().envelope(mbr);
        for (int d$ = 0; d$ < numDimensions; d$++) {
          minCoord[d$][i$] = mbr.minCoord[d$];
          maxCoord[d$][i$] = mbr.maxCoord[d$];
        }
        int size = (int) (tempIn.getPos() - recordOffsets[i$]);
        if (size > biggestFeatureSize)
          biggestFeatureSize = size;
      }
      assert tempIn.getPos() <= Integer.MAX_VALUE;
      recordOffsets[numFeatures] = (int) tempIn.getPos();

      // Now build the R-tree
      RTreeGuttman rtree;
      int M = 100;
      int m;
      switch (this.rtreeType) {
        case RTree:
          m = M / 2;
          rtree = new RTreeGuttman(m, M);
          break;
        case RSTree:
          m = M * 3 / 10;
          rtree = new RStarTree(m, M);
          break;
        case RRSTree:
          m = M * 2 / 10;
          rtree = new RRStarTree(m, M);
          break;
        default:
          throw new RuntimeException("Unsupported rtree type: " + this.rtreeType);
      }
      rtree.initializeFromBoxes(minCoord, maxCoord);

      if (finalOutputPath != null) {
        assert finalOutputStream == null;
        FileSystem outFS = finalOutputPath.getFileSystem(conf);
        finalOutputStream = outFS.create(finalOutputPath);
      }
      byte[] buffer = new byte[biggestFeatureSize];
      // Copy the header
      tempIn.readFully(0, buffer, 0, recordOffsets[0]);
      finalOutputStream.write(buffer, 0, recordOffsets[0]);
      // Then, write the entire tree
      rtree.write(finalOutputStream, (out1, iObject) -> {
        int recordSize = recordOffsets[iObject + 1] - recordOffsets[iObject];
        tempIn.readFully(recordOffsets[iObject], buffer, 0, recordSize);
        out1.write(buffer, 0, recordSize);
        return recordSize;
      });
      tempIn.close();
      finalOutputStream.close();
    } finally {
      tempFile.delete();
    }
  }

  protected CounterOutputStream counter = null;

  @Override
  public int estimateSize(IFeature value) {
    try {
      if (counter == null) {
        if (tempOut != null)
          tempOut.close();
        tempOut = new DataOutputStream(counter = new CounterOutputStream());
      }
      long sizeBefore = counter.getCount();
      if (numFeatures == 0) {
        // First record, write the header once
        this.f.copyAttributeMetadata(value);
        this.f.writeHeader(tempOut);
        numDimensions = value.getGeometry().getCoordinateDimension();
      }
      numFeatures++;
      this.f.copyAttributeValues(value);
      this.f.writeValue(tempOut);
      // Add 44 bytes as a rough estimate for the R-tree index overhead (empirically obtained from actual indexes)
      return (int) (counter.getCount() - sizeBefore) + 44;
    } catch (IOException e) {
      e.printStackTrace();
      return 0;
    }
  }
}
