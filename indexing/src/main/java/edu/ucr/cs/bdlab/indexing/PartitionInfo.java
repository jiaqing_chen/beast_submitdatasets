/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.indexing;

import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.twod.Polygon2D;
import edu.ucr.cs.bdlab.stsynopses.Summary;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.LineReader;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A class that holds the information about one partition in a global index
 */
public class PartitionInfo extends Summary implements IFeature {
  private static final Log LOG = LogFactory.getLog(PartitionInfo.class);

  /**A unique ID for the partition*/
  int partitionId;

  /**The filename (not full path) that has the contents of the file.*/
  String filename;

  public PartitionInfo() {}

  public PartitionInfo(Summary summary, int partitionId) {
    super(summary);
    this.partitionId = partitionId;
    this.filename = String.format("part-%d", partitionId);
  }

  public PartitionInfo(PartitionInfo other) {
    super(other);
    this.partitionId = other.partitionId;
    this.filename = other.filename;
  }

  /**The CSV head line of the master file*/
  public static final String CSVHeader = String.join("\t", "ID", "File Name",
      "Record Count", "Data Size", "Geometry");

  @Override
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();

    stringBuilder.append(partitionId);
    stringBuilder.append('\t');

    stringBuilder.append('\'');
    stringBuilder.append(filename);
    stringBuilder.append('\'');
    stringBuilder.append('\t');

    stringBuilder.append(getRecordCount());
    stringBuilder.append('\t');

    stringBuilder.append(getDataSize());
    stringBuilder.append('\t');

    if (getCoordinateDimension() == 2)
      super.toWKT(stringBuilder);

    for (int d = 0; d < getCoordinateDimension(); d++) {
      stringBuilder.append('\t');
      stringBuilder.append(minCoord[d]);
    }

    for (int d = 0; d < getCoordinateDimension(); d++) {
      stringBuilder.append('\t');
      stringBuilder.append(maxCoord[d]);
    }
    return stringBuilder.toString();
  }

  @Override
  public void write(DataOutput out) throws IOException {
    super.write(out);
    out.writeInt(partitionId);
    out.writeUTF(filename);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    super.readFields(in);
    this.partitionId = in.readInt();
    this.filename = in.readUTF();
  }

  /**
   * Read partition information from a master file as an array of partitions. The master file is a TSV
   * (tab-separated values) with a header line {@link #CSVHeader} and one line per partition. This method is supposed
   * to be called only when preparing a job and the master file is supposed to be of a decent size. Therefore, this
   * method does not optimize much for the object instantiation or memory overhead.
   * @param fs
   * @param file
   * @return
   * @throws IOException
   */
  public static PartitionInfo[] readPartitions(FileSystem fs, Path file) throws IOException {
    LineReader reader = null;
    try {
      List<PartitionInfo> partitions = new ArrayList<>();
      reader = new LineReader(fs.open(file));
      Text line = new Text();
      // Read header
      if (reader.readLine(line) == 0)
        throw new RuntimeException("Header not found!");
      if (!line.toString().startsWith(CSVHeader))
        throw new RuntimeException("Incorrect header");

      // read partition information
      Polygon2D poly = new Polygon2D();
      while (reader.readLine(line) > 0) {
        String[] parts = line.toString().split("\t");
        PartitionInfo partition = new PartitionInfo();
        partition.partitionId = Integer.parseInt(parts[0]); // partition ID
        partition.filename = parts[1];
        partition.numFeatures = Long.parseLong(parts[2]);
        partition.size = Long.parseLong(parts[3]);
        // Column #4 contains a WKT geom which is not used
        // Read the envelope
        int numDimensions = (parts.length - 5) / 2;
        double[] minCoords = new double[numDimensions];
        double[] maxCoords = new double[numDimensions];
        for (int i = 0; i < numDimensions; i++) {
          minCoords[i] = Double.parseDouble(parts[5 + i]);
          maxCoords[i] = Double.parseDouble(parts[5 + numDimensions + i]);
        }
        partition.set(minCoords, maxCoords);
        partitions.add(partition);
      }
      return partitions.toArray(new PartitionInfo[partitions.size()]);
    } finally {
      if (reader != null)
        reader.close();
    }

  }

  @Override
  public IGeometry getGeometry() {
    // Return the envelope
    return this;
  }

  @Override
  public Object getAttributeValue(int i) {
    switch (i) {
      case 0:
        return partitionId;
      case 1:
        return filename;
      case 2:
        return numFeatures;
      case 3:
        return size;
      case 4:
        // WKT representation of the envelope;
        StringBuilder str = new StringBuilder();
        this.toWKT(str);
        return str.toString();
      default:
        return null;
    }
  }

  @Override
  public Object getAttributeValue(String name) {
    // "Record Count", "Data Size", "Geometry");
    switch (name) {
      case "ID": return 0;
      case "File Name": return 1;
      case "Record Count": return 2;
      case "Data Size": return 3;
      case "Geometry": return 4;
      default: return -1;
    }
  }

  @Override
  public int getNumAttributes() {
    return 5;
  }

  @Override
  public String getAttributeName(int i) {
    switch (i) {
      case 0: return "ID";
      case 1: return "File Name";
      case 2: return "Record Count";
      case 3: return "Data Size";
      case 4: return "Geometry";
      default: return null;
    }
  }

  @Override
  public int getStorageSize() {
    // ID + file name + record count + data size + WKT geometry (approximate)
    return super.getGeometryStorageSize() + 8 + filename.length() + 8 + 8 + 100;
  }

  @Override
  public void setGeometry(IGeometry geom) {
    throw new RuntimeException("PartitionInfo is immutable");
  }
}
