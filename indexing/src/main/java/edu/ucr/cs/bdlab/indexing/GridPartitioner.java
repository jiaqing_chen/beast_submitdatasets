/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.indexing;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.stsynopses.AbstractHistogram;
import edu.ucr.cs.bdlab.stsynopses.Summary;
import edu.ucr.cs.bdlab.util.IntArray;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;

/**
 * A partitioner that partitions data using a uniform grid.
 * If a shape overlaps multiple grids, it replicates it to all overlapping
 * partitions.
 * @author Ahmed Eldawy
 *
 */
@SpatialPartitioner.Metadata(
    disjointSupported = true,
    extension = "grid",
    description = "Partitions the space using a uniform grid with roughly square cells")
public class GridPartitioner implements SpatialPartitioner {
  private static final Log LOG = LogFactory.getLog(GridPartitioner.class);

  /**The MBR of the region to be partitioned*/
  protected final Envelope gridMBR = new Envelope();

  /**Total number of partitions along each dimension*/
  public int[] numPartitions;

  /**The criterion used to calculate the number of buckets*/
  transient protected PartitionCriterion pCriterion;

  /**The value associated with the partitioning criterion*/
  transient protected long partitionValue;

  /**Create disjoint partitions*/
  private boolean disjointPartitions;

  /**
   * A default constructor to be able to dynamically instantiate it
   * and deserialize it
   */
  public GridPartitioner() {
  }

  public GridPartitioner(Envelope mbr, int[] numPartitions) {
    assert mbr.getCoordinateDimension() == numPartitions.length;
    this.gridMBR.set(mbr.minCoord, mbr.maxCoord);
    this.numPartitions = Arrays.copyOf(numPartitions, numPartitions.length);
    this.disjointPartitions = true;
  }

  @Override
  public void setup(Configuration conf, boolean disjoint, PartitionCriterion pCriterion, long pValue) {
    this.disjointPartitions = disjoint;
    this.pCriterion = pCriterion;
    this.partitionValue = pValue;
  }

  @Override
  public void construct(Summary summary, double[][] sample, AbstractHistogram histogram) {
    this.gridMBR.set(summary.minCoord, summary.maxCoord);
    int numBuckets = IndexerParams.computeNumberOfPartitions(pCriterion, partitionValue, summary);
    numPartitions = new int[summary.getCoordinateDimension()];
    computeNumberOfPartitionsAlongAxes(summary, numBuckets, numPartitions);
  }

  /**
   * Computes the total number of partitions along each axis in order to produce square cells of roughly equal volume
   * while adhering to the given number of partitions. It first computes the volume of the input space and divides it
   * by the desired number of partitions to compute the volume of each cell. Then, it takes the kth root to compute the
   * side length. Finally, it iterates over the dimensions to compute the desired number of partitions along each axis.
   * While doing that final step, it keeps into account that the total number of partitions should not exceed the given
   * number.
   * @param mbr the MBR of the input space
   * @param numCells the desired number of cells
   * @param numPartitions (out) the computed number of partitions along each axis
   */
  public static void computeNumberOfPartitionsAlongAxes(Envelope mbr, int numCells, int[] numPartitions) {
    // Divide the space into equi-sized square cells
    double cellSideLength = Math.pow(mbr.getVolume() / numCells, 1.0/mbr.getCoordinateDimension());
    int totalNumPartitions = 1;
    for (int d = 1; d < mbr.getCoordinateDimension(); d++) {
      numPartitions[d] = Math.max(1, (int) Math.floor(mbr.getSideLength(d) / cellSideLength));
      totalNumPartitions *= numPartitions[d];
    }
    numPartitions[0] = Math.max(1, (int) Math.floor((double) numCells / totalNumPartitions));
  }

  @Override
  public void write(DataOutput out) throws IOException {
    gridMBR.write(out);
    for (int d = 0; d < gridMBR.getCoordinateDimension(); d++)
      out.writeInt(numPartitions[d]);
    out.writeBoolean(disjointPartitions);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    gridMBR.readFields(in);
    if (numPartitions == null || numPartitions.length != gridMBR.getCoordinateDimension())
      numPartitions = new int[gridMBR.getCoordinateDimension()];
    for (int d = 0; d < gridMBR.getCoordinateDimension(); d++)
      numPartitions[d] = in.readInt();
    this.disjointPartitions = in.readBoolean();
  }

  @Override
  public int getPartitionCount() {
    int count = 1;
    for (int d = 0; d < gridMBR.getCoordinateDimension(); d++)
      count *= numPartitions[d];
    return count;
  }

  @Override
  public boolean isDisjoint() {
    return this.disjointPartitions;
  }

  @Override
  public int getCoordinateDimension() {
    return numPartitions.length;
  }

  @Override
  public void overlapPartitions(Envelope recordMBR, IntArray matchedPartitions) {
    int[] overlapMin = new int[gridMBR.getCoordinateDimension()];
    int[] overlapMax = new int[gridMBR.getCoordinateDimension()];
    int numResults = 1;
    for (int d = 0; d < gridMBR.getCoordinateDimension(); d++) {
      if (recordMBR.minCoord[d] == recordMBR.maxCoord[d] && gridMBR.maxCoord[d] == recordMBR.maxCoord[d]) {
        // Special case for a point record that is exactly at the upper boundary of the grid
        overlapMin[d] = numPartitions[d] - 1;
        overlapMax[d] = numPartitions[d];
      } else {
        // Find overlapping partitions
        overlapMin[d] = (int) Math.floor((recordMBR.minCoord[d] - gridMBR.minCoord[d]) * numPartitions[d] / gridMBR.getSideLength(d));
        overlapMax[d] = (int) Math.ceil((recordMBR.maxCoord[d] - gridMBR.minCoord[d]) * numPartitions[d] / gridMBR.getSideLength(d));
        if (overlapMax[d] == overlapMin[d]) {
          // Special case when the coordinate perfectly aligns with the grid boundary
          overlapMax[d]++;
        }
        if (overlapMin[d] < 0)
          overlapMin[d] = 0;
        if (overlapMax[d] > numPartitions[d])
          overlapMax[d] = numPartitions[d];
      }
      numResults *= overlapMax[d] - overlapMin[d];
    }
    matchedPartitions.clear();
    // The partition number is internally represented as a d-dimensional number
    // TODO avoid creating this array for each call of this function
    int[] partitionNumber = new int[gridMBR.getCoordinateDimension()];

    for (int d = 0; d < gridMBR.getCoordinateDimension(); d++)
      partitionNumber[d] = overlapMin[d];

    for (int p = 0; p < numResults; p++) {
      int combinedPartitionNumber = 0;
      int d = gridMBR.getCoordinateDimension();
      while (d-- > 0) {
        combinedPartitionNumber *= numPartitions[d];
        combinedPartitionNumber += partitionNumber[d];
      }
      matchedPartitions.add(combinedPartitionNumber);
      // Move to next partition
      d = 0;
      while (d < partitionNumber.length && ++partitionNumber[d] >= overlapMax[d]) {
        partitionNumber[d] = overlapMin[d];
        d++;
      }
    }
  }

  @Override
  public int overlapPartition(Envelope mbr) {
    int combinedPartitionNumber = 0;
    int d = gridMBR.getCoordinateDimension();
    while (d-- > 0) {
      int pos = (int) Math.floor((mbr.getCenter(d) - gridMBR.minCoord[d]) * numPartitions[d] / gridMBR.getSideLength(d));
      pos = Math.min(pos, numPartitions[d] - 1);
      combinedPartitionNumber *= numPartitions[d];
      combinedPartitionNumber += pos;
    }
    return combinedPartitionNumber;
  }

  @Override
  public void getPartitionMBR(int partitionID, Envelope mbr) {
    mbr.setCoordinateDimension(gridMBR.getCoordinateDimension());
    for (int d = 0; d < gridMBR.getCoordinateDimension(); d++) {
      int pos = partitionID % numPartitions[d];
      mbr.minCoord[d] = (gridMBR.minCoord[d] * (numPartitions[d] - pos) + gridMBR.maxCoord[d] * pos) / numPartitions[d];
      mbr.maxCoord[d] = (gridMBR.minCoord[d] * (numPartitions[d] - (pos+ 1)) + gridMBR.maxCoord[d] * (pos + 1)) / numPartitions[d];
      partitionID /= numPartitions[d];
    }
  }

  @Override
  public Envelope getEnvelope() {
    return gridMBR;
  }
}
