/***********************************************************************
* Copyright (c) 2015 by Regents of the University of Minnesota.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Apache License, Version 2.0 which 
* accompanies this distribution and is available at
* http://www.opensource.org/licenses/apache2.0.php.
*
*************************************************************************/
package edu.ucr.cs.bdlab.indexing;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.stsynopses.AbstractHistogram;
import edu.ucr.cs.bdlab.stsynopses.Summary;
import edu.ucr.cs.bdlab.util.IntArray;
import edu.ucr.cs.bdlab.util.OperationParam;
import org.apache.hadoop.conf.Configuration;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * A partitioner that uses an existing RTree as a black-box.
 * @see RTreeGuttman
 * @see RTreeGuttman#initializeFromPoints(double[][])
 * @author Ahmed Eldawy
 *
 */
public abstract class AbstractRTreeBBPartitioner implements SpatialPartitioner {
  @OperationParam(
      description = "The desired ratio between the minimum and maximum partitions sizes ]0,1[",
      defaultValue = "0.3",
      required = false
  )
  public static final String MMRatio = "mmratio";

  /**Arrays holding the coordinates*/
  protected double[][] minCoord;
  protected double[][] maxCoord;

  /**The ratio m/M used to construct the R-tree*/
  protected float mMRatio;

  /**The criterion used to calculate the number of buckets*/
  transient protected PartitionCriterion pCriterion;

  /**The value associated with the partitioning criterion*/
  transient protected long partitionValue;

  /**MBR of the points used to partition the space*/
  protected final Envelope mbrPoints = new Envelope();

  @Override
  public void setup(Configuration conf, boolean disjoint, PartitionCriterion pCriterion, long pValue) {
    mMRatio = conf.getFloat(MMRatio, 0.3f);
    if (disjoint)
      throw new RuntimeException("Black-box partitinoer does not support disjoint partitions");
    this.pCriterion = pCriterion;
    this.partitionValue = pValue;
  }

  @Override
  public void construct(Summary summary, @Required double[][] sample, AbstractHistogram histogram) {
    int numDimensions = sample.length;
    assert summary.getCoordinateDimension() == sample.length;
    mbrPoints.setCoordinateDimension(summary.getCoordinateDimension());
    mbrPoints.merge(summary);
    int numSamplePoints = sample[0].length;
    int numPartitions = IndexerParams.computeNumberOfPartitions(pCriterion, partitionValue, summary);
    int M = (int) Math.ceil((double)numSamplePoints / numPartitions);
    int m = (int) Math.ceil(mMRatio * M);

    RTreeGuttman rtree = createRTree(m, M);
    rtree.initializeFromPoints(sample);

    int numLeaves = rtree.getNumLeaves();
    minCoord = new double[numDimensions][numLeaves];
    maxCoord = new double[numDimensions][numLeaves];
    int iLeaf = 0;
    for (RTreeGuttman.Node node : rtree.getAllLeaves()) {
      for (int d = 0; d < numDimensions; d++) {
        minCoord[d][iLeaf] = node.min[d];
        maxCoord[d][iLeaf] = node.max[d];
      }
      iLeaf++;
    }
  }

  /**Create the RTree that will be used to index the sample points*/
  public abstract RTreeGuttman createRTree(int m, int M);

  @SpatialPartitioner.Metadata(
      disjointSupported = false,
      extension = "rtreebb",
      description = "Loads a sample points into an R-tree and use its leaf nodes as partition boundaries")
  public static class RTreeGuttmanBBPartitioner extends AbstractRTreeBBPartitioner {
    enum SplitMethod {LinearSplit, QuadraticSplit};
    SplitMethod splitMethod;

    @Override
    public void setup(Configuration conf, boolean disjoint, PartitionCriterion pCriterion, long pValue) {
      super.setup(conf, disjoint, pCriterion, pValue);
      if (conf.get("split", "linear").equalsIgnoreCase("linear")) {
        this.splitMethod = SplitMethod.LinearSplit;
      } else {
        this.splitMethod = SplitMethod.QuadraticSplit;
      }
    }

    public RTreeGuttman createRTree(int m, int M) {
      switch (splitMethod) {
        case LinearSplit: return new RTreeGuttman(m, M);
        case QuadraticSplit: return new RTreeGuttmanQuadraticSplit(m, M);
        default: return new RTreeGuttman(m, M);
      }

    }
  }

  @SpatialPartitioner.Metadata(
      disjointSupported = false,
      extension = "rstreebb",
      description = "Loads a sample points into an R*-tree and use its leaf nodes as partition boundaries")
  public static class RStarTreeBBPartitioner extends AbstractRTreeBBPartitioner {
    public RTreeGuttman createRTree(int m, int M) {
      return new RStarTree(m, M);
    }
  }

  @SpatialPartitioner.Metadata(
      disjointSupported = false,
      extension = "rrstreebb",
      description = "Loads a sample points into an RR*-tree and use its leaf nodes as partition boundaries")
  public static class RRStarTreeBBPartitioner extends AbstractRTreeBBPartitioner {
    public RTreeGuttman createRTree(int m, int M) {
      return new RRStarTree(m, M);
    }
  }

  /**
   * Tests if a partition overlaps a given rectangle
   * @param partitionID
   * @param ienv
   * @return
   */
  protected boolean Partition_overlap(int partitionID, Envelope ienv) {
    for (int d = 0; d < getCoordinateDimension(); d++) {
      if (maxCoord[d][partitionID] <= ienv.minCoord[d] || ienv.maxCoord[d] <= minCoord[d][partitionID])
        return false;
    }
    return true;
  }

  /**
   * Computes the area of a partition.
   * @param partitionID
   * @return
   */
  protected double Partition_volume(int partitionID) {
    double vol = 1.0;
    for (int d = 0; d < getCoordinateDimension(); d++)
      vol *= maxCoord[d][partitionID] - minCoord[d][partitionID];
    return vol;
  }

  /**
   * Computes the expansion that will happen on an a partition when it is
   * enlarged to enclose a given rectangle.
   * @param iPartition
   * @param env the MBR of the object to be added to the partition
   * @return
   */
  protected double Partition_expansion(int iPartition, Envelope env) {
    double volBefore = 1.0, volAfter = 1.0;
    assert env.getCoordinateDimension() == this.getCoordinateDimension();
    for (int d = 0; d < getCoordinateDimension(); d++) {
      volBefore *= maxCoord[d][iPartition] - minCoord[d][iPartition];
      volAfter *= Math.max(maxCoord[d][iPartition], env.maxCoord[d]) -
          Math.min(minCoord[d][iPartition], env.minCoord[d]);
    }
    return volAfter - volBefore;
  }

  @Override
  public void write(DataOutput out) throws IOException {
    out.writeInt(getCoordinateDimension());
    out.writeInt(getPartitionCount());
    for (int d = 0; d < getCoordinateDimension(); d++) {
      for (int i = 0; i < getPartitionCount(); i++) {
        out.writeDouble(minCoord[d][i]);
        out.writeDouble(maxCoord[d][i]);
      }
    }
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    int numDimensions = in.readInt();
    int numPartitions = in.readInt();
    minCoord = new double[numDimensions][numPartitions];
    maxCoord = new double[numDimensions][numPartitions];
    for (int d = 0; d < getCoordinateDimension(); d++) {
      for (int i = 0; i < getPartitionCount(); i++) {
        minCoord[d][i] = in.readDouble();
        maxCoord[d][i] = in.readDouble();
      }
    }
  }

  @Override
  public boolean isDisjoint() {
    return false;
  }

  @Override
  public int getCoordinateDimension() {
    return minCoord.length;
  }

  @Override
  public int getPartitionCount() {
    return minCoord == null? 0 : minCoord[0].length;
  }

  @Override
  public void overlapPartitions(Envelope mbr, IntArray matchedPartitions) {
    matchedPartitions.clear();
    for (int i = 0; i < minCoord[0].length; i++) {
      if (Partition_overlap(i, mbr))
        matchedPartitions.add(i);
    }
  }

  @Override
  public int overlapPartition(Envelope mbr) {
    double minExpansion = Double.POSITIVE_INFINITY;
    int chosenPartition = -1;
    for (int iPartition = 0; iPartition < minCoord[0].length; iPartition++) {
      double expansion = Partition_expansion(iPartition, mbr);
      if (expansion < minExpansion) {
        minExpansion = expansion;
        chosenPartition = iPartition;
      } else if (expansion == minExpansion) {
        // Resolve ties by choosing the entry with the rectangle of smallest area
        if (Partition_volume(iPartition) < Partition_volume(chosenPartition))
          chosenPartition = iPartition;
      }
    }
    return chosenPartition;
  }

  @Override
  public void getPartitionMBR(int partitionID, Envelope mbr) {
    for (int d = 0; d < getCoordinateDimension(); d++) {
      mbr.minCoord[d] = this.minCoord[d][partitionID];
      mbr.maxCoord[d] = this.maxCoord[d][partitionID];
    }
  }

  @Override
  public Envelope getEnvelope() {
    return  mbrPoints;
  }
}
