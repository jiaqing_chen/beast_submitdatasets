/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.indexing;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.stsynopses.AbstractHistogram;
import edu.ucr.cs.bdlab.stsynopses.Summary;
import edu.ucr.cs.bdlab.util.IntArray;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * A partitioner that partitions a file according to an existing set of cells (MBBs). It internally builds an in-memory
 * {@link RRStarTree} for the partitions to speed up the search.
 * @author Ahmed Eldawy
 *
 */
@SpatialPartitioner.Metadata(
    disjointSupported = false,
    description = "Partitions the space based on existing set of cells, e.g., another file",
    extension = "cells"
)
public class CellPartitioner implements SpatialPartitioner {

  /**An R-tree that indexes the set of existing partition for efficient search*/
  protected RTreeGuttman partitions;

  /**The list of cells that this partitioner can choose from*/
  protected PartitionInfo[] cells;

  /**The degree of the R-Tree index that we use to speed up node lookup.*/
  protected static final int RTreeDegree = 32;

  /**The MBR of the input space*/
  protected final Envelope envelope = new Envelope(2);

  /**
   * A default constructor to be able to dynamically instantiate it
   * and deserialize it
   */
  public CellPartitioner() {
  }

  public CellPartitioner(PartitionInfo[] cells) {
    this.cells = new PartitionInfo[cells.length];
    // We have to explicitly create each object as PartitionInfo to ensure that write/readFields will work correctly
    for (int i = 0; i < cells.length; i++)
      this.cells[i] = new PartitionInfo(cells[i]);
    initialize(cells);
  }

  @Override
  public void construct(Summary summary, double[][] sample, AbstractHistogram histogram) {
    throw new RuntimeException("Not supported! Please use the method initialize(PartitionInfo[]) to create.");
  }

  /**
   * Initialize the R-tree from the list of cells.
   * @param cells
   */
  private void initialize(Envelope[] cells) {
    double[] x1s = new double[cells.length];
    double[] y1s = new double[cells.length];
    double[] x2s = new double[cells.length];
    double[] y2s = new double[cells.length];

    envelope.setEmpty();

    for (int i = 0; i < cells.length; i++) {
      x1s[i] = cells[i].minCoord[0];
      y1s[i] = cells[i].minCoord[1];
      x2s[i] = cells[i].maxCoord[0];
      y2s[i] = cells[i].maxCoord[1];
      envelope.minCoord[0] = Math.min(envelope.minCoord[0], x1s[i]);
      envelope.minCoord[1] = Math.min(envelope.minCoord[1], y1s[i]);
      envelope.maxCoord[0] = Math.min(envelope.minCoord[0], x2s[i]);
      envelope.maxCoord[1] = Math.min(envelope.minCoord[1], y2s[i]);
    }

    // The RR*-tree paper recommends setting m = 0.2 M
    partitions = new RRStarTree(RTreeDegree / 5, RTreeDegree);
    partitions.initializeHollowRTree(x1s, y1s, x2s, y2s);
  }

  @Override
  public void write(DataOutput out) throws IOException {
    this.envelope.write(out);
    out.writeInt(cells.length);
    for (Envelope cell : cells)
      cell.write(out);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    this.envelope.readFields(in);
    int numCells = in.readInt();
    if (cells == null || cells.length != numCells) {
      // Initialize the array of cells only if needed
      cells = new PartitionInfo[numCells];
      for (int i = 0; i < numCells; i++)
        cells[i] = new PartitionInfo();
    }
    for (int i = 0; i < numCells; i++)
      cells[i].readFields(in);

    // Re-initialize the R-tree
    initialize(cells);
  }
  
  @Override
  public int getPartitionCount() {
    return cells == null ? 0 : cells.length;
  }

  @Override
  public void overlapPartitions(Envelope mbr, IntArray matchedPartitions) {
    throw new RuntimeException("Disjoint partitioning is not supported!");
  }

  @Override
  public int overlapPartition(Envelope geomMBR) {
    // TODO avoid construction of the IntArray multiple times
    IntArray tempPartitions = new IntArray();
    partitions.search(geomMBR.minCoord, geomMBR.maxCoord, tempPartitions);
    int chosenCellIndex;
    if (tempPartitions.size() == 1) {
      // Only one overlapping node, return it
      chosenCellIndex = tempPartitions.peek();
    } else if (tempPartitions.size() > 0) {
      // More than one overlapping cells, choose the best between them
      chosenCellIndex = -1;
      double minVol = Double.POSITIVE_INFINITY;
      double minPerim = Double.POSITIVE_INFINITY;
      for (int overlappingCellIndex : tempPartitions) {
        Envelope overlappingCell = cells[overlappingCellIndex];
        double vol = overlappingCell.getVolume();
        if (vol < minVol) {
          minVol = vol;
          minPerim = overlappingCell.getSideLength(0) + overlappingCell.getSideLength(1);
          chosenCellIndex = overlappingCellIndex;
        } else if (vol == minVol) {
          // This also covers the case of vol == minVol == 0
          double cellPerimeter = overlappingCell.getSideLength(0) + overlappingCell.getSideLength(1);
          if (cellPerimeter < minPerim) {
            minPerim = cellPerimeter;
            chosenCellIndex = overlappingCellIndex;
          }
        }
      }
    } else {
      // No overlapping cells, follow the (fake) insert choice
      if (geomMBR instanceof  Envelope) {
        // TODO avoid construction of the search coordinate array
        Envelope evn2d = (Envelope) geomMBR;
        chosenCellIndex = partitions.noInsert(new double[] {evn2d.minCoord[0], evn2d.minCoord[1]}, new double[] {evn2d.maxCoord[0], evn2d.maxCoord[1]});
      } else if (geomMBR instanceof Envelope) {
        Envelope env = (Envelope) geomMBR;
        chosenCellIndex = partitions.noInsert(env.minCoord, env.maxCoord);
      } else {
        throw new RuntimeException("Cannot handle the envelope " + geomMBR);
      }
    }
    return cells[chosenCellIndex].partitionId;
  }

  @Override
  public void getPartitionMBR(int partitionID, Envelope mbr) {
    for (PartitionInfo p : cells) {
      if (p.partitionId == partitionID)
        mbr.set(p.minCoord, p.maxCoord);
    }
  }

  @Override
  public boolean isDisjoint() {
    return false;
  }

  @Override
  public int getCoordinateDimension() {
    return cells[0].getCoordinateDimension();
  }

  @Override
  public Envelope getEnvelope() {
    return envelope;
  }
}
