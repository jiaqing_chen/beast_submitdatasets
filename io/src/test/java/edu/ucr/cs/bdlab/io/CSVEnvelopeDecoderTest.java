package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.Envelope;
import junit.framework.TestCase;


public class CSVEnvelopeDecoderTest extends TestCase {

  public void testParse3D() {
    Envelope expected = new Envelope(3, 0.0, 1.2, 3.4, 15.3, 13.4, 17.7);
    String str = "0.0,1.2,3.4,15.3,13.4,17.7";
    Envelope actual = CSVEnvelopeDecoder.instance.apply(str, null);
    assertEquals(expected, actual);
  }

  public void testReuseEnvelopes() {
    Envelope expected = new Envelope(3, 0.0, 1.2, 3.4, 15.3, 13.4, 17.7);
    String str = "0.0,1.2,3.4,15.3,13.4,17.7";
    Envelope actual = CSVEnvelopeDecoder.instance.apply(str, new Envelope(2));
    assertEquals(expected, actual);
  }
}