/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.GeometryType;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.test.SparkTest;
import edu.ucr.cs.bdlab.util.OperationException;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.spark.api.java.JavaPairRDD;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.Stack;

public class SpatialInputFormatTest extends SparkTest {

  public void testDoNotSkipHiddenFilesIfExplicitlySpecified() throws IOException {
    Path csvPath = new Path(scratchPath, "_temp.csv");
    copyResource("/test_wkt.csv", new File(csvPath.toString()));
    Configuration conf = getActualHadoopConfiguration(getSC());
    conf.set(SpatialInputFormat.InputFormat, "wkt(1)");
    conf.setBoolean(CSVFeatureReader.SkipHeader, true);
    conf.set(CSVFeatureReader.FieldSeparator, "\t");
    JavaPairRDD<Envelope, IFeature> records = getSC().newAPIHadoopFile(csvPath.toString(), SpatialInputFormat.class, Envelope.class, IFeature.class, conf);
    List<IFeature> features = collectClone(records.values());
    assertEquals(2, features.size());
  }

  public void testReadCSVFiles() throws IOException {
    Path csvPath = new Path(scratchPath, "temp.csv");
    copyResource("/test_wkt.csv", new File(csvPath.toString()));
    Configuration conf = getActualHadoopConfiguration(getSC());
    conf.set(SpatialInputFormat.InputFormat, "wkt(1)");
    conf.setBoolean(CSVFeatureReader.SkipHeader, true);
    conf.set(CSVFeatureReader.FieldSeparator, "\t");
    JavaPairRDD<Envelope, IFeature> records = getSC().newAPIHadoopFile(csvPath.toString(), SpatialInputFormat.class, Envelope.class, IFeature.class, conf);
    List<IFeature> features = collectClone(records.values());
    assertEquals(2, features.size());
  }

  public void testDuplicateAvoidanceWithNoRangeQuery() throws IOException {
    // Read a disjoint partitioned file and make sure that duplicate records are not repeated
    Path indexPath = new Path(scratchPath, "temp");
    copyDirectoryFromResources("/test_index", new File(indexPath.toString()));
    Configuration conf = getActualHadoopConfiguration(getSC());
    conf.set(SpatialInputFormat.InputFormat, "envelopek(2)");
    conf.set(CSVFeatureReader.FieldSeparator, ",");
    JavaPairRDD<Envelope, IFeature> records = getSC().newAPIHadoopFile(indexPath.toString(), SpatialInputFormat.class, Envelope.class, IFeature.class, conf);
    List<IFeature> features = collectClone(records.values());
    assertEquals(3, features.size());
  }

  public void testDuplicateAvoidanceWithRangeQuery() throws IOException {
    // Read a disjoint partitioned file and make sure that duplicate records are not repeated
    Path indexPath = new Path(scratchPath, "temp");
    copyDirectoryFromResources("/test_index", new File(indexPath.toString()));
    Configuration conf = getActualHadoopConfiguration(getSC());
    conf.set(SpatialInputFormat.FilterMBR, "2,2,4,4");
    conf.set(SpatialInputFormat.InputFormat, "envelopek(2)");
    conf.set(CSVFeatureReader.FieldSeparator, ",");
    JavaPairRDD<Envelope, IFeature> records = getSC().newAPIHadoopFile(indexPath.toString(), SpatialInputFormat.class, Envelope.class, IFeature.class, conf);
    List<IFeature> features = collectClone(records.values());
    assertEquals(2, features.size());
  }

  public void testRecursive() throws IOException {
    Path filePath = new Path(scratchPath, "dir");
    Path csvPath = new Path(filePath, "subdir/deeper/tree/temp.csv");
    copyResource("/test_points.csv", new File(csvPath.toString()));

    Configuration conf = getActualHadoopConfiguration(getSC());
    conf.set(CSVFeatureReader.FieldSeparator, ",");
    conf.setBoolean(CSVFeatureReader.SkipHeader, true);
    conf.setBoolean(SpatialInputFormat.Recursive, true);
    conf.set(SpatialInputFormat.InputFormat, "point(1,2)");
    JavaPairRDD<Envelope, IFeature> records = getSC().newAPIHadoopFile(filePath.toString(), SpatialInputFormat.class, Envelope.class, IFeature.class, conf);
    List<IFeature> features = collectClone(records.values());
    assertEquals(2, features.size());
  }

  public void testReadCSVPoints() throws IOException {
    Path csvPath = new Path(scratchPath, "temp.csv");
    copyResource("/test_points.csv", new File(csvPath.toString()));

    Configuration conf = getActualHadoopConfiguration(getSC());
    conf.set(CSVFeatureReader.FieldSeparator, ",");
    conf.setBoolean(CSVFeatureReader.SkipHeader, true);
    conf.set(SpatialInputFormat.InputFormat, "point(1,2)");
    SpatialInputFormat.getFeatureReaderClass("point(1,2)");
    JavaPairRDD<Envelope, IFeature> records = getSC().newAPIHadoopFile(csvPath.toString(), SpatialInputFormat.class, Envelope.class, IFeature.class, conf);
    List<IFeature> features = collectClone(records.values());
    assertEquals(2, features.size());
  }

  public void testReadShapefiles() throws IOException {
    Path shpPath = new Path(scratchPath, "temp.shp");
    copyResource("/usa-major-cities/usa-major-cities.shp", new File(shpPath.toString()));
    Path dbfPath = new Path(scratchPath, "temp.dbf");
    copyResource("/usa-major-cities/usa-major-cities.dbf", new File(dbfPath.toString()));

    Configuration conf = getActualHadoopConfiguration(getSC());
    conf.set(SpatialInputFormat.InputFormat, "shapefile");
    SpatialInputFormat.getFeatureReaderClass("shapefile");
    JavaPairRDD<Envelope, IFeature> records = getSC().newAPIHadoopFile(shpPath.toString(), SpatialInputFormat.class, Envelope.class, IFeature.class, conf);
    List<IFeature> features = collectClone(records.values());
    assertEquals(120, features.size());
  }

  public void testReadShapefilesFromDirectory() throws IOException {
    FileSystem fs = scratchPath.getFileSystem(new Configuration());
    Path inputPath = new Path(scratchPath, "temp");
    fs.mkdirs(inputPath);
    Path shpPath = new Path(inputPath, "temp.shp");
    copyResource("/usa-major-cities/usa-major-cities.shp", new File(shpPath.toString()));
    Path dbfPath = new Path(inputPath, "temp.dbf");
    copyResource("/usa-major-cities/usa-major-cities.dbf", new File(dbfPath.toString()));

    Configuration conf = getActualHadoopConfiguration(getSC());
    conf.set(SpatialInputFormat.InputFormat, "shapefile");
    JavaPairRDD<Envelope, IFeature> records = getSC().newAPIHadoopFile(inputPath.toString(), SpatialInputFormat.class, Envelope.class, IFeature.class, conf);
    List<IFeature> features = collectClone(records.values());
    assertEquals(120, features.size());
  }

  public void testReadManyShapefilesFromDirectory() throws IOException {
    FileSystem fs = scratchPath.getFileSystem(new Configuration());
    Path inputPath = new Path(scratchPath, "temp");
    ((FileSystem) fs).mkdirs(inputPath);
    copyResource("/usa-major-cities/usa-major-cities.shp", new File(new Path(inputPath, "temp.shp").toString()));
    copyResource("/usa-major-cities/usa-major-cities.dbf", new File(new Path(inputPath, "temp.dbf").toString()));
    copyResource("/usa-major-cities/usa-major-cities.shp", new File(new Path(inputPath, "temp2.shp").toString()));
    copyResource("/usa-major-cities/usa-major-cities.dbf", new File(new Path(inputPath, "temp2.dbf").toString()));

    Configuration conf = getActualHadoopConfiguration(getSC());
    conf.set(SpatialInputFormat.InputFormat, "shapefile");
    JavaPairRDD<Envelope, IFeature> records = getSC().newAPIHadoopFile(inputPath.toString(), SpatialInputFormat.class, Envelope.class, IFeature.class, conf);
    List<IFeature> features = collectClone(records.values());
    assertEquals(120 * 2, features.size());
  }

  public void testShouldGenerateSplitsFromMasterFile() throws IOException {
    // Create a directory with a master file and some data files
    // Make sure that getSplits method will be limited to what is listed in the master file
    Configuration conf = new Configuration();
    conf.set(SpatialInputFormat.InputFormat, "point(0,1)");
    Path indexPath = new Path(scratchPath, "index");
    FileSystem fileSystem = indexPath.getFileSystem(conf);
    fileSystem.mkdirs(indexPath);
    Path masterFilePath = new Path(indexPath, "_master.heap");
    copyResource("/test.partitions", new File(masterFilePath.toString()));
    // Create fake files
    for (int partitionID = 0; partitionID < 45; partitionID++) {
      fileSystem.create(new Path(indexPath, String.format("part-%05d", partitionID))).close();
    }
    Job job = Job.getInstance(conf);
    SpatialInputFormat.addInputPath(job, indexPath);
    SpatialInputFormat inputFormat = new SpatialInputFormat() {
      @Override
      protected boolean isSplitable(JobContext context, Path filename) {
        return false;
      }
    };
    List<InputSplit> status = inputFormat.getSplits(job);
    assertEquals(44, status.size());
  }

  public void testShouldPruneNonMatchingFiles() throws IOException {
    // Create a master file with different MBRs for different files. Make sure the getSplits method returns only
    // the splits that overlap the query MBR
    // Create a directory with a master file and some data files
    // Make sure that getSplits method will be limited to what is listed in the master file
    Configuration conf = new Configuration();
    conf.set(SpatialInputFormat.FilterMBR, "-125,55,-55,60");
    conf.set(SpatialInputFormat.InputFormat, "point(0,1)");
    Path indexPath = new Path(scratchPath, "index");
    FileSystem fileSystem = indexPath.getFileSystem(conf);
    fileSystem.mkdirs(indexPath);
    Path masterFilePath = new Path(indexPath, "_master.heap");
    copyResource("/test.partitions", new File(masterFilePath.toString()));
    // Create fake files
    for (int partitionID = 0; partitionID < 45; partitionID++) {
      fileSystem.create(new Path(indexPath, String.format("part-%05d", partitionID))).close();
    }
    Job job = Job.getInstance(conf);
    SpatialInputFormat.addInputPath(job, indexPath);
    SpatialInputFormat inputFormat = new SpatialInputFormat() {
      @Override
      protected boolean isSplitable(JobContext context, Path filename) {
        return false;
      }
    };
    List<InputSplit> status = inputFormat.getSplits(job);
    assertEquals(3, status.size());
  }

  public void testShouldWorkWithCompressedShapeFiles() throws IOException {
    // Point to an input directory that has several compressed zip files. Make sure that the records inside these
    // shapefiles will be returned.
    Path inputPath = new Path(scratchPath, "index");
    Configuration conf = getActualHadoopConfiguration(getSC());
    conf.set(SpatialInputFormat.InputFormat, "shapefile");
    SpatialInputFormat.getFeatureReaderClass("shapefile");
    inputPath.getFileSystem(conf).mkdirs(inputPath);
    copyResource("/usa-major-cities.zip", new File(new Path(inputPath, "temp1.zip").toString()));
    copyResource("/usa-major-cities.zip", new File(new Path(inputPath, "temp2.zip").toString()));

    JavaPairRDD<Envelope, IFeature> values = getSC().newAPIHadoopFile(inputPath.toString(), SpatialInputFormat.class, Envelope.class, IFeature.class, conf);
    List<IFeature> geometries = collectClone(values.values());
    assertEquals(240, geometries.size());
  }

  public void testShouldNotSplitZipFiles() throws IOException {
    Path inputPath = new Path(scratchPath, "shape.zip");
    copyResource("/usa-major-cities.zip", new File(inputPath.toString()));

    Configuration conf = getActualHadoopConfiguration(getSC());
    conf.set(SpatialInputFormat.InputFormat, "shapefile");
    conf.setLong(FileInputFormat.SPLIT_MAXSIZE, 2*1024);
    conf.setLong(FileInputFormat.SPLIT_MINSIZE, 1024);
    Job job = Job.getInstance(conf);
    SpatialInputFormat.addInputPath(job, inputPath);
    SpatialInputFormat inputFormat = new SpatialInputFormat();
    List<InputSplit> splits = inputFormat.getSplits(job);
    assertEquals(1, splits.size());
  }

  public void testShouldPruneHigherDimensional() throws IOException {
    // Create a master file with different MBRs for different files. Make sure the getSplits method returns only
    // the splits that overlap the query MBR
    // Create a directory with a master file and some data files
    // Make sure that getSplits method will be limited to what is listed in the master file
    Configuration conf = new Configuration();
    conf.set(SpatialInputFormat.FilterMBR, "3,2,11,5,3,12");
    conf.set(SpatialInputFormat.InputFormat, "pointk(3)");
    Path indexPath = new Path(scratchPath, "index");
    Path masterFilePath = new Path(indexPath, "_master.heap");
    FileSystem fileSystem = indexPath.getFileSystem(conf);
    fileSystem.mkdirs(indexPath);
    PrintStream ps = new PrintStream(fileSystem.create(masterFilePath));
    ps.println("ID\tFile Name\tRecord Count\tData Size\tGeometry");
    ps.println("1\tpart-00000\t10\t100\t\t1\t1\t8\t4\t3\t9");
    ps.println("2\tpart-00001\t20\t200\t\t4\t2\t10\t6\t4\t12");
    ps.close();
    // Create fake files
    fileSystem.create(new Path(indexPath, "part-00000")).close();
    fileSystem.create(new Path(indexPath, "part-00001")).close();

    Job job = Job.getInstance(conf);
    SpatialInputFormat.addInputPath(job, indexPath);
    SpatialInputFormat inputFormat = new SpatialInputFormat() {
      @Override
      protected boolean isSplitable(JobContext context, Path filename) {
        return false;
      }
    };
    List<InputSplit> status = inputFormat.getSplits(job);
    assertEquals(1, status.size());
    assertEquals("part-00001", ((FileSplit)status.get(0)).getPath().getName());
  }


  public void testSetInputFormatShouldThrowAnExceptionForWrongFormats() {
    // A valid input should not raise an exception
    boolean errorRaised = false;
    try {
      UserOptions opts = new UserOptions();
      opts.set(SpatialInputFormat.InputFormat, "point(1,2)");
    } catch (OperationException e) {
      errorRaised = true;
    }
    assertFalse("No error should be raised", errorRaised);

    // An invalid input should raise an exception
    errorRaised = false;
    try {
      UserOptions opts = new UserOptions();
      SpatialInputFormat.getFeatureReaderClass("errrr");
    } catch (OperationException e) {
      errorRaised = true;
    }
    assertTrue("An error should be raised", errorRaised);

    // A correction for slight typos should be reported in the error message
    errorRaised = false;
    try {
      UserOptions opts = new UserOptions();
      SpatialInputFormat.getFeatureReaderClass("piont");
    } catch (OperationException e) {
      errorRaised = true;
      assertTrue("The error message should suggest a correction", e.getMessage().contains("point"));
    }
    assertTrue("An error should be raised", errorRaised);

    // Suggest a correction when extra parameters are added
    errorRaised = false;
    try {
      SpatialInputFormat.getFeatureReaderClass("piont(1,2)");
    } catch (OperationException e) {
      errorRaised = true;
      assertTrue("The error message should suggest a correction", e.getMessage().contains("point"));
    }
    assertTrue("An error should be raised", errorRaised);
  }

  public void testSetInputFormatShouldAutoDetectTheInput() throws IOException {
    // A shapefile is detected from the file extension without opening the file
    String inputPath = "input.shp";
    UserOptions opts = new UserOptions();
    assertTrue(SpatialInputFormat.autodetectInputFormat(opts, inputPath));
    assertEquals("shapefile", opts.get(SpatialInputFormat.InputFormat));

    // Test a case of unsupported file
    inputPath = "input.xyz";
    opts = new UserOptions();
    assertFalse("Should not be able to autodetect the input", SpatialInputFormat.autodetectInputFormat(opts, inputPath));
    assertNull("iformat should not be set", opts.get(SpatialInputFormat.InputFormat));

    // Test auto detection when the input path points to a directory
    inputPath = new Path(scratchPath, "testdir").toString();
    copyDirectoryFromResources("/usa-major-cities", new File(inputPath));
    opts = new UserOptions();
    assertTrue("Should be able to auto-detect the input", SpatialInputFormat.autodetectInputFormat(opts, inputPath));
    assertEquals("shapefile", opts.get(SpatialInputFormat.InputFormat));
  }

  public void testAddDependentClassesWithMultipleInputFormats() {
    UserOptions opts = new UserOptions("iformat[1]:wkt", "iformat[0]:geojson", "separator:,");
    Stack<Class> dependentClasses = new Stack<>();
    SpatialInputFormat.addDependentClasses(opts, dependentClasses);
    assertEquals(2, dependentClasses.size());
  }

  public void testAddDependentClassesWithOneInputFormat() {
    UserOptions opts = new UserOptions("iformat:wkt");
    Stack<Class> dependentClasses = new Stack<>();
    SpatialInputFormat.addDependentClasses(opts, dependentClasses);
    assertEquals(1, dependentClasses.size());
  }

  public void testReadOneFeature() throws IOException {
    // Test read from an indexed directory
    Path inPath = new Path(scratchPath, "test_index");
    copyDirectoryFromResources("/test_index", new File(inPath.toString()));
    UserOptions opts = new UserOptions("iformat:point", "separator:,");
    FileSystem fileSystem = inPath.getFileSystem(opts);
    IFeature feature = SpatialInputFormat.readOne(fileSystem, inPath, opts);
    assertEquals(GeometryType.POINT, feature.getGeometry().getType());
    assertEquals(2, feature.getNumAttributes());

    // Test read from a file
    feature = SpatialInputFormat.readOne(fileSystem, new Path(inPath, "part-00.csv"), opts);
    assertEquals(GeometryType.POINT, feature.getGeometry().getType());
    assertEquals(2, feature.getNumAttributes());

    // Test read from a non-indexed directory
    fileSystem.delete(new Path(inPath, "_master.grid"), false);
    feature = SpatialInputFormat.readOne(fileSystem, inPath, opts);
    assertEquals(GeometryType.POINT, feature.getGeometry().getType());
    assertEquals(2, feature.getNumAttributes());
  }

  public void testReprojectFile() throws IOException {
    Path inputPath = new Path(scratchPath, "us48.zip");
    UserOptions opts = new UserOptions();
    opts.set(SpatialInputFormat.InputFormat, "shapefile");
    opts.set(SpatialInputFormat.TargetCRS, "epsg:4326");
    copyResource("/us48_epsg3857.zip", new File(inputPath.toString()));

    JavaPairRDD<Envelope, IFeature> values = getSC().newAPIHadoopFile(inputPath.toString(), SpatialInputFormat.class,
        Envelope.class, IFeature.class, opts);
    List<IFeature> geometries = collectClone(values.values());
    assertEquals(1, geometries.size());
    Envelope mbr = new Envelope(2);
    geometries.get(0).getGeometry().envelope(mbr);
    assertEquals(-124.64776, mbr.minCoord[0], 1E-3);
    assertEquals(-66.60283, mbr.maxCoord[0], 1E-3);
    assertEquals(25.125123, mbr.minCoord[1], 1E-3);
    assertEquals(48.97723, mbr.maxCoord[1], 1E-3);
  }

  public void testGetFeatureReaderClass() throws IOException {
    // 1- Test if the feature reader class is set
    Configuration conf = new Configuration();
    Class<? extends FeatureReader> expectedClass = CSVFeatureReader.class;
    conf.setClass(SpatialInputFormat.FeatureReaderClass, expectedClass, FeatureReader.class);
    Class<? extends FeatureReader> actualClass = SpatialInputFormat.getFeatureReaderClass(conf, new Path("."));
    assertEquals(expectedClass, actualClass);

    // 2- Test creating from iformat
    conf = new Configuration();
    conf.set(SpatialInputFormat.InputFormat, "wkt");
    actualClass = SpatialInputFormat.getFeatureReaderClass(conf, new Path("."));
    assertEquals(expectedClass, actualClass);
    // Assert that it was explicitly set for future calls
    actualClass = conf.getClass(SpatialInputFormat.FeatureReaderClass, null, FeatureReader.class);
    assertEquals(expectedClass, actualClass);

    // 3- Autodetect the input if not set
    conf = new Configuration();
    Path input = new Path(scratchPath, "input.csv");
    copyResource("/test_points.csv", new File(input.toString()));
    actualClass = SpatialInputFormat.getFeatureReaderClass(conf, input);
    assertEquals(expectedClass, actualClass);
    // Assert that the configuration was updated
    assertEquals("point(1,2)", conf.get(SpatialInputFormat.InputFormat));
    assertEquals(",", conf.get(CSVFeatureReader.FieldSeparator));
    assertTrue("skip header should be set", conf.getBoolean(CSVFeatureReader.SkipHeader, false));
    // Assert that it was explicitly set for future calls
    actualClass = conf.getClass(SpatialInputFormat.FeatureReaderClass, null, FeatureReader.class);
    assertEquals(expectedClass, actualClass);
  }
}