package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.Feature;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.test.SparkTest;

public class CSVPointEncoderTest extends SparkTest {

  public void testPointWriterNoFeatureAttributes() {
    Point p = new Point(4, 0.5, 0.1, 1.2, 3.5);
    Feature f = new Feature();
    f.setGeometry(p);
    CSVPointEncoder writer = new CSVPointEncoder('\t', 0, 1, 2, 3);
    String s = writer.apply(f, null).toString();
    assertEquals("0.5\t0.1\t1.2\t3.5", s);
  }

  public void testPointWriterWithFeatureAttributes() {
    Point p = new Point(4, 0.5, 0.1, 1.2, 3.5);
    CSVFeature f = new CSVFeature(p);
    String dataAttributes = "att1,att2,att3,att4";
    char fieldSeparator = ',';
    f.setFieldSeparator((byte) fieldSeparator);
    f.setGeometry(p);
    byte[] bytes = dataAttributes.getBytes();
    f.setFieldValues(bytes, 0, bytes.length);
    CSVPointEncoder writer = new CSVPointEncoder(',', 1, 2, 5, 6);
    String s = writer.apply(f, null).toString();
    assertEquals("att1,0.5,0.1,att2,att3,1.2,3.5,att4", s);
  }
}