/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.test.SparkTest;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;

public class ShapefileFeatureWriterTest extends SparkTest {

  public void testCreation() throws IOException, InterruptedException {
    IGeometry[] geometries = {
        new Point(2, 0.0,1.0),
        new Point(2, 3.0, 10.0)
    };

    Configuration conf = getActualHadoopConfiguration(getSC());
    Path outPath = new Path(scratchPath, "test");
    Path shpFileName = new Path(outPath, "test.shp");
    FileSystem fileSystem = outPath.getFileSystem(conf);
    fileSystem.mkdirs(outPath);

    ShapefileFeatureWriter writer = new ShapefileFeatureWriter();
    try {
      writer.initialize(shpFileName, conf);
      for (IGeometry geom : geometries) {
        CSVFeature f = new CSVFeature(geom);
        f.setFieldNames("name");
        f.setFieldValues("name-value");
        writer.write(null, f);
      }
    } finally {
      writer.close(null);
    }

    assertTrue("Shapefile not found", fileSystem.exists(shpFileName));
    Path shxFileName = new Path(outPath, "test.shx");
    Path dbfFileName = new Path(outPath, "test.dbf");
    assertTrue("Shape index file not found", fileSystem.exists(shxFileName));
    assertTrue("DBF file not found", fileSystem.exists(dbfFileName));
  }
}