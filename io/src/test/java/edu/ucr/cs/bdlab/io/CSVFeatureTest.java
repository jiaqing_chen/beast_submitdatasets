package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.EmptyGeometry;
import junit.framework.TestCase;

public class CSVFeatureTest extends TestCase {

  public void testGetNumAttributesShouldHandleQuotedFields() {
    CSVFeature f = new CSVFeature(EmptyGeometry.instance);
    // Test non-quoted attributes
    f.setFieldSeparator((byte)',');
    f.setFieldValues("att1,att2,att3");
    assertEquals(3, f.getNumAttributes());

    // Test quoted attributes
    f.setQuoteCharacters("\'\'");
    f.setFieldValues("att1,'att2,att2,att2',att3");
    assertEquals(3, f.getNumAttributes());

    // Test empty attributes
    f.setFieldValues("att0,att1,,");
    assertEquals(4, f.getNumAttributes());

    // Test empty attributes
    f.setFieldValues("att0,att1,,,,att3");
    assertEquals(6, f.getNumAttributes());

    // Test empty attributes
    f.setFieldValues(",att1,,,,att3");
    assertEquals(6, f.getNumAttributes());

    // Test special quote characters
    f.setQuoteCharacters("[]");
    f.setFieldValues("att1,[att2,att2,att2],att3");
    assertEquals(3, f.getNumAttributes());
  }

  public void testGetAttributeValueShouldHandleQuotedAttributes() {
    CSVFeature f = new CSVFeature(EmptyGeometry.instance);
    // Test non-quoted attributes
    f.setFieldSeparator((byte)',');
    f.setQuoteCharacters("\'\'");
    f.setFieldValues("att1,att2,att3");
    assertEquals("att2", f.getAttributeValue(1));

    // Test quoted attributes
    f.setFieldValues("att1,'att2,att2,att2',att3");
    assertEquals("att2,att2,att2", f.getAttributeValue(1));
    assertEquals("att3", f.getAttributeValue(2));

    // Test empty attributes
    f.setFieldValues("att0,att1,,");
    assertEquals(null, f.getAttributeValue(3));

    // Test empty attributes at the beginning
    f.setFieldValues(",att1,,");
    assertEquals(null, f.getAttributeValue(0));
  }
}