package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.Feature;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.test.SparkTest;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class DBFReaderTest extends SparkTest {

  public void testSimpleReader() throws IOException, InterruptedException {
    Path dbfPath = new Path(scratchPath, "temp.dbf");
    copyResource("/usa-major-cities/usa-major-cities.dbf", new File(dbfPath.toString()));
    DBFReader reader = new DBFReader();
    try {
      reader.initialize(dbfPath, new Configuration());
      assertEquals(120, reader.header.numRecords);
      assertEquals(4, reader.header.fieldDescriptors.length);
      int recordCount = 0;
      while (reader.nextKeyValue()) {
        IFeature feature = reader.getCurrentValue();
        assertEquals(4, feature.getNumAttributes());
        assertNotNull(feature.getAttributeValue("RuleID"));
        assertEquals(feature.getAttributeValue(3), feature.getAttributeValue("RuleID"));
        recordCount++;
      }
      assertEquals(120, recordCount);
    } finally {
      reader.close();
    }
  }

  public void testSerializeDBFFeature() throws IOException, InterruptedException {
    Path dbfPath = new Path(scratchPath, "temp.dbf");
    copyResource("/usa-major-cities/usa-major-cities.dbf", new File(dbfPath.toString()));
    DBFReader reader = new DBFReader();
    try {
      reader.initialize(dbfPath, new Configuration());
      reader.nextKeyValue();
      IFeature originalFeature = reader.getCurrentValue();
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      DataOutputStream dos = new DataOutputStream(baos);
      originalFeature.write(dos);
      dos.close();

      DataInputStream in = new DataInputStream(new ByteArrayInputStream(baos.toByteArray()));
      DBFFeature deserializedFeature = new DBFFeature();
      deserializedFeature.readFields(in);
      assertEquals(originalFeature, deserializedFeature);
    } finally {
      reader.close();
    }
  }

  public void testParseFields() throws IOException, InterruptedException {
    String[] dbfFiles = {
        "/usa-major-cities/usa-major-cities.dbf",
        "/usa-major-highways/usa-major-highways.dbf",
        "/simple-with-dates.dbf"
    };
    for (String dbfFile : dbfFiles) {
      Path dbfPath = new Path(scratchPath, "temp.dbf");
      copyResource(dbfFile, new File(dbfPath.toString()));
      DBFReader reader = new DBFReader();
      try {
        reader.initialize(dbfPath, new Configuration());
        while (reader.nextKeyValue()) {
          IFeature feature = reader.getCurrentValue();
          for (int i = 0; i < feature.getNumAttributes(); i++)
            assertNotNull(feature.getAttributeValue(i));
        }
      } finally {
        reader.close();
        new File(dbfPath.toString()).delete();
      }
    }
  }

  public void testParseDates() throws IOException, InterruptedException {
    String dbfFile = "/simple-with-dates.dbf";
    Path dbfPath = new Path(scratchPath, "temp.dbf");
    copyResource(dbfFile, new File(dbfPath.toString()));
    DBFReader reader = new DBFReader();
    try {
      reader.initialize(dbfPath, new Configuration());
      assertTrue("Did not find records in the file", reader.nextKeyValue());
      IFeature feature = reader.getCurrentValue();
      assertEquals(new GregorianCalendar(2018, 12 - 1, 26), feature.getAttributeValue(1));
    } finally {
      reader.close();
      new File(dbfPath.toString()).delete();
    }
  }

  public void testImmutableObjects() throws IOException, InterruptedException {
    Path dbfPath = new Path(scratchPath, "temp.dbf");
    copyResource("/usa-major-cities/usa-major-cities.dbf", new File(dbfPath.toString()));
    DBFReader reader = new DBFReader();
    try {
      Configuration conf = new Configuration();
      conf.setBoolean(SpatialInputFormat.ImmutableObjects, true);
      reader.initialize(dbfPath, conf);
      assertEquals(120, reader.header.numRecords);
      assertEquals(4, reader.header.fieldDescriptors.length);
      int recordCount = 0;
      List<IFeature> features = new ArrayList<IFeature>();
      while (reader.nextKeyValue()) {
        IFeature feature = reader.getCurrentValue();
        features.add(feature);
        assertEquals(4, feature.getNumAttributes());
        assertNotNull(feature.getAttributeValue("RuleID"));
        assertEquals(feature.getAttributeValue(3), feature.getAttributeValue("RuleID"));
        recordCount++;
      }
      assertEquals(120, recordCount);
      assertNotSame(features.get(0), features.get(1));
    } finally {
      reader.close();
    }
  }

  public void testShouldNotReturnNullOnEmptyStrings() throws IOException, InterruptedException {
    DBFReader reader = new DBFReader();
    try {
      DataInputStream in = new DataInputStream(this.getClass().getResourceAsStream("/file_with_empty_numbers.dbf"));
      reader.initialize(in, new Configuration());
      while (reader.nextKeyValue()) {
        IFeature feature = reader.getCurrentValue();
        Feature featureClone = new Feature();
        featureClone.copyAttributeMetadata(feature);
        featureClone.copyAttributeValues(feature);
      }
    } finally {
      reader.close();
    }
  }
}