/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.test.SparkTest;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class DBFWriterTest extends SparkTest {

  public void testSimpleWriter() throws IOException, InterruptedException {
    DBFReader dbfReader = new DBFReader();
    DBFWriter dbfWriter = new DBFWriter();
    String[] dbfFileNames = {"usa-major-cities"};
    for (String dbFileName : dbfFileNames) {
      Path dbfPath = new Path(scratchPath, dbFileName+".dbf");
      Configuration conf = new Configuration();
      // Create a DBF file that is a copy of the existing file
      try {
        DataInputStream dbfInput = new DataInputStream(getClass().getResourceAsStream("/" + dbFileName + "/" + dbFileName + ".dbf"));
        dbfReader.initialize(dbfInput, conf);
        dbfWriter.initialize(dbfPath, conf);
        while (dbfReader.nextKeyValue()) {
          dbfWriter.write(NullWritable.get(), dbfReader.getCurrentValue());
        }
      } catch (Exception e) {
        throw e;
      } finally {
        dbfReader.close();
        dbfWriter.close(null);
      }

      // Read the written file back and make sure it is similar to the original one
      DBFReader reader2 = new DBFReader();
      try {
        DataInputStream dbfInput = new DataInputStream(getClass().getResourceAsStream("/"+dbFileName+"/"+dbFileName+".dbf"));
        dbfReader.initialize(dbfInput, conf);
        reader2.initialize(new DataInputStream(new FileInputStream(dbfPath.toString())), conf);
        while (dbfReader.nextKeyValue()) {
          assertTrue("The written file contains too few records", reader2.nextKeyValue());
          assertEquals(dbfReader.getCurrentValue(), reader2.getCurrentValue());
        }
      } finally {
        dbfReader.close();
        reader2.close();
      }
    }
  }

  public void testShouldWriteFeaturesWithoutNames() throws IOException, InterruptedException {
    // A sample feature
    CSVFeature f = new CSVFeature(new Point(1.0, 2.0));
    f.setFieldSeparator((byte) ',');
    f.setFieldValues("abc,def");

    DBFWriter dbfWriter = new DBFWriter();
    Path dbfPath = new Path(scratchPath, "test.dbf");
    Configuration conf = new Configuration();
    // Create a DBF file with one record
    try {
      dbfWriter.initialize(dbfPath, conf);
      dbfWriter.write(null, f);
    } finally {
      dbfWriter.close(null);
    }

    // Read the written file back and make sure it is correct
    DBFReader reader = new DBFReader();
    try {
      reader.initialize(dbfPath, conf);
      int count = 0;
      while (reader.nextKeyValue()) {
        count++;
        assertEquals(reader.getCurrentValue(), f);
      }
      assertEquals(1, count);
    } finally {
      reader.close();
      reader.close();
    }
  }
}