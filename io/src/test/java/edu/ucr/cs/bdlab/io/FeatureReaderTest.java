package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.test.SparkTest;
import junit.framework.TestCase;

public class FeatureReaderTest extends SparkTest {

  public void testGetFileExtension() {
    String expectedExtension = ".geojson";
    String actualExtension = FeatureReader.getFileExtension("geojson");
    assertEquals(expectedExtension, actualExtension);
  }

}