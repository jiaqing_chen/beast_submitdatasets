/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.GeometryCollection;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.twod.LineString2D;
import edu.ucr.cs.bdlab.geolite.twod.Polygon2D;
import edu.ucr.cs.bdlab.test.SparkTest;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;

import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipFile;

public class ShapefileGeometryWriterTest extends SparkTest {

  public void testCreation() throws IOException {
    IGeometry[] geometries = {
      new Point(2, 0.0,1.0),
      new Point(2, 3.0, 10.0)
    };

    Configuration conf = new Configuration();
    Path outPath = new Path(scratchPath, "test");
    Path shpFileName = new Path(outPath, "test.shp");
    Path shxFileName = new Path(outPath, "test.shx");
    FileSystem fileSystem = outPath.getFileSystem(conf);
    fileSystem.mkdirs(outPath);

    ShapefileGeometryWriter writer = new ShapefileGeometryWriter();
    try {
      writer.initialize(shpFileName, conf);
      for (IGeometry geom : geometries) {
        writer.write(NullWritable.get(), geom);
      }
    } finally {
      writer.close(null);
    }

    assertTrue("Shapefile not found", fileSystem.exists(shpFileName));
    assertTrue("Shape index file not found", fileSystem.exists(shxFileName));
  }

  public void testWriteEnvelopes() throws IOException {
    IGeometry[] geometries = {
        new Envelope(2, 0.0,1.0, 2.0, 3.0),
        new Envelope(2, 3.0, 10.0, 13.0, 13.0)
    };

    Configuration conf = new Configuration();
    Path outPath = new Path(scratchPath, "test");
    Path shpFileName = new Path(outPath, "test.shp");
    Path shxFileName = new Path(outPath, "test.shx");
    FileSystem fileSystem = outPath.getFileSystem(conf);
    fileSystem.mkdirs(outPath);

    ShapefileGeometryWriter writer = new ShapefileGeometryWriter();
    try {
      writer.initialize(shpFileName, conf);
      for (IGeometry geom : geometries) {
        writer.write(NullWritable.get(), geom);
      }
    } finally {
      writer.close(null);
    }

    assertTrue("Shapefile not found", fileSystem.exists(shpFileName));
    assertTrue("Shape index file not found", fileSystem.exists(shxFileName));
    // Read the file back
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    reader.initialize(shpFileName, conf);
    try {
      int count = 0;
      while (reader.nextKeyValue()) {
        Envelope e = new Envelope(reader.getCurrentValue().getCoordinateDimension());
        reader.getCurrentValue().envelope(e);
        assertEquals(geometries[count], e);
        count++;
      }
      assertEquals(geometries.length, count);
    } finally {
      reader.close();
    }
  }

  public void testContents() throws IOException {
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    ShapefileGeometryWriter writer = new ShapefileGeometryWriter();
    Configuration conf = new Configuration();

    String[] shpFileNames = {"usa-major-cities", /*"usa-major-highways"*/};
    for (String shpFileName : shpFileNames) {
      Path outPath = new Path(scratchPath, "test");
      Path shpPath = new Path(outPath, shpFileName + ".shp");
      Path shxPath = new Path(outPath, shpFileName + ".shx");
      FileSystem fileSystem = outPath.getFileSystem(conf);
      fileSystem.mkdirs(outPath);

      try {
        DataInputStream shpIn = new DataInputStream(getClass().getResourceAsStream("/" + shpFileName + "/" + shpFileName + ".shp"));
        reader.initialize(shpIn, conf);
        writer.initialize(shpPath, conf);

        while (reader.nextKeyValue()) {
          writer.write(reader.getCurrentKey(), reader.getCurrentValue());
        }
      } finally {
        reader.close();
        writer.close(null);
      }
      // Make sure that the output files are exactly the same as the input file
      InputStream expectedShp = getClass().getResourceAsStream("/" + shpFileName + "/" + shpFileName + ".shp");
      InputStream actualShp = fileSystem.open(shpPath);
      try {
        assertEquals(expectedShp, actualShp);
      } finally {
        expectedShp.close();
        actualShp.close();
      }
      InputStream expectedShx = getClass().getResourceAsStream("/" + shpFileName + "/" + shpFileName + ".shx");
      FSDataInputStream actualShx = fileSystem.open(shxPath);
      try {
        assertEquals(expectedShx, actualShx);
      } finally {
        expectedShx.close();
        actualShx.close();
      }
    }
  }

  public void testContentsForPolygons() throws IOException {
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    ShapefileGeometryWriter writer = new ShapefileGeometryWriter();
    Configuration conf = new Configuration();

    String[] shpFileNames = {"simpleshape.zip"};
    for (String shpFileName : shpFileNames) {
      Path inZShp = new Path(scratchPath, "temp.zip");
      copyResource("/"+shpFileName, new File(inZShp.toString()));
      Path outPath = new Path(scratchPath, "test");
      Path shpPath = new Path(outPath, shpFileName + ".shp");
      Path shxPath = new Path(outPath, shpFileName + ".shx");
      FileSystem fileSystem = outPath.getFileSystem(conf);
      fileSystem.mkdirs(outPath);

      try {
        reader.initialize(inZShp, conf);
        writer.initialize(shpPath, conf);

        while (reader.nextKeyValue()) {
          writer.write(reader.getCurrentKey(), reader.getCurrentValue());
        }
      } finally {
        reader.close();
        writer.close(null);
      }
      // Make sure that the output files are exactly the same as the input file
      ZipFile zshp = new ZipFile(inZShp.toString());
      // Compare the shape index file as well
      InputStream expectedShx = zshp.getInputStream(zshp.getEntry("simpleshape.shx"));
      FSDataInputStream actualShx = fileSystem.open(shxPath);
      try {
        assertEquals(expectedShx, actualShx);
      } finally {
        expectedShx.close();
        actualShx.close();
      }

      // Compare the shapefile (.shp)
      InputStream expectedShp = zshp.getInputStream(zshp.getEntry("simpleshape.shp"));
      InputStream actualShp = fileSystem.open(shpPath);
      try {
        assertEquals(expectedShp, actualShp);
      } finally {
        expectedShp.close();
        actualShp.close();
        zshp.close();
      }
    }
  }

  public void testPolylines() throws IOException {
    List<IGeometry> geometries = new ArrayList();
    LineString2D linestring = new LineString2D();
    linestring.addPoint(0, 0);
    linestring.addPoint(2, 3);
    geometries.add(linestring);

    Configuration conf = new Configuration();
    Path outPath = new Path(scratchPath, "test");
    Path shpFileName = new Path(outPath, "test.shp");
    Path shxFileName = new Path(outPath, "test.shx");
    FileSystem fileSystem = outPath.getFileSystem(conf);
    fileSystem.mkdirs(outPath);

    ShapefileGeometryWriter writer = new ShapefileGeometryWriter();
    try {
      writer.initialize(shpFileName, conf);
      for (IGeometry geom : geometries) {
        writer.write(NullWritable.get(), geom);
      }
    } finally {
      writer.close(null);
    }

    assertTrue("Shapefile not found", fileSystem.exists(shpFileName));
    assertTrue("Shape index file not found", fileSystem.exists(shxFileName));
    // Read the file back
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    reader.initialize(shpFileName, conf);
    try {
      int count = 0;
      while (reader.nextKeyValue()) {
        IGeometry actualGeometry = reader.getCurrentValue();
        assertEquals(geometries.get(count), actualGeometry);
        count++;
      }
      assertEquals(geometries.size(), count);
    } finally {
      reader.close();
    }
  }


  public void testPolygons() throws IOException {
    List<IGeometry> geometries = new ArrayList();
    Polygon2D polygon = new Polygon2D();
    polygon.addPoint(0, 0);
    polygon.addPoint(2, 3);
    polygon.addPoint(3, 0);
    polygon.closeLastRing(false);
    geometries.add(polygon);

    Configuration conf = new Configuration();
    Path outPath = new Path(scratchPath, "test");
    Path shpFileName = new Path(outPath, "test.shp");
    Path shxFileName = new Path(outPath, "test.shx");
    FileSystem fileSystem = outPath.getFileSystem(conf);
    fileSystem.mkdirs(outPath);

    ShapefileGeometryWriter writer = new ShapefileGeometryWriter();
    try {
      writer.initialize(shpFileName, conf);
      for (IGeometry geom : geometries) {
        writer.write(NullWritable.get(), geom);
      }
    } finally {
      writer.close(null);
    }

    assertTrue("Shapefile not found", fileSystem.exists(shpFileName));
    assertTrue("Shape index file not found", fileSystem.exists(shxFileName));
    // Read the file back
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    reader.initialize(shpFileName, conf);
    try {
      int count = 0;
      while (reader.nextKeyValue()) {
        IGeometry actualGeometry = reader.getCurrentValue();
        assertEquals(geometries.get(count), actualGeometry);
        count++;
      }
      assertEquals(geometries.size(), count);
    } finally {
      reader.close();
    }
  }

  public void testGeometryCollection() throws IOException {
    GeometryCollection geometryCollection = new GeometryCollection();
    Polygon2D polygon = new Polygon2D();
    polygon.addPoint(0, 0);
    polygon.addPoint(2, 3);
    polygon.addPoint(3, 0);
    polygon.closeLastRing(false);
    geometryCollection.addGeometry(polygon);
    polygon = new Polygon2D();
    polygon.addPoint(10, 0);
    polygon.addPoint(12, 3);
    polygon.addPoint(13, 0);
    polygon.closeLastRing(false);
    geometryCollection.addGeometry(polygon);

    Configuration conf = new Configuration();
    Path outPath = new Path(scratchPath, "test");
    Path shpFileName = new Path(outPath, "test.shp");
    Path shxFileName = new Path(outPath, "test.shx");
    FileSystem fileSystem = outPath.getFileSystem(conf);
    fileSystem.mkdirs(outPath);

    ShapefileGeometryWriter writer = new ShapefileGeometryWriter();
    try {
      writer.initialize(shpFileName, conf);
      writer.write(NullWritable.get(), geometryCollection);
    } finally {
      writer.close(null);
    }

    assertTrue("Shapefile not found", fileSystem.exists(shpFileName));
    assertTrue("Shape index file not found", fileSystem.exists(shxFileName));
    // Read the file back
    ShapefileGeometryReader reader = new ShapefileGeometryReader();
    reader.initialize(shpFileName, conf);
    try {
      int count = 0;
      while (reader.nextKeyValue()) {
        IGeometry actualGeometry = reader.getCurrentValue();
        assertEquals(geometryCollection.getGeometry(count), actualGeometry);
        count++;
      }
      assertEquals(geometryCollection.numGeometries(), count);
    } finally {
      reader.close();
    }
  }
}