/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.Feature;
import edu.ucr.cs.bdlab.geolite.GeometryCollection;
import edu.ucr.cs.bdlab.geolite.GeometryType;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.MultiPoint;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.twod.LineString2D;
import edu.ucr.cs.bdlab.geolite.twod.MultiLineString2D;
import edu.ucr.cs.bdlab.geolite.twod.MultiPolygon2D;
import edu.ucr.cs.bdlab.geolite.twod.Polygon2D;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.Seekable;
import org.apache.hadoop.io.compress.CodecPool;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.CompressionCodecFactory;
import org.apache.hadoop.io.compress.CompressionInputStream;
import org.apache.hadoop.io.compress.Decompressor;
import org.apache.hadoop.io.compress.SplitCompressionInputStream;
import org.apache.hadoop.io.compress.SplittableCompressionCodec;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.IOException;
import java.io.InputStream;

/**
 * A record reader that reads CSV file with custom field delimiter
 */
@FeatureReader.Metadata(
    description = "Parses a GepJSON file as one record for each feature object",
    shortName = "geojson",
    extension = ".geojson"
)
public class GeoJSONFeatureReader extends FeatureReader {
  private static final Log LOG = LogFactory.getLog(GeoJSONFeatureReader.class);

  /**The mutable feature*/
  protected Feature feature;

  /**An optional attributed to filter the geometries in the input file*/
  private Envelope filterMBR;

  /**This flag signals the record reader to use immutable objects (a new object per record) which is useful with some
   * Spark operations that is not designed for mutable objects, e.g., partitionBy used in indexing*/
  protected boolean immutable;

  /**The start of the split*/
  protected long start;

  /**The end of the split*/
  protected long end;

  /**The input stream to the raw file (compressed)*/
  protected FSDataInputStream fileIn;

  /**The input stream to the decompressed file*/
  protected InputStream in;

  /**The current position in the file. Either the raw file or the comrpessed file*/
  protected Seekable filePosition;
  protected boolean isCompressedInput;
  protected Decompressor decompressor;

  /**The JSON parser that reads json tokens*/
  private JsonParser jsonParser;

  /**A flag that is raised when end-of-split is reached*/
  protected boolean eos;

  @Override
  public void initialize(InputSplit genericSplit, TaskAttemptContext context) throws IOException {
    // Open the input split and decompress if necessary
    Configuration conf = context.getConfiguration();
    FileSplit split = (FileSplit) genericSplit;
    this.initialize(split.getPath(), split.getStart(), split.getLength(), conf);
  }

  /**
   * An initializer that can be used outside the regular MapReduce context.
   * @param inputFile
   * @param conf
   * @throws IOException
   */
  public void initialize(Path inputFile, Configuration conf) throws IOException {
    FileStatus fileStatus = inputFile.getFileSystem(conf).getFileStatus(inputFile);
    this.initialize(inputFile, 0, fileStatus.getLen(), conf);
  }

  /**
   * An internal initializer that takes a file path, a start and length.
   * @param file path to the file to open. Works with both compressed and decompressed files (based on extension)
   * @param start the starting offset to parse
   * @param length the number of bytes to parse
   * @param conf the environment configuration
   * @throws IOException
   */
  protected void initialize(Path file, long start, long length, Configuration conf) throws IOException {
    this.start = start;
    this.eos = length == 0;
    this.end = start + length;

    // open the file and seek to the start of the split
    final FileSystem fs = file.getFileSystem(conf);
    fileIn = fs.open(file);

    CompressionCodec codec = new CompressionCodecFactory(conf).getCodec(file);
    if (null != codec) {
      // The file is compressed. Decompress it on the fly
      isCompressedInput = true;
      decompressor = CodecPool.getDecompressor(codec);
      if (codec instanceof SplittableCompressionCodec) {
        // Can decompress a small part of the file
        final SplitCompressionInputStream cIn = ((SplittableCompressionCodec)codec).createInputStream(
            fileIn, decompressor, start, end, SplittableCompressionCodec.READ_MODE.BYBLOCK);
        this.start = cIn.getAdjustedStart();
        this.end = cIn.getAdjustedEnd();
        this.filePosition = cIn;
        this.in = cIn;
      } else {
        // Need to decompress the entire file from the beginning
        final CompressionInputStream cIn = codec.createInputStream(fileIn, decompressor);
        this.in = cIn;
        this.filePosition = fileIn;
      }
    } else {
      // Not a compressed file
      fileIn.seek(start);
      filePosition = fileIn;
      in = fileIn;
    }

    this.immutable = conf.getBoolean(SpatialInputFormat.ImmutableObjects, false);

    JsonFactory jsonFactory = new JsonFactory();
    jsonParser = new SilentJsonParser(jsonFactory.createParser(in));

    // Retrieve the filter MBR
    String filterMBRStr = conf.get(SpatialInputFormat.FilterMBR);
    if (filterMBRStr != null) {
      String[] parts = filterMBRStr.split(",");
      double[] dblParts = new double[parts.length];
      for (int i = 0; i < parts.length; i++)
        dblParts[i] = Double.parseDouble(parts[i]);
      this.filterMBR = new Envelope(dblParts.length/2, dblParts);
    }
  }

  protected Feature createValue() {
    return new Feature();
  }

  /**
   * Reads the next feature from the input stream
   * @return
   * @throws IOException
   */
  @Override
  public boolean nextKeyValue() throws IOException {
    if (eos)
      return false;
    // The assumption is that the underlying parser now points to the next token after the "type": "feature"
    // The assumption also is that the "type": "feature" token appears before the geometry and properties
    // Create a new value if needed
    if (this.feature == null || immutable) {
      this.feature = createValue();
    } else {
      this.feature.clearAttributes();
    }
    JsonToken token;
    // Read until the beginning of the next feature
    boolean recordFound = false;
    long posOfLastStartObject = this.getFilePosition();
    while ((token = jsonParser.nextToken()) != null && posOfLastStartObject < this.end) {
      if (token == JsonToken.FIELD_NAME) {
        String fieldName = jsonParser.getCurrentName();
        if (fieldName.equalsIgnoreCase("type")) {
          String fieldValue = jsonParser.nextTextValue();
          if (fieldValue.equalsIgnoreCase("feature")) {
            recordFound = true;
            break;
          }
        }
      } else if (token == JsonToken.START_OBJECT) {
        posOfLastStartObject = this.getFilePosition();
      }
    }
    eos = !recordFound;
    // Now, read the actual feature
    while (!eos && (token = jsonParser.nextToken()) != JsonToken.END_OBJECT) {
      if (token == JsonToken.FIELD_NAME && jsonParser.getCurrentName().equalsIgnoreCase("geometry")) {
        consumeAndCheckToken(JsonToken.START_OBJECT);
        feature.setGeometry(readGeometry(feature.getGeometry()));
      } else if (token == JsonToken.FIELD_NAME && jsonParser.getCurrentName().equalsIgnoreCase("properties")) {
        readProperties();
      } else if (token == JsonToken.FIELD_NAME) {
        // Set additional attributes
        feature.appendAttribute(jsonParser.getCurrentName(), jsonParser.nextTextValue());
      } else if (token == null) {
        eos = true;
      }
    }
    return !eos;
  }

  /**
   * Read properties as key-value pairs. It starts by reading the start object, the attributes as key-value pairs, and
   * finaly the end object.
   */
  protected void readProperties() throws IOException {
    consumeAndCheckToken(JsonToken.START_OBJECT);
    while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
      String key = jsonParser.getCurrentName();
      JsonToken token = jsonParser.nextToken();
      Object value;
      switch (token) {
        case VALUE_FALSE: value = Boolean.FALSE; break;
        case VALUE_TRUE: value = Boolean.TRUE; break;
        case VALUE_STRING: value = jsonParser.getText(); break;
        default:
          throw new RuntimeException(String.format("Unsupported value type '%s'", token));
      }
      feature.appendAttribute(key, value);
    }
  }

  /**
   * Reads a geometry object from the JsonParser. The assumption is that the start object token of the geometry has
   * already been consumed. This function should read and consume the end object of the geometry
   */
  protected IGeometry readGeometry(IGeometry geom) throws IOException {
    // Clear the existing geometry to prepare it for possible reuse
    if (geom != null)
      geom.setEmpty();
    consumeAndCheckFieldName("type");
    String geometryType = jsonParser.nextTextValue().toLowerCase();
    switch (geometryType) {
      case "point":
        // http://wiki.geojson.org/GeoJSON_draft_version_6#Point
        Point point;
        if (geom == null || geom.getType() != GeometryType.POINT)
          geom = new Point(2);
        point = (Point) geom;
        consumeAndCheckFieldName("coordinates");
        consumeAndCheckToken(JsonToken.START_ARRAY);
        consumeNumber();
        point.coords[0] = jsonParser.getDoubleValue();
        consumeNumber();
        point.coords[1] = jsonParser.getDoubleValue();
        consumeAndCheckToken(JsonToken.END_ARRAY);
        break;
      case "linestring":
        // http://wiki.geojson.org/GeoJSON_draft_version_6#LineString
        LineString2D lineString2D;
        if (geom == null || geom.getType() != GeometryType.LINESTRING)
          geom = new LineString2D();
        lineString2D = (LineString2D) geom;
        consumeAndCheckFieldName("coordinates");
        consumeAndCheckToken(JsonToken.START_ARRAY);
        while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
          consumeNumber();
          double x = jsonParser.getDoubleValue();
          consumeNumber();
          double y = jsonParser.getDoubleValue();
          lineString2D.addPoint(x, y);
          consumeAndCheckToken(JsonToken.END_ARRAY);
        }
        break;
      case "polygon":
        // http://wiki.geojson.org/GeoJSON_draft_version_6#Polygon
        Polygon2D polygon;
        if (geom == null || geom.getType() != GeometryType.POLYGON)
          geom = new Polygon2D();
        polygon = (Polygon2D) geom;
        consumeAndCheckFieldName("coordinates");
        consumeAndCheckToken(JsonToken.START_ARRAY);
        while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
          // Read one linear ring
          while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
            consumeNumber();
            double x = jsonParser.getDoubleValue();
            consumeNumber();
            double y = jsonParser.getDoubleValue();
            polygon.addPoint(x, y);
            consumeAndCheckToken(JsonToken.END_ARRAY);
          }
          polygon.closeLastRing(true);
        }
        break;
      case "multipoint":
        // http://wiki.geojson.org/GeoJSON_draft_version_6#MultiPoint
        MultiPoint multiPoint;
        if (geom == null || geom.getType() != GeometryType.MULTIPOINT)
          geom = new MultiPoint();
        multiPoint = (MultiPoint) geom;
        consumeAndCheckFieldName("coordinates");
        consumeAndCheckToken(JsonToken.START_ARRAY);
        while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
          consumeNumber();
          double x = jsonParser.getDoubleValue();
          consumeNumber();
          double y = jsonParser.getDoubleValue();
          multiPoint.addPoint(x, y);
          consumeAndCheckToken(JsonToken.END_ARRAY);
        }
        break;
      case "multilinestring":
        // http://wiki.geojson.org/GeoJSON_draft_version_6#MultiLineString
        MultiLineString2D multiLineString;
        if (geom == null || geom.getType() != GeometryType.MULTILINESTRING)
          geom = new MultiLineString2D();
        multiLineString = (MultiLineString2D) geom;
        consumeAndCheckFieldName("coordinates");
        consumeAndCheckToken(JsonToken.START_ARRAY);
        while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
          // Read one line string
          while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
            consumeNumber();
            double x = jsonParser.getDoubleValue();
            consumeNumber();
            double y = jsonParser.getDoubleValue();
            multiLineString.addPoint(x, y);
            consumeAndCheckToken(JsonToken.END_ARRAY);
          }
          multiLineString.endCurrentLineString();
        }
        break;
      case "multipolygon":
        // http://wiki.geojson.org/GeoJSON_draft_version_6#MultiPolygon
        MultiPolygon2D multiPolygon;
        if (geom == null || geom.getType() != GeometryType.MULTIPOLYGON)
          geom = new MultiPolygon2D();
        multiPolygon = (MultiPolygon2D) geom;
        consumeAndCheckFieldName("coordinates");
        consumeAndCheckToken(JsonToken.START_ARRAY);
        while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
          // Read one polygon
          while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
            // Read one linear ring
            while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
              consumeNumber();
              double x = jsonParser.getDoubleValue();
              consumeNumber();
              double y = jsonParser.getDoubleValue();
              multiPolygon.addPoint(x, y);
              consumeAndCheckToken(JsonToken.END_ARRAY);
            }
            // Done with one linear ring
            multiPolygon.closeLastRing(true);
          }
          // Done with one polygon
          multiPolygon.endCurrentPolygon();
        }
        // Done with the multipolygon
        break;
    case "geometrycollection":
      // http://wiki.geojson.org/GeoJSON_draft_version_6#GeometryCollection
      GeometryCollection geometryCollection;
      if (geom == null || geom.getType() != GeometryType.GEOMETRYCOLLECTION)
        geom = new GeometryCollection();
      geometryCollection = (GeometryCollection) geom;
      geometryCollection.setEmpty();
      consumeAndCheckFieldName("geometries");
      consumeAndCheckToken(JsonToken.START_ARRAY);
      while (jsonParser.nextToken() != JsonToken.END_ARRAY)
        geometryCollection.addGeometry(readGeometry(null));
      break;
      default:
        throw new RuntimeException(String.format("Unexpected geometry type '%s'", geometryType));
    }
    consumeAndCheckToken(JsonToken.END_OBJECT);
    return geom;
  }

  /**
   * Read the next token and ensure it is a numeric token. Either Integer or Float
   */
  private void consumeNumber() throws IOException {
    JsonToken actual = jsonParser.nextToken();
    if (actual != JsonToken.VALUE_NUMBER_FLOAT && actual != JsonToken.VALUE_NUMBER_INT) {
      // Throw a parse exception.
      // TODO use a specialized exception(s)
      int lineNumber = jsonParser.getTokenLocation().getLineNr();
      int characterNumber = jsonParser.getTokenLocation().getColumnNr();
      throw new RuntimeException(String.format("Error parsing GeoJSON file. " +
          "Expected numeric value but found %s at line %d character %d", actual, lineNumber, characterNumber));
    }
  }

  private void consumeAndCheckToken(JsonToken expected) throws IOException {
    JsonToken actual = jsonParser.nextToken();
    if (actual != expected) {
      // Throw a parse exception.
      // TODO use a specialized exception(s)
      int lineNumber = jsonParser.getTokenLocation().getLineNr();
      int characterNumber = jsonParser.getTokenLocation().getColumnNr();
      throw new RuntimeException(String.format("Error parsing GeoJSON file. " +
          "Expected token %s but found %s at line %d character %d", expected, actual, lineNumber, characterNumber));
    }
  }

  private void consumeAndCheckFieldName(String expected) throws IOException {
    consumeAndCheckToken(JsonToken.FIELD_NAME);
    String actual = jsonParser.getCurrentName();
    if (!expected.equalsIgnoreCase(actual)) {
      // Throw a parse exception.
      // TODO use a specialized exception(s)
      int lineNumber = jsonParser.getTokenLocation().getLineNr();
      int characterNumber = jsonParser.getTokenLocation().getColumnNr();
      throw new RuntimeException(String.format("Error parsing GeoJSON file. " +
          "Expected field '%s' but found '%s' at line %d character %d", expected, actual, lineNumber, characterNumber));
    }
  }

  @Override
  public Envelope getCurrentKey() {
    return null;
  }

  @Override
  public Feature getCurrentValue() {
    return feature;
  }

  private long getFilePosition() throws IOException {
    long retVal;
    if (isCompressedInput && null != filePosition) {
      retVal = filePosition.getPos();
    } else {
      retVal = start + jsonParser.getTokenLocation().getByteOffset();
    }
    return retVal;
  }

  @Override
  public float getProgress() throws IOException {
    if (start == end) {
      return 0.0f;
    } else {
      return Math.min(1.0f, (getFilePosition() - start) / (float)(end - start));
    }
  }

  @Override
  public void close() throws IOException {
    jsonParser.close();
  }

}
