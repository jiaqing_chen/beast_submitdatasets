package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.util.WritableExternalizable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.function.BiFunction;

public class CSVWKTEncoder implements BiFunction<IFeature, StringBuilder, StringBuilder>, WritableExternalizable {

  /**The index of the field that will contains the WKT-encoded geometry*/
  protected int wktCol;

  /**Field separator*/
  protected char fieldSeparator;

  public CSVWKTEncoder() {
    // Default constructor for the Externalizable and Writable interfaces
  }

  public CSVWKTEncoder(char fieldSeparator, int wktColumn) {
    this.wktCol = wktColumn;
    this.fieldSeparator = fieldSeparator;
  }

  @Override
  public StringBuilder apply(IFeature feature, StringBuilder str) {
    if (str == null)
      str = new StringBuilder();
    IGeometry g = feature.getGeometry();
    int numCols = 1 + feature.getNumAttributes();
    int iAtt = 0; // The index of the next feature attribute to write
    for (int iCol = 0; iCol < numCols; iCol++) {
      if (iCol > 0)
        str.append(fieldSeparator);
      if (iCol == this.wktCol) {
        // Time to write the next coordinate
        g.toWKT(str);
      } else {
        // Time to write the next feature attribute
        str.append(feature.getAttributeValue(iAtt++));
      }
    }
    return str;
  }

  @Override
  public void write(DataOutput out) throws IOException {
    out.writeInt(wktCol);
    out.writeChar(fieldSeparator);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    this.wktCol = in.readInt();
    this.fieldSeparator = in.readChar();
  }
}
