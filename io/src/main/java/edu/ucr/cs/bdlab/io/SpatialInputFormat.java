/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.util.IOUtil;
import edu.ucr.cs.bdlab.util.OperationException;
import edu.ucr.cs.bdlab.util.OperationParam;
import edu.ucr.cs.bdlab.util.StringUtil;
import edu.ucr.cs.bdlab.util.UserOptions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.BlockLocation;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.CompressionCodecFactory;
import org.apache.hadoop.io.compress.SplittableCompressionCodec;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.security.TokenCache;
import org.apache.hadoop.util.LineReader;
import org.geotools.referencing.CRS;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CRSAuthorityFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * An input format that reads various spatial file formats such as shape files and WKT-encoded CSV files.
 * This input format can also take a minimum bounding rectangle (MBR) and retrieve only the records that overlap with
 * the MBR (with possible false positives). Depending on how the input file is stored or indexed, this input format will
 * try to be as efficient as possible in pruning non-relevant records. Nonetheless, there is no guarantee on how much
 * it will prune and it might end up returning records that do not overlap the given MBR. Therefore, if strictly
 * necessary, the application should further test each record against the MBR.
 */
public class SpatialInputFormat extends FileInputFormat<Envelope, IFeature> {
  @OperationParam(
      description = "The format of the input file {point(xcol,ycol),envelope(x1col,y1col,x2col,y2col),wkt(gcol)}\n" +
          "\tpoint(xcol,ycol) indicates a CSV input where xcol and ycol indicate the indexes of the columns that " +
          "contain the x and y coordinates\n" +
          "\tenvelope(x1col,y1col,x2col,y2col) indicate an input that contains rectangles stores in (x1,y1,x2,y2) format\n" +
          "\twkt(gcol) indicate a CSV file with the field (gcol) containing a WKT-encoded geometry.\n" +
          "\tshapefile: Esri shapefile. Accepts both .shp+.shx+.dbf files or a compressed .zip file with these three files\n" +
          "\trtree: An optimized R-tree index\n"+
          "\tgeojson: GeoJSON file containing features with geometries and properties (attributes)",
      required = true
  )
  /**A user-friendly input format string*/
  public static final String InputFormat = "iformat";

  @OperationParam(
      description = "An optional MBR to filter the input. Format: x1,y1,x2,y2"
  )
  /**Early filter the input based on this MBR*/
  public static final String FilterMBR = "filtermbr";

  @OperationParam(
      description = "Process the input recursively",
      defaultValue = "false"
  )
  /**Process the input recursively*/
  public static final String Recursive = "recursive";

  @OperationParam(description = "AWS Access Key ID")
  public static final String AWS_ACCESS_KEY_ID = "fs.s3a.access.key";

  @OperationParam(description = "AWS Access Key ID")
  public static final String AWS_SECRET_ACCESS_KEY = "fs.s3a.secret.key";

  /**A CRS to reproject all geometries to as they are read*/
  public static final String TargetCRS = "crs";

  /**Project records to the web mercator projection (TODO: User {@link #TargetCRS} instead)*/
  public static final String Mercator = "SpatialInputFormat.Mercator";

  /**Whether to split input files or not*/
  public static final String SplitFiles = "SpatialInputFormat.SplitFiles";

  /**A Boolean variable that can be set to true to enforce the use of immutable objects in the record reader*/
  public static final String ImmutableObjects = "SpatialInputFormat.Immutable";

  private static final Log LOG = LogFactory.getLog(SpatialInputFormat.class);

  /**A filter that prunes hidden files (files that start with _ or .)*/
  public static final PathFilter HiddenFileFilter = p -> p.getName().charAt(0) != '_' && p.getName().charAt(0) != '.';

  /**A filter that finds master files only*/
  public static final PathFilter MasterFileFilter = p -> p.getName().startsWith("_master");

  /**The class name of the feature reader*/
  static final String FeatureReaderClass = "SpatialInputFormat.FeatureReader";

  /**
   * Proxy PathFilter that accepts a path only if all filters given in the constructor do. Used by the listPaths()
   * to apply the built-in HiddenFileFilter together with a user provided one (if any).
   */
  private static class MultiPathFilter implements PathFilter {
    private List<PathFilter> filters;

    public MultiPathFilter(List<PathFilter> filters) {
      this.filters = filters;
    }

    public boolean accept(Path path) {
      for (PathFilter filter : filters) {
        if (!filter.accept(path)) {
          return false;
        }
      }
      return true;
    }
  }

  /**
   * Sets a spatial filter to limit the read data
   * @param conf
   * @param env
   */
  public static void setSpatialFilter(Configuration conf, Envelope env) {
    String envStr = "";
    for (int d = 0; d < env.getCoordinateDimension(); d++) {
      envStr += env.minCoord[d];
      envStr += ',';
    }
    for (int d = 0; d < env.getCoordinateDimension(); d++) {
      envStr += env.maxCoord[d];
      if (d < env.getCoordinateDimension() - 1)
        envStr += ',';
    }
    conf.set(FilterMBR, envStr);
  }

  /**
   * Adds a path to the list of splits by creating the appropriate file splits. If the file is splittable
   * ({@link #isSplitable(JobContext, Path)}), multiple splits might be added.
   * @param job
   * @param fileSystem
   * @param path
   * @param start
   * @param length
   * @param splits (output) the created splits are added to this list
   */
  protected void addSplits(JobContext job, FileSystem fileSystem, Path path, long start, long length,
                           List<InputSplit> splits) throws IOException {
    if (!isSplitable(job, path)) {
      // Create one split
      List<String> diskHosts = new ArrayList<>();
      List<String> memoryHosts = new ArrayList<>();
      for (BlockLocation location : fileSystem.getFileBlockLocations(path, start, length)) {
        for (String diskHost : location.getHosts()) {
          if (diskHosts.indexOf(diskHost) == -1)
            diskHosts.add(diskHost);
        }
        for (String memoryHost : location.getCachedHosts()) {
          if (memoryHosts.indexOf(memoryHost) == -1)
            memoryHosts.add(memoryHost);
        }
      }
      splits.add(new FileSplit(path, start, length,
          diskHosts.toArray(new String[diskHosts.size()]),
          memoryHosts.toArray(new String[memoryHosts.size()])));
    } else {
      long minSize = Math.max(getFormatMinSplitSize(), getMinSplitSize(job));
      long maxSize = getMaxSplitSize(job);
      // A splittable file, create multiple splits
      FileStatus fileStatus = fileSystem.getFileStatus(path);
      long blockSize = fileStatus.getBlockSize();
      long splitSize = computeSplitSize(blockSize, minSize, maxSize);

      long bytesRemaining = length;
      final double SPLIT_SLOP = 1.1;   // 10% slop
      BlockLocation[] blkLocations = fileSystem.getFileBlockLocations(path, start, length);
      while (((double) bytesRemaining)/splitSize > SPLIT_SLOP) {
        int blkIndex = getBlockIndex(blkLocations, length-bytesRemaining);
        splits.add(makeSplit(path, length-bytesRemaining, splitSize,
            blkLocations[blkIndex].getHosts(),
            blkLocations[blkIndex].getCachedHosts()));
        bytesRemaining -= splitSize;
      }

      if (bytesRemaining != 0) {
        int blkIndex = getBlockIndex(blkLocations, length-bytesRemaining);
        splits.add(makeSplit(path, length-bytesRemaining, bytesRemaining,
            blkLocations[blkIndex].getHosts(),
            blkLocations[blkIndex].getCachedHosts()));
      }
    }
  }

  @Override
  protected boolean isSplitable(JobContext context, Path file) {
    Configuration conf = context.getConfiguration();
    if (!conf.getBoolean(SplitFiles, true))
      return false;
    final CompressionCodec codec = new CompressionCodecFactory(conf).getCodec(file);
    if (codec != null && !(codec instanceof SplittableCompressionCodec))
      return false;

    String fileName = file.getName();
    int lastDot = fileName.lastIndexOf('.');
    if (lastDot == -1)
      return true;
    String extension;
    if (codec != null) {
      int beforeLastDot = fileName.lastIndexOf('.', lastDot - 1);
      extension = beforeLastDot == -1?
          fileName.substring(lastDot).toLowerCase() :
          fileName.substring(beforeLastDot, lastDot);
    } else {
      extension = fileName.substring(lastDot).toLowerCase();
    }
    if (extension.equals(".zip"))
      return false;
    if (extension.equals(".shp"))
      return false;
    if (extension.equals(".rtree"))
      return false;
    return true;
  }

  /**
   * @param conf the configuration to look into
   * @return {@code true} if the input should be processed recursively. {@code false} otherwise.
   */
  public static boolean getInputDirRecursive(Configuration conf) {
    return conf.getBoolean(Recursive, conf.getBoolean(INPUT_DIR_RECURSIVE, false));
  }


  /**
   * If the input path points to a directory with a master file in it, use the master file to list the paths
   * Otherwise, use the regular logic of listing all files in the input paths.
   * @param job
   * @return
   * @throws IOException
   */
  @Override
  public List<InputSplit> getSplits(JobContext job) throws IOException {
    long t1 = System.nanoTime();
    Path[] dirs = getInputPaths(job);
    if (dirs.length == 0)
      throw new IOException("No input paths specified in job");
    List<Path> dirsList = Arrays.asList(dirs);

    // get tokens for all the required FileSystems.
    TokenCache.obtainTokensForNamenodes(job.getCredentials(), dirs, job.getConfiguration());

    // Whether we need to recursive look into the directory structure
    boolean recursive = getInputDirRecursive(job.getConfiguration());

    Stack<Path> pathsToInspect = new Stack<>();
    for (Path dir : dirs)
      pathsToInspect.add(dir);

    // creates a MultiPathFilter with the HiddenFileFilter and the user provided one (if any).
    List<PathFilter> filters = new ArrayList<PathFilter>();
    filters.add(HiddenFileFilter);
    PathFilter jobFilter = getInputPathFilter(job);
    if (jobFilter != null)
      filters.add(jobFilter);
    Configuration conf = job.getConfiguration();
    Class<? extends FeatureReader> readerClass = getFeatureReaderClass(conf, pathsToInspect.get(0));
    if (readerClass == ShapefileFeatureReader.class) {
      // TODO add meta data to filter the files
      // Add a filter for shapefiles
      filters.add(path -> {
        String extension = IOUtil.getExtension(path.getName());
        if (extension == null)
          return false;
        extension = extension.toLowerCase();
        return extension.equals(".shp") || extension.equals(".zip");
      });
    }
    PathFilter inputFilter = new MultiPathFilter(filters);

    List<InputSplit> splits = new ArrayList<>();

    // A configuration used to open the master file
    Configuration masterFileConf = null;

    // The CSV reader is used with master files if needed
    CSVFeatureReader masterFileReader = null;
    while (!pathsToInspect.isEmpty()) {
      Path pathBeingInspected = pathsToInspect.pop();
      FileSystem fileSystem = pathBeingInspected.getFileSystem(conf);
      FileStatus fileStatus = fileSystem.getFileStatus(pathBeingInspected);
      if (fileStatus.isDirectory()) {
        // A directory.
        FileStatus[] masterFiles = fileSystem.listStatus(pathBeingInspected, MasterFileFilter);
        if (masterFiles.length != 0) {
          // 1- If it has a master file, use the most recent one to list the files.
          int mostRecent = 0;
          for (int i = 1; i < masterFiles.length; i++) {
            if (masterFiles[i].getModificationTime() > masterFiles[mostRecent].getModificationTime())
              mostRecent = i;
          }
          // Now list the partitions in the master file while applying the inputFilter to the file name
          // and the spatial filter, if any, to the spatial boundaries of partitions
          if (masterFileReader == null) {
            // To determine the number of dimensions for the master file, we need to read a data line and count columns
            LineReader reader = new LineReader(fileSystem.open(masterFiles[mostRecent].getPath()));
            Text line = new Text();
            reader.readLine(line); // Skip the header line
            line.clear();
            reader.readLine(line);
            reader.close();
            // Count number of columns
            int numColumns = line.toString().split("\t").length;
            int numDimensionColumns = numColumns - 5;
            masterFileConf = new Configuration(conf);
            masterFileConf.set(InputFormat, String.format("envelopek(%d,5)", numDimensionColumns / 2));
            masterFileConf.setBoolean(CSVFeatureReader.SkipHeader, true);
            masterFileConf.set(CSVFeatureReader.FieldSeparator, "\t");
            masterFileReader = new CSVFeatureReader();
          }
          try {
            masterFileReader.initialize(masterFiles[mostRecent].getPath(), masterFileConf);
            while (masterFileReader.nextKeyValue()) {
              CSVFeature partition = masterFileReader.getCurrentValue();
              Path partitionPath = new Path(pathBeingInspected, (String) partition.getAttributeValue("File Name"));
              if (inputFilter.accept(partitionPath) || dirsList.indexOf(partitionPath) != -1) {
                long start = 0;
                long length = Long.parseLong((String)partition.getAttributeValue("Data Size"));
                addSplits(job, fileSystem, partitionPath, start, length, splits);
              }
            }
          } finally {
            masterFileReader.close();
          }
        } else {
          // 2- If no master file exists, list all the contents while applying the filter. Files are added to the list
          // of splits, while directories are added recursively only if the recursive flag is on.
          FileStatus[] statuses = fileSystem.listStatus(pathBeingInspected, inputFilter);
          for (FileStatus status : statuses) {
            if (status.isFile())
              addSplits(job, fileSystem, status.getPath(), 0, status.getLen(), splits);
            else if (status.isDirectory() && recursive)
              pathsToInspect.add(status.getPath());
          }
        }

      } else if (fileStatus.isFile() &&
          (inputFilter.accept(fileStatus.getPath()) || dirsList.indexOf(fileStatus.getPath())!= -1)) {
        // If it is a file, apply the filter to decide whether or not to take it
        addSplits(job, fileSystem, fileStatus.getPath(), 0, fileStatus.getLen(), splits);
      }
    }
    long t2 = System.nanoTime();
    LOG.info(String.format("Created %d splits in %f seconds", splits.size(), (t2-t1)*1E-9));
    return splits;
  }

  public FeatureReader createAndInitRecordReader(InputSplit inputSplit, Configuration conf) {
    FileSplit fsplit = (FileSplit)inputSplit;
    // Create a record reader based on the configured FeatureReader class
    Class<? extends FeatureReader> readerClass = getFeatureReaderClass(conf, fsplit.getPath());

    try {
      FeatureReader reader = readerClass.newInstance();
      // Check if there is a master file to apply duplicate avoidance
      Path splitPath = fsplit.getPath();
      FileSystem fileSystem = splitPath.getFileSystem(conf);
      Path masterFile = SpatialInputFormat.getMasterFilePath(fileSystem, splitPath.getParent());
      if (masterFile != null)
        reader = new DuplicateAvoidanceReader(reader, masterFile);
      if (conf.getBoolean(Mercator, false))
        reader = new MercatorProjector(reader);
      // Check if a reprojection is required
      String targetCRS = conf.get(TargetCRS);
      if (targetCRS != null) {
        try {
          CRSAuthorityFactory authorityFactory = CRS.getAuthorityFactory(true);
          reader = new CRSConverter(reader, authorityFactory.createCoordinateReferenceSystem(targetCRS));
        } catch (FactoryException e) {
          e.printStackTrace();
        }
      }
      reader.initialize(inputSplit, conf);
      return reader;
    } catch (InstantiationException e) {
      throw new RuntimeException(String.format("Error while instantiating reader '%s'", readerClass), e);
    } catch (IllegalAccessException e) {
      throw new RuntimeException(String.format("Could not find an appropriate constructor for '%s'", readerClass), e);
    } catch (IOException e) {
      throw new RuntimeException("Could not retrieve the master file");
    } catch (InterruptedException e) {
      throw new RuntimeException("Could not initialize the reader");
    }
  }

  @Override
  public FeatureReader createRecordReader(InputSplit inputSplit, TaskAttemptContext taskAttemptContext) {
    return createAndInitRecordReader(inputSplit, taskAttemptContext.getConfiguration());
  }

  /**
   * Returns the FeatureReaderClass to the given path. Depending on the configuration, this method does the following
   * in order.
   * <ol>
   *   <li>If conf[{@link #FeatureReaderClass}] is set, return that class</li>
   *   <li>If conf[{@link #InputFormat}] is set, use that value and set conf[{@link #FeatureReaderClass}]
   *   for future calls</li>
   *   <li>Call {@link #autodetectInputFormat(Configuration, String)} to detect the input format and set both
   *     conf[{@link #InputFormat}] and conf[{@link #FeatureReaderClass}] for future calls.</li>
   *   <li>If the input path cannot be created, {@code null} is returned.</li>
   * </ol>
   * @param conf
   * @param path
   * @return
   */
  public static Class<? extends FeatureReader> getFeatureReaderClass(Configuration conf, Path path) {
    // 1- Try if the class is explicitly set
    Class<? extends FeatureReader> readerClass = conf.getClass(FeatureReaderClass, null, FeatureReader.class);
    if (readerClass == null) {
      // 2- Test if iformat is set
      String iFormat = conf.get(InputFormat);
      if (iFormat != null) {
        readerClass = getFeatureReaderClass(iFormat);
      } else if (autodetectInputFormat(conf, path.toString())) {
        // 3- Autodetected input
        readerClass = conf.getClass(FeatureReaderClass, null, FeatureReader.class);
      }
      conf.setClass(FeatureReaderClass, readerClass, FeatureReader.class);
    }
    return readerClass;
  }

  /**
   * Given a user-friendly input format, this function sets it in the given configuration and sets the corresponding
   * FeatureReader class. If the given iFormat is not detected, an exception is thrown with possibly some suggested
   * corrections.
   * @param iFormat the user-friendly input format string
   */
  static Class<? extends FeatureReader> getFeatureReaderClass(String iFormat) {
    Class<? extends FeatureReader> readerClass;
    if (CSVFeatureReader.detect(iFormat)) {
      // CSVFeatureReader was able to detect the input format.
      readerClass = CSVFeatureReader.class;
    } else {
      readerClass = FeatureReader.featureReaders.get(iFormat);
      if (readerClass == null) {
        int i1 = iFormat.indexOf('(');
        if (i1 != -1)
          iFormat = iFormat.substring(0, i1);
        String errorMessage = String.format("Unrecognized input format '%s'", iFormat);
        // Try to suggest some corrections
        List<String> possibleCorrections = new ArrayList<>();
        possibleCorrections.addAll(FeatureReader.featureReaders.keySet());
        possibleCorrections.add("point");
        possibleCorrections.add("envelope");
        possibleCorrections.add("wkt");
        Collection<String> suggestions = StringUtil.nearestLevenshteinDistance(iFormat, possibleCorrections, 2);
        if (!suggestions.isEmpty())
          errorMessage += String.format(". Did you mean %s instead of '%s'",
              StringUtil.humandReable(suggestions, "or"), iFormat);
        throw new OperationException(errorMessage);
      }
    }
    //conf.setClass(FeatureReaderClass, readerClass, FeatureReader.class);
    return readerClass;
  }

  /**
   * Try to auto-detect the input format if the input is given. The return value indicates whether this method was
   * successful in finding a valid input format. The input format, along with any other necessary parameters, are set
   * in the given configuration.
   * @param conf
   * @param inputPath
   */
  public static boolean autodetectInputFormat(Configuration conf, String inputPath) {
    // Keep the original configuration to print the different
    Configuration originalConf = new Configuration(conf);
    long t1 = System.nanoTime();
    // A costly auto detection of the input. Should only be used when explicitly asked by the user.
    LOG.warn("Running a costly auto-detection of the input");
    boolean inputDetected = false;
    Iterator<Class<? extends FeatureReader>> readers = FeatureReader.featureReaders.values().iterator();
    Class<? extends FeatureReader> readerClass = null;
    while (!inputDetected && readers.hasNext()) {
      readerClass = readers.next();
      try {
        FeatureReader reader = readerClass.newInstance();
        inputDetected = reader.autoDetect(conf, inputPath);
      } catch (InstantiationException | IllegalAccessException e) {
        e.printStackTrace();
      }
    }
    long t2 = System.nanoTime();
    LOG.warn(String.format("Auto detection of the input took %f seconds", (t2-t1)*1E-9));
    if (inputDetected) {
      List<String> parameters = new ArrayList<>();
      for (Map.Entry<String, String> e : conf) {
        String originalValue = originalConf.get(e.getKey());
        if (originalValue == null || !originalValue.equals(e.getValue())) {
          if (e.getValue().equals("true"))
            parameters.add("-"+e.getKey());
          else if (e.getValue().equals("false"))
            parameters.add("-no-"+e.getKey());
          else
            parameters.add(e.getKey()+":"+e.getValue());
        }
      }
      // Print out the added parameters
      LOG.warn("Input format detected. In the future, please add these parameters to your command line to avoid " +
          "running this costly step. "+StringUtil.humandReable(parameters, "and"));
      conf.setClass(FeatureReaderClass, readerClass, FeatureReader.class);
    }
    return inputDetected;
  }

  /**
   * Appends any classes that have additional command line parameters
   * @param parameterClasses
   */
  public static void addDependentClasses(UserOptions opts, Stack<Class> parameterClasses) {
    if (opts == null)
      return;
    for (String iFormat : opts.getValues(InputFormat)) {
      if (iFormat.equals("*auto*")) {
        autodetectInputFormat(opts, opts.getInput());
        iFormat = opts.get(InputFormat);
      }
      Class<? extends FeatureReader> readerClass = getFeatureReaderClass(iFormat);
      if (readerClass != null) {
        opts.setClass(FeatureReaderClass, readerClass, FeatureReader.class);
        parameterClasses.push(readerClass);
      }
    }
  }

  /**
   * Returns the path of the master file within the given index if exists.
   * If more than one master file exists, the most recent one is returned.
   * @param fileSystem the file system that contains the index
   * @param indexPath the path to the index
   * @return the path to the master file if exists. {@code null} otherwise.
   * @throws IOException if an error happens while reading the master file
   */
  public static Path getMasterFilePath(FileSystem fileSystem, Path indexPath) throws IOException {
    FileStatus[] masterFiles = fileSystem.listStatus(indexPath, SpatialInputFormat.MasterFileFilter);
    if (masterFiles.length == 0)
      return null;
    int iMostRecent = 0;
    for (int $i = 1; $i < masterFiles.length; $i++)
      if (masterFiles[$i].getModificationTime() > masterFiles[iMostRecent].getModificationTime())
        iMostRecent = $i;
    return masterFiles[iMostRecent].getPath();
  }

  /**
   * Reads one feature from the given path.
   * @param fileSystem the file system that contains the data files
   * @param inputPath a path to either a file or a directory
   * @param opts user options that contain the input format
   * @return a single feature from one of the data files in the input or {@code null} if failed to read the input
   */
  public static IFeature readOne(FileSystem fileSystem, Path inputPath, Configuration opts) throws IOException {
    // Get a single file to read.
    Path filePath;
    if (fileSystem.getFileStatus(inputPath).isFile()) {
      // If the given path points to a file, read it.
      filePath = inputPath;
    } else {
      // The given path points to a directory, read one file from it.
      // If there is a master file, use it to retrieve the list of files
      Path masterFilePath = getMasterFilePath(fileSystem, inputPath);
      if (masterFilePath == null) {
        // No master file, just list the non-hidden files and read the first one
        FileStatus[] fileStatuses = fileSystem.listStatus(inputPath, HiddenFileFilter);
        if (fileStatuses.length == 0)
          return null;
        filePath = fileStatuses[0].getPath();
      } else {
        // There is a master file, retrieve the name of the first file from it
        LineReader masterFileReader = new LineReader(fileSystem.open(masterFilePath));
        try {
          Text line = new Text();
          // Skip header line
          masterFileReader.readLine(line);
          // Read the first line
          masterFileReader.readLine(line);
          // Get the file name
          String filename = CSVFeature.deleteAttribute(line, '\t', 1, "\'\'\"\"");
          filePath = new Path(inputPath, filename);
        } finally {
          masterFileReader.close();
        }
      }
    }
    // Now, we have a file path to read. Initialize a record reader on that file
    SpatialInputFormat inputFormat = new SpatialInputFormat();
    long fileLength = fileSystem.getFileStatus(filePath).getLen();
    FileSplit fileSplit = new FileSplit(filePath, 0, fileLength, new String[0]);
    FeatureReader featureReader = inputFormat.createAndInitRecordReader(fileSplit, opts);
    try {
      return featureReader.nextKeyValue() ? featureReader.getCurrentValue() : null;
    } catch (InterruptedException e) {
      throw new RuntimeException("Error reading the file", e);
    } finally {
      if (featureReader != null)
        featureReader.close();
    }
  }
}
