package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.Envelope;

import java.util.function.BiFunction;

/**
 * A function that parses a CSV representation of envelopes represented as (minimum coordinates, maximum coordinates)
 * Each of them is a coordinate of a point as (x, y, z, ...).
 */
public class CSVEnvelopeDecoder implements BiFunction<String, Envelope, Envelope> {

  private CSVEnvelopeDecoder() {}

  public static final CSVEnvelopeDecoder instance = new CSVEnvelopeDecoder();

  @Override
  public Envelope apply(String s, Envelope env) {
    String[] parts = s.split(",");
    assert (parts.length % 2) == 0;
    int numDimensions = parts.length / 2;
    if (env == null)
      env = new Envelope(numDimensions);
    else
      env.setCoordinateDimension(numDimensions);
    for (int d$ = 0; d$ < numDimensions; d$++) {
      env.minCoord[d$] = Double.parseDouble(parts[d$]);
      env.maxCoord[d$] = Double.parseDouble(parts[numDimensions + d$]);
    }
    return env;
  }
}
