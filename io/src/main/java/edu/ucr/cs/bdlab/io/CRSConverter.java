/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.GeometryCollection;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.MultiPoint;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.twod.LineString2D;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.geotools.referencing.CRS;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;

import java.io.IOException;

/**
 * Converts features to a target CRS.
 */
public class CRSConverter extends FeatureReader {

  /**The internal feature reader that reads the features*/
  protected final FeatureReader wrapped;

  /**The target coordinate reference system to convert to*/
  protected final CoordinateReferenceSystem targetCRS;

  /**MBR of the returned records*/
  protected final Envelope mbr = new Envelope();

  /**The CRS of the underlying reader*/
  protected CoordinateReferenceSystem sourceCRS;

  /**The transform operation for points*/
  protected MathTransform transform;

  /**Temporary arrays for conversion*/
  private transient double[] tmpCoord1, tmpCoord2;

  public CRSConverter(FeatureReader wrapped, CoordinateReferenceSystem targetCRS) {
    this.wrapped = wrapped;
    this.targetCRS = targetCRS;
  }

  @Override
  public void initialize(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
    wrapped.initialize(split, context);
    this.sourceCRS = wrapped.getCRS();
    try {
      this.transform = CRS.findMathTransform(sourceCRS, targetCRS, true);
    } catch (FactoryException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void initialize(InputSplit split, Configuration conf) throws IOException, InterruptedException {
    wrapped.initialize(split, conf);
  }

  @Override
  public boolean nextKeyValue() throws IOException, InterruptedException {
    if (wrapped.nextKeyValue()) {
      // Transform the wrapped geometry and its MBR
      this.mbr.set(wrapped.getCurrentKey());

      if (tmpCoord1 == null || tmpCoord1.length != this.mbr.getCoordinateDimension()) {
        tmpCoord1 = new double[this.mbr.getCoordinateDimension()];
        tmpCoord2 = new double[this.mbr.getCoordinateDimension()];
      }

      try {
        // Convert the key (MBR)
        convertGeometry(this.mbr);
        // Convert the value (geometry)
        convertGeometry(wrapped.getCurrentValue().getGeometry());
      } catch (TransformException e) {
        e.printStackTrace();
      }
      return true;
    }
    return false;
  }

  /**
   * Converts the given geometry
   * @param geometry
   */
  protected void convertGeometry(IGeometry geometry) throws TransformException {
    // Convert the geometry inplace
    switch (geometry.getType()) {
      case POINT:
        Point p = (Point) geometry;
        transform.transform(p.coords, 0, tmpCoord2, 0, 1);
        p.set(tmpCoord2);
        break;
      case MULTIPOINT:
        MultiPoint multiPoint = (MultiPoint) geometry;
        for (int $i = 0; $i < multiPoint.getNumPoints(); $i++) {
          multiPoint.getPointN($i, tmpCoord1);
          transform.transform(tmpCoord1, 0, tmpCoord2, 0, 1);
          multiPoint.setPointN($i, tmpCoord2);
        }
        break;
      case ENVELOPE:
        Envelope e = (Envelope) geometry;
        transform.transform(e.minCoord, 0, tmpCoord1, 0, 1);
        transform.transform(e.maxCoord, 0, tmpCoord2, 0, 1);
        e.set(tmpCoord1, tmpCoord2);
        break;
      case LINESTRING:
      case MULTILINESTRING:
      case POLYGON:
      case MULTIPOLYGON:
        LineString2D linestring = (LineString2D) geometry;
        for (int $i = 0; $i < linestring.getNumPoints(); $i++) {
          linestring.getPointCoordinates($i, tmpCoord1);
          transform.transform(tmpCoord1, 0, tmpCoord2, 0, 1);
          linestring.setPointCoordinate($i, tmpCoord2);
        }
        break;
      case GEOMETRYCOLLECTION:
        GeometryCollection geometryCollection = (GeometryCollection) geometry;
        for (int $i = 0; $i < geometryCollection.numGeometries(); $i++) {
          convertGeometry(geometryCollection.getGeometry($i));
        }
        break;
      default:
        throw new RuntimeException(String.format("Cannot reproject geometries of type '%s'", geometry.getType()));
    }
  }

  @Override
  public Envelope getCurrentKey() throws IOException, InterruptedException {
    return mbr;
  }

  @Override
  public IFeature getCurrentValue() throws IOException, InterruptedException {
    return wrapped.getCurrentValue();
  }

  @Override
  public float getProgress() throws IOException, InterruptedException {
    return wrapped.getProgress();
  }

  @Override
  public void close() throws IOException {
    wrapped.close();
  }
}
