package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.Point;

import java.util.function.BiFunction;

public class CSVPointEncoder implements BiFunction<IFeature, StringBuilder, StringBuilder> {

  /**The indexes of the columns that store the point coordinates*/
  protected int[] coordCols;

  /**Field separator*/
  protected char fieldSeparator;

  public CSVPointEncoder(char fieldSeparator, int ... coordColumns) {
    this.coordCols = coordColumns;
    this.fieldSeparator = fieldSeparator;
  }

  @Override
  public StringBuilder apply(IFeature feature, StringBuilder str) {
    if (str == null)
      str = new StringBuilder();
    Point p = (Point) feature.getGeometry();
    assert p.getCoordinateDimension() == this.coordCols.length;
    int numCols = coordCols.length + feature.getNumAttributes();
    int iCoord = 0; // The index of the coordinate to be written next
    int iAtt = 0; // The index of the next feature attribute to write
    for (int iCol = 0; iCol < numCols; iCol++) {
      if (iCol > 0)
        str.append(fieldSeparator);
      if (iCoord < coordCols.length && coordCols[iCoord] == iCol) {
        // Time to write the next coordinate
        str.append(p.coords[iCoord++]);
      } else {
        // Time to write the next feature attribute
        str.append(feature.getAttributeValue(iAtt++));
      }
    }
    return str;
  }
}
