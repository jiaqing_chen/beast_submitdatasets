/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

/**
 * A file split that also contains the index of the input in the list of inputs provided by the user.
 */
public class IndexedFileSplit extends FileSplit {
  /**The index of the file that this split belongs to*/
  protected int fileIndex;

  public IndexedFileSplit(Path file, long start, long length, String[] hosts, int fileIndex) {
    super(file, start, length, hosts);
    this.fileIndex = fileIndex;
  }

  public IndexedFileSplit(Path file, long start, long length, String[] hosts, String[] inMemoryHosts, int fileIndex) {
    super(file, start, length, hosts, inMemoryHosts);
    this.fileIndex = fileIndex;
  }

  public int getFileIndex() {
    return fileIndex;
  }
}
