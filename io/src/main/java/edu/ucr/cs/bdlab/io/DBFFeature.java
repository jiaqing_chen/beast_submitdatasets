/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.io;

import edu.ucr.cs.bdlab.geolite.EmptyGeometry;
import edu.ucr.cs.bdlab.geolite.GeometryType;
import edu.ucr.cs.bdlab.geolite.IFeature;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.util.WritableExternalizable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

/**
 * A feature with a geometry and attributes read from a DBF file
 */
public class DBFFeature implements IFeature, WritableExternalizable {

  /**The geometry associated with this feature*/
  protected IGeometry geometry = EmptyGeometry.instance;

  /**Field descriptors including names, types, and sizes*/
  protected DBFReader.FieldDescriptor[] descriptors;

  /**The values of all the fields in one concatenated array in the same format of DBF files.*/
  protected byte[] fieldValues;

  /**The starting offset of each field in the {@link #fieldValues} array*/
  protected int[] fieldStartOffset;

  /**Map each field name to its position in the array*/
  private Map<String, Integer> fieldNameMap;

  public DBFFeature() {
    // Empty constructor for externalization
  }

  /**
   * Set the geometry used by the feature.
   * @param geometry
   */
  @Override
  public void setGeometry(IGeometry geometry) {
    this.geometry = geometry;
  }

  public DBFFeature(DBFReader.FieldDescriptor[] descriptors) {
    this.descriptors = descriptors;
    int offset = 0;
    fieldStartOffset = new int[descriptors.length + 1];
    for (int iField = 0; iField < this.descriptors.length; iField++) {
      fieldStartOffset[iField] = offset;
      offset += this.descriptors[iField].fieldLength;
    }
    // An additional offset equal to the size of all fields to simplify the calculation of length
    fieldStartOffset[this.descriptors.length] = offset;
    fieldValues = new byte[offset];
  }

  public void setFieldValues(byte[] data) {
    if (data.length != this.fieldValues.length)
      throw new RuntimeException(String.format("Data size mismatch %d != %d", data.length, fieldValues.length));
    System.arraycopy(data, 0, this.fieldValues, 0, data.length);
  }

  @Override
  public IGeometry getGeometry() {
    return geometry;
  }

  @Override
  public Object getAttributeValue(int iField) {
    int startOffset;
    int endOffset;
    switch (descriptors[iField].fieldType) {
      case 'C':
        // String
        endOffset = fieldStartOffset[iField + 1] - 1;
        while (endOffset >= fieldStartOffset[iField] && fieldValues[endOffset] == ' ')
          endOffset--;
        return new String(fieldValues, fieldStartOffset[iField], endOffset - fieldStartOffset[iField] + 1);
      case 'N':
      case 'F':
        // The number stored as a string of a given length with a specified number of digits after the point
        // aligned to the right by blanks. An empty value is specified by a space bar
        startOffset = fieldStartOffset[iField];
        // Skip any leading blanks
        while (startOffset < fieldStartOffset[iField+1] && !Character.isDigit(fieldValues[startOffset]))
          startOffset++;
        // Check if it is an empty value
        if (startOffset == fieldStartOffset[iField+1])
          return null;
        if (descriptors[iField].decimalCount == 0) {
          // No decimal point. Return the value as an integer (Double in case the number is too large)
          return Long.parseLong(new String(fieldValues, startOffset, fieldStartOffset[iField + 1] - startOffset));
        } else {
          double value = Double.parseDouble(new String(fieldValues, startOffset, fieldStartOffset[iField + 1] - startOffset));
          for (int i = 0; i < descriptors[iField].decimalCount; i++)
            value /= 10.0;
          return Double.valueOf(value);
        }
      case 'D':
        // Date - 8 bytes - date stored as a string in the format YYYYMMDD.
        startOffset = fieldStartOffset[iField];
        int yyyy = (fieldValues[startOffset] - '0') * 1000 + (fieldValues[startOffset + 1] - '0') * 100 +
            (fieldValues[startOffset + 2] - '0') * 10 + (fieldValues[startOffset + 3] - '0');
        int mm = (fieldValues[startOffset + 4] - '0') * 10 + (fieldValues[startOffset + 5] - '0');
        int dd = (fieldValues[startOffset + 6] - '0') * 10 + (fieldValues[startOffset + 7] - '0');
        return new GregorianCalendar(yyyy, mm - 1, dd);
      case 'B':
        // 10 digits representing a .DBT block number. The number is stored as a string,
        // right justified and padded with blanks.
        startOffset = fieldStartOffset[iField];
        endOffset = fieldStartOffset[iField + 1];
        // Skip leading spaces
        while (startOffset < endOffset && fieldValues[startOffset] == ' ')
          startOffset++;
        return startOffset == endOffset ? null : new String(fieldValues, startOffset, endOffset - startOffset);
      default:
        throw new RuntimeException(String.format("Field type '%c' not supported", descriptors[iField].fieldType));
    }
  }

  @Override
  public Object getAttributeValue(String name) {
    if (fieldNameMap == null)
      fieldNameMap = createFieldNameMap();
    Integer iField = fieldNameMap.get(name);
    return iField == null ? null : getAttributeValue(iField);
  }

  private Map<String, Integer> createFieldNameMap() {
    Map<String, Integer> fieldNameMap = new HashMap<>();
    for (int iField = 0; iField < descriptors.length; iField++) {
      fieldNameMap.put(descriptors[iField].getFieldName(), iField);
    }
    return fieldNameMap;
  }

  @Override
  public int getNumAttributes() {
    return descriptors.length;
  }

  @Override
  public String getAttributeName(int i) {
    return descriptors[i].getFieldName();
  }

  @Override
  public int getStorageSize() {
    return getGeometry().getGeometryStorageSize() + fieldStartOffset[descriptors.length];
  }

  @Override
  public void writeExternal(ObjectOutput out) throws IOException {
    this.write(out);
  }

  @Override
  public void readExternal(ObjectInput in) throws IOException {
    this.readFields(in);
  }

  @Override
  public void write(DataOutput out) throws IOException {
    out.writeInt(descriptors.length);
    for (DBFReader.FieldDescriptor descriptor : descriptors)
      descriptor.write(out);
    out.writeInt(fieldValues.length);
    out.write(fieldValues);
    out.writeInt(geometry.getType().ordinal());
    geometry.write(out);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    // Read descriptors
    int numFields = in.readInt();
    if (descriptors == null || descriptors.length != numFields) {
      descriptors = new DBFReader.FieldDescriptor[numFields];
      for (int iField = 0; iField < numFields; iField++)
        descriptors[iField] = new DBFReader.FieldDescriptor();
    }
    for (int iField = 0; iField < numFields; iField++)
      descriptors[iField].readFields(in);
    // Read other properties
    int fieldValueLength = in.readInt();
    if (fieldValues == null || fieldValues.length != fieldValueLength)
      fieldValues = new byte[fieldValueLength];
    in.readFully(fieldValues);
    GeometryType geomType = GeometryType.values()[in.readInt()];
    if (geometry == null || geomType != geometry.getType())
      geometry = geomType.createInstance();
    geometry.readFields(in);

    // Initialize fieldStartOffset
    int offset = 0;
    fieldStartOffset = new int[descriptors.length + 1];
    for (int iField = 0; iField < this.descriptors.length; iField++) {
      fieldStartOffset[iField] = offset;
      offset += this.descriptors[iField].fieldLength;
    }
    // An additional offset equal to the size of all fields to simplify the calculation of length
    fieldStartOffset[this.descriptors.length] = offset;
  }

  @Override
  public boolean equals(Object that) {
    return IFeature.equals(this, (IFeature) that);
  }

  @Override
  public String toString() {
    StringBuilder b = new StringBuilder();
    b.append("Feature(");
    this.getGeometry().toWKT(b);
    for (int $i = 0; $i < getNumAttributes(); $i++) {
      b.append(',');
      b.append(getAttributeValue($i));
    }
    b.append(')');
    return b.toString();
  }
}
