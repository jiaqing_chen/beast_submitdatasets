/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.raptor;

import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.twod.MultiPolygon2D;
import edu.ucr.cs.bdlab.geolite.twod.Polygon2D;
import edu.ucr.cs.bdlab.test.SparkTest;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;

public class IntersectionsTest extends SparkTest {

  static Polygon2D p1, p2, p3, p4, p5, p6, p7;

  static {
    p1 = new Polygon2D();
    p1.addPoint(1.0, 1.0);
    p1.addPoint(9.0, 3.0);
    p1.addPoint(3.0, 5.0);
    p1.closeLastRing(false);

    p2 = new Polygon2D();
    p2.addPoint(12.0, 3.0);
    p2.addPoint(18.0, 5.0);
    p2.addPoint(15.0, 7.0);
    p2.closeLastRing(false);

    p3 = new Polygon2D();
    p3.addPoint(-3.0, 6.0);
    p3.addPoint(12.0, 9.0);
    p3.addPoint(3.0, 11.0);
    p3.closeLastRing(false);

    p4 = new Polygon2D();
    p4.addPoint(5.0, 1.0);
    p4.addPoint(8.0, 2.0);
    p4.addPoint(6.0, 5.0);
    p4.closeLastRing(false);

    p5 = new Polygon2D();
    p5.addPoint(6.0, 11.0);
    p5.addPoint(14.0, 13.0);
    p5.addPoint(8.0, 15.0);
    p5.closeLastRing(false);

    p6 = new Polygon2D();
    p6.addPoint(9.0, 21.0);
    p6.addPoint(12.0, 22.0);
    p6.addPoint(10.0, 25.0);
    p6.closeLastRing(false);

    p7 = new Polygon2D();
    p7.addPoint(15.3, 8.3);
    p7.addPoint(15.8, 8.4);
    p7.addPoint(15.5, 8.8);
    p7.closeLastRing(false);
  }

  public void testComputeOnePolygon() {
    RasterReader simpleRaster = new FakeRaster(1, 1, 10, 10);
    Intersections intersections = new Intersections();
    intersections.compute(new IGeometry[] {p1}, simpleRaster);
    assertEquals(4, intersections.getNumIntersections());
    int[] x1s = new int[intersections.getNumIntersections()];
    int[] x2s = new int[intersections.getNumIntersections()];
    int[] ys = new int[intersections.getNumIntersections()];
    for (int $i = 0; $i < intersections.getNumIntersections(); $i++) {
      x1s[$i] = intersections.getX1($i);
      x2s[$i] = intersections.getX2($i);
      ys[$i] = intersections.getY($i);
    }
    assertArrayEquals(new int[] {1, 2, 2, 3}, x1s);
    assertArrayEquals(new int[] {2, 6, 7, 4}, x2s);
    assertArrayEquals(new int[] {1, 2, 3, 4}, ys);
  }

  public void testSmallPolygon() {
    RasterReader simpleRaster = new FakeRaster(2, 2, 10, 10);
    Intersections intersections = new Intersections();
    intersections.compute(new IGeometry[] {p7}, simpleRaster);
    assertEquals(1, intersections.getNumIntersections());
    int[] x1s = new int[intersections.getNumIntersections()];
    int[] x2s = new int[intersections.getNumIntersections()];
    int[] ys = new int[intersections.getNumIntersections()];
    for (int $i = 0; $i < intersections.getNumIntersections(); $i++) {
      x1s[$i] = intersections.getX1($i);
      x2s[$i] = intersections.getX2($i);
      ys[$i] = intersections.getY($i);
    }
    assertArrayEquals(new int[] {15}, x1s);
    assertArrayEquals(new int[] {15}, x2s);
    assertArrayEquals(new int[] {8}, ys);
  }

  public void testComputeOnePolygonWithOutOfBounds() {
    RasterReader simpleRaster = new FakeRaster(1, 1, 10, 10);
    Intersections intersections = new Intersections();
    intersections.compute(new IGeometry[] {p3}, simpleRaster);
    assertEquals(3, intersections.getNumIntersections());
    int[] x1s = new int[intersections.getNumIntersections()];
    int[] x2s = new int[intersections.getNumIntersections()];
    int[] ys = new int[intersections.getNumIntersections()];
    for (int $i = 0; $i < intersections.getNumIntersections(); $i++) {
      x1s[$i] = intersections.getX1($i);
      x2s[$i] = intersections.getX2($i);
      ys[$i] = intersections.getY($i);
    }
    assertArrayEquals(new int[] {0, 0, 1}, x1s);
    assertArrayEquals(new int[] {4, 9, 9}, x2s);
    assertArrayEquals(new int[] {7, 8, 9}, ys);
  }

  public void testComputeTwoDisjointPolygonsInTwoTiles() {
    RasterReader simpleRaster = new FakeRaster(2, 2, 10, 10);
    Intersections intersections = new Intersections();
    intersections.compute(new IGeometry[] {p1, p2}, simpleRaster);
    assertEquals(8, intersections.getNumIntersections());
    int[] x1s = new int[intersections.getNumIntersections()];
    int[] x2s = new int[intersections.getNumIntersections()];
    int[] ys = new int[intersections.getNumIntersections()];
    for (int $i = 0; $i < intersections.getNumIntersections(); $i++) {
      x1s[$i] = intersections.getX1($i);
      x2s[$i] = intersections.getX2($i);
      ys[$i] = intersections.getY($i);
    }
    assertArrayEquals(new int[] {1, 2, 2, 3, 12, 13, 14, 15}, x1s);
    assertArrayEquals(new int[] {2, 6, 7, 4, 13, 16, 16, 15}, x2s);
    assertArrayEquals(new int[] {1, 2, 3, 4, 3, 4, 5, 6}, ys);
  }

  public void testComputeTwoOverlappingPolygonsInOneTile() {
    RasterReader simpleRaster = new FakeRaster(1, 1, 10, 10);
    Intersections intersections = new Intersections();
    intersections.compute(new IGeometry[] {p1, p4}, simpleRaster);
    assertEquals(7, intersections.getNumIntersections());
    int[] x1s = new int[intersections.getNumIntersections()];
    int[] x2s = new int[intersections.getNumIntersections()];
    int[] ys = new int[intersections.getNumIntersections()];
    int[] pids = new int[intersections.getNumIntersections()];
    for (int $i = 0; $i < intersections.getNumIntersections(); $i++) {
      x1s[$i] = intersections.getX1($i);
      x2s[$i] = intersections.getX2($i);
      ys[$i] = intersections.getY($i);
      pids[$i] = intersections.getPolygonIndex($i);
    }
    assertArrayEquals(new int[] {1, 5, 2, 5, 2, 6, 3}, x1s);
    assertArrayEquals(new int[] {2, 6, 6, 7, 7, 6, 4}, x2s);
    assertArrayEquals(new int[] {1, 1, 2, 2, 3, 3, 4}, ys);
    assertArrayEquals(new int[] {0, 1, 0, 1, 0, 1, 0}, pids);
  }

  public void testComputeOnePolygonCrossesTiles() {
    RasterReader simpleRaster = new FakeRaster(2, 2, 10, 10);
    Intersections intersections = new Intersections();
    intersections.compute(new IGeometry[] {p5}, simpleRaster);
    assertEquals(6, intersections.getNumIntersections());
    int[] x1s = new int[intersections.getNumIntersections()];
    int[] x2s = new int[intersections.getNumIntersections()];
    int[] ys = new int[intersections.getNumIntersections()];
    for (int $i = 0; $i < intersections.getNumIntersections(); $i++) {
      x1s[$i] = intersections.getX1($i);
      x2s[$i] = intersections.getX2($i);
      ys[$i] = intersections.getY($i);
    }
    assertArrayEquals(new int[] {6, 7, 7, 8, 10, 10}, x1s);
    assertArrayEquals(new int[] {7, 9, 9, 9, 11, 12}, x2s);
    assertArrayEquals(new int[] {11, 12, 13, 14, 12, 13}, ys);
  }

  public void testComputeMultiPolygon() {
    RasterReader simpleRaster = new FakeRaster(2, 2, 10, 10);
    Intersections intersections = new Intersections();
    intersections.compute(new IGeometry[] {new MultiPolygon2D(p1, p2)}, simpleRaster);
    assertEquals(8, intersections.getNumIntersections());
    int[] x1s = new int[intersections.getNumIntersections()];
    int[] x2s = new int[intersections.getNumIntersections()];
    int[] ys = new int[intersections.getNumIntersections()];
    for (int $i = 0; $i < intersections.getNumIntersections(); $i++) {
      x1s[$i] = intersections.getX1($i);
      x2s[$i] = intersections.getX2($i);
      ys[$i] = intersections.getY($i);
    }
    assertArrayEquals(new int[] {1, 2, 2, 3, 12, 13, 14, 15}, x1s);
    assertArrayEquals(new int[] {2, 6, 7, 4, 13, 16, 16, 15}, x2s);
    assertArrayEquals(new int[] {1, 2, 3, 4, 3, 4, 5, 6}, ys);
  }

  public void testVectorOutOfBounds() {
    RasterReader simpleRaster = new FakeRaster(1, 1, 10, 10);
    Intersections intersections = new Intersections();
    intersections.compute(new IGeometry[] {p6}, simpleRaster);
    assertEquals(0, intersections.getNumIntersections());
  }

  public void testWriteReadWhole() throws IOException {
    RasterReader simpleRaster = new FakeRaster(1, 1, 10, 10);
    Intersections intersections = new Intersections();
    intersections.compute(new IGeometry[] {p3}, simpleRaster);
    // Now, write it to disk and read it back
    Path tempPath = new Path(scratchPath, "temp");
    FileSystem fs = tempPath.getFileSystem(new Configuration());
    intersections.writeAll(fs, tempPath);

    // Read it back
    intersections = new Intersections();
    intersections.readAll(fs, tempPath);

    assertEquals(3, intersections.getNumIntersections());
    int[] x1s = new int[intersections.getNumIntersections()];
    int[] x2s = new int[intersections.getNumIntersections()];
    int[] ys = new int[intersections.getNumIntersections()];
    for (int $i = 0; $i < intersections.getNumIntersections(); $i++) {
      x1s[$i] = intersections.getX1($i);
      x2s[$i] = intersections.getX2($i);
      ys[$i] = intersections.getY($i);
    }
    assertArrayEquals(new int[] {0, 0, 1}, x1s);
    assertArrayEquals(new int[] {4, 9, 9}, x2s);
    assertArrayEquals(new int[] {7, 8, 9}, ys);
  }

  public void testWriteAllReadOneTile() throws IOException {
    RasterReader simpleRaster = new FakeRaster(2, 2, 10, 10);
    Intersections intersections = new Intersections();
    intersections.compute(new IGeometry[] {p5}, simpleRaster);

    // Now, write it to disk and read it back
    Path tempPath = new Path(scratchPath, "temp");
    FileSystem fs = tempPath.getFileSystem(new Configuration());
    intersections.writeAll(fs, tempPath);

    // Read the first tile back
    intersections = new Intersections();
    intersections.readIntersectionsInTiles(fs, tempPath, 2, 2);

    assertEquals(4, intersections.getNumIntersections());
    int[] x1s = new int[intersections.getNumIntersections()];
    int[] x2s = new int[intersections.getNumIntersections()];
    int[] ys = new int[intersections.getNumIntersections()];
    for (int $i = 0; $i < intersections.getNumIntersections(); $i++) {
      x1s[$i] = intersections.getX1($i);
      x2s[$i] = intersections.getX2($i);
      ys[$i] = intersections.getY($i);
    }
    assertArrayEquals(new int[] {6, 7, 7, 8}, x1s);
    assertArrayEquals(new int[] {7, 9, 9, 9}, x2s);
    assertArrayEquals(new int[] {11, 12, 13, 14}, ys);

    // Read the second tile back
    intersections = new Intersections();
    intersections.readIntersectionsInTiles(fs, tempPath, 3, 3);

    assertEquals(2, intersections.getNumIntersections());
    x1s = new int[intersections.getNumIntersections()];
    x2s = new int[intersections.getNumIntersections()];
    ys = new int[intersections.getNumIntersections()];
    for (int $i = 0; $i < intersections.getNumIntersections(); $i++) {
      x1s[$i] = intersections.getX1($i);
      x2s[$i] = intersections.getX2($i);
      ys[$i] = intersections.getY($i);
    }
    assertArrayEquals(new int[] {10, 10}, x1s);
    assertArrayEquals(new int[] {11, 12}, x2s);
    assertArrayEquals(new int[] {12, 13}, ys);

  }
}