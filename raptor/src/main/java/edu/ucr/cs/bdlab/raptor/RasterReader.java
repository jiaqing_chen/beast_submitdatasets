/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.raptor;

import org.opengis.referencing.crs.CoordinateReferenceSystem;

import java.awt.geom.Point2D;
import java.io.Closeable;
import java.io.IOException;

public interface RasterReader extends Closeable {

  /**
   * Returns the unique ID of the tile that contains the given pixel coordinate.
   * @param iPixel
   * @param jPixel
   * @return
   */
  int getTileID(int iPixel, int jPixel);

  /**
   * The total number of tiles in this raster layer.
   * @return
   */
  int getNumTiles();

  /**
   * Number of tiles along the x-coordinate, i.e. number of tiles per row.
   * @return
   */
  int getNumTilesX();

  /**
   * Number of tiles along the y-coordinate, i.e., number of tiles per column.
   * @return
   */
  int getNumTilesY();

  /**
   * The index of the lowest row (scanline)
   * @return
   */
  int getY1();

  /**
   * The index of the highest row (scanline)
   * @return
   */
  int getY2();

  /**
   * The index of the lowest column
   * @return
   */
  int getX1();

  /**
   * The index of the lowest column
   * @return
   */
  int getX2();

  /**
   * Returns the coordinate of the left column of the given tile
   * @return
   */
  int getTileX1(int tileID);

  /**
   * Returns the coordinate of the last column of the given tile (inclusive)
   * @return
   */
  int getTileX2(int tileID);

  int getRasterWidth();

  int getRasterHeight();

  /**
   * Converts a point in the vector space (double coordinates, model coordinates) to a point in
   * the raster space (grid coordinates). Even though the output point is in grid coordinates, it is still
   * represented as double to provide information on where the point is within the pixel.
   * If the returned value is floored, then it gives the pixel that contains the point.
   * If it is rounded, then it gives the pixel with the nearest lower corner.
   * @param wx
   * @param wy
   * @param outPoint
   */
  void modelToGrid(double wx, double wy, Point2D.Double outPoint);

  /**
   * Converts a point from the raster space (integer coordinates) to the vector
   * space (double coordinates). This function returns the coordinates of the
   * center of the corresponding pixel. In other words, if a pixel maps to a
   * rectangle, this function returns the center of that rectangle. Notice that
   * the geographic coordinate system (projection) is not taken into account.
   * The returned value is in whatever that coordinate system is.
   * @param gx
   * @param gy
   * @param outPoint
   */
  void gridToModel(int gx, int gy, Point2D.Double outPoint);

  /**
   * Returns the value of a given pixel. This function assumes that the raster has been already loaded and contains
   * this pixel. While this function can automatically check and load the appropriate part of the raster, it could be
   * too much overhead when reading big portions of the raster file. For performance consideration, the developer must
   * first load the appropriate part of the raster in one call before reading all pixels.
   *
   * @param iPixel
   * @param jPixel
   * @param value
   */
  void getPixelValueAsInt(int iPixel, int jPixel, int[] value) throws IOException;

  /**
   * Similar to {@link #getPixelValueAsInt(int, int, int[])} but returns the values as float. This method is
   * useful if the underlying GeoTIFF file contains float values and the conversion to int will lose the details.
   * @param iPixel
   * @param jPixel
   * @param value
   * @throws IOException
   */
  void getPixelValueAsFloat(int iPixel, int jPixel, float[] value) throws IOException;

  /**
   * Returns the special value that marks undefined values.
   * @return
   */
  int getFillValue();

  /**
   * Number of components for each pixel in this raster file.
   * @return
   */
  int getNumComponents();

  /**
   * Returns the value of all the components of the pixel that contains the given point in world coordinate
   * @param x
   * @param y
   * @param value
   * @throws IOException
   */
  default void getPointValueAsInt(double x, double y, int[] value) throws IOException {
    Point2D.Double outPoint = new Point2D.Double();
    modelToGrid(x, y, outPoint);
    getPixelValueAsInt((int) outPoint.getX(), (int) outPoint.getY(), value);
  }

  /**
   * Returns the value of all the components of the pixel that contains the given point in world coordinate
   * @param x
   * @param y
   * @param value
   * @throws IOException
   */
  default void getPointValueAsFloat(double x, double y, float[] value) throws IOException {
    Point2D.Double outPoint = new Point2D.Double();
    modelToGrid(x, y, outPoint);
    getPixelValueAsFloat((int) outPoint.getX(), (int) outPoint.getY(), value);
  }

  /**
   * The coordinate reference system of this raster layer.
   * @return
   */
  CoordinateReferenceSystem getCRS();
}
