/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.raptor;

/**
 * An interface for a process that collects pixel values from the raster layer and accumulates them.
 * Created by Ahmed Eldawy on 5/19/2017.
 */
public interface Collector {

  /**
   * Initializes the collector with the given number of bands.
   * @param n
   */
  void setNumBands(int n);

  /**
   * Collects a value of the given location which has multiple bands.
   * @param column
   * @param row
   * @param value
   */
  void collect(int column, int row, int[] value);

  void collect(int column, int row, float[] value);

  /**
   * Collects all values in a given block range. The array values is assumed to contain consecutive values for each
   * band in each pixel stored row-wise. In other words, the first N entries in the array <code>values</code> are used
   * where N = width * height * number of bands.
   * @param column
   * @param row
   * @param width
   * @param height
   * @param values
   */
  void collect(int column, int row, int width, int height, int[] values);

  /**
   * Accumulates the value of this collector with another one. Helpful for combining several partial results.
   * @param c
   */
  void accumulate(Collector c);

  /**
   * Return the total number of bands
   * @return
   */
  int getNumBands();

  /**
   * Make the value of this collector invalid to indicate an error in processing
   */
  void invalidate();

  /**Whether the value of the collector is valid or not*/
  boolean isValid();
}
