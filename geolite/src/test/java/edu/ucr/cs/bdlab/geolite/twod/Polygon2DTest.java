package edu.ucr.cs.bdlab.geolite.twod;

import edu.ucr.cs.bdlab.geolite.ILineString;
import junit.framework.TestCase;

public class Polygon2DTest extends TestCase {
  public void testCreateSimple() {
    Polygon2D polygon = new Polygon2D();
    polygon.addPointXYM(0.0, 0.0, 3.0);
    polygon.addPointXYM(1.0, 0.0, 3.0);
    polygon.addPointXYM(1.0, 1.0, 3.0);
    polygon.addPointXYM(0.0, 0.0, 3.0);
    polygon.closeLastRing(true);
  }

  public void testGetRingWhenClosedWithNoCheck() {
    Polygon2D polygon = new Polygon2D();
    polygon.addPoint(0, 0);
    polygon.addPoint(1, 1);
    polygon.addPoint(0, 1);
    polygon.addPoint(0.1, 0.1);
    polygon.closeLastRingNoCheck();

    int numPoints = 0;
    for (int iRing = 0; iRing < polygon.getNumRings(); iRing++) {
      ILineString ring = polygon.getRingN(iRing);
      numPoints += ring.getNumPoints();
    }
    assertEquals(4, numPoints);
  }

}