package edu.ucr.cs.bdlab.geolite.util;

import com.esri.core.geometry.Geometry;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.geometry.ogc.OGCGeometry;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.wktparser.ParseException;
import edu.ucr.cs.bdlab.wktparser.WKTParser;
import junit.framework.TestCase;

import java.io.IOException;

public class EsriConverterTest extends TestCase {

  public void testAllGeometries() throws IOException, ParseException {
    String[] wkts = {
        "POINT (1.0 2.0)",
        "LINESTRING (1.0 3.0, 2.5 5.0)",
        "POLYGON ((2.0 3.0, 2.0 7.0, 5.0 8.0, 2.0 3.0))",
        "MULTILINESTRING ((1.0 3.0, 2.5 5.0), (11.0 3.0, 12.5 5.0))",
        "MULTIPOLYGON (((2.0 3.0, 2.0 7.0, 5.0 8.0, 2.0 3.0)), ((12.0 3.0, 12.0 7.0, 15.0 8.0, 12.0 3.0)))",
    };
    WKTParser wktParser = new WKTParser();
    for (String wkt : wkts) {
      IGeometry expectedGeom = wktParser.parse(wkt, null);
      Geometry esriGeom = EsriConverter.createEsriGeometry(expectedGeom);
      OGCGeometry anotherEsriGeom = OGCGeometry.fromText(wkt);
      assertEquals(anotherEsriGeom, OGCGeometry.createFromEsriGeometry(esriGeom, SpatialReference.create(4326)));
    }
  }

  public void testFromEsriGeometryPoint() {
    com.esri.core.geometry.Point esriPoint = new com.esri.core.geometry.Point(123.0, 555.0);
    edu.ucr.cs.bdlab.geolite.Point litePoint = (Point) EsriConverter.fromEsriGeometry(esriPoint, null);
    assertNotNull(litePoint);
    assertEquals(2, litePoint.getCoordinateDimension());
    assertEquals(123.0, litePoint.coords[0], 1E-3);
    assertEquals(555.0, litePoint.coords[1], 1E-3);
  }

  public void testFromEsriGeometryMixed() throws IOException, ParseException {
    String[] wkts = {
        "POINT (1.0 2.0)",
        "LINESTRING (1.0 3.0, 2.5 5.0)",
        "POLYGON ((2.0 3.0, 5.0 8.0, 2.0 7.0, 2.0 3.0))",
        "MULTILINESTRING ((1.0 3.0, 2.5 5.0), (11.0 3.0, 12.5 5.0))",
        "MULTIPOLYGON (((2.0 3.0, 5.0 8.0, 2.0 7.0, 2.0 3.0)), ((12.0 3.0, 15.0 8.0, 12.0 7.0, 12.0 3.0)))",
    };
    for (String wkt : wkts) {
      com.esri.core.geometry.Geometry esriGeom = OGCGeometry.fromText(wkt).getEsriGeometry();
      edu.ucr.cs.bdlab.geolite.IGeometry actualGeom = EsriConverter.fromEsriGeometry(esriGeom, null);
      edu.ucr.cs.bdlab.geolite.IGeometry expectedGeom = new WKTParser().parse(wkt, null);
      assertEquals(expectedGeom, actualGeom);
    }
  }
}