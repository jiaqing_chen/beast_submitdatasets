package edu.ucr.cs.bdlab.geolite.twod;

import junit.framework.TestCase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class LineString2DTest extends TestCase {

  public void testLineStringWithMValue() throws IOException {
    LineString2D l = new LineString2D();

    l.addPointXYM(1.0, 2.2, 1234.5);
    assertEquals(1, l.getNumPoints());
    assertEquals(1234.5, l.ms[0]);

    // Test serialization/deserilization
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    l.write(dos);
    dos.close();

    ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
    DataInputStream dis = new DataInputStream(bais);
    l = new LineString2D();
    l.readFields(dis);
    assertTrue("LineString should has m values", l.hasMValues());
    assertEquals(1, l.getNumPoints());
    assertEquals(2.2, l.ys[0]);
    assertEquals(1234.5, l.ms[0]);
  }
}