/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite;

import edu.ucr.cs.bdlab.geolite.twod.LineString2D;
import junit.framework.TestCase;
import org.junit.Test;

public class LineStringTest extends TestCase {

  @Test
  public void testPointOverlap() {
    LineString2D ls = new LineString2D();
    ls.addPoint(0, 0);
    ls.addPoint(2, 1);
    ls.addPoint(0, 2);
    assertFalse(ls.intersects(new Point(2, 4.0, 2.0)));
    assertTrue(ls.intersects(new Point(2, 1.0, 1.5)));
  }

  @Test
  public void testLineStringOverlap() {
    LineString2D ls1 = new LineString2D();
    ls1.addPoint(0, 0);
    ls1.addPoint(2, 1);
    ls1.addPoint(0, 2);
    LineString2D ls2 = new LineString2D();
    ls2.addPoint(2, 0);
    ls2.addPoint(3, 1);
    assertFalse(ls1.intersects(ls2));
    ls2.addPoint(0, 1);
    assertTrue(ls1.intersects(ls2));
    assertTrue(ls2.intersects(ls1));
  }
}