/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite;

import junit.framework.TestCase;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.GregorianCalendar;

public class FeatureTest extends TestCase {

  public void testReadAttributes() {
    Feature f = new Feature();
    f.appendAttribute("Area", 125.0);
    f.appendAttribute("Count", -10);
    String str = "Test Attribute";
    f.appendAttribute("Name", str);
    f.geometry = new Point(2, -1.0, -5.0);

    assertEquals(125.0, f.getAttributeValue("Area"));
    assertEquals(-10, f.getAttributeValue("Count"));
    assertEquals(str, f.getAttributeValue("Name"));
    assertNull(f.getAttributeValue("non-existent"));
 }

  public void testCopyFrom() {
    Feature f = new Feature();
    f.appendAttribute("Area", 125.0);
    f.appendAttribute("Count", -10);
    f.appendAttribute("Date", new GregorianCalendar(2019, 01, 03));
    String str = "Test Attribute";
    f.appendAttribute("Name", str);
    f.geometry = new Point(2, -1.0, -5.0);

    Feature f2 = new Feature();
    f2.copyAttributeMetadata(f);
    f2.copyAttributeValues(f);

    assertEquals(125.0, f2.getAttributeValue("Area"));
    assertEquals(-10, f2.getAttributeValue("Count"));
    assertEquals(new GregorianCalendar(2019, 01, 03), f2.getAttributeValue("Date"));
    assertEquals(str, f2.getAttributeValue("Name"));
  }

  public void testVariableLengthAttributes() {
    Feature f = new Feature();
    String str = "Test Attribute";
    f.appendAttribute("Name", str);
    f.appendAttribute("Area", 125.0);
    f.appendAttribute("Count", -10);
    f.geometry = new Point(2, -1.0, -5.0);

    assertEquals(125.0, f.getAttributeValue("Area"));
    assertEquals(-10, f.getAttributeValue("Count"));
    assertEquals(str, f.getAttributeValue("Name"));
  }

  public void testCopyMultipleFeatures() {
    Feature f1 = new Feature();
    String str1 = "Test Attribute";
    f1.appendAttribute("Name", str1);
    f1.appendAttribute("Area", 125.0);
    f1.appendAttribute("Count", -10);
    f1.geometry = new Point(2, -1.0, -5.0);

    Feature f2 = new Feature();
    String str2 = "Test Attribute";
    f2.appendAttribute("Name", str2);
    f2.appendAttribute("Area", 325.0);
    f2.appendAttribute("Count", -50);
    f2.geometry = new Point(2, -4.0, -10.0);

    Feature f3 = new Feature();
    f3.copyAttributeMetadata(f1);
    f3.copyAttributeValues(f1);

    assertEquals(125.0, f3.getAttributeValue("Area"));
    assertEquals(-10, f3.getAttributeValue("Count"));
    assertEquals(str1, f3.getAttributeValue("Name"));

    f3.copyAttributeValues(f2);
    assertEquals(325.0, f3.getAttributeValue("Area"));
    assertEquals(-50, f3.getAttributeValue("Count"));
    assertEquals(str2, f3.getAttributeValue("Name"));
  }

  public void testSerializeDeserialize() throws IOException {
    Feature f = new Feature();
    String str1 = "Test Attribute";
    f.appendAttribute("Name", str1);
    f.appendAttribute("Area", 125.0);
    f.appendAttribute("Count", -10);
    f.geometry = new Point(2, -1.0, -5.0);

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    f.write(dos);
    dos.close();

    DataInputStream in = new DataInputStream(new ByteArrayInputStream(baos.toByteArray()));
    Feature f2 = new Feature();
    f2.readFields(in);

    assertEquals(f.getStorageSize(), f2.getStorageSize());
  }

  public void testSerializeDeserializeWithLong() throws IOException {
    Feature f = new Feature();
    String str1 = "Test Attribute";
    f.appendAttribute("Name", str1);
    f.appendAttribute("Area", 125.0);
    f.appendAttribute("Count", -10);
    f.appendAttribute("Length", Long.MAX_VALUE);
    f.appendAttribute("Description", "description");
    f.geometry = new Point(2, -1.0, -5.0);

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    f.write(dos);
    dos.close();

    DataInputStream in = new DataInputStream(new ByteArrayInputStream(baos.toByteArray()));
    Feature f2 = new Feature();
    f2.readFields(in);

    for (int $i = 0; $i < f2.getNumAttributes(); $i++)
      assertEquals(f.getAttributeValue($i), f2.getAttributeValue($i));
  }

  public void testGetStorageSizeWithEmptyAttributes() {
    Feature f = new Feature(new Point(1.0, 2.0));
    try {
      f.getStorageSize();
      f.getAttributeValue(0);
      f.getAttributeValue("test");
    } catch (Exception e) {
      e.printStackTrace();
      fail("Should not raise an exception");
    }
  }
}