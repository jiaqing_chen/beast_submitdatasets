package edu.ucr.cs.bdlab.geolite;

import junit.framework.TestCase;

public class GeometryCollectionTest extends TestCase {

  public void testCentroidWithEmptyGeometries() {
    GeometryCollection collection = new GeometryCollection();
    collection.addGeometry(new Point(1.0, 2.0));
    collection.addGeometry(EmptyGeometry.instance);
    collection.addGeometry(new Point(3.0, 3.0));
    Point centroid = new Point(collection.getCoordinateDimension());
    collection.centroid(centroid);
    assertEquals(2.0, centroid.coords[0], 1E-5);
    assertEquals(2.5, centroid.coords[1], 1E-5);
  }
}