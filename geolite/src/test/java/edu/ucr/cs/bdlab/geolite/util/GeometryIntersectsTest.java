package edu.ucr.cs.bdlab.geolite.util;

import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.twod.LineString2D;
import edu.ucr.cs.bdlab.wktparser.ParseException;
import edu.ucr.cs.bdlab.wktparser.WKTParser;
import junit.framework.TestCase;

import java.io.IOException;

public class GeometryIntersectsTest extends TestCase {

  public void testPoints() {
    assertFalse(GeometryIntersects.intersects(new Point(0.0, 1.0), new Point(0.0, 2.0)));
    assertTrue(GeometryIntersects.intersects(new Point(0.0, 1.0), new Point(0.0, 1.0)));
  }

  public void testEnvelope() {
    assertTrue(GeometryIntersects.intersects(new Point(0.0, 1.0), new Envelope(2, 0.0, 0.0, 2.0, 2.0)));
    assertFalse(GeometryIntersects.intersects(new Envelope(2, 0.0, 1.0, 2.0, 3.0), new Point(5.0, 1.0)));
    assertTrue(GeometryIntersects.intersects(new Envelope(2, 0.0, 1.0, 2.0, 3.0), new Envelope(2, 0.0, 0.0, 5.0, 5.0)));
    assertFalse(GeometryIntersects.intersects(new Envelope(2, 0.0, 1.0, 2.0, 3.0), new Envelope(2, 0.0, 5.0, 7.0, 7.0)));
  }

  public void testLineString() throws IOException, ParseException {
    WKTParser wktParser = new WKTParser();
    LineString2D linestring1 = (LineString2D) wktParser.parse("Linestring (0 0, 1 1, 3 1)", null);
    LineString2D linestring2 = (LineString2D) wktParser.parse("Linestring (2 0, 2 2, 0 2)", null);
    assertFalse(GeometryIntersects.intersects(new Point(2.0, 0.0), linestring1));
    assertTrue(GeometryIntersects.intersects(new Point(2.0, 0.0), linestring2));
    assertTrue(GeometryIntersects.intersects(new Point(2.0, 1.0), linestring1));
    assertTrue(GeometryIntersects.intersects(new Point(2.0, 1.0), linestring2));
    assertTrue(GeometryIntersects.intersects(linestring1, new Envelope(2, 0.0, 0.5, 0.75, 1.0)));
    assertFalse(GeometryIntersects.intersects(linestring2, new Envelope(2, 0.0, 0.0, 1.9, 1.0)));
  }
}