/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite;

import edu.ucr.cs.bdlab.geolite.twod.MultiPolygon2D;
import junit.framework.TestCase;

public class MultiPolygonTest extends TestCase {

  public void testArea() {
    MultiPolygon2D mpoly = new MultiPolygon2D();
    mpoly.addPoint(0, 0);
    mpoly.addPoint(1, 0);
    mpoly.addPoint(1, 1);
    mpoly.addPoint(0, 0);
    mpoly.closeLastRing(true);
    assertEquals(1, mpoly.getNumPolygons());
    assertEquals(0.5, mpoly.getVolume());
    mpoly.endCurrentPolygon();
    assertEquals(1, mpoly.getNumPolygons());
    mpoly.addPoint(3, 0);
    mpoly.addPoint(4, 0);
    mpoly.addPoint(4, 1);
    mpoly.addPoint(3, 0);
    mpoly.closeLastRing(true);
    assertEquals(2, mpoly.getNumPolygons());
    assertEquals(1.0, mpoly.getVolume());
  }
}