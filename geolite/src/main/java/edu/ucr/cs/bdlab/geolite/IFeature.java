/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite;

import edu.ucr.cs.bdlab.util.WritableExternalizable;

/**
 * A feature represents a geometry associated with zero or more fields.
 */
public interface IFeature extends WritableExternalizable {

  /**
   * The geometry contained in the feature.
   * @return
   */
  IGeometry getGeometry();

  /**
   * If this feature is mutable, this function sets the geometry in the feature.
   * It is allowed to throw an exception if this feature is immutable (constant).
   * @param geom
   */
  void setGeometry(IGeometry geom);

  /**
   * Returns the value of the attribute at position {@code i} 0-based.
   * @param i
   * @return
   */
  Object getAttributeValue(int i);

  /**
   * If names are associated with attributes, this method returns the attribute value with the given name.0
   * @param name
   * @return
   */
  Object getAttributeValue(String name);

  /**
   * Returns the total number of attributes
   * @return
   */
  int getNumAttributes();

  /**
   * If names are associated with attributes, this function returns the name of the attribute at the given position
   * (0-based).
   * @return
   */
  String getAttributeName(int i);

  /**The estimated total size of the feature in bytes including the geometry and features*/
  int getStorageSize();

  static boolean equals(IFeature f1, IFeature f2) {
    int numAttrs = f1.getNumAttributes();
    if (numAttrs != f2.getNumAttributes())
      return false;
    for (int iAttr = 0; iAttr < numAttrs; iAttr++) {
      String name1 = f1.getAttributeName(iAttr);
      String name2 = f2.getAttributeName(iAttr);
      boolean namesEqual = ((name1 == null || name1.length() == 0) && (name2 == null || name2.length() == 0)) ||
          (name1 != null && name2 != null && name1.equals(name2));
      if (!namesEqual)
        return false;
      if (!f1.getAttributeValue(iAttr).equals(f2.getAttributeValue(iAttr)))
        return false;
    }
    return true;
  }
}
