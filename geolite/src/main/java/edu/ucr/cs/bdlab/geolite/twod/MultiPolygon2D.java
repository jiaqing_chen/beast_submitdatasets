/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite.twod;

import edu.ucr.cs.bdlab.geolite.GeometryException;
import edu.ucr.cs.bdlab.geolite.GeometryType;
import edu.ucr.cs.bdlab.util.DynamicArrays;
import edu.ucr.cs.bdlab.util.MathUtil;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * A geometry that stores a set of polygons.
 */
public class MultiPolygon2D extends Polygon2D {
  /**Number of polygons stored in this multipolygon*/
  public int numPolygons;

  /**The index of the first point in each polygon*/
  public int[] firstPointInPolygon;

  /**A flag that signals that the last polygon has ended and the next call to {@link #addPoint(double, double)}
   * will create a new polygon*/
  public boolean lastPolygonEnded;

  public MultiPolygon2D() {
    // Initially, no polygons are added so it's logically correct to set this flag to true. This ensures creating a new
    // polygon with the first call to addPoint(double, double)
    lastPolygonEnded = true;
  }

  /**
   * Creates a MultiPolygon from a set of simple polygons
   * @param ps a list of polygons
   */
  public MultiPolygon2D(Polygon2D ... ps) {
    int totalNumPoints = 0;
    int totalNumRings = 0;
    int totalNumPolygons = ps.length;
    for (Polygon2D p : ps) {
      totalNumPoints += p.getNumPoints();
      totalNumRings = p.getNumRings();
    }
    ensurePointCapacity(totalNumPoints);
    ensureRingCapacity(totalNumRings + 1);
    ensurePolygonCapacity(totalNumPolygons);
    for (Polygon2D p : ps) {
      addPolygon(p);
    }
    firstPointInRing[numRings] = numPoints;
  }

  @Override
  public void setEmpty() {
    super.setEmpty();
    numPolygons = 0;
    lastPolygonEnded = true;
  }

  /**
   * Ends the current polygon and prepares the multipolygon to create a new polygon with the next call to
   * {@link #addPoint(double, double)}
   */
  public void endCurrentPolygon() {
    lastPolygonEnded = true;
    if (!lastRingClosed)
      throw new GeometryException("The last ring must be closed before ending the polygon", this);
  }

  @Override
  public void addPoint(double x, double y) {
    if (lastPolygonEnded) {
      ensurePolygonCapacity(numPolygons + 1);
      firstPointInPolygon[numPolygons] = numPoints;
      numPolygons++;
      lastPolygonEnded = false;
    }
    super.addPoint(x, y);
  }

  protected void ensurePolygonCapacity(int newSize) {
    firstPointInPolygon = DynamicArrays.expand(firstPointInPolygon, MathUtil.nextPowerOfTwo(newSize));
  }

  @Override
  public void addPointXYM(double x, double y, double m) {
    if (lastPolygonEnded) {
      ensurePolygonCapacity(numPolygons + 1);
      firstPointInPolygon[numPolygons] = numPoints;
      numPolygons++;
      lastPolygonEnded = false;
    }
    super.addPointXYM(x, y, m);
  }

  @Override
  public void addPointXYZM(double x, double y, double z, double m) {
    if (lastPolygonEnded) {
      ensurePolygonCapacity(numPolygons + 1);
      firstPointInPolygon[numPolygons] = numPoints;
      numPolygons++;
      lastPolygonEnded = false;
    }
    super.addPointXYZM(x, y, z, m);
  }

  @Override
  public GeometryType getType() {
    return GeometryType.MULTIPOLYGON;
  }

  /**
   * Returns the number of polygons in this multipolygon
   * @return
   */
  public int getNumPolygons() {
    return numPolygons;
  }

  @Override
  public int getGeometryStorageSize() {
    return super.getGeometryStorageSize() +
        4 + // numPolygons
        4 * numPolygons + // firstPointInPolygon
        1; // lastPolygonClosed
  }

  public void getPolygonN(int n, Polygon2D polygon) {
    int firstPoint = firstPointInPolygon[n];
    int lastPoint = n == numPolygons - 1? numPoints : firstPointInPolygon[n+1];
    int firstRing = 0;
    while (firstPointInRing[firstRing] < firstPoint)
      firstRing++;
    int lastRing = firstRing;
    while (lastRing < numRings && firstPointInRing[lastRing] < lastPoint)
      lastRing++;
    polygon.ensurePointCapacity(lastPoint - firstPoint);
    polygon.ensureRingCapacity(lastRing - firstRing);
    System.arraycopy(this.xs, firstPoint, polygon.xs, 0, lastPoint - firstPoint);
    System.arraycopy(this.ys, firstPoint, polygon.ys, 0, lastPoint - firstPoint);
    if (this.ms != null) {
      polygon.ensurePointCapacityM(lastPoint - firstPoint);
      System.arraycopy(this.ms, firstPoint, polygon.ms, 0, lastPoint - firstPoint);
    }
    if (this.zs != null) {
      polygon.ensurePointCapacityMZ(lastPoint - firstPoint);
      System.arraycopy(this.zs, firstPoint, polygon.zs, 0, lastPoint - firstPoint);
    }
    System.arraycopy(this.firstPointInRing, firstRing, polygon.firstPointInRing, 0, lastRing - firstRing + 1);
    polygon.numPoints = lastPoint - firstPoint;
    polygon.numRings = lastRing - firstRing;
    // Adjust the first point on ring based on the points that were copied
    for (int iRing = polygon.numRings; iRing >= 0; iRing--) {
      polygon.firstPointInRing[iRing] -= polygon.firstPointInRing[0];
    }
  }

  @Override
  public double getVolume() {
    double area = 0.0;
    int iPolygon = 0;
    int iRing = 0;

    while (iRing < numRings) {
      double ringArea = getRingArea(iRing);
      // The first ring in each polygon indicates a positive area while the remaining rings in each polygon are holes.
      if (firstPointInPolygon[iPolygon] == firstPointInRing[iRing])
        area += ringArea;
      else
        area -= ringArea;
      iRing++;
      // Check if the new ring is in the next polygon
      if (iRing < numRings && iPolygon < numPolygons-1 && firstPointInRing[iRing] >= firstPointInPolygon[iPolygon+1])
        iPolygon++;
    }
    return area;
  }

  @Override
  public void write(DataOutput out) throws IOException {
    super.write(out);
    out.writeInt(numPolygons);
    for (int iPoly = 0; iPoly < numPolygons; iPoly++)
      out.writeInt(firstPointInPolygon[iPoly]);
    out.writeBoolean(lastPolygonEnded);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    super.readFields(in);
    numPolygons = in.readInt();
    ensurePolygonCapacity(numPolygons);
    for (int iPoly = 0; iPoly < numPolygons; iPoly++)
      firstPointInPolygon[iPoly] = in.readInt();
    lastPolygonEnded = in.readBoolean();
  }

  @Override
  public void toWKT(StringBuilder out) {
    out.append("MULTIPOLYGON(");
    int iPoly = -1;
    for (int iRing = 0; iRing < numRings; iRing++) {
      if (iPoly < numPolygons-1 && firstPointInPolygon[iPoly+1] == firstPointInRing[iRing]) {
        // First ring in polygon
        iPoly++;
        if (iPoly != 0) {
          // Close the previous polygon
          out.append(')');
          out.append(',');
        }
        // Open a new polygon
        out.append('(');
      }
      if (iRing > 0 && firstPointInPolygon[iPoly] != firstPointInRing[iRing])
        out.append(',');
      out.append('(');
      int i1Point = firstPointInRing[iRing];
      int i2Point = iRing == numRings - 1 ? numPoints : firstPointInRing[iRing + 1];
      for (int iPoint = i1Point; iPoint < i2Point; iPoint++) {
        out.append(xs[iPoint]); out.append(' '); out.append(ys[iPoint]); out.append(',');
      }
      // Close the loop by writing the first point again
      out.append(xs[i1Point]); out.append(' '); out.append(ys[i1Point]);
      out.append(')'); // Close the ring
    }
    out.append(')'); // Close last polygon
    out.append(')'); // Close the multipolygon
  }

  @Override
  public ByteBuffer toWKB(ByteBuffer out) {
    byte byteOrder = (byte)(out.order() == ByteOrder.LITTLE_ENDIAN ? 1 : 0);
    out.put(byteOrder);
    out.putInt(6);
    out.putInt(numPolygons);
    int iPoly = -1;
    for (int iRing = 0; iRing < numRings; iRing++) {
      if (iPoly < numPolygons-1 && firstPointInPolygon[iPoly+1] == firstPointInRing[iRing]) {
        // First ring in polygon
        iPoly++;
        out.put(byteOrder);
        out.putInt(3);
        // Number of rings in the polygon
        if (iPoly == numPolygons - 1)
          out.putInt(numRings - iRing);
        else {
          int numRing;
          for(numRing = iRing; firstPointInRing[numRing] < firstPointInPolygon[iPoly+1]; numRing++);
          out.putInt(numRing);
        }
      }
      int i1Point = firstPointInRing[iRing];
      int i2Point = iRing == numRings - 1 ? numPoints : firstPointInRing[iRing + 1];
      out.putInt(i2Point - i1Point);
      for (int iPoint = i1Point; iPoint < i2Point; iPoint++) {
        out.putDouble(xs[iPoint]);
        out.putDouble(ys[iPoint]);
      }
    }
    return out;
  }

  /**
   * Adds an existing polygon to this multipolygon
   * @param poly
   */
  public void addPolygon(Polygon2D poly) {
    if (numPolygons > 0 && (!isClosed() || !lastPolygonEnded))
      throw new GeometryException("The polygon must be closed before adding a polygon to it");
    // Append a new polygon
    ensurePolygonCapacity(numPolygons + 1);
    firstPointInPolygon[numPolygons++] = numPoints;
    // Append rings
    ensureRingCapacity(this.numRings + poly.numRings);
    System.arraycopy(poly.firstPointInRing, 0, this.firstPointInRing, numRings, poly.numRings);
    for (int iRing = this.numRings; iRing < this.numRings + poly.numRings; iRing++)
      this.firstPointInRing[iRing] += this.numPoints;
    this.numRings += poly.numRings;
    // Append point coordinates
    ensurePointCapacity(numPoints + poly.numPoints);
    System.arraycopy(poly.xs, 0, xs, numPoints, poly.numPoints);
    System.arraycopy(poly.ys, 0, ys, numPoints, poly.numPoints);
    this.numPoints += poly.numPoints;
    endCurrentPolygon();
  }
}
