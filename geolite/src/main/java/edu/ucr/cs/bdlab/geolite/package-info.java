/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This package contains a light-weight computational geometry library to be used with Spark RDD. This library is designed
 * with the distributed processing of Spark in mind. It has the following main features:
 * <ul>
 *   <li>The data types are designed to be used as mutable objects.</li>
 *   <li>The data types have a minimal memory footprint</li>
 *   <li>The data types can be serialized/deserialized efficiently</li>
 *   <li>There is a minimum sharing between classes allowing for efficient use among threads and machines.</li>
 * </ul>
 * This library follows the OGC Simple Feature standard which makes it compatible with popular libraries and systems
 * such as:
 *   <ul>
 *     <li>Java Topology Suite (JTS)</li>
 *     <li>PostGIS</li>
 *     <li>Oracle Spatial</li>
 *     <li>Esri Geometry API</li>
 *   </ul>
 * However, unlike the above systems and libraries, this package is designed for RDD processing with minimimal memory
 * footprint and minimal object creation.
 */
package edu.ucr.cs.bdlab.geolite;
