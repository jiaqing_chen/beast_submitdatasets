/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite;

import com.esri.core.geometry.Geometry;
import edu.ucr.cs.bdlab.geolite.util.GeometryContains;
import edu.ucr.cs.bdlab.geolite.util.GeometryIntersection;
import edu.ucr.cs.bdlab.geolite.util.GeometryIntersects;
import edu.ucr.cs.bdlab.util.WritableExternalizable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.nio.ByteBuffer;

/**
 * An abstract Geometry class that can be implemented for different objects. This interface follows the
 * OGC Simple Feature standard which makes it compatible with popular libraries and systems such as:
 * <ul>
 *   <li>Java Topology Suite (JTS)</li>
 *   <li>PostGIS</li>
 *   <li>Oracle Spatial</li>
 *   <li>Esri Geometry API</li>
 * </ul>
 */
public interface IGeometry extends WritableExternalizable {
  /**A logger for all geometry classes*/
  Log LOG = LogFactory.getLog(IGeometry.class);

  /**
   * Letters to use for dimensions in case arbitrarily high number of dimensions is used. If we need more than this,
   * we can start concatenating two letters, e.g., xx, xy, and xz, for dimensions 27, 28, and 29, respectively.
   */
  char[] DimensionNames = {'x', 'y', 'z', 'w', 'v', 'u', 't', 's', 'r', 'q', 'p', 'o', 'n', 'm', 'l', 'k', 'j',
      'i', 'h', 'g', 'f', 'e', 'd', 'c', 'b', 'a'};

  /**
   * Computes the envelope of this geometry and returns it in the given envelope. The same envelope is returned.
   * This function does not create a new object to allow for efficient object reuse.
   */
  Envelope envelope(Envelope envelope);

  /**
   * Tests if the geometry intersects (not disjoint) with another geometry.
   * @param geometry
   * @return
   */
  default boolean intersects(IGeometry geometry) {
    return GeometryIntersects.intersects(this, geometry);
  }

  /**
   * Returns true if this geometry contains the given geometry.
   * @param geometry
   * @return
   */
  default boolean contains(IGeometry geometry) {
    return GeometryContains.contains(this, geometry);
  }

  /**
   * Computes the area of the geometry.
   * @return
   */
  double getVolume();

  /**
   * Returns the type of the geometry.
   * @return
   */
  GeometryType getType();

  /**
   * Computes an estimate size for the geometry which is based on the memory size of the data stored in it.
   * Assuming tha the polygon is stored efficiently on disk, this size will be very similar to the disk size as well.
   * @return
   */
  int getGeometryStorageSize();

  /**
   * Returns {@code true} if this geometric object is the empty Geometry. If true, then this
   * geometric object represents the empty point set &empty; for the coordinate space.
   * @return
   */
  boolean isEmpty();

  /**
   * Makes this geometry empty.
   */
  void setEmpty();

  /**
   * Serializes the geometry in WKT format and appends it to the given StringBuilder
   * @param out
   */
  void toWKT(StringBuilder out);

  default String asText() {
    StringBuilder wkt = new StringBuilder();
    toWKT(wkt);
    return wkt.toString();
  }

  /**
   * Serializes the geometry in WKB format
   * @param out
   */
  ByteBuffer toWKB(ByteBuffer out);

  /**
   * Computes the centroid of the object and returns it in the given point.
   * @param centroid
   */
  Point centroid(Point centroid);

  /**
   * Computes the intersection of this geometry with the geometry {@code another} and stores the result in the
   * geometry {@code result} if possible. If not possible, a new geometry is created and returned.
   * @param another
   * @param result
   * @return the result of the geometry
   */
  default IGeometry intersection(IGeometry another, IGeometry result) {
    return GeometryIntersection.intersection(this, another, result);
  }

  @Override
  default void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
    readFields(in);
  }

  @Override
  default void writeExternal(ObjectOutput out) throws IOException {
    write(out);
  }

  /**
   * The number of measurements or axes needed to describe a position in this geometry.
   * For this standard, the coordinate dimension can be 2 (for x and y), 3
   * (with z or m added), or 4 (with both z and m added). The ordinates x, y and z are spatial, and the ordinate
   * m is a measure.
   * @return
   */
  int getCoordinateDimension();

  /**
   * The total number of points in this geometry. Empty geometries contribute zero points. A point contributes a value
   * of 1. Multipoints, linestrings and polygons can contribute more than one point.
   * @return
   */
  long getNumPoints();

}
