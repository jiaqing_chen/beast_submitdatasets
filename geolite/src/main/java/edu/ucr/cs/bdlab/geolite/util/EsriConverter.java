/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite.util;

import edu.ucr.cs.bdlab.geolite.GeometryException;
import edu.ucr.cs.bdlab.wktparser.ParseException;
import edu.ucr.cs.bdlab.wktparser.WKTParser;

import java.io.IOException;

/**
 * A utility class that converts geolite geometries to Esri API geometries.
 */
public class EsriConverter {
  public static com.esri.core.geometry.Geometry createEsriGeometry(edu.ucr.cs.bdlab.geolite.IGeometry geom) {
    switch (geom.getType()) {
      case POINT:
        edu.ucr.cs.bdlab.geolite.Point point = (edu.ucr.cs.bdlab.geolite.Point) geom;
        if (point.getCoordinateDimension() == 2)
          return new com.esri.core.geometry.Point(point.coords[0], point.coords[1]);
        if (point.getCoordinateDimension() == 3)
          return new com.esri.core.geometry.Point(point.coords[0], point.coords[1], point.coords[2]);
        else
          throw new GeometryException("Esri API does not support higher than 3D points");
      case ENVELOPE:
        edu.ucr.cs.bdlab.geolite.Envelope envelope = (edu.ucr.cs.bdlab.geolite.Envelope) geom;
        if (envelope.getCoordinateDimension() == 2)
          return new com.esri.core.geometry.Envelope(envelope.minCoord[0], envelope.minCoord[1],
              envelope.maxCoord[0], envelope.maxCoord[1]);
        else
          throw new GeometryException("Esri API does not support higher than 2D Envelopes");
      case LINESTRING:
        edu.ucr.cs.bdlab.geolite.twod.LineString2D linestring = (edu.ucr.cs.bdlab.geolite.twod.LineString2D) geom;
        com.esri.core.geometry.Polyline polyline = new com.esri.core.geometry.Polyline();
        for (int $i = 1; $i < linestring.getNumPoints(); $i++) {
          com.esri.core.geometry.Line segment = new com.esri.core.geometry.Line(
              linestring.xs[$i-1], linestring.ys[$i-1], linestring.xs[$i], linestring.ys[$i]);
          polyline.addSegment(segment, false);
        }
        return polyline;
      case POLYGON:
        edu.ucr.cs.bdlab.geolite.twod.Polygon2D litePolygon = (edu.ucr.cs.bdlab.geolite.twod.Polygon2D) geom;
        com.esri.core.geometry.Polygon esriPolygon = new com.esri.core.geometry.Polygon();
        int iRing = 0;
        int ringStart = 0;
        int ringEnd = iRing == litePolygon.numRings - 1? litePolygon.numPoints : litePolygon.firstPointInRing[iRing+1];
        for (int $i = 0; $i < litePolygon.getNumPoints(); $i++) {
          if ($i == ringEnd - 1) {
            // Esri API automatically closes the polygon. No need to explicitly add the last segment
            // Move to next ring
            iRing++;
            ringStart = ringEnd;
            ringEnd = iRing >= litePolygon.numRings - 1? litePolygon.numPoints : litePolygon.firstPointInRing[iRing+1];
          } else {
            com.esri.core.geometry.Line segment = new com.esri.core.geometry.Line(
                litePolygon.xs[$i], litePolygon.ys[$i], litePolygon.xs[$i+1], litePolygon.ys[$i+1]);
            esriPolygon.addSegment(segment, $i == ringStart);
          }
        }
        return esriPolygon;
      case MULTILINESTRING:
        edu.ucr.cs.bdlab.geolite.twod.MultiLineString2D multiLineString = (edu.ucr.cs.bdlab.geolite.twod.MultiLineString2D) geom;
        polyline = new com.esri.core.geometry.Polyline();
        int iLine = 0;
        int lineStart = 0;
        int lineEnd = iLine == multiLineString.numLineStrings - 1?
            multiLineString.numPoints : multiLineString.firstPointInLineString[iLine+1];
        for (int $i = 0; $i < multiLineString.getNumPoints(); $i++) {
          if ($i == lineEnd - 1) {
            // Move to next linestring
            iLine++;
            lineStart = lineEnd;
            lineEnd = iLine >= multiLineString.numLineStrings - 1?
                multiLineString.numPoints : multiLineString.firstPointInLineString[iLine+1];
          } else {
            com.esri.core.geometry.Line segment = new com.esri.core.geometry.Line(
                multiLineString.xs[$i], multiLineString.ys[$i], multiLineString.xs[$i+1], multiLineString.ys[$i+1]);
            polyline.addSegment(segment, $i == lineStart);
          }
        }
        return polyline;
      case MULTIPOLYGON:
        edu.ucr.cs.bdlab.geolite.twod.MultiPolygon2D multiPolygon = (edu.ucr.cs.bdlab.geolite.twod.MultiPolygon2D) geom;
        esriPolygon = new com.esri.core.geometry.Polygon();
        iRing = 0;
        ringStart = 0;
        ringEnd = iRing == multiPolygon.numRings - 1? multiPolygon.numPoints : multiPolygon.firstPointInRing[iRing+1];
        int iPolygon = 0;
        int polygonStart = 0;
        int polygonEnd = iPolygon == multiPolygon.numPolygons - 1? multiPolygon.numPoints : multiPolygon.firstPointInPolygon[iRing+1];
        for (int $i = 0; $i < multiPolygon.getNumPoints(); $i++) {
          if ($i == ringEnd - 1) {
            // Esri API automatically closes the polygon. No need to explicitly add the last segment
            // Move to next ring
            iRing++;
            ringStart = ringEnd;
            ringEnd = iRing >= multiPolygon.numRings - 1? multiPolygon.numPoints : multiPolygon.firstPointInRing[iRing+1];
          } else {
            com.esri.core.geometry.Line segment = new com.esri.core.geometry.Line(
                multiPolygon.xs[$i], multiPolygon.ys[$i], multiPolygon.xs[$i+1], multiPolygon.ys[$i+1]);
            esriPolygon.addSegment(segment, $i == ringStart);
          }
        }
        return esriPolygon;
      case GEOMETRYCOLLECTION:
        // Esri Geometry does not support GeometryCollection type. Only OGCGeometry does

      default:
        return null;
    }
  }

  public static edu.ucr.cs.bdlab.geolite.IGeometry fromEsriGeometry(com.esri.core.geometry.Geometry esriGeom,
                                                                    edu.ucr.cs.bdlab.geolite.IGeometry result) {
    if (esriGeom instanceof com.esri.core.geometry.Point) {
      com.esri.core.geometry.Point esriPoint = (com.esri.core.geometry.Point) esriGeom;
      edu.ucr.cs.bdlab.geolite.Point litePoint;
      int numDimensions = 2;
      if (esriPoint.hasZ())
        numDimensions = 3;
      if (esriPoint.hasM())
        numDimensions = 4;
      if (result == null || !(result instanceof edu.ucr.cs.bdlab.geolite.Point)) {
        result = litePoint = new edu.ucr.cs.bdlab.geolite.Point(numDimensions);
      } else {
        litePoint = (edu.ucr.cs.bdlab.geolite.Point) result;
        litePoint.setCoordinateDimension(numDimensions);
      }
      litePoint.coords[0] = esriPoint.getX();
      litePoint.coords[1] = esriPoint.getY();
      if (esriPoint.hasZ())
        litePoint.coords[2] = esriPoint.getZ();
      if (esriPoint.hasM())
        litePoint.coords[3] = esriPoint.getM();
    } else {
      // TODO make it more efficient by avoided generating and parsing WKT
      String wkt = com.esri.core.geometry.ogc.OGCGeometry.createFromEsriGeometry(esriGeom,
          com.esri.core.geometry.SpatialReference.create(4326)).asText();
      try {
        return new WKTParser().parse(wkt, result);
      } catch (ParseException e) {
        e.printStackTrace();
        return null;
      } catch (IOException e) {
        e.printStackTrace();
        return null;
      }
    }
    return result;
  }
}
