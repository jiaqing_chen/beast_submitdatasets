/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite.util;

import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.SpatialReference;
import edu.ucr.cs.bdlab.geolite.Envelope;
import edu.ucr.cs.bdlab.geolite.IGeometry;
import edu.ucr.cs.bdlab.geolite.Point;
import edu.ucr.cs.bdlab.geolite.twod.LineString2D;

/**
 * A utility class for testing the overlap between two geometries
 */
public class GeometryContains {

  public static boolean contains(IGeometry geom1, IGeometry geom2) {
    if (geom1.getCoordinateDimension() != geom2.getCoordinateDimension())
      return false;
    // Exclude the case of an empty geometry
    if (geom1.isEmpty() || geom2.isEmpty())
      return false;
    // All unsupported geometry tests are passed to Esri library
    return GeometryEngine.contains(EsriConverter.createEsriGeometry(geom1),
        EsriConverter.createEsriGeometry(geom2), SpatialReference.create(4326));
  }

  public static boolean pointInEnvelope(Point p, Envelope e) {
    for (int $d = 0; $d < p.getCoordinateDimension(); $d++) {
      if (p.coords[$d] < e.minCoord[$d] || p.coords[$d] >= e.maxCoord[$d])
        return false;
    }
    return true;
  }
}
