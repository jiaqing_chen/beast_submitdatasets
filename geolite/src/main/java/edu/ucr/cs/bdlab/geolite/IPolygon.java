/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite;

/**
 * Interface for a polygon with holes
 */
public interface IPolygon extends ILineString {

  /**
   * Total number of rings in the polygon including the exterior ring as one of them
   * @return
   */
  int getNumRings();

  /**
   * Number of interior rings.
   * @return
   */
  default int getNumInteriorRings() {
    return getNumRings() - 1;
  }

  /**
   * Returns the exterior ring. The returned line string has its first and last points equal to each other
   * @return
   */
  default ILineString getExteriorRing() {
    return getRingN(0);
  }

  /**
   * Get the interior ring #n 0-based.
   * @param n
   * @return
   */
  default ILineString getInteriorRingN(int n) {
    return getRingN(n - 1);
  }

  /**
   * Returns the ring #n where ring #0 is the exterior ring and the rest are the interior rings
   * @param n
   * @return
   */
  ILineString getRingN(int n);

  @Override
  default GeometryType getType() {
    return GeometryType.POLYGON;
  }

  @Override
  default int getGeometryStorageSize() {
    return (int) (getNumPoints() * 8 * getCoordinateDimension() + getNumRings() * 4);
  }

  @Override
  default boolean isEmpty() {
    return getNumPoints() == 0;
  }
}
