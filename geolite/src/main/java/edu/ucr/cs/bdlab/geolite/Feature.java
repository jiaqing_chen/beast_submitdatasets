/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite;

import edu.ucr.cs.bdlab.util.DynamicArrays;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

/**
 * A standard feature implementation that balances the parsing time and access time.
 */
public class Feature implements IFeature {
  enum FieldType {
    /**Marker for string type (character array).*/
    StringType((short) -1),
    /**Marker for big-endian 32-bit integer type*/
    IntegerType((short) 4),
    /**Marker for big-endian 64-bit long type*/
    LongType((short) 8),
    /**Marker for big-endian 64-bit double-precision floating point type*/
    DoubleType((short) 8),
    /**A timestamp represents a date/time in milliseconds*/
    TimestampType((short) 8);

    /**Size of this field in bytes or -1 if it is variable size*/
    short size;
    FieldType(short s) {
      this.size = s;
    }
  }

  /**The types of attributes. This helps in parsing them from the ByteBuffer*/
  protected FieldType[] attributeTypes;

  /**All attribute values*/
  protected ByteBuffer attributeValues;

  /**The offset of each attribute in the */
  transient protected short[] attributeOffsets;

  /**An optional list of attribute names*/
  protected String[] attributeNames;

  /**The geometry associated with this feature*/
  protected IGeometry geometry;

  /**Maps each attribute name to its index for fast lookup*/
  private Map<Object, Object> attributeNameToIndex;

  public Feature() {}

  public Feature(IGeometry geometry) {
    this.geometry = geometry;
  }

  public Feature(IFeature f) {
    this.copyAttributeMetadata(f);
    this.copyAttributeValues(f);
    this.setGeometry(f.getGeometry());
  }

  @Override
  public IGeometry getGeometry() {
    return this.geometry;
  }

  @Override
  public void setGeometry(IGeometry geometry) {
    this.geometry = geometry;
  }

  /**
   * Populate the metadata (attribute types and names) from another IFeature
   * @param feature
   */
  public void copyAttributeMetadata(IFeature feature) {
    int numAttributes = feature.getNumAttributes();
    if (numAttributes == 0) {
      attributeTypes = null;
      attributeValues = null;
      attributeNames = null;
      return;
    }
    if (feature instanceof Feature) {
      // An efficient method to copy attribute metadata from another feature
      Feature that = (Feature) feature;
      this.attributeTypes = Arrays.copyOf(that.attributeTypes, numAttributes);
      this.attributeNames = Arrays.copyOf(that.attributeNames, numAttributes);
      if (that.attributeNameToIndex != null)
        this.attributeNameToIndex = new HashMap<>(that.attributeNameToIndex);
    } else {
      // A generic method to copy metadata from another feature
      setNumAttribute(numAttributes);
      for (int iAttr = 0; iAttr < numAttributes; iAttr++) {
        this.attributeNames[iAttr] = feature.getAttributeName(iAttr);
        Object attr = feature.getAttributeValue(iAttr);
        if (attr == null)
          this.attributeTypes[iAttr] = FieldType.StringType;
        else if (attr instanceof String)
          this.attributeTypes[iAttr] = FieldType.StringType;
        else if (attr instanceof Integer)
          this.attributeTypes[iAttr] = FieldType.IntegerType;
        else if (attr instanceof Long)
          this.attributeTypes[iAttr] = FieldType.LongType;
        else if (attr instanceof Double)
          this.attributeTypes[iAttr] = FieldType.DoubleType;
        else if (attr instanceof GregorianCalendar)
          this.attributeTypes[iAttr] = FieldType.TimestampType;
        else
          throw new RuntimeException("Unsupported attribute type " + attr.getClass());
      }
    }
  }

  /**
   * Populate the data of this feature (geometry and attribute values) from another feature.
   * @param feature
   */
  public void copyAttributeValues(IFeature feature) {
    assert feature.getNumAttributes() == this.getNumAttributes();
    // Set geometry
    this.geometry = feature.getGeometry();
    // Set attribute values
    // Reset the value buffer if it is already in use
    if (this.attributeValues != null)
      this.attributeValues.position(0);
    for (int iAttr = 0; iAttr < this.getNumAttributes(); iAttr++) {
      Object attrValue = feature.getAttributeValue(iAttr);
      if (attrValue == null) {
        // Treat it as an empty string
        expandAttributeValues(2);
        attributeValues.putShort((short)0);
      } else if (attrValue instanceof Double) {
        expandAttributeValues(8);
        attributeValues.putDouble((Double) attrValue);
      } else if (attrValue instanceof Integer) {
        expandAttributeValues(4);
        attributeValues.putInt((Integer) attrValue);
      } else if (attrValue instanceof Long) {
        expandAttributeValues(8);
        attributeValues.putLong((Long) attrValue);
      } else if (attrValue instanceof String) {
        byte[] strBytes = ((String)attrValue).getBytes();
        expandAttributeValues(2 + strBytes.length);
        attributeValues.putShort((short) strBytes.length);
        attributeValues.put(strBytes);
      } else if (attrValue instanceof GregorianCalendar) {
        expandAttributeValues(8);
        attributeValues.putLong(((GregorianCalendar)attrValue).getTimeInMillis());
      } else {
        throw new RuntimeException("Unsupported type "+attrValue.getClass());
      }
    }
  }

  /**
   * Expand the byte buffer of attribute values with the given number of bytes
   * @param numBytesToExpand
   */
  private void expandAttributeValues(int numBytesToExpand) {
    if (attributeValues == null) {
      attributeValues = ByteBuffer.allocate(numBytesToExpand * 2);
      attributeValues.limit(numBytesToExpand);
      return;
    }
    int currentPosition = attributeValues.position();
    int newSize = currentPosition + numBytesToExpand;
    if (newSize > attributeValues.capacity()) {
      byte[] newArray = DynamicArrays.expand(attributeValues.array(), newSize * 2);
      attributeValues = ByteBuffer.wrap(newArray, 0, currentPosition);
      attributeValues.position(currentPosition);
    }
    // Expand the limit
    attributeValues.limit(newSize);
  }

  private void setNumAttribute(int numAttributes) {
    if (numAttributes != getNumAttributes()) {
      attributeTypes = new FieldType[numAttributes];
      attributeNames = new String[numAttributes];
      attributeOffsets = null; // Invalidate offsets
      attributeNameToIndex = null; // Invalidate the cache
    }
  }

  /**
   * Returns the offset of an attribute in the byte buffer array
   * @param iAttr
   * @return
   */
  protected short getAttributeOffset(int iAttr) {
    computeAttributeOffsets();
    if (attributeOffsets[iAttr] != -1)
      return attributeOffsets[iAttr];
    int lastKnownAttribute = iAttr;
    while (lastKnownAttribute > 0 && attributeOffsets[lastKnownAttribute] == -1)
      lastKnownAttribute--;
    short offset = attributeOffsets[lastKnownAttribute];
    while (lastKnownAttribute < iAttr) {
      int attributeLength = attributeTypes[lastKnownAttribute].size;
      if (attributeLength == -1)
        attributeLength = attributeValues.getShort(offset) + 2; // 2 is for the length itself
      offset += attributeLength;
      lastKnownAttribute++;
    }
    return offset;
  }

  private void computeAttributeOffsets() {
    if (attributeOffsets == null) {
      // Fill in the attributeOffsets array
      attributeOffsets = new short[getNumAttributes()];
      short attributeOffset = 0;
      for (int i = 0; i < getNumAttributes(); i++) {
        attributeOffsets[i] = attributeOffset;
        short attrLength = attributeTypes[i].size;
        attributeOffset = (short) (attrLength == -1 || attributeOffset == -1 ? -1 : (attributeOffset + attrLength));
      }
    }
  }

  @Override
  public Object getAttributeValue(int iAttr) {
    if (iAttr >= getNumAttributes())
      return null;
    int offset = getAttributeOffset(iAttr);
    switch (attributeTypes[iAttr]) {
      case StringType:
        short stringLength = attributeValues.getShort(offset);
        return new String(attributeValues.array(), offset + 2, stringLength);
      case IntegerType:
        return attributeValues.getInt(offset);
      case LongType:
        return attributeValues.getLong(offset);
      case DoubleType:
        return attributeValues.getDouble(offset);
      case TimestampType:
        GregorianCalendar c = new GregorianCalendar();
        c.setTimeInMillis(attributeValues.getLong(offset));
        return c;
      default:
        throw new RuntimeException("Unsupported type " + attributeTypes[iAttr]);
    }
  }

  @Override
  public Object getAttributeValue(String name) {
    if (getNumAttributes() == 0)
      return null;
    if (attributeNameToIndex == null) {
      // Lazily initialize the attribute name to index map
      this.attributeNameToIndex = new HashMap<>();
      for (int iAttr = 0; iAttr < attributeNames.length; iAttr++)
        attributeNameToIndex.put(attributeNames[iAttr], iAttr);
    }
    return attributeNameToIndex.containsKey(name) ? getAttributeValue((int)attributeNameToIndex.get(name)) : null;
  }

  @Override
  public int getNumAttributes() {
    return attributeTypes == null ? 0 : attributeTypes.length;
  }

  @Override
  public String getAttributeName(int iAttr) {
    return attributeNames[iAttr];
  }

  @Override
  public int getStorageSize() {
    computeAttributeOffsets();
    int storageSize = 0;
    storageSize += getGeometry().getGeometryStorageSize();
    if (getNumAttributes() > 0) {
      storageSize += attributeValues.limit() + 4;
      storageSize += attributeTypes.length;
      storageSize += attributeOffsets.length * 2;
      for (String name : attributeNames)
        storageSize += name.length() + 1;
    }
    return storageSize;
  }

  /**
   * Write only the header (field types, lengths, and names) with no values or geometries. This is useful when storing
   * a large set of features in a file where we can write the header once and use the method
   * {@link #writeValue(DataOutput)} to write only the values for each record
   * @param out
   * @throws IOException
   */
  public void writeHeader(DataOutput out) throws IOException {
    // Write number of attributes (maximum 127 attributes in a byte)
    out.writeByte(getNumAttributes());
    if (getNumAttributes() > 0) {
      // Write attribute types
      for (FieldType f : attributeTypes)
        out.writeByte(f.ordinal());
      // Write attribute names
      for (String name : attributeNames)
        out.writeUTF(name == null? "" : name);
    }
  }

  /**
   * Reader the header back from the given input.
   * @param in
   * @throws IOException
   */
  public void readHeader(DataInput in) throws IOException {
    int numAttributes = in.readUnsignedByte();
    setNumAttribute(numAttributes);
    if (numAttributes > 0) {
      for (int $i = 0; $i < getNumAttributes(); $i++)
        attributeTypes[$i] = FieldType.values()[in.readByte()];

      // Read attribute names
      for (int iAttr = 0; iAttr < attributeNames.length; iAttr++) {
        String name = in.readUTF();
        if (attributeNameToIndex != null && !name.equals(attributeNames[iAttr]))
          attributeNameToIndex = null; // Invalidate the cache
        attributeNames[iAttr] = name;
      }
    }
    // Invalidate the attributeOffsets cache
    attributeOffsets = null;
  }

  /**
   * Write the geometry and field values to the output. It is assumed that the field type is stored once using the
   * {@link #writeHeader(DataOutput)} method.
   * @param out
   * @throws IOException
   */
  public void writeValue(DataOutput out) throws IOException {
    if (getNumAttributes() == 0)
      out.writeInt(0);
    else {
      // Write the value length
      out.writeInt(attributeValues.limit());
      // Write the values
      out.write(attributeValues.array(), 0, attributeValues.limit());
    }
    if (geometry == null) {
      out.writeInt(GeometryType.EMPTY.ordinal());
    } else {
      // Write the geometry type
      out.writeInt(geometry.getType().ordinal());
      // Write the geometry value
      geometry.write(out);
    }
  }

  /**
   * Read only the value part of the feature from the given {@link DataInput}.
   * @param in
   * @throws IOException
   */
  public void readValue(DataInput in) throws IOException {
    // Read value length
    int valueSize = in.readInt();
    // Initialize the buffer to hold the values
    if (attributeValues == null || valueSize > attributeValues.capacity()) {
      attributeValues = ByteBuffer.allocate(valueSize);
    } else {
      attributeValues.limit(valueSize);
    }

    // Read the values as a byte array
    in.readFully(attributeValues.array(), 0, attributeValues.limit());

    // Read the geometry type
    GeometryType type = GeometryType.values()[in.readInt()];
    // Prepare the geometry object
    if (geometry == null || geometry.getType() != type)
      geometry = type.createInstance();
    // Read the geometry value
    geometry.readFields(in);
  }

  /**
   * Write both the header (field types, names, and lengths) and value (geometry and attribute values) to the given
   * output. This is helpful when writing a single feature or when the feature is passed between machines in Spark RDD
   * or Hadoop MapReduce.
   * @param out
   * @throws IOException
   */
  @Override
  public void write(DataOutput out) throws IOException {
    writeHeader(out);
    writeValue(out);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    readHeader(in);
    readValue(in);
  }

  /**
   * Add an attribute to the list of attributes
   * @param name
   * @param value
   */
  public void appendAttribute(String name, Object value) {
    int newAttributeIndex = getNumAttributes();
    int newNumAttributes = newAttributeIndex + 1;
    // Add attribute name
    attributeNames = DynamicArrays.expand(attributeNames, newNumAttributes);
    attributeNames[newNumAttributes - 1] = name;
    if (attributeNameToIndex != null)
      attributeNameToIndex.put(name, newAttributeIndex);

    // Set attribute type and value
    if (attributeTypes == null)
      attributeTypes = new FieldType[newNumAttributes];
    else
      attributeTypes = Arrays.copyOf(attributeTypes, newNumAttributes);

    if (value instanceof String) {
      attributeTypes[newAttributeIndex] = FieldType.StringType;
      byte[] strBytes = ((String)value).getBytes();
      expandAttributeValues(2 + strBytes.length);
      attributeValues.putShort((short) strBytes.length);
      attributeValues.put(strBytes);
    } else if (value instanceof Integer) {
      attributeTypes[newAttributeIndex] = FieldType.IntegerType;
      expandAttributeValues(4);
      attributeValues.putInt((Integer)value);
    } else if (value instanceof Long) {
      attributeTypes[newAttributeIndex] = FieldType.LongType;
      expandAttributeValues(8);
      attributeValues.putLong((Long)value);
    } else if (value instanceof Double) {
      attributeTypes[newAttributeIndex] = FieldType.DoubleType;
      expandAttributeValues(8);
      attributeValues.putDouble((Double)value);
    } else if (value instanceof GregorianCalendar) {
      attributeTypes[newAttributeIndex] = FieldType.TimestampType;
      expandAttributeValues(8);
      attributeValues.putLong(((GregorianCalendar)value).getTimeInMillis());
    } else
      throw new RuntimeException("Unsupported value type " + value.getClass());
  }

  @Override
  public String toString() {
    StringBuilder b = new StringBuilder();
    b.append("Feature(");
    this.getGeometry().toWKT(b);
    for (int $i = 0; $i < getNumAttributes(); $i++) {
      b.append(',');
      b.append(getAttributeValue($i));
    }
    b.append(')');
    return b.toString();
  }


  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof IFeature))
      return false;
    return IFeature.equals(this, (IFeature) obj);
  }

  public void clearAttributes() {
    attributeTypes = null;
    attributeValues = null;
    attributeNames = null;
  }

}
