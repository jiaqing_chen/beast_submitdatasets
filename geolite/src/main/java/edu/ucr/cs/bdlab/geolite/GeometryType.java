/*
 * Copyright 2018 University of California, Riverside
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.ucr.cs.bdlab.geolite;

import edu.ucr.cs.bdlab.geolite.twod.LineString2D;
import edu.ucr.cs.bdlab.geolite.twod.MultiLineString2D;
import edu.ucr.cs.bdlab.geolite.twod.MultiPolygon2D;
import edu.ucr.cs.bdlab.geolite.twod.Polygon2D;

/**
 * An enumeration of the supported geometry types.
 */
public enum GeometryType {
  /**An empty geometry*/
  EMPTY,

  /**A 2D point with two coordinates (x,y)*/
  POINT,

  /**An envelope is an orthogonal rectangle with two corners (x1, y1) and (x2, y2)*/
  ENVELOPE,

  /**A polyline or a linestring is a sequence of line segments connected to each other*/
  LINESTRING,

  /**A polygon consists of an outer shell as a closed linestring and zero or more holes each as a closed linestring*/
  POLYGON,

  /**A set of independent LineStrings*/
  MULTILINESTRING,

  /**A geometry that contains a set of independent polygons*/
  MULTIPOLYGON,

  /**A collection of geometries of any type including other geometry collections.*/
  GEOMETRYCOLLECTION,

  /**A collection of points*/
  MULTIPOINT;

  public IGeometry createInstance() {
    switch (this) {
      case EMPTY: return EmptyGeometry.instance;
      case POINT: return new Point();
      case MULTIPOINT: return new MultiPoint();
      case ENVELOPE: return new Envelope();
      case LINESTRING: return new LineString2D();
      case POLYGON: return new Polygon2D();
      case MULTILINESTRING: return new MultiLineString2D();
      case MULTIPOLYGON: return new MultiPolygon2D();
      case GEOMETRYCOLLECTION: return new GeometryCollection();
      default: return null;
    }
  }

  /**
   * Returns a geometry type that can include this type and the given type
   * @param other
   * @return
   */
  public GeometryType coerce(GeometryType other) {
    if (this == EMPTY)
      return other;
    if (this == other)
      return this;
    if (this == GEOMETRYCOLLECTION || other == GEOMETRYCOLLECTION)
      return GEOMETRYCOLLECTION;
    if (this.ordinal() > other.ordinal())
      return other.coerce(this);
    if (this == POINT && other == MULTIPOINT)
      return MULTIPOINT;
    if (this == ENVELOPE && (other == POLYGON || other == MULTIPOLYGON ))
      return other;
    if (this == POLYGON || other == MULTIPOLYGON)
      return MULTIPOLYGON;
    if (this == LINESTRING || other == MULTILINESTRING)
      return MULTILINESTRING;
    return GEOMETRYCOLLECTION;
  }

}
